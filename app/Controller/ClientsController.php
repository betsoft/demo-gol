<?php
App::uses('AppController', 'Controller');

class ClientsController extends AppController {

	public function index($filterableFields = [])
	{
		$this->loadModel('Payments');
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Client','Csv']);

		if(MODULO_CANTIERI)
        {
            $conditionsArray = ['Client.company_id' => MYCOMPANY, 'Client.state >' => 0];
        }
		else {
            $conditionsArray = ['Client.company_id' => MYCOMPANY, 'Client.state' => ATTIVO];
        }

        $filterableFields = ['code','ragionesociale','indirizzo','cap','citta','provincia','telefono','mail','piva','cf',null];
        $filterableFields_fix = ['code','ragionesociale','indirizzo','cap','citta','provincia','telefono','mail','piva','cf',null];
        $sortableFields = [['code','codice'],['ragionesociale', 'Ragione sociale'],['indirizzo', 'Indirizzo'],['cap','Cap'],['citta','Città'],['provincia','Prov.'],['telefono','Telefono'],['mail','Email'],['piva','P.iva'],['cf','Codice fiscale'],['#actions']];
        $xlsTitle = 'Codice;Ragione sociale;Indirizzo;Cap;Città;Provincia;Telefono;Email;P.iva;Codice fiscale;Codice Univoco;Banca;Metodo di Pagamento;'."\r\n";

		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields_fix, $this->request->data['filters']);

			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
		}

	    // Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Client->find('all',['contain' => ['Bank', 'Nation'], 'conditions'=>$conditionsArray,'order' => ['Client.ragionesociale' => 'asc']]);
			echo $xlsTitle;
			foreach ($dataForXls as $xlsRow)
			{
                $xlsRow['Bank']['description'] != null ? $bank = $xlsRow['Bank']['description'].' '.$xlsRow['Bank']['abi'].' '.$xlsRow['Bank']['cab'] : $bank = '';

                $paymentMethod = $this->Payments->findById($xlsRow['Client']['payment_id']);
                $paymentMethod != null ? $payment = $paymentMethod['Payments']['metodo'] : $payment = '';

                echo $xlsRow['Client']['code']. ';'.$xlsRow['Client']['ragionesociale']. ';' .$xlsRow['Client']['indirizzo']. ';'.$xlsRow['Client']['cap']. ';'.$xlsRow['Client']['citta']. ';'.$xlsRow['Client']['provincia'].';'.$xlsRow['Client']['telefono'].';'.$xlsRow['Client']['mail'].';'.$xlsRow['Client']['piva'].';'.$xlsRow['Client']['cf'].';'.$xlsRow['Nation']['name'].';'.$xlsRow['Client']['codiceDestinatario'].';'.$bank.';'.$payment.';'."\r\n";
			}
		}
		else
		{
			$this->paginate = ['conditions' => 	$conditionsArray ,'order' => ['Client.ragionesociale' => 'asc'], 'limit' => 100 ];
			$this->Client->recursive = 0;
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
			$this->set('clients', $this->paginate());
		}
	}

	public function indexResellerClient($resellerId)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Clientreseller']);
		$currentClient = $this->Client->find('first',['conditions'=>['Client.id'=>$resellerId,'Client.company_id'=>MYCOMPANY]]);
		$resellerName = $currentClient['Client']['ragionesociale'];
		$clientReseller = $this->Clientreseller->find('all',['contain'=>['Client'=>['Nation']], 'conditions'=> ['Clientreseller.reseller_id' => $resellerId, 'Clientreseller.company_id' => MYCOMPANY,'Clientreseller.state'=>ATTIVO]]);

		$this->set('resellerId',$resellerId);
		$this->set('resellerName',$resellerName);
		$this->set('clients', $clientReseller);
	}

	public function addNewResellerClient($resellerId)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Clientreseller','Client']);
		$currentClient = $this->Client->find('first',['conditions'=>['Client.id'=>$resellerId,'Client.company_id'=>MYCOMPANY]]);
		$resellerName = $currentClient['Client']['ragionesociale'];
		$clientReseller = $this->Clientreseller->find('all', ['conditions'=>['Clientreseller.company_id' =>MYCOMPANY, 'Clientreseller.client_id <>' => $resellerId ,'Clientreseller.state'=>ATTIVO]]);
		$clients = $this->Client->find('list',['fields'=>['Client.id','Client.ragionesociale'],['contain'=>'Clientreseller'], 'conditions'=> ['Client.reseller' => 0, 'Client.company_id' =>MYCOMPANY,'Client.state'=>ATTIVO]]);

		$checkedArray = null;

		// Da ottimizzare
		foreach($clientReseller as $reselled)
		{
			foreach ($clients as $key =>  $client)
			{
				if($reselled['Clientreseller']['client_id'] == $key)
				{
					if($reselled['Clientreseller']['reseller_id'] != $resellerId)
					{
						unset($clients[$key]);
					}
					else
					{
						$checkedArray[] = $reselled['Clientreseller']['client_id'];
					}
				}
			}
		}

		$this->set('checkedArray',$checkedArray);
		$this->set('resellerId',$resellerId);
		$this->set('resellerName',$resellerName);
		$this->set('clients', $clients);
	}

	public function saveClientsReferent()
	{
		$this->autoRender = false;
		$this->Utilities->loadModels($this,['Clientreseller']);

		$reseller = $this->request->data['Client']['resellerId'];

		$this->Clientreseller->deleteAll(['Clientreseller.company_id'=>MYCOMPANY, 'reseller_id' => $this->request->data(['Client'])['resellerId']]);

		foreach($this->request->data['Client']['client_id'] as  $client)
		{
			$newClientReseller = $this->Clientreseller->create();
			$newClientReseller['Clientreseller']['client_id'] = $client;
			$newClientReseller['Clientreseller']['reseller_id'] = $reseller;
			$newClientReseller['Clientreseller']['company_id'] = MYCOMPANY;

			$this->Clientreseller->save($newClientReseller);
		}

		$this->redirect(['action'=>'indexResellerClient', $reseller]);
	}

	public function add()
	{
		$this->loadModel('Utilities');
		$this->loadModel('Bank');
		$this->Utilities->loadModels($this,['Myhelper']);
		//debug($this->request->is('post'));
        //die;
		if ($this->request->is('post'))
		{
			$this->Client->create();
			$this->request->data['Client']['company_id']=MYCOMPANY;

			if($this->request->data['Client']['pec'] == '') { unset($this->request->data['Client']['pec']); }
			if($this->request->data['Client']['codiceDestinatario'] == ''){ unset($this->request->data['Client']['codiceDestinatario']); }

			if ($newClient = $this->Client->save($this->request->data))
			{
				$this->Session->setFlash(__('Cliente salvato'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Session->setFlash(__('Il cliente non è stato salvato'), 'custom-danger');
			}
		}

		$helperMessage['invoice'] = $this->Myhelper->getMessage('electronicInvoiceClientCode');
		$helperMessage['splitpayment'] = $this->Myhelper->getMessage('clientSplitPayment');
		$helperMessage['billsdelay'] = $this->Myhelper->getMessage('clientBillDelay');
		$this->set('helperMessage',$helperMessage);

		//$banks = $this->Utilities->getBanksList();
		$banks = $this->Bank->getBanksListWithAbiAndCab();
		$payments = $this->Utilities->getPaymentsList();
		$vats = $this->Utilities->getVatsList();
		$nations = $this->Utilities->getNationsList();
		$currencies = $this->Utilities->getCurrenciesList();

		$this->set(compact('ivas','banks','payments','vats','nations','currencies'));
	}

    public function add_fast($redirect = 'index')
    {
        $this->layout = 'voidMegaformLayout';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Client']);
        if ($this->request->is('post')) {

            $this->Client->create();
            $this->request->data['Client']['company_id'] = MYCOMPANY;
            $this->request->data['Client']['state'] = 2;

            if ($newClient = $this->Client->save($this->request->data)) {

            } else {
                $this->Session->setFlash(__('Il cliente non è stato salvato'), 'custom-danger');
            }

            if ($this->request->is('ajax')) {
                $this->layout = false;
                $dataArray = ['entityData' => $newClient['Client']];
                print(json_encode($dataArray));
                die;
            }
        }
        $this->set('nations',$this->Utilities->getNationsList());
    }

	public function edit($id = null)
	{
		$this->loadModel('Utilities');
		$this->loadModel('Bank');
		$this->Utilities->loadModels($this,['Myhelper']);
		$this->Client->id = $id;
		if (!$this->Client->exists())
		{
			throw new NotFoundException(__('Cliente non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{

			if($this->request->data['Client']['pec'] == '') { unset($this->request->data['Client']['pec']); }
			if($this->request->data['Client']['codiceDestinatario'] == ''){ unset($this->request->data['Client']['codiceDestinatario']); }

			if ($this->Client->save($this->request->data))
			{
				$this->Session->setFlash(__('Cliente salvato correttamente'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Session->setFlash(__('Il cliente non é stato salvato, riprovare.'), 'custom-danger');
			}
		}
		else
		{
			$this->request->data = $this->Client->read(null, $id);
		}

		//$banks = $this->Utilities->getBanksList();
		$banks = $this->Bank->getBanksListWithAbiAndCab();
		$payments = $this->Utilities->getPaymentsList();
		$vats = $this->Utilities->getVatsList();
		$nations = $this->Utilities->getNationsList();
		$currencies = $this->Utilities->getCurrenciesList();

		$helperMessage['invoice'] = $this->Myhelper->getMessage('electronicInvoiceClientCode');
		$helperMessage['splitpayment'] = $this->Myhelper->getMessage('clientSplitPayment');
		$helperMessage['billsdelay'] = $this->Myhelper->getMessage('clientBillDelay');
		$this->set('helperMessage',$helperMessage);

		$this->set(compact('ivas','banks','payments','vats','nations','currencies'));
	}

	public function delete($id = null)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Client','Messages']);
        $asg =  ["il","cliente","M"];
		if($this->Client->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Client->find('first',['conditions'=>['Client.id'=>$id,'Client.company_id'=>MYCOMPANY]]);
        if ($this->Client->hide($currentDeleted['Client']['id']))
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}

	public function getClientPaymentMethod()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$customerName = $_POST['clientId'];
		is_numeric($_POST['clientId']) ? $paymentMethod =  $this->Utilities->getDefaultClientPaymentMethodId($_POST['clientId']) : $paymentMethod = $this->Utilities->getDefaultClientPaymentMethodId($this->Utilities->getCustomerId($customerName));
        //return $this->Utilities->getDefaultClientPaymentMethodId($this->Utilities->getCustomerId($customerName));
        return $paymentMethod;
	}



	public function getDefaultClientPaymentMethodCollectionfees()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$customerName = $_POST['clientId'];
		is_numeric($_POST['clientId']) ? $collectionfees =  $this->Utilities->getDefaultClientPaymentMethodCollectionfees($_POST['clientId']) : $collectionfees = $this->Utilities->getDefaultClientPaymentMethodCollectionfees($this->Utilities->getCustomerId($customerName));
        //return $this->Utilities->getDefaultClientPaymentMethodCollectionfees($this->Utilities->getCustomerId($customerName));
        return $collectionfees;
	}

	public function getClientSplitPayment()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$customerName = $_POST['clientId'];
		is_numeric($_POST['clientId']) ? $splitPayment =  $this->Utilities->getDefaultClientSplitPayment($_POST['clientId']) : $splitPayment = $this->Utilities->getDefaultClientSplitPayment($this->Utilities->getCustomerId($customerName));
        //return $this->Utilities->getDefaultClientSplitPayment($this->Utilities->getCustomerId($customerName));
        return $splitPayment;
	}

	public function isClientPa()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$customerName = $_POST['clientId'];
		is_numeric($_POST['clientId']) ? $isPa =  $this->Utilities->isClientPa($_POST['clientId']) : $isPa = $this->Utilities->isClientPa($this->Utilities->getCustomerId($customerName));
        // return $this->Utilities->isClientPa($this->Utilities->getCustomerId($customerName));
        return $isPa;
	}

	public function loadClientWithholdingTax()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$customerName = $_POST['clientId'];
		is_numeric($_POST['clientId']) ? $clientWithholdingtax =  $this->Utilities->loadClientWithholdingTax($_POST['clientId']) : $clientWithholdingtax = $this->Utilities->loadClientWithholdingTax($this->Utilities->getCustomerId($customerName));
        // return $this->Utilities->loadClientWithholdingTax($this->Utilities->getCustomerId($customerName));
        return $clientWithholdingtax;
	}

	public function loadClientCurrency()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$customerName = $_POST['clientId'];
		is_numeric($_POST['clientId']) ? $currency =  $this->Utilities->loadClientCurrency($_POST['clientId']) : $currency = $this->Utilities->loadClientCurrency($this->Utilities->getCustomerId($customerName));
        // return $this->Utilities->loadClientCurrency($this->Utilities->getCustomerId($customerName));
        return $currency;
	}


	public function getClientVat()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$customerName = $_POST['clientId'];
		is_numeric( $_POST['clientId']) ? $vat =  $this->Utilities->getDefaultClientVat($_POST['clientId']) : $vat = $this->Utilities->getDefaultClientVat($this->Utilities->getCustomerId($customerName));
        //return $this->Utilities->getDefaultClientVat($this->Utilities->getCustomerId($customerName));
        return  $vat;
	}

		// Controllo duplicato fattura
	public function checkClientPivaDuplicate()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$return =  $this->Utilities->checkClientPivaDuplicate($_POST['piva']);
		return json_encode($return);
	}

	// Controllo duplicato fattura
	public function checkClientCfDuplicate()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$return =  $this->Utilities->checkClientCfDuplicate($_POST['cf']);
		return json_encode($return);
	}

    public function getClientConstructionsites()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        return $this->Client->getConstructionsites($_POST['clientId']);
    }

    public function getClientQuotes()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        return $this->Client->getQuotes($_POST['clientId']);
    }

    public function enable()
    {
        $this->autoRender = false;
        $this->Client->enable();
    }
}
