<?php
App::uses('AppController', 'Controller');

class CategoriesController extends AppController
{

    public function index()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Category', 'Csv']);

        $conditionsArray = ['Category.company_id' => MYCOMPANY, 'Category.state' => ATTIVO];
        $filterableFields = ['description', null,null];
        $sortableFields = [['code', 'Codice categoria merceologica'],['description', 'Categoria merceologica'], ['#actions']];

        if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
        }

        $this->set('filterableFields', $filterableFields);
        $this->set('sortableFields', $sortableFields);

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Categories->find('all', ['conditions' => $conditionsArray, 'order' => ['Categories.description' => 'asc']]);
            echo 'Categoria' . "\r\n";
            foreach ($dataForXls as $xlsRow) {
                echo $xlsRow['Category']['description'] . ';' . "\r\n";
            }
        } else {
            $this->paginate = ['conditions' => $conditionsArray];
            $this->set('categories', $this->paginate());
        }
    }

    public function add()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Category','Messages']);
        $messageArray = ["La", "Categoria merceologica", "F"];
        if ($this->request->is('post')) {
            $this->Category->create();
            $this->request->data['Category']['company_id'] = MYCOMPANY;
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__($this->Messages->successOfAdd($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__($this->Messages->failOfAdd($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
            }
        }
    }

    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Category','Messages']);
        $messageArray = ["La", "Categoria merceologica", "F"];
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new Exception($this->Messages->notFound($messageArray [0], $messageArray [1], $messageArray [2]));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__($this->Messages->successOfUpdate($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__($this->Messages->failOfUpdate($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
            }
        } else {
            $this->request->data = $this->Category->read(null, $id);
        }
    }


    public function delete($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Messages', 'Category']);

        $messageArray = ["La", "Categoria merceologica", "F"];
        if ($this->Category->isHidden($id))
            throw new Exception($this->Messages->notFound($messageArray [0], $messageArray [1], $messageArray [2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Category->find('first', ['conditions' => ['Category.id' => $id, 'Category.company_id' => MYCOMPANY]]);
        if ($this->Category->hide($currentDeleted['Category']['id']))
            $this->Session->setFlash(__($this->Messages->successOfDelete($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
        else
            $this->Session->setFlash(__($this->Messages->failOfDelete($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-danger');
        return $this->redirect(['action' => 'index']);
    }
}
