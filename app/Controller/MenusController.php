<?php
App::uses('AppController', 'Controller');

class MenusController extends AppController 
{
	public function ajaxSetActiveMenuItem() {
		$this->autoRender = false;
		if($this->request->is('ajax'))
		{
			$_SESSION['Auth']['User']['activeMenuItemId'] = $_POST['id'];
			$_SESSION['Auth']['User']['activeMenuItemParentId'] = $_POST['parentId'];
		}
	}
}
