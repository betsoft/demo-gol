<?php
App::uses('AppController', 'Controller');

 
class DepositsController extends AppController {

		public function index()
		{
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this,['Deposit','Csv','Graphicsutilities']);
			if(ADVANCED_STORAGE_ENABLED) // La gestione dei depositi è nel modulo magazzino
			{ 
				$conditionsArray = [ 'Deposit.company_id' => MYCOMPANY,'Deposit.state'=>ATTIVO];
				$filterableFields = ['code','deposit_name',null,null];
				$sortableFields = [['code','Codice deposito'],['deposit_name','Deposito'],['','Principale'],'#actions'];
				
				$automaticFilter = $this->Session->read('arrayOfFilters') ;
				if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

				
				if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
				{
					$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
					
					$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
					$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
					$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
				}
				
				// Generazione XLS
				if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
				{
					$this->autoRender = false;
					$dataForXls = $this->Deposit->find('all',['conditions'=>$conditionsArray,'order' => ['code' => 'asc']]); 			
					echo 'Codice deposito;Deposito;'."\r\n";
					foreach ($dataForXls as $xlsRow)
					{
						echo $xlsRow['Deposit']['code']. ';' .$xlsRow['Deposit']['deposit_name']. ';'."\r\n";
					}
				}
				else
				{
					$this->paginate = ['conditions' =>$conditionsArray,'order'=> 'deposit_name'];
					$this->set('filterableFields',$filterableFields);
					$this->set('sortableFields',$sortableFields);
					$this->set('deposits', $this->paginate());
				}
			}
			else
			{
				$this->Utilities->throwException('notfound');
			}
		}
	
		public function add($redirect = 'index') 
		{
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this,['Deposit']);
			
			if(ADVANCED_STORAGE_ENABLED) // La gestione dei depositi è nel modulo magazzino
			{
				$datasource = $this->Deposit->getDataSource();
				try 
				{
					$datasource->begin();
					if ($this->request->is('post')) 
					{
						
						$this->Deposit->create();
						$this->request->data['Deposit']['company_id']=MYCOMPANY;
						if ($newDeposit = $this->Deposit->save($this->request->data)) 
						{
							if($this->Utilities->getDepositsCount() == 1)
							{
								$this->Utilities->setNewMainDeposit($newDeposit['Deposit']['id']);
							}
						
							$this->Session->setFlash(__('Deposito salvato'), 'custom-flash');
							$datasource->commit();		
							$this->redirect(['action' => $redirect]);
						} 
						else 
						{ 
							$this->Session->setFlash(__('Errore durante la creazione del nuovo deposito.'), 'custom-danger');
						}
					}
					
					$this->set('numberofdepoits',$this->Utilities->getDepositsCount());
				
				} 
				catch(Exception $e) 
				{
		    		$datasource->rollback();
		    		$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
				}
			}
			else{ $this->Utilities->throwException('notfound'); }
		}
		
	
		public function edit($id = null, $redirect = 'index') 
		{
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this,['Deposit']);
			if(ADVANCED_STORAGE_ENABLED) // La gestione dei depositi è nel modulo magazzino
			{
				$this->Deposit->id = $id;
				if (!$this->Deposit->exists()) { throw new NotFoundException(__('Deposito  non valido')); }
				if ($this->request->is('post') || $this->request->is('put')) 
				{
					$datasource = $this->Deposit->getDataSource();
					try 
					{
						$datasource->begin();
		
						if ($this->Deposit->save($this->request->data)) 
						{
							$this->Session->setFlash(__('Deposito salvato'), 'custom-flash');
							$datasource->commit();
							$this->redirect(['action' => $redirect]);
						}
						else 
						{
							$this->Session->setFlash(__('Errore durante la modifica del deposito.'), 'custom-danger');
						}
					
					}
					catch(Exception $e) 
					{
		    			$datasource->rollback();
		    			$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
					}
				}
				else 
				{
					$this->request->data = $this->Deposit->read(null, $id);
				}
				$this->set('numberofdepoits',$this->Utilities->getDepositsCount());
			}
			else{ $this->Utilities->throwException('notfound'); }
		}
		
		public function delete($id = null) 
		{
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this,['Deposit','Messages']);
	        $asg = ["il","deposito","M"];
			if($this->Deposit->isHidden($id))
				throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));
	
			$this->request->allowMethod(['post', 'delete']);
			
	        $currentDeleted = $this->Deposit->find('first',['conditions'=>['Deposit.id'=>$id,'Deposit.company_id'=>MYCOMPANY]]);
	        if ($this->Deposit->hide($currentDeleted['Deposit']['id'])) 
		      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
	        else
	           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
			return $this->redirect(['action' => 'index']);
		}
		
		public function setMainDeposit()
		{
			$this->loadModel('Utilities');
			if(ADVANCED_STORAGE_ENABLED) // La gestione dei depositi è nel modulo magazzino
			{
				$this->autoRender = false;
				$depositId = $_POST['id'];
				$this->Utilities->setNewMainDeposit($depositId);
			}
			else{ $this->Utilities->throwException('notfound'); }
		}
	
}
