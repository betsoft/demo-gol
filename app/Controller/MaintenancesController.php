<?php
App::uses('AppController', 'Controller');
require 'PHPMailer/PHPMailerAutoload.php';

class MaintenancesController extends AppController
{
    public $components = ['Mpdf'];

    public function index()
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance']);

            $conditionsArray = ['Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => ATTIVO];
            $filterableFields = ['Maintenance_number', 'Technician__name',  '#htmlElements[0]', 'Client__ragionesociale', 'Constructionsite__name', 'Constructionsite__description', 'Maintenance__intervention_description', null];
            $sortableFields = [['maintenance_number', 'Numero'], ['Technician.name', 'Tecnico'], [null, 'Data'], ['Client.ragionesociale', 'Cliente'], ['Constructionsite.name', 'Cantiere'], ['Constructionsite.description', 'Descrizione cantiere'], ['intervention_description', 'Descrizione intervento'], ['#actions']];

            if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

                if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '') {
                    $conditionsArray['maintenance_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));
                }
                if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '') {
                    $conditionsArray['maintenance_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
                }
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->paginate = ['conditions' => $conditionsArray, 'order' => 'maintenance_date desc, maintenance_number desc'];

            $this->set('maintenances', $this->paginate());
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function add()
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance', 'Technician', 'Constructionsite', 'Quote', 'Units', 'Maintenancerow', 'Client', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Outsideoperator', 'Outsideoperator', 'Maintenancehoursoutsideoperator']);

            if ($this->request->is('post')) {

                $this->request->data['Maintenances']['company_id'] = MYCOMPANY;
                $this->request->data['Maintenances']['state'] = 1;
                $this->request->data['Maintenances']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenances']['maintenance_date']));

                // Fix Muliselect nelle ore (andrebbe modificato nella vista ma ci metto troppo tempo e ho scadenza
                if (isset($this->request->data['Maintenances']['operator_id'])) {
                    $this->request->data['Hours'][0]['operator_id_multiple_select_'] = $this->request->data['Maintenances']['operator_id'];
                }

                // Manage clonable/ddt/data
                isset($this->request->data['Ddt_0_ddtdate']) ? $this->request->data['Ddt'][0]['ddtdate'] = $this->request->data['Ddt_0_ddtdate'] : null;
                isset($this->request->data['Maintenances']['rowsupplier_id']) ? $this->request->data['Ddt'][0]['supplier_id_multiple_select'] = $this->request->data['Maintenances']['rowsupplier_id'] : null;


                foreach ($this->request->data['Ddt'] as $key => $ddt) {
                    if ($ddt['ddtdate'] != '') {
                        $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                    }
                }

                /*if (isset($this->request->data['Maintenances']['maintenance_hour_from'])) {
                    $this->request->data['Maintenances']['maintenance_hour_from'] = date("H:i:s", strtotime($this->request->data['Maintenances']['maintenance_hour_from']));
                }

                if (isset($this->request->data['Maintenances']['maintenance_hour_to'])) {
                    $this->request->data['Maintenances']['maintenance_hour_to'] = date("H:i:s", strtotime($this->request->data['Maintenances']['maintenance_hour_to']));
                }*/

                if ($newMaintenance = $this->Maintenance->save($this->request->data['Maintenances'])) {

                    foreach ($this->request->data['Good'] as $key => $good) {
                        $newMaintenanceRow = $this->Maintenancerow->create();
                        $newMaintenanceRow['Maintenancerow']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                        $newMaintenanceRow['Maintenancerow']['codice'] = $good['codice'];
                        $newMaintenanceRow['Maintenancerow']['description'] = $good['description'];
                        $newMaintenanceRow['Maintenancerow']['customdescription'] = $good['customdescription'];
                        $newMaintenanceRow['Maintenancerow']['quantity'] = $good['quantity'];
                        $newMaintenanceRow['Maintenancerow']['unit_of_measure_id'] = $good['unit_of_measure_id'];
                        $newMaintenanceRow['Maintenancerow']['storage_id'] = $good['storage_id'];
                        $newMaintenanceRow['Maintenancerow']['company_id'] = MYCOMPANY;
                        $newMaintenanceRow['Maintenancerow']['state'] = 1;
                        $this->Maintenancerow->save($newMaintenanceRow);
                    }


                    foreach ($this->request->data['Ddt'] as $key => $ddt) {
                        $newMaintenanceDdt = $this->Maintenanceddt->create();
                        $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                        $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'];
                        $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                        $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                        $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'];
                        $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                        $this->Maintenanceddt->save($newMaintenanceDdt);
                    }

                    /* Salvataggio ore */
                    foreach ($this->request->data['Hours'] as $key => $hour) {
                        $newMaintenanceHour = $this->Maintenancehour->create();
                        $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                        $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                        $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                        $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating'];
                        $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;

                        if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour)) {
                            if (isset($hour['operator_id_multiple_select'])) {
                                foreach ($hour['operator_id_multiple_select'] as $key => $outsideoperatorhour) {
                                    $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = 1;
                                    $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                }
                            }
                        }
                    }

                    $this->Session->setFlash(__("Scheda d'intervento salvata"), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__("La scheda d'intervento non è stata salvata"), 'custom-danger');
                }
            }

            /* Passo list */
            $this->set('nations', $this->Utilities->getNationsList());
            $this->set('technicians', $this->Technician->getList());
            $this->set('constructionsites', $this->Constructionsite->getList());
            $this->set('quotes', $this->Quote->getList());
            $this->set('units', $this->Units->getList());
            $this->set('outsideoperators', $this->Outsideoperator->getList());

            // Carico i magazzini
            if (ADVANCED_STORAGE_ENABLED) {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            } else {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
            }

            $this->set('clients', $this->Client->getList());
            $this->set('suppliers', $this->Supplier->getList());

            $this->set('maintenance_next_number', $this->Maintenance->getNextMaintenanceNumber(date("Y")));
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function edit($id = null)
    {

        if (MODULO_CANTIERI) {
            $conditionArray = ['Maintenance.id'=>$id,'Maintenance.company_id'=>MYCOMPANY,'Maintenance.state'=>1];
            $testMaintenance = $this->Maintenance->find('first',['conditions'=>$conditionArray]);

            //if ($testMaintenance['Maintenance']['sent'] == 0 && date_diff(date_create($testMaintenance['Maintenance']['maintenance_date']), date_create(date('Y-m-d')))->format('%d') < 8)
            if ($testMaintenance['Maintenance']['sent'] == 0)
            {

                $this->loadModel('Utilities');
                $this->Utilities->loadModels($this, ['Maintenance', 'Technician', 'Constructionsite', 'Quote', 'Units', 'Client', 'Messages', 'Maintenancerow', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Outsideoperator', 'Maintenancehoursoutsideoperator']);
                $messageParameter = ["la", "scheda d'intervento", "F"];
                $this->Maintenance->id = $id;

                if (!$this->Maintenance->exists()) {
                    throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));
                }

                if ($this->request->is('post') || $this->request->is('put')) {
                    $this->request->data['Maintenance']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenance']['maintenance_date']));

                    // Manage clonable/ddt/data
                    isset($this->request->data['Ddt_0_ddtdate']) ? $this->request->data['Ddt'][0]['ddtdate'] = $this->request->data['Ddt_0_ddtdate'] : null;
                    isset($this->request->data['Maintenances']['rowsupplier_id']) ? $this->request->data['Ddt'][0]['supplier_id_multiple_select'] = $this->request->data['Maintenances']['rowsupplier_id'] : null;
                    if (isset($this->request->data['Ddt'])) {
                        foreach ($this->request->data['Ddt'] as $key => $ddt) {
                            if ($ddt['ddtdate'] != '') {
                                $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                            }
                        }
                    }

                    if ($newMaintenanceRow = $this->Maintenance->save($this->request->data)) {
                        //$this->Maintenancerow->deleteAll(['Maintenancerow.maintenance_id' => $id,'Maintenancerow.company_id'=>MYCOMPANY]);
                        $this->hideStorages($id);
                        foreach ($this->request->data['Maintenancerow'] as $Maintenancerow) {
                            $newMaintenanceRow = $this->Maintenancerow->create();
                            $newMaintenanceRow = $Maintenancerow;
                            $newMaintenanceRow['maintenance_id'] = $id;
                            $newMaintenanceRow['state'] = ATTIVO;
                            $newMaintenanceRow['company_id'] = MYCOMPANY;
                            $saved = $this->Maintenancerow->save($newMaintenanceRow);
                        }

                        if (isset($this->request->data['Ddt'])) {
                            $this->hideDdts($id);
                            foreach ($this->request->data['Ddt'] as $key => $ddt) {
                                $newMaintenanceDdt = $this->Maintenanceddt->create();
                                $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $id;
                                $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'];
                                $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                                $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                                isset( $ddt['supplier_id_multiple_select']) ? $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'] : $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = null;
                                $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                                $this->Maintenanceddt->save($newMaintenanceDdt);
                            }
                        }

                        // Fix Muliselect nelle ore (andrebbe modificato nella vista ma ci metto troppo tempo e ho scadenza
                        if (isset($this->request->data['Maintenances']['operator_id'])) {
                            $this->request->data['Hours'][0]['operator_id_multiple_select_'] = $this->request->data['Maintenances']['operator_id'];
                        }

                        if (isset($this->request->data['Hours'])) {
                            //$this->Maintenanceddt->deleteAll(['Maintenancehour.maintenance_id' => $id,'Maintenancehour.company_id'=>MYCOMPANY]);
                            $this->hideHours($id);
                            foreach ($this->request->data['Hours'] as $key => $hour) {
                                $newMaintenanceHour = $this->Maintenancehour->create();

                                $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $id;
                                $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                                $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                                $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating'];
                                $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;

                                if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour)) {
                                    if (isset($hour['operator_id_multiple_select'])) {
                                        foreach ($hour['operator_id_multiple_select'] as $key => $outsideoperatorhour) {
                                            $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = 1;
                                            $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                        }
                                    }
                                }

                            }
                        }

                        $this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                        $this->redirect(['action' => 'index']);
                    } else {
                        $this->Session->setFlash(__($this->Messages->failOfupdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                    }
                } else {
                    $this->request->data = $this->Maintenance->find('first', ['contain' => ['Client', 'Maintenancerow' => ['Storage'], 'Maintenanceddt', 'Maintenancehour' => 'Maintenancehoursoutsideoperator'], 'recursive' => 2, 'conditions' => ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY]]);
                }

                // Carico i magazzini
                if (ADVANCED_STORAGE_ENABLED) {
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
                } else {
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
                }

                $this->set('outsideoperators', $this->Outsideoperator->getList());
                $this->set('suppliers', $this->Supplier->getList());
                $this->set('clients', $this->Client->getList());
                $this->set('nations', $this->Utilities->getNationsList());
                $this->set('technicians', $this->Technician->getList());
                //$this->set('constructionsites', $this->Constructionsite->getList());
                $this->set('constructionsites', $this->Constructionsite->getListAndSelected($this->request->data['Maintenance']['constructionsite_id'], $this->Constructionsite->getClient($this->request->data['Maintenance']['constructionsite_id'])));
                $this->set('quotes', $this->Quote->getList());
                $this->set('units',  $this->Units->getList());
                //$this->set('outsideoperators', $this->Outsideoperator->getList());
            }
            else
                {
                    throw new MethodNotAllowedException(__('Sezione non abilitata'));
                }
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }


    public function delete($id = null)
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Messages', 'Maintenance', 'Maintenancehour', 'Maintenanceddt', 'Maintenancerow']);

            $messageParameter = ["la", "scheda d'intervento", "F"];

            if ($this->Maintenance->isHidden($id))
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

            // $maintenancehours = $this->Maintenancehour->find('all', ['conditions' => ['Maintenancehour.maintenance_id' => $id, 'Maintenancehour.state' => 1, 'Maintenancehour.company_id' => MYCOMPANY]]);
            // $maintenanceddts = $this->Maintenanceddt->find('all', ['conditions' => ['Maintenanceddt.maintenance_id' => $id, 'Maintenanceddt.state' => 1, 'Maintenanceddt.company_id' => MYCOMPANY]]);
            // $maintenancerows = $this->Maintenancerow->find('all', ['conditions' => ['Maintenancerow.maintenance_id' => $id, 'Maintenancerow.state' => 1, 'Maintenancerow.company_id' => MYCOMPANY]]);

            $this->request->allowMethod(['post', 'delete']);

            $currentDeleted = $this->Maintenance->find('first', ['conditions' => ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY]]);
            if ($this->Maintenance->hide($currentDeleted['Maintenance']['id'])) {
                $this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');

                $this->hideHours($id);
                $this->hideDdts($id);
                $this->hideStorages($id);

                /*  foreach ($maintenancehours as $maintenancehour) {
                      $this->Maintenancehour->hide($maintenancehour['Maintenancehour']['id']);
                  }

                  foreach ($maintenanceddts as $maintenanceddt) {
                      $this->Maintenanceddt->hide($maintenanceddt['Maintenanceddt']['id']);
                  }

                  foreach ($maintenancerows as $maintenancerow) {
                      $this->Maintenancerow->hide($maintenancerow['Maintenancerow']['id']);
                  }*/
            } else
                $this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');

            return $this->redirect(['action' => 'index']);
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    /** Viste per i tecnici */

    public function technicianIndex()
    {

        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance', 'Technician']);

            $currentTechinicianId = $this->Auth->user()['id'];
            isset($this->Technician->find('first', ['conditions' => ['user_id' => $this->Auth->user()['id']]])['Technician']['id']) ? $currentTechinicianId = $this->Technician->find('first', ['conditions' => ['user_id' => $this->Auth->user()['id']]])['Technician']['id'] : null;

            $conditionsArray = ['Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => ATTIVO, 'Maintenance.technician_id' => $currentTechinicianId];
            $filterableFields = ['maintenance_number', '#htmlElements[0]', 'Client__ragionesociale', 'Constructionsite__name', 'Constructionsite__description', 'Maintenance__intervention_description', null];
            $sortableFields = [['maintenance_number', 'Numero'], [null, 'Data'], ['Client.ragionesociale', 'Cliente'], ['Constructionsite.name', 'Cantiere'], ['Constructionsites.description', 'Descrizione cantiere'], ['Maintenance.intervention_description', 'Descrizione intervento'], ['#actions']];


            if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

                if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '') {
                    $conditionsArray['maintenance_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));
                }
                if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '') {
                    $conditionsArray['maintenance_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
                }
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            $this->paginate = ['conditions' => $conditionsArray, 'order' => 'maintenance_date desc, maintenance_number desc'];

            $this->set('maintenances', $this->paginate());
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }


    public function technicianAdd()
    {
        if (MODULO_CANTIERI) {
            $currentTechinicianId = $this->Auth->user()['id'];

            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance', 'Technician', 'Constructionsite', 'Quote', 'Units', 'Maintenancerow', 'Client', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Outsideoperator', 'Maintenancehoursoutsideoperator']);

            if ($this->request->is('post')) {

                $this->request->data['Maintenances']['company_id'] = MYCOMPANY;
                $this->request->data['Maintenances']['state'] = 1;
                $this->request->data['Maintenances']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenances']['maintenance_date']));
                $this->request->data['Maintenances']['technician_id'] = $this->Technician->getTechnicianIdFromUserId($this->Auth->User()['id']);


                // Manage clonable/ddt/data
                isset($this->request->data['Ddt_0_ddtdate']) ? $this->request->data['Ddt'][0]['ddtdate'] = $this->request->data['Ddt_0_ddtdate'] : null;
                isset($this->request->data['Maintenances']['rowsupplier_id']) ? $this->request->data['Ddt'][0]['supplier_id_multiple_select'] = $this->request->data['Maintenances']['rowsupplier_id'] : null;
                foreach ($this->request->data['Ddt'] as $key => $ddt) {
                    if ($ddt['ddtdate'] != '') {
                        $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                    }
                }

                if (isset($this->request->data['Maintenances']['maintenance_hour_from'])) {
                    $this->request->data['Maintenances']['maintenance_hour_from'] = date("H:i:s", strtotime($this->request->data['Maintenances']['maintenance_hour_from']));
                }

                if (isset($this->request->data['Maintenances']['maintenance_hour_to'])) {
                    $this->request->data['Maintenances']['maintenance_hour_to'] = date("H:i:s", strtotime($this->request->data['Maintenances']['maintenance_hour_to']));
                }
                // Fix Muliselect nelle ore (andrebbe modificato nella vista ma ci metto troppo tempo e ho scadenza
                if (isset($this->request->data['Maintenances']['operator_id'])) {
                    $this->request->data['Hours'][0]['operator_id_multiple_select_'] = $this->request->data['Maintenances']['operator_id'];
                }

                if ($newMaintenance = $this->Maintenance->save($this->request->data['Maintenances'])) {

                    /* Salvataggio articoli */
                    foreach ($this->request->data['Good'] as $key => $good) {
                        $newMaintenanceRow = $this->Maintenancerow->create();
                        $newMaintenanceRow['Maintenancerow']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                        $newMaintenanceRow['Maintenancerow']['codice'] = $good['codice'];
                        $newMaintenanceRow['Maintenancerow']['description'] = $good['description'];
                        $newMaintenanceRow['Maintenancerow']['customdescription'] = $good['customdescription'];
                        $newMaintenanceRow['Maintenancerow']['quantity'] = $good['quantity'];
                        $newMaintenanceRow['Maintenancerow']['unit_of_measure_id'] = $good['unit_of_measure_id'];
                        $newMaintenanceRow['Maintenancerow']['storage_id'] = $good['storage_id'];
                        $newMaintenanceRow['Maintenancerow']['company_id'] = MYCOMPANY;
                        $newMaintenanceRow['Maintenancerow']['state'] = 1;
                        $this->Maintenancerow->save($newMaintenanceRow);
                    }

                    /* Salvataggio bolle */
                    foreach ($this->request->data['Ddt'] as $key => $ddt) {
                        $newMaintenanceDdt = $this->Maintenanceddt->create();
                        $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                        $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'];
                        $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                        $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                        $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'];
                        $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                        $this->Maintenanceddt->save($newMaintenanceDdt);
                    }

                    /* Salvataggio ore */
                    foreach ($this->request->data['Hours'] as $key => $hour) {
                        $newMaintenanceHour = $this->Maintenancehour->create();
                        $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                        $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                        $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                        $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating']; // Presenza del tecnico
                        $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;

                        if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour)) {
                            if (isset($hour['operator_id_multiple_select_']) && $hour['operator_id_multiple_select_'] != '') {
                                foreach ($hour['operator_id_multiple_select_'] as $key => $outsideoperatorhour) {
                                    $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = 1;
                                    $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                }
                            }
                        }
                    }

                    $this->Session->setFlash(__("Scheda d'intervento salvata"), 'custom-flash');
                    $this->redirect(['action' => 'technicianindex']);
                } else {
                    $this->Session->setFlash(__("La scheda d'intervento non è stata salvata"), 'custom-danger');
                }
            }

            /* Passo list */
            $this->set('nations', $this->Utilities->getNationsList());
            $this->set('technicians', $this->Technician->getList());
            $this->set('outsideoperators', $this->Outsideoperator->getList());

            $this->set('quotes', $this->Quote->getList());
            $this->set('units', $this->Units->getList());
            $this->set('suppliers', $this->Supplier->getList());
            $this->set('technician', $this->Technician->getSurnameAndName($this->Technician->getTechnicianIdFromUserId($this->Auth->User()['id'])));

            // Carico i magazzini
            if (ADVANCED_STORAGE_ENABLED) {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            } else {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
            }

            $this->set('clients', $this->Client->getCompleteList());
            $this->set('constructionsites', $this->Constructionsite->getCompleteList());
            $this->set('maintenance_next_number', $this->Maintenance->getNextMaintenanceNumber(date("Y")));
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function technicianEdit($id)
    {
        if (MODULO_CANTIERI) {
            $conditionArray = ['Maintenance.id'=>$id,'Maintenance.company_id'=>MYCOMPANY,'Maintenance.state'=>1];
            $testMaintenance = $this->Maintenance->find('first',['conditions'=>$conditionArray]);
            if ($testMaintenance['Maintenance']['sent'] == 0 && date_diff(date_create($testMaintenance['Maintenance']['maintenance_date']), date_create(date('Y-m-d')))->format('%d') < 8)
            {

                $currentTechinicianId = $this->Auth->user()['id'];

                $this->loadModel('Utilities');
                $this->Utilities->loadModels($this, ['Outsideoperator', 'Maintenance', 'Technician', 'Constructionsite', 'Quote', 'Units', 'Client', 'Messages', 'Maintenancerow', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Maintenancehoursoutsideoperator']);
                $messageParameter = ["la", "scheda d'intervento", "F"];
                $this->Maintenance->id = $id;

                if (!$this->Maintenance->exists()) {
                    throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));
                }

                if ($this->request->is('post') || $this->request->is('put')) {

                    $this->request->data['Maintenance']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenance']['maintenance_date']));
                    /*$this->request->data['Maintenance']['maintenance_hour_from'] = date("H:i:s",strtotime($this->request->data['Maintenance']['maintenance_hour_from']));
                    $this->request->data['Maintenance']['maintenance_hour_to'] = date("H:i:s",strtotime($this->request->data['Maintenance']['maintenance_hour_to']));*/

                    // Manage clonable/ddt/data
                    isset($this->request->data['Ddt_0_ddtdate']) ? $this->request->data['Ddt'][0]['ddtdate'] = $this->request->data['Ddt_0_ddtdate'] : null;
                    isset($this->request->data['Maintenances']['rowsupplier_id']) ? $this->request->data['Ddt'][0]['supplier_id_multiple_select'] = $this->request->data['Maintenances']['rowsupplier_id'] : null;
                    if (isset($this->request->data['Ddt'])) {
                        foreach ($this->request->data['Ddt'] as $key => $ddt) {
                            if ($ddt['ddtdate'] != '') {
                                $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                            }
                        }
                    }

                    if ($newMaintenanceRow = $this->Maintenance->save($this->request->data)) {
                        if (isset($this->request->data['Maintenancerow'])) {
                            //  $this->Maintenancerow->deleteAll(['Maintenancerow.maintenance_id' => $id,'Maintenancerow.company_id'=>MYCOMPANY]);
                            $this->hideStorages($id);
                            foreach ($this->request->data['Maintenancerow'] as $Maintenancerow) {
                                $newMaintenanceRow = $this->Maintenancerow->create();
                                $newMaintenanceRow = $Maintenancerow;
                                $newMaintenanceRow['maintenance_id'] = $id;
                                $newMaintenanceRow['state'] = ATTIVO;
                                $newMaintenanceRow['company_id'] = MYCOMPANY;
                                $saved = $this->Maintenancerow->save($newMaintenanceRow);
                            }
                        }

                        if (isset($this->request->data['Ddt'])) {
                            //  $this->Maintenanceddt->deleteAll(['Maintenanceddt.maintenance_id' => $id,'Maintenanceddt.company_id'=>MYCOMPANY]);
                            $this->hideDdts($id);
                            foreach ($this->request->data['Ddt'] as $key => $ddt) {

                                $newMaintenanceDdt = $this->Maintenanceddt->create();
                                $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $id;
                                isset($ddt['ddtnumber']) ? $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'] : $newMaintenanceDdt['Maintenanceddt']['number'] = '';
                                $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                                $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                                $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'];
                                $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                                $newMaintenanceDdt['Maintenanceddt']['state'] = ATTIVO;
                                $this->Maintenanceddt->save($newMaintenanceDdt);
                            }
                        }

                        if (isset($this->request->data['Hours'])) {
                            //$this->Maintenanceddt->deleteAll(['Maintenancehour.maintenance_id' => $id,'Maintenancehour.company_id'=>MYCOMPANY]);
                            $this->hideHours($id);
                            foreach ($this->request->data['Hours'] as $key => $hour) {
                                $newMaintenanceHour = $this->Maintenancehour->create();
                                $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $id;
                                $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                                $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                                $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating'];
                                $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;
                                //  $this->Maintenancehour->save($newMaintenanceHour);
                                if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour)) {
                                    if (isset($hour['operator_id_multiple_select'])) {
                                        foreach ($hour['operator_id_multiple_select'] as $key => $outsideoperatorhour) {
                                            $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = 1;
                                            $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                        }
                                    }
                                }
                            }
                        }

                        $this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                        $this->redirect(['action' => 'technicianindex']);
                    } else {
                        $this->Session->setFlash(__($this->Messages->failOfupdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                    }
                } else {
                    $conditionsArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY];
                    $this->request->data = $this->Maintenance->find('first', ['contain' => ['Client', 'Maintenancerow' => ['Storage'], 'Maintenanceddt', 'Maintenancehour' => 'Maintenancehoursoutsideoperator'], 'recursive' => 2, 'conditions' => $conditionsArray]);
                }

                // Carico i magazzini
                if (ADVANCED_STORAGE_ENABLED) {
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
                } else {
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
                }

                // $this->set('clients', $this->Client->getListAndSelected($this->request->data['Maintenance']['client_id']));
                // $this->set('constructionsites', $this->Constructionsite->getListAndSelected($this->request->data['Maintenance']['constructionsite_id']));
                $this->set('clients', $this->Client->getListAndSelected($this->request->data['Maintenance']['client_id']));
                $this->set('constructionsites', $this->Constructionsite->getListAndSelected($this->request->data['Maintenance']['constructionsite_id']));
                //  $this->loadModel('Outsideoperator');
                $this->set('outsideoperators', $this->Outsideoperator->getList());


                $this->set('nations', $this->Utilities->getNationsList());
                $this->set('technicians', $this->Technician->getList());
                $this->set('technician', $this->Technician->getSurnameAndName($this->Technician->getTechnicianIdFromUserId($this->Auth->User()['id'])));
                $this->set('quotes', $this->Quote->getList());
                $this->set('units', $this->Units->getList());
                $this->set('suppliers', $this->Supplier->getList());
            }
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function checkDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Maintenance');
        $return = $this->Maintenance->checkDuplicate($_POST['maintenanceNumber'], $_POST['maintenanceDate']);
        return json_encode($return);
    }

    public function hideDdts($oldMaintenanceId)
    {
        $this->loadModel('Maintenanceddt');
        $newMaintenanceDdt = $this->Maintenanceddt->find('all', ['conditions' => ['Maintenanceddt.maintenance_id' => $oldMaintenanceId, 'Maintenanceddt.state' => 1, 'Maintenanceddt.company_id' => MYCOMPANY]]);
        foreach ($newMaintenanceDdt as $maintenanceddt) {
            $this->Maintenanceddt->hide($maintenanceddt['Maintenanceddt']['id']);
        }
    }

    public function hideHours($oldMaintenanceId)
    {
        $this->loadModel('Maintenancehour');
        $maintenancehours = $this->Maintenancehour->find('all', ['conditions' => ['Maintenancehour.maintenance_id' => $oldMaintenanceId, 'Maintenancehour.state' => 1, 'Maintenancehour.company_id' => MYCOMPANY]]);
        foreach ($maintenancehours as $maintenancehour) {
            $this->Maintenancehour->hide($maintenancehour['Maintenancehour']['id']);
        }
    }

    public function hideStorages($oldMaintenanceId)
    {
        $this->loadModel('Maintenancerows');
        $maintenancerows = $this->Maintenancerow->find('all', ['conditions' => ['Maintenancerow.maintenance_id' => $oldMaintenanceId, 'Maintenancerow.state' => 1, 'Maintenancerow.company_id' => MYCOMPANY]]);
        foreach ($maintenancerows as $maintenancerow) {
            $this->Maintenancerow->hide($maintenancerow['Maintenancerow']['id']);
        }
    }


    public function maintenancepdf($id)
    {
        if(MODULO_CANTIERI) {

            $this->layout = 'pdf';
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Client', 'Setting', 'Currencies']);

            $settings = $this->Setting->GetMySettings(MYCOMPANY);
            $this->set('settings', $settings);

            $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);

            $this->Pdf->setMpdf("title", "filename", $this->Mpdf);

            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => 1];
            $maintencance = $this->Maintenance->find('first',['conditions'=>$conditionArray,'recursive'=>'2']);
            $this->set('maintenance',$maintencance);


            $this->set('pdfHeader', $this->Pdf->getMaintenanceHeader());
            $this->set('pdfBody', $this->Pdf->getMaintenanceBody());
            $this->set('pdfFooter', $this->Pdf->getMaintenanceFooter());

        }

    }

    public function sendMaintenanceMail($id)
    {
        if (MODULO_CANTIERI) {
            $this->autoRender = false;
            $this->loadModel('Utilities');
            $id = $this->params['pass'][0];
            //$bill = $this->Bill->find('first', ['contain' => ['Client' => ['Bank', 'Nation'], 'Good' => ['Iva']], 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
            // $maintenance = $this->Maintenance->find('first', ['contain' => ['Client' => ['Bank', 'Nation'], 'Good' => ['Iva']], 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => 1];
            $maintenance = $this->Maintenance->find('first', ['conditions' => $conditionArray, 'recursive' => '2']);
            $settings = $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY]]);

            $currentDbName = ConnectionManager::getDataSource('default')->config['database'];

            $message = "<h3>Cordiale " . $maintenance['Maintenance']['handling'] . "</h3><br/> In allegato potete trovare la scheda d'intervento. <br/><br/>Cordiali Saluti";
            $okMessage = 'La scheda d\'intervento è stata spedita correttamente via mail al cliente';



            try {

                $mail = new PHPMailer;
                $mail->isSMTP();
                $mail->Host = $settings['Setting']['host'];
                $mail->SMTPAuth = true;
                $mail->Username = $settings['Setting']['username'];   // SMTP username
                $mail->Password = $this->Utilities->mydecrypt($settings['Setting']['password'], 'aes-256-cbc', 'noratechdemovladh2018', 0, 'demo2314xxx20189');
                $mail->SMTPSecure = $settings['Setting']['smtpSecure'];      // Enable TLS encryption, `ssl` also accepted
                $mail->Port = $settings['Setting']['port']; // TCP port to connect to
                if (isset($settings['Setting']['email']) && $settings['Setting']['email'] != '') {
                    $mail->SetFrom($settings['Setting']['email'], $settings['Setting']['name']);
                } else {
                    $mail->SetFrom($settings['Setting']['username'], $settings['Setting']['name']);
                }
                $mail->Timeout = 5;
                $mail->addAddress($maintenance['Maintenance']['email']); // Add a recipient
                $mail->isHTML(true);   // Set email format to HTML
                $mail->Subject = $settings['Setting']['name'] . ' Invio scheda d\'intervento a: ' . $maintenance['Maintenance']['handling'];
                $mail->Body = $message;

                $createdFileName = $this->createMailMaintenancePdf($id);

                $mail->addAttachment($createdFileName);

                if ($mail->send() == 1) {
                    $this->Maintenance->setSent($maintenance['Maintenance']['id']);
                    $this->Session->setFlash(($okMessage), 'custom-flash');
                } else {
                    $this->Session->setFlash(("Errore durante l'invio della scheda d'intevento, controllare i dati di configurazione dell'smtp nella sezione impostazioni"), 'custom-danger');
                }
            } catch (Exception $ecc) {
                $this->Session->setFlash(($ecc), 'custom-danger');
                $this->Session->setFlash(("Errore durante l'invio della scheda d'intervento, controllare i dati di configurazione dell'smtp nella sezione impostazioni"), 'custom-danger');
            }

            unlink($createdFileName);
            $this->redirect($this->referer());
        }
    }



    public function createMailMaintenancePdf($id)
    {
        if(MODULO_CANTIERI) {
            $view = new View(null, false);

            /* Questo non ha crypt e decrypt e restituisce un file da scaricare */
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
            error_reporting(0);

            $nomefile = str_replace(' ', '_', $id);

            $this->layout = 'pdf';
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Client', 'Transport', 'Setting', 'Currencies']);

            $configuration = array('win-1252', 'A4', 0, 0, 0, 0, 0, 0, 0, 0);
            $this->Mpdf->init($configuration);
            $this->Mpdf->showImageErrors = true;
            $this->Mpdf->setFilename($nomefile . '.pdf');
            $this->Mpdf->SetTitle("Fattura");
            $this->Mpdf->SetAuthor("");
            $this->Mpdf->SetDisplayMode('fullpage');

            $maintenanceFileName = APP . 'tmp' . DS . 'scheda_intervento_' . time() . '_' . $id . '.pdf';



            $viewPath = 'Maintenances';
            $view->viewPath = $viewPath;  //folder to look in in Views directory
            $settings = $this->Setting->GetMySettings(MYCOMPANY);

            $view->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
            $view->set('settings', $settings);

            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => 1];
            $maintencance = $this->Maintenance->find('first', ['conditions' => $conditionArray, 'recursive' => '2']);

            $view->set('maintenance', $maintencance);
            $view->set('pdfHeader', $this->Pdf->getMaintenanceHeader());
            $view->set('pdfBody', $this->Pdf->getMaintenanceBody());
            $view->set('pdfFooter', $this->Pdf->getMaintenanceFooter());

            $output = $view->render('maintenancepdf', 'pdf');

            $this->Mpdf->WriteHTML($output);
            $this->Mpdf->Output($maintenanceFileName, 'F');
            return $maintenanceFileName;
        }
    }

}
