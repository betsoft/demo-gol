<?php
App::uses('AppController', 'Controller');

class GroupsController extends AppController {


	public function index() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Groups']);
		$this->Groups->paginate = ['conditions' => ['Groups.company_id' => MYCOMPANY]];
		$this->set('groups', $this->paginate());
	}

	public function add() {
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Groups']);

		if ($this->request->is('post')) 
		{
			$this->Groups->create();
			$this->request->data['Groups']['company_id']=MYCOMPANY;
			
			if ($this->Groups->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Gruppo salvata'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} 
			else 
			{
				$this->Session->setFlash(__('Il Gruppo non è stata salvata'), 'custom-danger');
			}
		}
	}

	public function edit($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Groups']);
		$this->Groups->id = $id;
		if (!$this->Groups->exists()) {
			throw new NotFoundException(__('Gruppo non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Groups->save($this->request->data)) {
				$this->Session->setFlash(__('Gruppo salvato'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('Il gruppo non è stata salvato, riprovare'), 'custom-danger');
			}
		} else {
			$this->request->data = $this->Groups->read(null, $id);
		
		}
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Groups->id = $id;
		if (!$this->Groups->exists()) {
			throw new NotFoundException(__('Gruppo non valido'));
		}
		if ($this->Groups->delete($id, false)) {
			$this->Session->setFlash(__('Gruppo eliminato'), 'custom-flash');
			$this->redirect(['action' => 'index']);
		}
		$this->Session->setFlash(__('Il gruppo non è stata eliminato'), 'custom-danger');
		$this->redirect(['action' => 'index']);
	}
}
