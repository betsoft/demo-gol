<?php
App::uses('AppController', 'Controller');

class SupplierdestinationsController extends AppController 
{
	
	public function index($supplierId) 
	{
		$this->paginate = ['conditions' => ['Supplierdestination.supplier_id' => $supplierId, 'Supplierdestination.company_id' => MYCOMPANY,'state'=>ATTIVO],'order' => ['Supplierdestination.name' => 'asc'], 'limit' => 100 ];
		$this->Supplierdestination->recursive = 0;
		$this->set('Supplierdestinations', $this->paginate());
		$this->set('supplierId',$supplierId);
	}

	public function add($supplierId) 
	{
		$this->loadModel('Utilities');
		if ($this->request->is('post')) 
		{
			$this->Supplierdestination->create();
			$this->request->data['Supplierdestination']['company_id']=MYCOMPANY;
			$this->request->data['Supplierdestination']['supplier_id']=$supplierId;
			if ($this->Supplierdestination->save($this->request->data)) {
				$this->Session->setFlash(__('destino fornitore salvato'), 'custom-flash');
				$this->redirect(['action' => 'index',$supplierId]);
			} else {
				$this->Session->setFlash(__('Il destino fornitore non è stato salvato'), 'custom-danger');
			}
		}
		$this->set(compact('Supplierdestinations'));
		$this->set('supplierId',$supplierId);
		$this->set('nations', $this->Utilities->getNationsList());
	}
	

	public function edit($id = null,$supplierId = null)
	{
		$this->loadModel('Utilities');
		$this->Supplierdestination->id = $id;
		if (!$this->Supplierdestination->exists())
		{
			throw new NotFoundException(__('Destino fornitore non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Supplierdestination->save($this->request->data))
			{
				$this->Session->setFlash(__('fornitore salvato correttamente'), 'custom-flash');
				$this->redirect(['action' => 'index',$supplierId]);
			}
			else
			{
				$this->Session->setFlash(__('Il destino fornitore non é stato salvato, riprovare.'), 'custom-danger');
			}
		}
		else
		{
			$this->request->data = $this->Supplierdestination->read(null, $id);
		}

		$this->set(compact('Supplierdestinations'));
		$this->set('supplierId',$supplierId);
		$this->set('nations', $this->Utilities->getNationsList());
	}

	public function delete($id = null,$supplierId)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Supplierdestination','Messages']);
        $asg =  ["l'","articolo","M"];
		if($this->Supplierdestination->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Supplierdestination->find('first',['conditions'=>['Supplierdestination.id'=>$id,'Supplierdestination.company_id'=>MYCOMPANY]]);
        if ($this->Supplierdestination->hide($currentDeleted['Supplierdestination']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		   $this->redirect(['action' => 'index',$supplierId]);
	}

}
