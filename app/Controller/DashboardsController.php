<?php
App::uses('AppController', 'Controller');

class DashboardsController extends AppController
{

    public function commercialdashboard()
    {
        if (MODULO_CRM) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Client', 'Service', 'Clientassistance', 'Assistance', 'Appointment', 'Clientproposedservice', 'Clientproposedservicesstate', 'Clientserviceschecklistrowdependence', 'Clientserviceschecklistrow', 'Clientserviceschecklist', 'Clientservice', 'Proposalstate', 'Checklistrowdependence', 'Checklistrow', 'Checklist', 'Clientcommercial']);

            if ($this->request->is('post')) {

                foreach ($this->request->data['Clientproposedservice']['service'] as $id => $proposedservice) {
                    $clientproposedservice = $this->Clientproposedservice->findById($id);

                    $state = 1;
                    if ($proposedservice['proposed'] == 1) {
                        $state = 2;
                        $proposedservice['date'] = date('Y-m-d');
                    } elseif ($proposedservice['accepted'] == 1) {
                        $state = 3;

                        $this->Client->updateAll(['isLead' => 0], ['Client.id' => $this->request->data['Clientproposedservice']['client_id'], 'Client.isLead' => 1]);

                        if ($clientproposedservice['Clientproposedservice']['proposal_state_id'] !== '3') {
                            $service = $this->Service->findById($clientproposedservice['Clientproposedservice']['service_id']);
                            $checklist = $this->Checklist->findById($service['Checklist']['id']);

                            // Creo la nuova checklist cliente
                            $clientservicechecklist = $this->Clientserviceschecklist->createNew($service['Checklist']['id']);

                            // Cre il nuovo client service
                            $clientservice = $this->Clientservice->createNew($service['Service']['id'], $this->request->data['Clientproposedservice']['client_id'], $clientservicechecklist['Clientserviceschecklist']['id'], $this->request->data['Clientproposedservice']['custom_description']);

                            // Creo tutte le righe della checklist (per il cliente)
                            $checklistRows = $this->Clientserviceschecklist->createRows($service['Checklist']['id'], $clientservicechecklist['Clientserviceschecklist']['id']);

                            $checklistrows = $this->Checklistrow->find('all', ['conditions' => ['Checklistrow.checklist_id' => $service['Checklist']['id'], 'Checklistrow.company_id' => MYCOMPANY, 'Checklistrow.state' => ATTIVO], 'fields' => '', 'order' => '']);
                        }

                    } elseif ($proposedservice['refused'] == 1)
                        $state = 5;

                    $notes = "'" . $proposedservice['notes'] . "'";
                    $date = "'" . $proposedservice['date'] . "'";
                    $customdescription = "'" . $proposedservice['custom_description'] . "'";

                    $this->Clientproposedservice->updateAll(['Clientproposedservice.proposal_state_id' => $state, 'Clientproposedservice.last_proposed_date' => $date, 'Clientproposedservice.outside_supplier' => $proposedservice['outside_supplier'], 'Clientproposedservice.custom_description' => $customdescription, 'Clientproposedservice.notes' => $notes], ['Clientproposedservice.id' => $clientproposedservice['Clientproposedservice']['id']]);
                }
            }

            $this->set('clients', $this->Client->getCommercialVirtualList($this->Auth->User()['id']));
            $this->set('assistances', $this->Assistance->getList());
            $this->set('appointments', $this->Appointment->getListByUserId($this->Auth->User()['id']));
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }

    }

    public function sellerdashboard()
    {
        if (MODULO_REGISTRATORE_DI_CASSA) {
            $this->loadModel('Utilities');

            $this->Utilities->loadModels($this, ['Storage','Receipt','Iva','User']);
            $this->set('currentReceiptNumber', $this->Receipt->getNextReceiptNumber(date('d-m-Y')));
            $this->set('storages', $this->Storage->getMovableList());
            $this->set('notmovables', $this->Storage->getNotMovableList());
            $this->set('vats', $this->Iva->getPercentagesValues());
            $this->set('userCashRegisterAddress', $this->User->getCashRegisterIp());
            $defaultIva = $this->Utilities->getDefaultReceiptIva();
            $this->set('defaultIva', $defaultIva);
        }
    }

    public function getServices()
    {
        if (MODULO_CRM) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Clientproposedservice', 'Client', 'Clienttypeservice', 'Service']);
            $this->autoRender = false;
            $clientId = $_POST['clientId'];

            $clientProposedServices = $this->Clientproposedservice->find('all', ['conditions' => ['Clientproposedservice.client_id' => $clientId, 'Service.parent_id IS NULL', 'Clientproposedservice.state' => 1], 'order' => ['Service.description' => 'asc']]);

            if (count($clientProposedServices) == 0) {
                $client = $this->Client->findById($clientId);
                if ($client['Client']['client_type_id'] > 0) {
                    $clientTypeServices = $this->Clienttypeservice->find('all', ['conditions' => ['Clienttypeservice.client_type_id' => $client['Client']['client_type_id'], 'Clienttypeservice.company_id' => MYCOMPANY, 'Clienttypeservice.state' => ATTIVO], 'fields' => '', 'order' => '']);
                    foreach ($clientTypeServices as $clientTypeService) {
                        $clientProposedService = $this->Clientproposedservice->create();
                        $clientProposedService['Clientproposedservice']['client_id'] = $client['Client']['id'];
                        $clientProposedService['Clientproposedservice']['service_id'] = $clientTypeService['Clienttypeservice']['service_id'];
                        $clientProposedService['Clientproposedservice']['proposal_state_id'] = 1;
                        $clientProposedService['Clientproposedservice']['company_id'] = MYCOMPANY;
                        $this->Clientproposedservice->save($clientProposedService);
                    }
                } else {
                    $services = $this->Service->find('all', ['conditions' => ['Service.always_proposed' => 1]]);
                    foreach ($services as $service) {
                        $clientProposedService = $this->Clientproposedservice->create();
                        $clientProposedService['Clientproposedservice']['client_id'] = $client['Client']['id'];
                        $clientProposedService['Clientproposedservice']['service_id'] = $service['Service']['id'];
                        $clientProposedService['Clientproposedservice']['proposal_state_id'] = 1;
                        $clientProposedService['Clientproposedservice']['company_id'] = MYCOMPANY;
                        $this->Clientproposedservice->save($clientProposedService);
                    }
                }

                $clientProposedServices = $this->Clientproposedservice->find('all', ['conditions' => ['Clientproposedservice.client_id' => $clientId, 'Service.parent_id IS NULL', 'Clientproposedservice.state' => 1], 'order' => ['Service.description' => 'asc']]);
            }

            print_r(json_encode($clientProposedServices));
        }
    }

    public function getServicesChildren()
    {
        if (MODULO_CRM) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Clientproposedservice', 'Service', 'Client']);
            $this->autoRender = false;
            $clientProposedServiceId = $_POST['clientProposedServiceId'];
            $clientId = $_POST['clientId'];

            $parent = $this->Clientproposedservice->findById($clientProposedServiceId);

            $children = $this->Clientproposedservice->find('all', ['conditions' => ['Clientproposedservice.client_id' => $clientId, 'Clientproposedservice.company_id' => MYCOMPANY, 'Clientproposedservice.client_proposed_service_parent_id' => $clientProposedServiceId, 'Clientproposedservice.state' => 1], 'order' => ['Service.description' => 'asc']]);

            if (count($children) == 0) {
                $client = $this->Client->findById($clientId);
                $services = $this->Service->find('all', ['conditions' => ['Service.company_id' => MYCOMPANY, 'Service.parent_id' => $parent['Clientproposedservice']['service_id'], 'Service.state' => 1], 'order' => ['Service.description' => 'asc']]);

                foreach ($services as $service) {
                    $clientProposedService = $this->Clientproposedservice->create();
                    $clientProposedService['Clientproposedservice']['client_id'] = $client['Client']['id'];
                    $clientProposedService['Clientproposedservice']['service_id'] = $service['Service']['id'];
                    $clientProposedService['Clientproposedservice']['client_proposed_service_parent_id'] = $clientProposedServiceId;
                    $clientProposedService['Clientproposedservice']['proposal_state_id'] = 1;
                    $clientProposedService['Clientproposedservice']['company_id'] = MYCOMPANY;
                    $this->Clientproposedservice->save($clientProposedService);
                }

                $children = $this->Clientproposedservice->find('all', ['conditions' => ['Clientproposedservice.client_id' => $clientId, 'Clientproposedservice.company_id' => MYCOMPANY, 'Clientproposedservice.client_proposed_service_parent_id' => $clientProposedServiceId, 'Clientproposedservice.state' => 1], 'order' => ['Service.description' => 'asc']]);
            }

            print_r(json_encode($children));
        }
    }

    public function getRelatedServices()
    {
        if (MODULO_CRM) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Relatedservice']);
            $this->autoRender = false;
            $serviceId = $_POST['serviceId'];

            $related = $this->Relatedservice->find('all', ['conditions' => ['Relatedservice.service_id' => $serviceId, 'Relatedservice.company_id' => MYCOMPANY, 'Relatedservice.state' => 1], 'order' => ['Service.description' => 'asc']]);

            print_r(json_encode($related));
        }
    }

    public function newContract()
    {
        if (MODULO_CRM) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Clientassistance', 'Assistance', 'Clientassistancemovement', 'Assistancetype']);

            $this->autoRender = false;
            $id = $_POST['id'];
            $state = $_POST['state'];
            $clientId = $_POST['clientId'];

            $saved = false;

            if ($state == 1) {

                $conditionArray = ['Clientassistance.company_id' => MYCOMPANY, 'Clientassistance.id' => $id, 'Clientassistance.state' => 1];

                $clientAssistance = $this->Clientassistance->find('first', ['conditions' => $conditionArray]);
                $assistanceDefaultHour = $this->Assistance->getHourQuantity($clientAssistance['Assistance']['id']);

                $startDate = date('Y-m-d');
                if ($clientAssistance['Clientassistance']['end_date'] > $startDate)
                    $startDate = $clientAssistance['Clientassistance']['end_date'];

                $assistance = $this->Assistance->find('first', ['conditions' => ['Assistance.id' => $clientAssistance['Assistance']['id']]])['Assistance']['assistance_type_id'];
                $defaultDate = $this->Assistancetype->getNewDate($assistance, $startDate);

                $currentClientAssistanceid = $this->Clientassistance->find('first', ['conditions' => $conditionArray]);
                $currentClientAssistanceid['Clientassistance']['end_date'] = $defaultDate;

                if ($this->Clientassistance->save($currentClientAssistanceid))
                    $saved = true;
                $this->Clientassistancemovement->renewalClientAssistance($id, $clientAssistance['Assistance']['hour_price'], $assistanceDefaultHour);
            } else {

                $assistance = $this->Assistance->find('first', ['conditions' => ['Assistance.id' => $id]])['Assistance']['assistance_type_id'];
                $defaultDate = $this->Assistancetype->getNewDate($assistance, date('Y-m-d'));

                $clientAssistance = $this->Clientassistance->create();
                $clientAssistance['Clientassistance']['company_id'] = MYCOMPANY;
                $clientAssistance['Clientassistance']['client_id'] = $clientId;
                $clientAssistance['Clientassistance']['assistance_id'] = $id;
                $clientAssistance['Clientassistance']['start_date'] = date("Y-m-d");
                $clientAssistance['Clientassistance']['end_date'] = $defaultDate;
                $clientAssistance['Clientassistance']['state'] = 1;

                if ($newClientAssistance = $this->Clientassistance->save($clientAssistance)) {
                    $saved = true;
                    $this->Clientassistancemovement->createNewclientAssistance($newClientAssistance['Clientassistance']['id']);
                }
            }

            print_r(json_encode($saved));
        }
    }

    public function getClientServices()
    {
        if (MODULO_CRM) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Clientservice']);
            $this->autoRender = false;
            $clientId = $_POST['clientId'];

            $clientServices = $this->Clientservice->find('all', ['conditions' => ['Clientservice.client_id' => $clientId, 'Clientservice.company_id' => MYCOMPANY, 'Clientservice.state' => 1]]);

            print_r(json_encode($clientServices));
        }
    }

    public function getAssistances()
    {
        if (MODULO_CRM) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Clientassistance']);
            $this->autoRender = false;
            $clientId = $_POST['clientId'];

            $clientassistances = $this->Clientassistance->find('all', ['conditions' => ['Clientassistance.client_id' => $clientId, 'Clientassistance.company_id' => MYCOMPANY, 'Clientassistance.state >' => 0], 'order' => ['Assistance.description' => 'asc']]);
            print_r(json_encode($clientassistances));
        }
    }

}
