<?php
App::uses('AppController', 'Controller');

class CatalogsController extends AppController {


	public function index($id) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalog']);

		$conditionsArray = ['Catalog.company_id' => MYCOMPANY,'client_id'=>$id];
		$filterableFields = ['Catalog__description',null,null];

		if($this->request->is('ajax') && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
		}
		
		$this->set('filterableFields',$filterableFields);
		
		$this->paginate = ['contain' => ['Currencies'] , 'recursive' => 1,'conditions' => $conditionsArray ];
		$this->set('catalogs',$this->paginate());
		$this->loadModel('Utilities');
		$this->set('customerName', $this->Utilities->getCustomerName($id));
		$this->set('id',$id);
	}

	public function add($id = null) 
	{
		// il camion viene aggiunto al cliente selezionato
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalogs']);

		if ($this->request->is('post')) {
			$this->Catalogs->create();
				// Il Cliente è quello dove sono entrato
				$this->request->data['Catalogs']['client_id']=$id; 
				// La company_id è quella dell'utente
				$this->request->data['Catalogs']['company_id']=MYCOMPANY;
				
			if ($this->Catalogs->save($this->request->data)) {

				$this->Session->setFlash(__('Listino salvato'), 'custom-flash');
				$this->redirect(array('action' => 'index',$id));
			} else {
				$this->Session->setFlash(__('Il listino non è stata salvato'), 'custom-danger');
			}
		}
		
		$this->loadModel('Utilities');
		$this->set('customerName', $this->Utilities->getCustomerName($id));
		$this->set('currencies', $this->Utilities->getCurrenciesList());
		$this->set('clientId',$id);
	}

	public function edit($id = null,$id2 = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalogs']);

		$this->Catalogs->id = $id;
		if (!$this->Catalogs->exists()) {
			throw new NotFoundException(__('Listino non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Catalogs->save($this->request->data)) {
				$this->Session->setFlash(__('Listino salvato'), 'custom-flash');
				$this->redirect(array('action' => 'index',$id2));
			} else {
				$this->Session->setFlash(__('Il listino non è stato salvato, riprovare'), 'custom-danger');
			}
		} else {
			$this->request->data = $this->Catalogs->read(null, $id);
		}
		
		$this->loadModel('Utilities');
		$this->set('customerName', $this->Utilities->getCustomerName($id2));
		$this->set('currencies', $this->Utilities->getCurrenciesList());
		$this->set('clientId',$id2);
	}

	public function editCatalogStorage($id = null,$id2=null,$idCatalog = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Catalogs','Catalogstorages']);
		$this->Catalogstorages->id = $id;
		$currentCatalog = $this->Catalogs->findById($idCatalog);
		if (!$this->Catalogstorages->exists()) {
			throw new NotFoundException(__('Prodotto a listino non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			if ($this->Catalogstorages->save($this->request->data))
			{
				$this->Session->setFlash(__('Modifiche al prodotto del listino salvate'), 'custom-flash');
				
				$this->redirect(array('action' => 'storagesList', $idCatalog,$currentCatalog['Catalogs']['client_id']));
			} else {
				$this->Session->setFlash(__('Il prodotto a listino non è stato salvato, riprovare'), 'custom-danger');
			}
		} else {
			$this->request->data = $this->Catalogstorages->read(null, $id);
		}
		
		$this->loadModel('Utilities');
		$this->set('storageName', $this->Utilities->getStorageName($id2));
		$this->set('id',$id);
		$this->set('idCatalog',$idCatalog);
		$this->set('currentCatalog',$currentCatalog['Catalogs']['client_id']);
	}

	public function delete($id = null,$id2 = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Catalogs']);

		if (!$this->request->is(['post','get'])) {
			throw new MethodNotAllowedException();
		}
		
		$this->Catalogs->id = $id;
		if (!$this->Catalogs->exists()) {
			throw new NotFoundException(__('Listino non valido'));
		}
		if ($this->Catalogs->delete()) {
			$this->Session->setFlash(__('Listino eliminato'), 'custom-flash');
			$this->redirect(array('action' => 'index',$id2));
		}
		$this->Session->setFlash(__('Il camion non è stata eliminata'), 'custom-danger');
		$this->redirect(array('action' => 'index',$id2));
	}

	public function storagesList($idCatalog,$idClient)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Storages']);
		
		// Aggiorno il listino se non è stato aggiornato
		$this->updateCatalog($idCatalog); 

		$conditionsArray = ['catalog_id'=>$idCatalog,'Storages.state'=>ATTIVO];
		$filterableFields = ['descrizione',null,null,null];

		if($this->request->is('ajax') && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
		}
		
		$this->set('filterableFields',$filterableFields);
			
		$storageList = $this->Catalogstorages->find('all',['conditions'=>$conditionsArray,'order'=>'descrizione']);
		$this->paginate($this->set('storageList',$this->Catalogstorages->find('all',['conditions'=>$conditionsArray,'order'=>'descrizione'])));
		$this->set('id',$idClient);
		$this->set('idCatalog',$idCatalog);
	}
	
	// Aggiornamnto del catalogo ogni volta che si entra in una "gestione catalogo"
	public function updateCatalog($idCatalog)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalogstorages']);
		
		// Recupero l'elenco dei prodotti
		$storages = $this->Storages->find('all',['conditions'=>['company_id'=>MYCOMPANY]]);

		foreach($storages as $storage)
		{
			// Recupero l'elenco dei prodotti 
			$existCatalogStorage = $this->Catalogstorages->find('all',['contain' => [ 'Storages' ], 'conditions'=>['catalog_id'=>$idCatalog, 'storage_id'=>$storage['Storages']['id']]]);	
			if(count($existCatalogStorage) > 0)
			{ /* nothing */ }
			else
			{
				$newCatalogStorage = $this->Catalogstorages->create();
				$newCatalogStorage['Catalogstorages']['storage_id'] =  $storage['Storages']['id'];
				$newCatalogStorage['Catalogstorages']['catalog_id'] = $idCatalog;
				$temp = $this->Catalogstorages->save($newCatalogStorage);
			}
		}
	}
}
