<?php
App::uses('AppController', 'Controller');

class CurrenciesController extends AppController {

	public function index() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Currency','Csv','Graphicsutilities']);

		$conditionsArray = ['Currency.company_id' => MYCOMPANY , 'Currency.state' => ATTIVO];
		$filterableFields = ['description',null,null,null];
		$sortableFields = [['description','Valuta'],[null,'Simbolo'],[null,'Codice'],['#actions']];								 

		if($this->request->is('ajax') && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
		}
		
			// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Currency->find('all',['conditions'=>$conditionsArray,'order' => ['Currency.description' => 'asc']]); 			
			echo 'Valuta;Simbolo;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				echo $xlsRow['Currency']['description']. ';' .$xlsRow['Currency']['symbol']. ';'."\r\n";
			}
		}
		else
		{
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
		}

		$this->paginate = ['conditions' => $conditionsArray];
		$this->set('currencies', $this->paginate());
	}

	public function add() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Currencies']);
		$datasource = $this->Currencies->getDataSource();
		try 
		{
			$datasource->begin();
			if ($this->request->is('post')) 
			{
				$this->Currencies->create();
				$this->request->data['Currencies']['company_id']=MYCOMPANY;
				if (!$this->Currencies->save($this->request->data)) 
				{
					throw new Exception('Errore durante la creazione della nuova valuta.');
				}
				else
				{
					$this->Session->setFlash(__('Valuta salvata'), 'custom-flash');
					$datasource->commit();
					$this->redirect(['action' => 'index']);
				}
			}
		} 
		catch(Exception $e) 
		{
    		$datasource->rollback();
    		$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
		}
	}

	public function edit($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Currencies']);

		$this->Currencies->id = $id;
		if (!$this->Currencies->exists()) 
		{
			throw new NotFoundException(__('Valuta non valida'));
		}
		
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			$datasource = $this->Currencies->getDataSource();
			try 
			{
				$datasource->begin();
				if (!$this->Currencies->save($this->request->data)) 
				{
						throw new Exception('Errore durante la modifica della valuta.');
				}
				else
				{
					$this->Session->setFlash(__('Valuta salvata'), 'custom-flash');
					$datasource->commit();
					$this->redirect(['action' => 'index']);
				}
			} 
			catch(Exception $e) 
			{
	    		$datasource->rollback();
	    		$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
			}
		}
		else 
		{
			$this->request->data = $this->Currencies->read(null, $id);
		}
	}

	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Currencies','Messages']);
        $asg =  ["la","valuta","F"];
		if($this->Currencies->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Currencies->find('first',['conditions'=>['Currencies.id'=>$id,'Currencies.company_id'=>MYCOMPANY]]);
        if ($this->Currencies->hide($currentDeleted['Currencies']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}
}
