<?php

class UsersController extends AppController {

	var $uses=array('User','Company');


    public function beforeFilter()
	{
		parent::beforeFilter();
		//$this->Auth->allow('login'); // Permette il login
		//$this->Auth->allow('registration'); // Permette la registrazione
	}

	public function login()
	{
		$this->layout = 'loginLayout';
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Groups']);
		if ($this->request->is('post'))
		{
			if ($this->Auth->login())
			{
				$userGroup = $this->Auth->user('group_id');
				$company = $this->Auth->user('company_id');
				$typeOfUser = $this->Groups->find('first',['conditions'=>['id'=>$userGroup,'company_id'=>$company]]);
				// Controllo utente loggato
				switch($_SESSION['Auth']['User']['group_id'])
				{
					case '20': // Login Tecnici
						 $this->redirect(['controller'=>'maintenances','action'=>'technicianindex']);
						break;
					default:
						$this->redirect($this->Auth->redirect());
					break;
				}
			}
			else
			{
				$this->Session->setFlash(__('Username o password non validi, riprovare.'), 'custom-danger');
			}
		}
	}
	
	public function registration()
	{
		if ($this->request->is('post'))
		{
		}
	}


	public function logout()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Groups']);
		$userGroup = $this->Auth->user('group_id');
		$company = MYCOMPANY;
		$typeOfUser = $this->Groups->find('first',['conditions'=>['id'=>$userGroup,'company_id'=>$company]]);
		session_destroy();
		$this->redirect($this->Auth->logout());		
		$log=0;
	}

    public function index() 
    {
			$this->paginate = ['conditions' => [ 'User.company_id' => MYCOMPANY ],'order'=>['User.id'=>'desc'],'limit' => 100];
	        $this->User->recursive = 0;
    	    $this->set('users', $this->paginate());
    }

	public function indexExtended() 
    {
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalog']);

		$conditionsArray =[ 'User.company_id' => MYCOMPANY ];
		$filterableFields = ['username',null,null];

		if($this->request->is('ajax') && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
		}
		
		$this->set('filterableFields',$filterableFields);
		$this->paginate = ['conditions' => $conditionsArray ,'order'=>['User.id'=>'desc'],'limit' => 100];
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
        $this->render('index_extended');
    }
    
    public function indexExtendedVfs() 
    {
			$this->paginate = ['conditions' => [ 'User.company_id' => MYCOMPANY ],'order'=>['User.id'=>'desc'],'limit' => 100];
	        $this->User->recursive = 0;
    	    $this->set('users', $this->paginate());
    	    $this->render('index_extended_vfs');
    }

    public function add() {
        if ($this->request->is('post')) {
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this, ['Messages', 'Users']);
			$messageParameter = ["l\'", "utente", "M"];
            $this->User->create();
			$comp2=$this->Company->find('first',['fields'=>['Company.id']]);
			$this->request->data['User']['company_id']=$comp2['Company']['id'];
			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(['action' => 'index']);
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
    }

	public function addExtended()
	{
		if ($this->request->is('post')) {
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this, ['Messages', 'Users']);
			$messageParameter = ["l\'", "utente", "M"];
			$this->User->create();

			$comp2 = $this->Company->find('first', array('fields' => array('Company.id')));
			$this->request->data['User']['company_id'] = MYCOMPANY;


			if ($this->request->data['User']['password'] != $this->request->data['User']['password_confirm']) {
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			} else {
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(array('action' => 'indexExtended'));
				} else {
					$this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
		}

		$this->loadModel('Utilities');
		$this->set('groups', $this->Utilities->getCompanyUserGroup());
		$this->render('add_extended');
	}


	public function addExtendedVfs() {
        if ($this->request->is('post')) {
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this, ['Messages', 'Users']);
			$messageParameter = ["l\'", "utente", "M"];
			$this->User->create();
			$comp2 = $this->Company->find('first', array('fields' => array('Company.id')));
			$this->request->data['User']['company_id'] = MYCOMPANY;


			if ($this->request->data['User']['password'] != $this->request->data['User']['password_confirm']) {
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			} else {
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(array('action' => 'indexExtendedVfs'));
				} else {
					$this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
		}
        
        $this->loadModel('Utilities');
        $this->set('groups',$this->Utilities->getCompanyUserGroup());
        $this->render('add_extended_vfs');
    }


    public function edit($id = null) 
    {
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];
    	// Mi connetto al database globale
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Users']);
    	
        $this->User->id = $id;
        if (!$this->User->exists()) {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1],$messageParameter[2]));
        }
        if ($this->request->is('post') || $this->request->is('put'))
		{
			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				
				$currentUser = $this->User->find('first',['conditions'=>['User.id'=>$id,'User.company_id'=>MYCOMPANY]]);
				
				// Prendo i dati dell'utente collegato
				$currentPassword = $currentUser['User']['password'];
				$currentCompanyId = $currentUser['User']['company_id'];
				$currentUsername = $currentUser['User']['username'];

				// Se il salvataggio funziona correttamente
				if ($currentUser = $this->User->save($this->request->data)) 
				{
				
					// Recupero la nuova password criptata
					$newPassword = $currentUser['User']['password'];
					
					// Connetto al database principale
					$this->Utilities->connectToMainDb();
					
					// Recupero l'utente dal database principale
					$mainUser = $this->Users->find('first',['conditions'=>['password'=>$currentPassword,'usercompany'=>$currentCompanyId, 'username'=>$currentUsername]]);

					// Aggiorno la password sull'utente principale
					$this->Users->updateAll(['password'=>'\''.$newPassword.'\''],['id'=>$mainUser['Users']['id']]);
				
					// Riconnetto al database utilizzo
					$this->Utilities->connectToDb($mainUser['Users']['dbname']);

					$this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(['action' => 'index']);
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
		else
		{
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
    }
    
    
    public function editExtended($id = null) {
        $this->User->id = $id;
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];
        if (!$this->User->exists()) {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1],$messageParameter[2]));
        }
        if ($this->request->is('post') || $this->request->is('put'))
		{
			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(['action' => 'indexExtended']);
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
		else
		{
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
        
        $this->loadModel('Utilities');
        $this->set('groups',$this->Utilities->getCompanyUserGroup());
        $this->render('edit_extended');
        
    }



public function editExtendedVfs($id = null) {
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];
        if (!$this->User->exists()) {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1],$messageParameter[2]));
        }
        if ($this->request->is('post') || $this->request->is('put'))
		{
			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(array('action' => 'indexExtendedVfs'));
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
		else
		{
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
        
        $this->loadModel('Utilities');
        $this->set('groups',$this->Utilities->getCompanyUserGroup());
        $this->render('edit_extended_vfs');
        
    }


    public function delete($id = null) {
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));
        }
        if ($this->User->delete($id, false)) {
			$this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
            $this->redirect(['action' => 'index']);
        }
		$this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
        $this->redirect(['action' => 'index']);
    }

    public function createUserFromTechnician()
	{
		$this->autoRender = false;

		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Utilities','Technician','User']);

		$randomPassword = $this->Utilities->getRandomPassword();
		$currentTechnician = $this->Technician->find('first', ['conditions' => ['id' => $_POST['technicianId']]]);
		$dbName = $_SESSION['Auth']['User']['dbname'];
		$dbUser = $_SESSION['Auth']['User']['dbuser'];
		$dbPassword = $_SESSION['Auth']['User']['dbpassword'];
		$email = $currentTechnician['Technician']['email'];

		$this->Utilities->connectToDb(MAINDB);
		$newMainUser = $this->User->create();
		$newMainUser ['username'] = $email;
		$newMainUser ['password'] = $randomPassword;
		$newMainUser ['group_id'] = 20;
		$newMainUser ['dbname'] = $dbName;
		$newMainUser ['dbpassword'] = $dbPassword;
		$newMainUser ['dbuser'] = $dbUser;
		$newMainUser ['usercompany'] = MYCOMPANY;
		$newMainUser ['tare'] = 0;
		$this->User->Save($newMainUser);

		//$this->Utilities->connectToDb($_SESSION['Auth']['User']['dbname']);
		$this->Utilities->connectToDb($dbName);
		$newUser = $this->User->create();
		$newUser['username'] = $email;
		$newUser['password'] = $randomPassword;
		$newUser['group_id'] = 20;
		$newUser['company_id'] = MYCOMPANY;
		$this->User->Save($newUser);
	}

	public function enableUserFromTechnician()
	{
		$this->autoRender = false;
		$this->User->enableUserFromTechnician();
	}

	public function disableUserFromTechnician()
	{
		$this->autoRender = false;
		$this->User->disableUserFromTechnician();
	}


}
?>
