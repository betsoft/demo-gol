<?php
App::uses('AppController', 'Controller');

class GoodsController extends AppController {

	public function index() {
		$this->Good->recursive = 0;
		$this->set('goods', $this->paginate());
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->Good->create();
			if ($this->Good->save($this->request->data)) {
				$this->Session->setFlash(__('Articolo salvato'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('L\'articolo non è stato salvato, riprovare.'), 'custom-danger');
			}
		}
		$goods = $this->Good->Bill->find('list');
		$ivas = $this->Good->Iva->find('list');
		$this->set(compact('bills', 'ivas'));
	}

	public function edit($id = null) {
		$this->Good->id = $id;
		if (!$this->Good->exists()) {
			throw new NotFoundException(__('Articolo non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Good->save($this->request->data)) {
				$this->Session->setFlash(__('L\'articolo è stato salvato'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('L\'articolo non può essere salvato, riprovare.'), 'custom-danger');
			}
		} else {
			$this->request->data = $this->Good->read(null, $id);
		}
		$goods = $this->Good->Bill->find('list');
		$ivas = $this->Good->Iva->find('list');
		$this->set(compact('bills', 'ivas'));
	}

	// Lasciati volontariamente col vecchio delete
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Good->id = $id;
		if (!$this->Good->exists()) {
			throw new NotFoundException(__('Articolo non valido'));
		}
		if ($this->Good->delete($id, falses)) {
			$this->Session->setFlash(__('Articolo eliminato'), 'custom-flash');
			$this->redirect(['action' => 'index']);
		}
		$this->Session->setFlash(__('Articolo non eliminato'), 'custom-danger');
		$this->redirect(['action' => 'index']);
	}

    public function categorize($billId)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Category','Bill']);
        $conditionsArray = ['Good.company_id'=>MYCOMPANY,'Good.state'=>1, 'Good.bill_id'=>$billId];
        $conditionsArrayforBill = ['Bill.company_id'=>MYCOMPANY,'Bill.state'=>1, 'Bill.id'=>$billId];
        $good = $this->Good->find('all',['conditions'=>$conditionsArray]);
        $currentBill = $this->Bill->find('first',['conditions'=>$conditionsArrayforBill]);
        $this->set('billNumber',$currentBill['Bill']['numero_fattura']);
        $this->set('billDate',date("d-m-Y", strtotime($currentBill['Bill']['date'])));
        $this->set('billType' , $this->Bill->getBillTypeName($currentBill['Bill']['tipologia']));
        $this->set('billReturnAction', $this->Bill->getBillReturnAction(($currentBill['Bill']['tipologia'])));
        $this->set('categories', $this->Category->getList());
        $this->set('goods', $good);
    }


    public function updateCategory()
    {
        if (MODULO_CONTI) {
            $this->autoRender = false;
            $conditionsArray = ['Good.company_id' => MYCOMPANY, 'Good.state' => 1, 'Good.id' => $_POST['goodId']];
            $good = $this->Good->find('first', ['conditions' => $conditionsArray]);
            $good['Good']['category_id'] = $_POST['categoryValue'];
            $this->Good->Save($good);
        }
    }
}
