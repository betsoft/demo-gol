<?php
App::uses('AppController', 'Controller');

class PaypalsController extends AppController 
{
	public function recoverPaypalTransaction() 
	{
		$this->autoRender = false;
		$this->Paypal->login();
	}

}
