String.prototype.pad = function(direction, str, max) {
    if(this.length < max)
        if(direction == 'left')
            return (str + this).pad(direction, str, max);
        else
            return (this + str).pad(direction, str, max);
    return this;
};

String.prototype.trimAll = function() {
    return this.split(" ").join("");
};

String.prototype.extractNumber = function() {
    return parseFloat(this.replace(/[^-\d\.]/g, ''));
};

var uniqueId = {
   counter: 0,
   get: function(prefix) {
       if(!prefix)
           prefix = "uniqueid";
       
       var id =  prefix + "_" + uniqueId.counter++;
       if($("#" + id).length == 0)
           return id;
       else
           return uniqueId.get()
   }
}

function getCaretPosition (field) {

  // Initialize
  var iCaretPos = 0;

  // IE Support
  if (document.selection) {

    // Set focus on the element
    field.focus();

    // To get cursor position, get empty selection range
    var oSel = document.selection.createRange();

    // Move selection start to 0 position
    oSel.moveStart('character', -field.value.length);

    // The caret position is selection length
    iCaretPos = oSel.text.length;
  }

  // Firefox support
  else if (field.selectionStart || field.selectionStart == '0')
    iCaretPos = field.selectionStart;

  // Return results
  return iCaretPos;
}
    
String.prototype.toDate = function(format) {
    var separator = format[1];
    var values = this.split(separator);
    var formats = format.split(separator);

    var year, month, day;
    $.each(values, function(index) {
      var currentFormat = formats[index].toLowerCase();

      if(currentFormat == 'y')
          year = values[index];
      else if(currentFormat == 'm')
          month = values[index];
      else if(currentFormat == 'd')
          day = values[index];
    });
    
    if(year == undefined || month == undefined || day == undefined)
         return new Date('0000', '00', '00');
        
    return new Date(year, month, day);
};

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.truncate = function(length, ellipsis) {
    ellipsis = ellipsis || '...'
    return this.slice(0,length) + (this.length > length ? ellipsis : '');
}

function temporizedRedirect(url, wait) {
    addLoadEvent(function() {
        setTimeout(function() {
           window.location.replace(url);
        }, wait);
    })
}

// Opera 8.0+
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
// Firefox 1.0+
var isFirefox = typeof InstallTrigger !== 'undefined';
// Safari 3.0+ "[object HTMLElementConstructor]" 
var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
// Internet Explorer 6-11
var isIE = /*@cc_on!@*/false || !!document.documentMode;
// Edge 20+
var isEdge = !isIE && !!window.StyleMedia;
// Chrome 1+
var isChrome = !!window.chrome && !!window.chrome.webstore;
// Blink engine detection
var isBlink = (isChrome || isOpera) && !!window.CSS;

function getBrowser() {  
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])) {
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE',version:(tem[1]||'')};
    }   
    if(M[1]==='Chrome') {
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
    }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {
        M.splice(1, 1, tem[1]);
    }
    return {
      name: M[0],
      version: M[1]
    };
}

var test = {
    0 : {
        "a" : 1,
        "b" : {
            0 : 1,
            1 : 3
        }
    },
    1 : "c"
    
}

function findRecursive(object, needle) {
    for (var property in object) {
        if (object.hasOwnProperty(property)) {
            if (typeof object[property] == "object") {
                var path = findRecursive(object[property], needle);
                if($.isArray(path))
                {
                    return path.unshift(property);
                }
            }
            else {
                if(object[property] === needle)
                    return [property];
            }
        }
    }
    return false;
};

$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
    if(jqXHR.status == "403")
        window.location.reload()
});

$.fn.animateRotate = function(angle, duration, easing, complete) {
  var args = $.speed(duration, easing, complete);
  var step = args.step;
  return this.each(function(i, e) {
    args.complete = $.proxy(args.complete, e);
    args.step = function(now) {
      $.style(e, 'transform', 'rotate(' + now + 'deg)');
      if (step) return step.apply(e, arguments);
    };

    $({deg: 0}).animate({deg: angle}, args);
  });
};