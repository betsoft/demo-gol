<?php
App::uses('AppModel', 'Model');

class Userdeposit extends AppModel
{
    public $useTable = 'user_deposit';

    public function hide($id)
    {
        return $this->updateAll(['state' => 0, 'company_id' => MYCOMPANY], ['id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['id' => $id, 'state' => 0]]) != null;
    }

    public function getAll()
    {
        $this->Userdeposit = ClassRegistry::init('Userdeposit');
        return $this->Userdeposit->find('all', ['conditions' => ['Userdeposit.company_id' => MYCOMPANY, 'Userdeposit.state' => ATTIVO]]);
    }
}