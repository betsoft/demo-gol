<?php
App::uses('AppModel', 'Model');

class Versioning extends AppModel 
{
	public $useTable = 'versioning';
	
	public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ]]) != null;
    }
    
    // Non la disabilito anche se è disabilitato arxivar
    public function getArxivarLink()
    {
        // E' sempre diverso da null altrimenti è il db errato
       return $this->find('first',['conditions'=>['description'=>'arxivarlink']])['Versioning']['value'];
    }
	
}
