<?php

App::uses('AppModel', 'Model');

class Messages extends AppModel 
{
    public $article = '';
    public $subject = '';
	public $gender = null;
	public $plural = false;
    
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('messages');
    }
    
    public function __call($method, $parameters)
	{
		if(!method_exists($this, $method) && $method != "find")
		{
			$methodNameComponents = explode('Of', $method);

			if(count($methodNameComponents) > 0)
			{

				if(!$type = $this->find('first',['conditions'=>['type'=>$methodNameComponents[0]]])['Messages'])
					throw new Exception("Method not found");
				
				// if(!$action = $this->find()->where(['action'=>$methodNameComponents[1]])->first())
				if(!$action = $this->find('first',['conditions'=>['action'=>$methodNameComponents[1]]])['Messages'])
					throw new Exception("Method not found");

				$article = $subject = $gender = $plural = null;
				if(isset($parameters[0]))
					$article = $parameters[0];
					
				if(isset($parameters[1]))
					$subject = $parameters[1];
				
				if(isset($parameters[2]))
					$gender = $parameters[2];
					
				if(isset($parameters[3]))
					$plural = $parameters[3];
				
				return $this->_getMessage($action['action'], $type['type'], $article, $subject, $gender, $plural);
			}
		}
	}
    
    private function _getMessage($action, $type, $article = null, $subject = null, $gender = null, $plural = 0)
	{
		$article = $article ? $article : $this->article;
		$subject = $subject ? $subject : $this->subject;
		$gender = $gender ? $gender : $this->gender;
		$plural = $plural ? $plural : $this->plural;

		if(!$message = $this->find('first', ['conditions'=>["action" => strtolower($action),"type" => $type,"gender" => strtolower($gender),	"plural" => $plural]]))
		{
			$message = $this->find('first',['conditions'=>["action" => $action,
					"type" => $type,
					"or" => [
						"gender" => $gender,
						"gender IS NULL",
					],
					"or" => [
						"plural" => $plural,
						"plural IS NULL",
					]]]);
		}	

		$message = $message['Messages']['ita'];
		$message = str_replace('_(art)', strtolower($article), $message);
		$message = str_replace('_(ART)', strtoupper($article), $message);
		$message = str_replace('_(Art)', ucfirst($article), $message);
		$message = str_replace('_(sbj)', strtolower($subject), $message);
		$message = str_replace('_(SBJ)', strtoupper($subject), $message);
		$message = str_replace('_(Sbj)', ucfirst($subject), $message);
		return $message;
	}
}

?>