<?php
App::uses('AppModel', 'Model');


class Transportgood extends AppModel 
{

	public $belongsTo = 
	[
	//	'Bill'  => ['className' => 'Bill','foreignKey' => 'bill_id','conditions' => '','fields' => '','order' => ''],
		'Iva'   => ['className' => 'Iva','foreignKey' => 'iva_id','conditions' => '','fields' => '','order' => ''],
		'Storage' => ['className' => 'Storage','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => ''],
		'Transports' => ['className' => 'Transport','foreignKey' => 'transport_id','conditions' => '','fields' => '','order' => ''],
		'Units' => ['className' => 'Units','foreignKey' => 'unita','conditions' => '','fields' => '','order' => ''],
	];
	
	public $hasMany = [];

	public function hide($id)
    {
        return $this->updateAll(['Transportgood.state' => 0,'Transportgood.company_id'=>MYCOMPANY],['Transportgood.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Transportgood.id'=>$id, 'Transportgood.state' =>0 ]]) != null;
    }

}

