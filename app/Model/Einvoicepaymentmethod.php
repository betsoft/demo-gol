<?php

App::uses('AppModel', 'Model');

class Einvoicepaymentmethod extends AppModel 
{
	public $useTable = 'einvoice_payment_method';
	
	
	public function findEPaymentMethodId($code)
	{
	    return $this->find('first',['fields'=>'id','conditions'=>['code'=>$code]])['Einvoicepaymentmethod']['id'];
	}

}

?>
