<?php
App::uses('AppModel', 'Model');

class Quotegoodrow extends AppModel {

public $useTable = 'quotes_goods_rows';


	public $belongsTo = 
	[ 
		'Iva' => ['className' => 'Iva','foreignKey' => 'quote_good_row_vat_id','conditions' => '','fields' => '','order' => ''],
		'Units' => ['className' => 'Units','foreignKey' => 'unit_of_measure_id','conditions' => '','fields' => '','order' => ''],
		'Storage' => ['className' => 'Storage','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => '']
	];
	
}
