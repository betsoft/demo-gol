<?php
App::uses('AppModel', 'Model');

class Category extends AppModel
{
    public $useTable = 'categories';

    public $validate =
        [
            "description" =>
                [
                    "rule" => ["isUnique", ["description", "company_id", "state"], false],
                    "message" => "Il campo descrizione deve essere univoco."
                ]
        ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0, 'company_id' => MYCOMPANY], ['id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['id' => $id, 'state' => 0]]) != null;
    }

    public function getList()
    {
        $this->Category = ClassRegistry::init('Category');
        $conditionArray = ['Category.company_id' => MYCOMPANY, 'Category.state' => ATTIVO];
        $fieldsArray = ['Category.description'];
        $orderArray = ['Category.description' => 'asc'];
        return $this->Category->find('list', ['conditions' => $conditionArray, 'fields' => $fieldsArray, 'order' => $orderArray]);
    }
}
