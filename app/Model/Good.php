<?php
App::uses('AppModel', 'Model');

class Good extends AppModel 
{

	public $useTable = 'goods';
	
	public $belongsTo = 
	[
		'Bill'  => ['className' => 'Bill','foreignKey' => 'bill_id','conditions' => '','fields' => '','order' => ''],
		'Iva'   => ['className' => 'Iva','foreignKey' => 'iva_id','conditions' => '','fields' => '','order' => ''],
		'Storage' => ['className' => 'Storage','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => ''],
		'Transports' => ['className' => 'Transport','foreignKey' => 'transport_id','conditions' => '','fields' => '','order' => ''],
		'Units' => ['className' => 'Units','foreignKey' => 'unita','conditions' => '','fields' => '','order' => ''],
        'Category'   => ['className' => 'Category','foreignKey' => 'category_id','conditions' => '','fields' => '','order' => ''],
	];
	
	public $hasMany = 
	[
		'Goodgestionaldata' => ['className' => 'Goodgestionaldata','foreignKey' => 'good_id','dependent' => true,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => ''],
	];

	public function hide($id)
    {
        return $this->updateAll(['Good.state' => 0,'Good.company_id'=>MYCOMPANY],['Good.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Good.id'=>$id, 'Good.state' =>0 ]]) != null;
    }

}
