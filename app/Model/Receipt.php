<?php
App::uses('AppModel', 'Model');

class Receipt extends AppModel
{

    public $useTable = 'bills';

    public function getNextReceiptNumber($date, $retailStore = null)
    {
        // Scontrino di default è Tipologia [7]
        $this->Bills = ClassRegistry::init('Bills');
        $fields = ['MAX(numero_fattura * 1) as maxFattura'];


        if ($retailStore == null) {
            $conditionArray = ['company_id' => MYCOMPANY, 'state' => ATTIVO, 'date = ' => date("Y-m-d", strtotime($date)), 'tipologia' => '7'];
            $fatture = $this->Bills->find('first', array('fields' => $fields, 'conditions' => $conditionArray, 'order' => 'id desc'));
            if ($fatture[0]['maxFattura'] == null) {
                return 1;
            } else {
                return $fatture[0]['maxFattura'] + 1;
            }
        } else {
            $conditionArray = ['company_id' => MYCOMPANY, 'state' => ATTIVO, 'date = ' => date("Y-m-d", strtotime($date)), 'tipologia' => '7', 'user_id' => $retailStore];
            $fatture = $this->Bills->find('first', array('fields' => $fields, 'conditions' => $conditionArray, 'order' => 'id desc'));
            if ($fatture[0]['maxFattura'] == null) {
                return 1;
            } else {
                return $fatture[0]['maxFattura'] + 1;
            }
        }
    }


    //public function createReceiptFromCashRegister($receiptNumber,$cf = null,$date,$paymentMethod = null,$goods)
    public function createReceiptFromCashRegister($cf = null, $paymentMethod = -1, $goods)
    {



        $this->Bill = ClassRegistry::init('Bill');
        $this->Currencies = ClassRegistry::init('Currencies');
        $this->Utilities = ClassRegistry::init('Utilities');
        $this->Goods = ClassRegistry::init('Goods');
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Receipt = ClassRegistry::init('Receipt');
        $this->User = ClassRegistry::init('User');


        $newBill = $this->Bill->create();
        $newBill['Bill']['numero_fattura'] = $this->Receipt->getNextReceiptNumber(date("Y-m-d"), $this->User->getMyId());

        if ($cf != null) {
            // todo se è stato segnato il codice fiscale posso vedere chi è il cliente
        } else {
            $newBill['Bill']['client_id'] = '10000'; // todo a settaggio il cliente per gli scontrini
        }

        $newBill['Bill']['date'] = date("Y-m-d");
        $newBill['Bill']['importo'] = '1.000.000'; // todo guardare gli articoli
        $newBill['Bill']['company_id'] = MYCOMPANY;

        $newBill['Bill']['iva_id'] = -1; // todo
        $newBill['Bill']['mailsent'] = 0;
        $newBill['Bill']['state'] = 1;
        $newBill['Bill']['tipologia'] = 7;

        $newBill['Bill']['payment_id'] = $paymentMethod; // todo
        $newBill['Bill']['user_id'] = $this->User->getMyId();
        $newBill['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode('EUR'); // to do

        $newBill = $this->Bill->save($newBill);

        foreach ($goods as $good) {
            $vatPercentage = intval($good['vat'] / $good['price'] * 100);
            $vatId = $this->Utilities->getVatIdFromPercentage($vatPercentage);

            $newGood = $this->Goods->create();

            $newGood['Good']['bill_id'] = $newBill['Bill']['id'];

            $newGood['Good']['oggetto'] = $good['description'];
            $newGood['Good']['quantita'] = $good['quantity'];
            $newGood['Good']['prezzo'] = $good['price'];
            $newGood['Good']['iva_id'] = $good['vat'];
            $newGood['Good']['storage_id'] = $good['storage_id'];


            $newGood['Good']['codice'] = ''; // todo
            $newGood['Good']['customdescription'] = '';
            $newGood['Good']['company_id'] = MYCOMPANY;
            $newGood['Good']['vat'] = $good['price'] / (100 + str_replace('%', '', $good['vatvalue'])) * 100;

            $newGood['Good']['tipo'] = 1;
            $newGood['Good']['movable'] = $good['movable'];


            $newGood['Good']['unita'] = null; // todo
            $newGood['Good']['discount'] = null; // todo

            $newGood['Good']['state'] = 1;
            $newGood['Good']['bill_id'] = $newBill['Bill']['id'];

            debug($newGood['Good']);

            if ($newdGood = $this->Goods->Save($newGood)) {
                $lastBuyPrice = "TODO"; // todo
                $depositId = "TODO"; // todo
                $variatonId = "TODO"; // todo
                if ($good['price'] < 0)
                    //$this->Storagemovement->storageLoad($good['storage_id'], 0, $good['description'], $lastBuyPrice, $depositId, $movementNumber = 1, $movementTag = 'CH', $variatonId, 0, null, date("Y-m-d"));
                    $this->Storagemovement->returned($good['storage_id'], $good['quantity'],$good['description'],null,$depositId,date("Y-m-d"));
                else
                    $this->Storagemovement->storageUnload($good['storage_id'], $good['quantity'], $good['description'], $lastBuyPrice, $depositId, $movementNumber = 1, $movementTag = 'CH', $variatonId, 0, null, null);
            }

        }
    }

}