<?php
App::uses('AppModel', 'Model');

class Maintenancehoursoutsideoperator extends AppModel {

    public $useTable = 'maintenance_hours_outsideoperator';

    public function hide($id)
    {
        return $this->updateAll(['Maintenancehoursoutsideoperator.state' => 0, 'Maintenancehoursoutsideoperator.company_id' => MYCOMPANY],['Maintenancehoursoutsideoperator.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Maintenancehoursoutsideoperator.id'=>$id, 'Maintenancehoursoutsideoperator.state' =>0, 'Maintenancehoursoutsideoperator.company_id' => MYCOMPANY]]) != null;
    }

    public $hasMany =
        [
            // 'Outsideoperator' => ['className' => 'Outsideoperator', 'foreignKey' => 'operator_id', 'conditions' => '', 'fields' => '', 'order' => '']
        ];

    public $belongsTo =
        [
            'Maintenancehour' => ['className' => 'Maintenancehour','foreignKey' => 'maintenance_hours_id','conditions' => '','fields' => '','order' => ''],
            'Outsideoperator' => ['className' => 'Outsideoperator', 'foreignKey' => 'operator_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        ];

}
