<?php
App::uses('AppModel', 'Model');

class Paymentdeadlines extends AppModel {

	  public $useTable = 'payments_deadlines';
	  
	  public $belongsTo = ['Payments' => ['className' => 'Payments','foreignKey' => 'payment_id','conditions' => '','fields' => '','order' => ''],];
}
