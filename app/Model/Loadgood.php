<?php
App::uses('AppModel', 'Model');

class Loadgood extends AppModel {

public $useTable = 'load_goods';

	public $belongsTo =
	[
		'Supplier' => ['className' => 'Supplier','foreignKey' => 'supplier_id','conditions' => '','fields' => '','order' => ''],
		'Deposit' => ['className' => 'Deposit','foreignKey' => 'deposit_id','conditions' => '','fields' => '','order' => ''],
	];

	public $hasMany =
	[
		'Loadgoodrow' => ['className' => 'Loadgoodrow','foreignKey' => 'load_good_id','conditions' => '','fields' => '','order' => '']
	];


	public function hide($id)
    {
        return $this->updateAll(['Loadgood.state' => 0,'Loadgood.company_id'=>MYCOMPANY],['Loadgood.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Loadgood.id'=>$id, 'Loadgood.state' =>0 ]]) != null;
    }

    public function createLoadgoodFromPurchaseOrder($purchaseOrderId)
    {
        if (MODULO_CANTIERI) {
            $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
            $currentOrder = $this->Purchaseorder->getPurchaseorder($purchaseOrderId);
            $loadGoodHeader = $this->createLoadGoodHeaderFromPurchaesOrder($currentOrder['Purchaseorder']['supplier_id'], $currentOrder['Purchaseorder']['id']);
            foreach ($currentOrder['Purchaseorderrow'] as $row) {
                $this->createLoadgoodRow($row, $loadGoodHeader['Loadgood']['id']);
            }
            $this->setLoadgoodcreated($currentOrder);
            return $loadGoodHeader['Loadgood']['id'];
        }
    }

    public function createLoadGoodHeaderFromPurchaesOrder($supplierId, $purchaseOrderId)
    {
        $this->Utilities = ClassRegistry::init('Utilities');
        $newLoadGood = $this->create();
        $newLoadGood['Loadgood']['load_good_number'] = $this->Utilities->getNextLoadgoodNumber(date("Y"));
        $newLoadGood['Loadgood']['load_good_date'] = date("Y-m-d");
        $newLoadGood['Loadgood']['supplier_id'] = $supplierId;
        $newLoadGood['Loadgood']['company_id'] = MYCOMPANY;
        $newLoadGood['Loadgood']['state'] = 1;
        $newLoadGood['Loadgood']['purchaseorder_id'] = $purchaseOrderId;
        return $this->save($newLoadGood);
    }

    public function createLoadgoodrow($PurchaseOrderRow, $loadGoodId)
    {
        $this->Loadgoodrow = ClassRegistry::init('Loadgoodrow');
        $newLoadGoodRow = $this->Loadgoodrow->create();
        $newLoadGoodRow['Loadgoodrow']['load_good_id'] = $loadGoodId;
        $newLoadGoodRow['Loadgoodrow']['description'] = $PurchaseOrderRow['description'];
        $newLoadGoodRow['Loadgoodrow']['unit_of_measure_id'] = $PurchaseOrderRow['unit_of_measure_id'];
        $newLoadGoodRow['Loadgoodrow']['load_good_row_price'] = $PurchaseOrderRow['price'];
        $newLoadGoodRow['Loadgoodrow']['load_good_row_vat_id'] = $PurchaseOrderRow['vat_id'];
        $newLoadGoodRow['Loadgoodrow']['storage_id'] = $PurchaseOrderRow['storage_id'];
        $newLoadGoodRow['Loadgoodrow']['article_type'] = $PurchaseOrderRow['article_type'];
        $newLoadGoodRow['Loadgoodrow']['quantity'] = $PurchaseOrderRow['quantity'];
        $newLoadGoodRow['Loadgoodrow']['state'] = 1;
        $newLoadGoodRow['Loadgoodrow']['company_id'] = MYCOMPANY;
        $this->Loadgoodrow->save($newLoadGoodRow);
    }

    public function setLoadgoodCreated($currentOrder)
    {
        $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
        $currentOrder['Purchaseorder']['loadgoodcreated'] = 1;
        $this->Purchaseorder->save($currentOrder);
    }

}
