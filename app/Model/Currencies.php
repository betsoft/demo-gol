<?php
App::uses('AppModel', 'Model');

class Currencies extends AppModel 
{
	public $useTable = 'currencies';
	
	public $validate = 
	[
		"description"=>
		[
            "rule"=>["isUnique", ["description", "company_id","state"], false], 
            "message"=>"Il campo descrizione deve essere univoco." 
        ],
        "isocode"=>
		[
            "rule"=>["isUnique", ["description", "company_id","state"], false], 
            "message"=>"Il campo Codice iso essere univoco." 
        ]
    ];
	
	public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ]]) != null;
    }
    
    public function GetCurrencyName($id)
    {
        $clientCurrency = $this->find('first',['conditions'=>['Currencies.id'=>$id,'Currencies.company_id' => MYCOMPANY]]);
        if(isset($clientCurrency['Currencies']['isocode']))
        {
            return $clientCurrency['Currencies']['isocode'];
        }
        else
        {
            return '';
        }
    }
    
    public function GetCurrencySymbol($id)
    {
            $clientCurrency = $this->find('first',['conditions'=>['Currencies.id'=>$id,'Currencies.company_id' => MYCOMPANY]]);
            if(isset($clientCurrency['Currencies']['symbol']))
            {
                return $clientCurrency['Currencies']['symbol'];
            }
            else
            {
                return '';
            }
    }


    public function GetCurrencyIdFromCode($isocode)
    {
            $currencyId = $this->find('first',['conditions'=>['Currencies.isocode'=>$isocode,'Currencies.company_id' => MYCOMPANY]]);
            if(isset($currencyId['Currencies']['id']))
            {
                return $currencyId['Currencies']['id'];
            }
            else
            {
                return '';
            }
    }

    
    public function GetLastCurrencyChange($currencyId)
    {
        $this->Exchanges = ClassRegistry::init('Exchanges');
        $exchangeCurrencyValue =  $this->Exchanges->find('first',['conditions'=>['Exchanges.currency_id'=>$currencyId,'Exchanges.company_id' => MYCOMPANY,'Exchanges.state'=>ATTIVO], 'order'=>'Exchanges.id desc']);
        if(isset($exchangeCurrencyValue['Exchanges']['rate']))
        {
            return $exchangeCurrencyValue['Exchanges']['rate'];
        }
        else
        {
            return '';
        }
        
    }

}
