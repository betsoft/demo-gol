<?php
App::uses('AppModel', 'Model');

class Storagemovement extends AppModel
{
    public $useTable = 'storage_movements';

    public $belongsTo =
        [
            'Deposit' => ['className' => 'Deposit', 'foreignKey' => 'deposit_id', 'conditions' => '', 'fields' => '', 'order' => ''],
            'Storage' => ['className' => 'Storage', 'foreignKey' => 'storage_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        ];

    public function removeTransportMovement($transportId, $storageId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        // $conditionsArray = ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => 1, 'Storagemovement.transport_id' => $transportId, 'Storagemovement.storage_id' => $storageId];
        $conditionsArray = ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => 1, 'Storagemovement.transport_id' => $transportId];
        $this->Storagemovement->updateAll(['state' => '0'], $conditionsArray);
    }

    public function removeReserveMovement($reservationId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => 1, 'Storagemovement.reservation_id' => $reservationId];

        $this->Storagemovement->updateAll(['state' => '0'], $conditionsArray);
    }

    public function removeQuoteMovement($quoteId, $storageId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => 1, 'Storagemovement.quote_id' => $quoteId, 'Storagemovement.storage_id' => $storageId];
        $this->Storagemovement->updateAll(['state' => '0'], $conditionsArray);
    }

    public function removeBillMovement($billId, $storageId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => 1, 'Storagemovement.bill_id' => $billId, 'Storagemovement.storage_id' => $storageId];
        $this->Storagemovement->updateAll(['state' => '0'], $conditionsArray);
    }

    public function removeOrder($orderId, $storageId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => 1, 'Storagemovement.order_id' => $orderId, 'Storagemovement.storage_id' => $storageId];
        $this->Storagemovement->$deletedTransport(['state' => '0'], $conditionsArray);
    }


    public function storageLoad($storageId, $quantity, $description, $lastBuyPrice = 0, $depositId = -1, $movementNumber = 1, $movementTag = '', $variatonId = null, $transferQuantity = 0, $documentId = null,$documentDate = null)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Storage = ClassRegistry::init('Storage');

        // Fix magico
        $lastBuyPrice < 0 ? $price = 0: $price = $lastBuyPrice;

        // Creo il movimento di carico di magazzino
        switch ($movementTag) {
            case 'TR': // Transport
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $price, $depositId, $movementTag, $variatonId, 0, 0, $documentId, null, null, null, null, null, null, $documentDate);
                break;
            case 'MA': // Maintenance
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, null, $documentId, $documentDate);
                break;
            case 'CH': // CashRecipt
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                break;
            case 'BI': // Bill
            case 'BI_ED': // Edit bill
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                break;
            case 'IN':  // Inventario rapido
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'I', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                break;
            /*case 'NEW_SER_LG':  // Seriali da entrata merce
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, $documentId, null, $documentDate);
                break;*/
            case 'LG_AD': // Entrata merce
            case 'LG_ED': // Edit entrata merce
            case 'LE_AU_AD': // Auto create
            case 'NEW_SER_LG':  // Seriali da entrata merce
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, $documentId, null, $documentDate);
                break;
            default:
                $this->Storagemovement->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, null, null, $documentDate);
                break;

                // Aggiorno ultimo prezzo di acquisto su tabella storage
                if ($lastBuyPrice > 0)
                    $this->Storage->updateStorageLastPrice($storageId, $lastBuyPrice);
        }
    }

    // Rimozione articoli di magazzino
    public function storageUnload($storageId, $quantity, $description, $lastBuyPrice = 0, $depositId = -1, $movementNumber = 1, $movementTag = '', $variatonId = null, $transferQuantity = 0, $documentId = null,$documentDate = null)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Bundlestorage = ClassRegistry::init('Bundlestorage');

        // Fix magico
        $lastBuyPrice < 0 ? $price = 0 : $price = $lastBuyPrice;

        // Non cancellare momentaneamente
        //if (($this->Storage->getAvailableQuantity($storageId, $depositId) - $quantity) < 0) // Controllo magazzino unico
        // {
        //   throw new Exception('Attenzione la quantità che si sta cercando di scaricare è superiore a quella presente in magazzino. Prima di proseguire caricare il magazzino. Quantità presente a magazzino: ' . number_format($this->getAvailableQuantity($storageId, -1),2) . ' Si è cercata di scaricare una quantità pari a : '. number_format($quantity,2));
        //}
        $isBundle = 0;

        $storage = $this->Storage->findById($storageId);
        if($storage['Storage']['isbundle'] == 1){
            $isBundle = 1;
            $storages = $this->Bundlestorage->find('list', ['conditions' => ['Bundlestorage.bundle_id' => $storageId, 'Bundlestorage.company_id' => MYCOMPANY, 'Bundlestorage.state >' => 0], 'fields' => ['Bundlestorage.storage_id']]);
        }

        if($isBundle == 1){
            $description = $description . ' - bundle ' . $storage['Storage']['descrizione'];
            // Creo il movimento di carico di magazzino (per articoli non presenti - virtuale)
            switch ($movementTag) {
                case 'TR': // Transport
                case 'TR_ED':
                    $this->Storagemovement->CreateMovement($storageId, 0, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag . '_BU', $variatonId, 0, 0, $documentId, null, null, null, null, null, null, $documentDate);
                    break;
                case 'MA': // Maintenance
                case 'MA_ED': // Maintenance
                    $this->Storagemovement->CreateMovement($storageId, 0, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag . '_BU', $variatonId, 0, 0, null, null, null, null, null, null, $documentId, $documentDate);
                    break;
                case 'BI': // Bill
                case 'BI_ED': // Edit bill
                    $this->Storagemovement->CreateMovement($storageId, 0, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag . '_BU', $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                    break;
                case 'CH': // CashRecipt
                    $this->Storagemovement->CreateMovement($storageId, 0, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag . '_BU', $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                    break;
                case 'IN': // Inventario rapido
                    $this->Storagemovement->CreateMovement($storageId, 0, $description, $transferQuantity, $movementNumber, 'I', $price, $depositId, $movementTag . '_BU', $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                    break;
                case 'LG_AD': // Entrata merce
                case 'LG_ED': // Edit entrata merce
                case 'LE_AU_AD': // Auto create
                case 'NEW_SER_LG':  // Seriali da entrata merce
                    $this->Storagemovement->CreateMovement($storageId, 0, $description, $transferQuantity, $movementNumber, 'U', $price, $depositId, $movementTag . '_BU', $variatonId, 0, 0, null, null, null, null, null, $documentId, null, $documentDate);
                    break;
                default:
                    $this->Storagemovement->CreateMovement($storageId, 0, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag . '_BU', $variatonId, 0, 0, null, null, null, null, null, null, null, $documentDate);
                    break;
            }

            // Scarica a magazzino le quantità
            $this->Storagemovement->setTransferredQuantity($storageId, 0, $depositId, $movementTag . '_BU', $lastBuyPrice, $documentId);

            foreach ($storages as $storageBundle){
                // Creo il movimento di carico di magazzino (per articoli non presenti - virtuale)
                switch ($movementTag) {
                    case 'TR': // Transport
                    case 'TR_ED':
                        $this->Storagemovement->CreateMovement($storageBundle, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', 0, $depositId, $movementTag, $variatonId, 0, 0, $documentId, null, null, null, null, null, null, $documentDate);
                        break;
                    case 'MA': // Maintenance
                    case 'MA_ED': // Maintenance
                        $this->Storagemovement->CreateMovement($storageBundle, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', 0, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, null, $documentId, $documentDate);
                        break;
                    case 'BI': // Bill
                    case 'BI_ED': // Edit bill
                        $this->Storagemovement->CreateMovement($storageBundle, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', 0, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                        break;
                    case 'CH': // CashRecipt
                        $this->Storagemovement->CreateMovement($storageBundle, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', 0, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                        break;
                    case 'IN': // Inventario rapido
                        $this->Storagemovement->CreateMovement($storageBundle, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'I', 0, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                        break;
                    case 'LG_AD': // Entrata merce
                    case 'LG_ED': // Edit entrata merce
                    case 'LE_AU_AD': // Auto create
                    case 'NEW_SER_LG':  // Seriali da entrata merce
                        $this->Storagemovement->CreateMovement($storageBundle, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', 0, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, $documentId, null, $documentDate);
                        break;
                    default:
                        $this->Storagemovement->CreateMovement($storageBundle, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', 0, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, null, null, $documentDate);
                        break;
                }

                // Scarica a magazzino le quantità
                $this->Storagemovement->setTransferredQuantity($storageBundle, $quantity, $depositId, $movementTag, 0, $documentId);
            }

        } else {
            // Creo il movimento di carico di magazzino (per articoli non presenti - virtuale)
            switch ($movementTag) {
                case 'TR': // Transport
                case 'TR_ED':
                    $this->Storagemovement->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag, $variatonId, 0, 0, $documentId, null, null, null, null, null, null, $documentDate);
                    break;
                case 'MA': // Maintenance
                case 'MA_ED': // Maintenance
                    $this->Storagemovement->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, null, $documentId, $documentDate);
                    break;
                case 'BI': // Bill
                case 'BI_ED': // Edit bill
                    $this->Storagemovement->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                    break;
                case 'CH': // CashRecipt
                    $this->Storagemovement->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                    break;
                case 'IN': // Inventario rapido
                    $this->Storagemovement->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'I', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, $documentId, null, null, null, $documentDate);
                    break;
                case 'LG_AD': // Entrata merce
                case 'LG_ED': // Edit entrata merce
                case 'LE_AU_AD': // Auto create
                case 'NEW_SER_LG':  // Seriali da entrata merce
                    $this->Storagemovement->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', $price, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, $documentId, null, $documentDate);
                    break;
                default:
                    $this->Storagemovement->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag, $variatonId, 0, 0, null, null, null, null, null, null, null, $documentDate);
                    break;
            }

            // Scarica a magazzino le quantità
            $this->Storagemovement->setTransferredQuantity($storageId, $quantity, $depositId, $movementTag, $lastBuyPrice, $documentId);
        }

    }


    public function CreateMovement($storageId, $quantity = 0, $description = '', $transferedQuantity = 0, $movementNumber = 0, $typeOfGoodMovement, $purchasePrice = 0, $depositId = -1, $movementTag = null, $variationId = null
        , $reserved = 0, $ordered = 0, $transportId = null,$reservationId = null,$quoteId = null,$billId = null, $purchaseOrderId = null, $loadGoodId = null , $maintenanceId = null, $movementDate = null,$returned=null,$orderQuantityArrived = null,$reservedQuantitySold = null)
    {

        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Storage = ClassRegistry::init('Storage');

        // Bypassa l'errore di un passaggio di quantità nulla
        $quantity == null ? $quantity = 0 : null;

        if ($depositId == '') {
            $depositId = -1;
        }

        // Controllo che sia un articolo movimentabile, se non lo è non creare il movimento.
        if ($this->Storage->isStorageMovable($storageId)) {

            $newStorageMovement = $this->Storagemovement->create();
            $newStorageMovement['Storagemovement']['movement_tag'] = $movementTag;
            isset($typeOfGoodMovement) ? $newStorageMovement['Storagemovement']['type_of_good_movement'] = $typeOfGoodMovement : $newStorageMovement['Storagemovement']['type_of_good_movement'] = ''; // Tipo di movimento L (load) U (Unload)
            $newStorageMovement['Storagemovement']['deposit_id'] = $depositId; // id del deposito ( non gestito ) // -1
            $newStorageMovement['Storagemovement']['storage_id'] = $storageId; // Id dell'articolo a magazzino
            $newStorageMovement['Storagemovement']['purchase_price'] = $purchasePrice; // 0 di default (non gestito)
            $newStorageMovement['Storagemovement']['quantity'] = $quantity;
            $newStorageMovement['Storagemovement']['description'] = $description;
            $newStorageMovement['Storagemovement']['transfered_quantity'] = $transferedQuantity;
            $newStorageMovement['Storagemovement']['movement_number'] = $movementNumber;
            $newStorageMovement['Storagemovement']['company_id'] = MYCOMPANY;
            $newStorageMovement['Storagemovement']['variation_id'] = $variationId;
            $newStorageMovement['Storagemovement']['ordered_quantity'] = $ordered;
            $newStorageMovement['Storagemovement']['reserved_quantity'] = $reserved;
            $newStorageMovement['Storagemovement']['transport_id'] = $transportId;
            $newStorageMovement['Storagemovement']['reservation_id'] = $reservationId;
            $newStorageMovement['Storagemovement']['quote_id'] = $quoteId;
            $newStorageMovement['Storagemovement']['bill_id'] = $billId;
            $newStorageMovement['Storagemovement']['order_id'] = $purchaseOrderId;
            $newStorageMovement['Storagemovement']['loadgood_id'] = $loadGoodId;
            $newStorageMovement['Storagemovement']['maintenance_id'] = $maintenanceId;
            $newStorageMovement['Storagemovement']['date'] = $movementDate == null ? date("Y-m-d") : $movementDate;
            $newStorageMovement['Storagemovement']['returned_quantity'] = $returned;
            $newStorageMovement['Storagemovement']['order_quantity_arrived'] = $orderQuantityArrived;
            $newStorageMovement['Storagemovement']['reserved_quantity_sold'] = $reservedQuantitySold;
            $this->Storagemovement->save($newStorageMovement);
        }
    }


    // Scarica dai carichi le voci di magazzino
    public function setTransferredQuantity($storageId, $quantity, $depositId, $movementTag = 'notset', $storagemovementprice = null, $transportId = null)
    {
        return
            $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Storage = ClassRegistry::init('Storage');
        $depositId == 'all' ? $depositCondition = '1 = 1' : $depositCondition = 'deposit_id = ' . $depositId;

        $methodValue = $this->Storage->getStorageEvaluationMethod();
        if ($movementTag == 'LG_ED' || $movementTag == 'LG_DE') {
            $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => ['Storagemovement.company_id' => MYCOMPANY, $depositCondition, 'Storagemovement.quantity > Storagemovement.transfered_quantity', 'Storagemovement.storage_id' => $storageId, 'Storagemovement.purchase_price' => $storagemovementprice, 'Storagemovement.state' => 1], 'order' => ['Storagemovement.id desc']]);
        } else {
            // Vado a controllare qual è la tipologia da aggiornare

            if ($quantity > 0) {
                $conditionArray = ['Storagemovement.company_id' => MYCOMPANY, $depositCondition, 'Storagemovement.quantity > Storagemovement.transfered_quantity', 'Storagemovement.storage_id' => $storageId, 'Storagemovement.state' => 1];
                switch ($methodValue) {
                    case 'CMP' :
                        $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => $conditionArray, 'order' => ['Storagemovement.id asc']]);
                        break;
                    case 'LIFO':
                        $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => $conditionArray, 'order' => ['Storagemovement.id desc']]);
                        break;
                    case 'FIFO':
                        $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => $conditionArray, 'order' => ['Storagemovement.id asc']]);
                        break;
                }
            }

            if ($quantity < 0) {
                // Se devo effettuare uno storno
                $conditionArray = ['Storagemovement.company_id' => MYCOMPANY, $depositCondition, 'Storagemovement.quantity >= ' => 0, 'Storagemovement.quantity <= Storagemovement.transfered_quantity', 'Storagemovement.storage_id' => $storageId, 'Storagemovement.state' => 1];
                switch ($methodValue) {
                    case 'CMP' :
                        $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => $conditionArray, 'order' => ['Storagemovement.id asc']]);
                        break;
                    case 'LIFO':
                        $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => $conditionArray, 'order' => ['Storagemovement.id desc']]);
                        break;
                    case 'FIFO':
                        $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => $conditionArray, 'order' => ['Storagemovement.id asc']]);
                        break;
                }
            }
        }




        // Se non viene trovata la riga da aggiornare - quindi si stanno scaricando merci non presenti a magazzino

        if ($rowToUpdate == null) {

            /** ERRORESTORAGE **/
            // Non faccio niente
            switch ($movementTag) {
                case 'LG_ED': // Controllare TO DO
                    $movementTag = 'LG_ED_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Scarico per modifica entrata merce quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                case 'LG_DE': // Controllare TO DO
                    $movementTag = 'LG_DE_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Scarico per eliminazione entrata merce quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                case 'TR_AD':
                    $movementTag = 'TR_AD_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Carico per creazione bolla quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                case 'TR_ED':
                    $movementTag = 'TR_ED_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Carico per modifica bolla quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                // Eliminazione bolla  no perché carico
                case 'BI_AD':
                    $movementTag = 'BI_AD_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Carico per creazione fattura accomapagnatoria quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                case 'BI_ED':
                    $movementTag = 'BI_ED_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Carico per modifica fattura accomapagnatoria  quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                case 'SC_AD':
                    $movementTag = 'SC_AD_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Carico per creazione scontrino quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                case 'SC_ED':
                    $movementTag = 'SC_ED_MANC';
                    $this->Storagemovement->storageLoad($storageId, 0, 'Carico per modifica scontrino  quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity, $transportId);
                    break;
                // Eliminazione fattura accompagnatoria no perché carico
            }
        } else {

            //debug($quantity);

            // Sto stornando
            if ($quantity < 0) {
                //   debug("Quantity < 0");

                // se la riga di aggiornare
                if ($rowToUpdate['Storagemovement']['quantity'] == 0) {
                    //       debug("Quantity Null");
                    //       debug($rowToUpdate['Storagemovement']['quantity']);
                    //       debug($rowToUpdate['Storagemovement']['transfered_quantity']);
                    if ($rowToUpdate['Storagemovement']['transfered_quantity'] > -1 * $quantity) {
                        //            debug("B");
                        //            debug(-1 * $quantity);
                        // Rimuovo la quantità stornata
                        $this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => $rowToUpdate['Storagemovement']['transfered_quantity'] + $quantity], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
                        $quantity = 0;
                        //           debug($quantity);

                    } else {
                        // Rimuovo l'intera riga
                        //          debug($quantity);
                        //          debug("C");

                        $this->Storagemovement->UpdateAll(['Storagemovement.state' => 0], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
                        $quantity = $quantity + $rowToUpdate['Storagemovement']['transfered_quantity'];
                        //           debug($quantity);
                        if ($quantity < 0) {
                            $this->Storagemovement->setTransferredQuantity($storageId, $quantity, $depositId, $movementTag, $storagemovementprice, $transportId);
                        }
                    }
                } else {
                    // Se ho più quantità di quella da togliere
                    if ($rowToUpdate['Storagemovement']['transfered_quantity'] > -1 * $quantity) {
                        $this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => $rowToUpdate['Storagemovement']['transfered_quantity'] + $quantity], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
                        //             debug(">>>1");
                        $quantity = 0;
                    } else {
                        // Metto quantità a zero
                        $this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => 0], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
                        $quantity = $quantity + $rowToUpdate['Storagemovement']['transfered_quantity'];
                    }
                    //        debug("START QUANTITY > 0");
                    //        debug($quantity);
                    //        debug("QUI");


                    if ($quantity < 0) {
                        $this->Storagemovement->setTransferredQuantity($storageId, $quantity, $depositId, $movementTag, $storagemovementprice, $transportId);
                    }
                }
                //$this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => $rowToUpdate['Storagemovement']['quantity']], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
            } else {


                //        debug("I'm Here");

                // Calcolo quanti spazi liberi ci sono nella riga (quantità disponibile)
                $maxQuantityToUpdate = $rowToUpdate['Storagemovement']['quantity'] - $rowToUpdate['Storagemovement']['transfered_quantity'];

                $valueToUpdate = $quantity + $rowToUpdate['Storagemovement']['transfered_quantity'];


                // Controllo se la quantity da aggiornare è maggiore di quella disponibile
                if ($quantity > $maxQuantityToUpdate) {
                    $this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => $rowToUpdate['Storagemovement']['quantity']], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
                    // Scalo la quantità da qelle da aggiornare
                    $quantity = $quantity - $maxQuantityToUpdate;
                } else {
                    $this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => $valueToUpdate], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);

                    $quantity = 0;
                }

                // Vado in ricorsione
                if ($quantity > 0) {
                    $this->Storagemovement->setTransferredQuantity($storageId, $quantity, $depositId, $movementTag, $storagemovementprice, $transportId);
                }
            }
        }
    }



    public function createDepositStorageTransfer($storageId, $quantity, $originDeposit, $destinationDeposit, $movementNumber, $variationId = null)
    {
        // Scarico il magazzino di provenienza
        $this->storageUnload($storageId, $quantity, '[TR_DEP_SC] Trasferimento tra depositi', 0, $originDeposit, $movementNumber, '', $variationId);
        // Carico il magazzino di destinazione
        $this->storageLoad($storageId, $quantity, '[TR_DEP_CA] Trasferimento tra depositi', 0, $destinationDeposit, $movementNumber + 1, '', $variationId);
    }


    // Prenotazione articolo
    public function reserve($storageId, $quantity,$description,$reservationId,$depositId,$ReservedDate)
    {
        $this->CreateMovement($storageId,null , $description, 0, 1, 'R', null, $depositId, "PR",
            null, $quantity, null, null,$reservationId,null,null,null,null, null, $ReservedDate,null);
    }

    public function reservedSold($reservationId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storagemovement.company_id'=>MYCOMPANY,'Storagemovement.state'=>ATTIVO,'Storagemovement.reservation_id'=>$reservationId];
        $this->Storagemovement->updateAll(['Storagemovement.reserved_quantity_sold'=>'Storagemovement.reserved_quantity'],$conditionsArray);
    }

    public function reservedCanceled($reservationId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storagemovement.company_id'=>MYCOMPANY,'Storagemovement.state'=>ATTIVO,'Storagemovement.reservation_id'=>$reservationId];
        $this->Storagemovement->updateAll(['Storagemovement.state'=>0],$conditionsArray);
    }

    // Reso  articolo
    public function returned($storageId, $quantity,$description,$reservationId,$depositId,$returnDate)
    {
        $this->CreateMovement($storageId,0 , $description, 0, 1, 'R', null, $depositId, "RT",  null, null, null, null,$reservationId,null,null,null,null, null, $returnDate,$quantity);
    }

    public function reserveFromQuote($storageId, $quantity,$description,$quoteId = 0)
    {
        $this->CreateMovement($storageId,null , $description, 0, 1, 'R', null, '0', "QR",
            null, $quantity, null, null,null,$quoteId,null,null,null,null,null);
    }

    public function unreserveFromQuote($storageId, $quantity,$description,$quoteId = 0)
    {
        $this->CreateMovement($storageId,null , $description, 0, 1, 'UR', null, '0', "QU",
            null, -1 * $quantity, null, null,null,$quoteId,null,null,null,null,null);
    }

    // Ordine articolo
    public function order($storageId, $quantity,$description,$purchaseOrderId,$depositId,$orderDate)
    {
        $this->CreateMovement($storageId,null , $description, 0, 1, 'O', null, $depositId, "PO",
            null, null , $quantity, null,null ,null,null,$purchaseOrderId,null, null, $orderDate,null,0);
    }

    // Aggiornamento quantità su ordine arrivato
    public function orderLoaded($storageId,$purhcaseOrderRowId,$quantity)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Purchaseorderrow = ClassRegistry::init('Purchaseorderrow');

        $purchaseOrderId = $this->Purchaseorderrow->getPurchaseorderId($purhcaseOrderRowId);
        $conditionsArray = ['Storagemovement.order_id' => $purchaseOrderId, 'Storagemovement.state' => 1, 'Storagemovement.company_id' => MYCOMPANY];

        $movements = $this->Storagemovement->find('all', ['conditions' => $conditionsArray, 'order' => ['Storagemovement.id asc']]);
        $remaingingQuantity = $quantity;

        foreach ($movements as $movement) {

            if ($remaingingQuantity > 0) {
                $possibleLoadingQuantity = $movement['Storagemovement']['ordered_quantity'] - $movement['Storagemovement']['ordered_quantity_arrived'];
                if ($possibleLoadingQuantity > 0) {
                    if ($possibleLoadingQuantity >= $remaingingQuantity) {
                        $this->updateAll(['Storagemovement.ordered_quantity_arrived' => $movement['Storagemovement']['ordered_quantity_arrived'] + $remaingingQuantity], ['Storagemovement.id' => $movement['Storagemovement']['id']]);
                        $remaingingQuantity = 0;
                    } else {
                        $this->updateAll(['Storagemovement.ordered_quantity_arrived' => $movement['Storagemovement']['ordered_quantity']], ['Storagemovement.id' => $movement['Storagemovement']['id']]);
                        $remaingingQuantity = $remaingingQuantity - $possibleLoadingQuantity;
                    }
                }
            }
        }
    }


    public function getOrderedDisponible($storageId)
    {
        $conditionsArray = ['Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO];
        return $this->find('first', ['fields' => ['SUM(ordered_quantity) as ordered_quantity'], 'conditions' =>$conditionsArray])[0]['ordered_quantity'];
    }

    public function  getReservedNotArrived($storageId)
    {
        $conditionsArray = ['Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.reserved_quantity_sold IS NULL', 'Storagemovement.state' => ATTIVO];
        return $this->find('first', ['fields' => ['SUM(reserved_quantity) as reserved_quantity'], 'conditions' => $conditionsArray])[0]['reserved_quantity'];
    }

    // todo ampliare i campi filtratai
    public function getMovements($storageId)
    {
        $conditionsArray = ['Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO];
        //$fieldsArray = ['Storagemovement.storage_id','Storagemovement.quantity','Storagemovement.deposit_id','Storagemovement.reserved_quantity_sold','Storagemovement.ordered_quantity_arrived','Storagemovement.state',,'Storagemovement.ordered_quantity_arrived'];
        //return $this->find('all',['conditions'=>$conditionsArray, 'fields'=>$fieldsArray]);
        return $this->find('all',['conditions'=>$conditionsArray]);
    }

}
