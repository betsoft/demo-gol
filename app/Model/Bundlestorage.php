<?php
App::uses('AppModel', 'Model');

class Bundlestorage extends AppModel
{

    public $useTable = 'bundle_storages';

    public $belongsTo =
        [
            'Bundle' => ['className' => 'Storage', 'foreignKey' => 'bundle_id'],
            'Storage' => ['className' => 'Storage', 'foreignKey' => 'storage_id'],
        ];

    public function hide($id)
    {
        return $this->updateAll(['Bundlestorage.state' => 0, 'Bundlestorage.company_id' => MYCOMPANY], ['Bundlestorage.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Bundlestorage.id' => $id, 'Bundlestorage.state' => 0]]) != null;
    }

}
