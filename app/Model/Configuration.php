<?php
App::uses('AppModel', 'Model');
/**
 * Good Model
 *
 * @property Bill $Bill
 */
class Configuration extends AppModel 
{
    public $useTable = 'configurations';
}
