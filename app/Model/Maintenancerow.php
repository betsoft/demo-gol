<?php
App::uses('AppModel', 'Model');

class Maintenancerow extends AppModel {

public $useTable = 'maintenance_rows';

	public $belongsTo =
	[ 
		 'Storage' => ['className' => 'Storage','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => '']
	];

    public function hide($id)
    {
        return $this->updateAll(['Maintenancerow.state' => 0, 'Maintenancerow.company_id' => MYCOMPANY],['Maintenancerow.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Maintenancerow.id'=>$id, 'Maintenancerow.state' =>0, 'Maintenancerow.company_id' => MYCOMPANY]]) != null;
    }
}
