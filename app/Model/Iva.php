<?php
App::uses('AppModel', 'Model');

class Iva extends AppModel
{

    public $useTable = 'ivas';

    public $belongsTo = [
        'Einvoicevatnature' => ['className' => 'Einvoicevatnature', 'foreignKey' => 'einvoicenature_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];

    public $hasMany = [
        'Good' => ['className' => 'Good', 'foreignKey' => 'iva_id', 'dependent' => false, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
    ];

    public $validate =
        [
            "codice" =>
                [
                    "rule" => ["isUnique", ["codice", "company_id", "state"], false],
                    "message" => "Il campo codice deve essere univoco."
                ],
            "descrizione" =>
                [
                    "rule" => ["isUnique", ["descrizione", "company_id", "state"], false],
                    "message" => "Il campo descrizione deve essere univoco."
                ]
        ];

    public function hide($id)
    {
        return $this->updateAll(['Iva.state' => 0, 'Iva.company_id' => MYCOMPANY], ['Iva.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Iva.id' => $id, 'Iva.state' => 0]]) != null;
    }

    public function getList()
    {
        $this->Iva = ClassRegistry::init('Iva');
        $conditionArray = ['Iva.company_id' => MYCOMPANY, 'Iva.state' => ATTIVO];
        $fieldsArray = ['Iva.descrizione'];
        $orderArray = ['Iva.descrizione' => 'asc'];
        return $this->Iva->find('list', ['conditions' => $conditionArray, 'fields' => $fieldsArray, 'order' => $orderArray]);
    }

    public function getListWithPercentage()
    {
        $this->Iva = ClassRegistry::init('Iva');
        $vats = $this->Iva->find('all', ['conditions' => ['company_id' => MYCOMPANY, 'Iva.state' => ATTIVO]]);

        $arrayVat = null;

        foreach ($vats as $vat) {
            $arrayVat[$vat['Iva']['id']] = $vat['Iva']['descrizione'] . ' - ' . $vat['Iva']['percentuale'];
        }

        return $arrayVat;
    }

    public function getPercentagesValues()
    {
        $this->Iva = ClassRegistry::init('Iva');
        $conditionArray = ['Iva.company_id' => MYCOMPANY, 'Iva.state' => ATTIVO];
        $fieldsArray = ['Iva.percentuale'];
        $orderArray = ['Iva.percentuale' => 'asc'];
        return $this->Iva->find('list', ['conditions' => $conditionArray, 'fields' => $fieldsArray, 'order' => $orderArray]);
    }

}
