<?php
App::uses('AppModel', 'Model');

class Technician extends AppModel
{
	public $useTable = 'technicians';

    public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ]]) != null;
    }

    public $virtualFields = ['surnameandname' => 'CONCAT(Technician.surname, " " ,Technician.name)'];
    public function getList()
    {
        return $this->find('list', ['fields' => ['Technician.id', 'surnameandname'], 'order' => ['Technician.surname' => 'asc']]);
    }


    /*public function getTechnicianState($technicianId)
    {
        switch($this->find('first',['conditions'=>['id'=>$technicianId]])['Technician']['accountstate'])
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }*/

    public function getTechnicianIdFromUserId($userId)
    {
        return $this->find('first',['conditions'=>['user_id' => $userId]])['Technician']['id'];
    }

    public function getSurnameAndName($technicianId)
    {
        return $this->find('first',['conditions'=>['company_id'=>MYCOMPANY ,'state'=>1 ,'id'=>$technicianId]])['Technician']['surnameandname'];
    }

    public function getWorkedHour($tecnichianId,$conditionsArray = null)
    {
        $this->Maintenance = ClassRegistry::init('Maintenance');
        if($conditionsArray == null) {
            $conditionsArray = ['Maintenance.technician_id' => $tecnichianId, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => 1];
        }
        return $this->Maintenance->find('all',['conditions'=> $conditionsArray,'order'=>['Maintenance.id desc']]);
    }

}
