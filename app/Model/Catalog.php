<?php
App::uses('AppModel', 'Model');


class Catalog extends AppModel {

public $useTable = 'catalogs';


	public $hasMany = [
		'CatalogStorage' =>  ['className' => 'CatalogStorage','foreignKey' => 'catalog_id','dependent' => false,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => '']
		];

	public $belongsTo = [
		'Currencies' => ['className' => 'Currencies','foreignKey' => 'currency_id','conditions' => '','fields' => '','order' => ''],
	]; 
		
}
