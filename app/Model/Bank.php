<?php
App::uses('AppModel', 'Model');

class Bank extends AppModel 
{
	public $useTable = 'banks';

    public $validate = 
	[
		"description"=>
		[
            "rule"=>["isUnique", ["description", "company_id","state"], false], 
            "message"=>"Il campo descrizione deve essere univoco" 
        ]
    ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ,'company_id'=>MYCOMPANY]]) != null;
    }
    
     public $virtualFields = ['longDescription' => 'CONCAT(Bank.description, "  " ,Bank.abi, " ", Bank.cab,"")'];
    
    public function getBanksListWithAbiAndCab()
    {
        $this->Bank = ClassRegistry::init('Bank');
        $conditionArray = ['company_id' =>MYCOMPANY,'Bank.state'=>ATTIVO];
       
        return $this->Bank->find('list',['fields' => ['id', 'longDescription'],'conditions'=>$conditionArray,'order'=>'description']);
    }
    
   

	
}
