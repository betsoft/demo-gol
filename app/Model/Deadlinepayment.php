<?php
App::uses('AppModel', 'Model');

class Deadlinepayment extends AppModel 
{

    public $useTable = 'deadline_payments';

    public function deletePayment($deadlinePaymentId)
	{
		$this->Deadlinepayment = ClassRegistry::init('Deadlinepayment');
		$this->Deadlinepayment->updateAll(['Deadlinepayment.state'=>0],['Deadlinepayment.id'=>$deadlinePaymentId]);
	}

}