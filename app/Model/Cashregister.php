<?php
App::uses('AppModel', 'Model');

class Cashregister extends AppModel
{

    public $useTable = 'cashregisters';

    public $belongsTo =
        [

        ];

    public function hide($id)
    {
        return $this->updateAll(['Cashregister.state' => 0, 'Cashregister.company_id' => MYCOMPANY], ['Cashregister.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Cashregister.id' => $id, 'Cashregister.state' => 0]]) != null;
    }

    public function getList()
    {
        $this->Cashregister = ClassRegistry::init('Cashregister');
        return $this->Cashregister->find('list', ['conditions' => ['Cashregister.company_id' => MYCOMPANY, 'Cashregister.state' => ATTIVO], 'fields' => ['Cashregister.description'], 'order' => ['Cashregister.description' => 'asc']]);
    }
}
