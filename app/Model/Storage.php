<?php
App::uses('AppModel', 'Model');

class Storage extends AppModel
{

    public $useTable = 'storages';

    public $belongsTo =
        [
            'Supplier' => ['className' => 'Supplier', 'foreignKey' => 'supplier_id', 'conditions' => '', 'fields' => '', 'order' => ''],
            'Units' => ['className' => 'Units', 'foreignKey' => 'unit_id', 'conditions' => '', 'fields' => '', 'order' => ''],
            'Ivas' => ['className' => 'Ivas', 'foreignKey' => 'vat_id', 'conditions' => '', 'fields' => '', 'order' => ''],
            'Productcategory' => ['className' => 'Productcategory', 'foreignKey' => 'product_category_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        ];

    public $hasMany =
        [
            'Good' => ['className' => 'Good', 'foreignKey' => 'storage_id', 'dependent' => true],
            'Catalogstorages' => ['className' => 'Catalogstorages', 'foreignKey' => 'storage_id', 'conditions' => '', 'fields' => '', 'order' => ''],
            'Storagemovement' => ['className' => 'Storagemovement', 'foreignKey' => 'storage_id', 'dependent' => true],
            'Bundlestorage' => ['className' => 'Bundlestorage', 'foreignKey' => 'bundle_id', 'conditions' => ['Bundlestorage.state >' => 0, 'Bundlestorage.company_id' => MYCOMPANY], 'dependent' => true],
        ];

    public $validate =
        [
            "descrizione" =>
                [
                    "rule" => ["isUnique", ["descrizione", "company_id", "state"], false],
                    "message" => "Articolo già presente a magazzino."
                ],
            "codice" =>
                [
                    "codicerule" =>
                        [
                            "rule" => ["isUnique", ["codice", "company_id", "state"], false],
                            'allowEmpty' => true,
                            "message" => "Codice articolo già utilizzato.",
                        ],
                ]
        ];

    public function hide($id)
    {
        return $this->updateAll(['Storage.state' => 0, 'Storage.company_id' => MYCOMPANY], ['Storage.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Storage.id' => $id, 'Storage.state' => 0]]) != null;
    }

    public function getMovableList()
    {
        IF(MODULO_REGISTRATORE_DI_CASSA) {
            $this->Storage = ClassRegistry::init('Storage');
            $conditionsArray =  ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 1];
            return $this->Storage->find('list', ['conditions' => [$conditionsArray], 'fields' => ['Storage.descrizione'], 'order' => ['Storage.descrizione' => 'asc']]);
        }
    }

    public function getNotMovableList()
    {
        IF(MODULO_REGISTRATORE_DI_CASSA) {
            $this->Storage = ClassRegistry::init('Storage');
            $conditionsArray =  ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 0];
            return $this->Storage->find('list', ['conditions' => [$conditionsArray], 'fields' => ['Storage.descrizione'], 'order' => ['Storage.descrizione' => 'asc']]);
        }
    }

    public function getMovableStorageByBarcode($barcode)
    {
        IF(MODULO_REGISTRATORE_DI_CASSA) {
            $this->Storage = ClassRegistry::init('Storage');
            $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 1,'barcode'=>$barcode,'parent_id'=>null];
            return isset($this->Storage->find('first', ['conditions' =>$conditionsArray])['Storage']) ? $this->Storage->find('first', ['conditions' =>$conditionsArray])['Storage'] : null;
            //return $this->Storage->find('first', ['conditions' =>$conditionsArray])['Storage'];
        }
    }

    /** Con questa funzione si può risalire solo all'id del padre gerarchico degli artcioli e sn */
    public function getMovableStorageIdByBarcode($barcode)
    {
        IF(MODULO_REGISTRATORE_DI_CASSA) {
            $this->Storage = ClassRegistry::init('Storage');
            $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 1,'barcode'=>$barcode,'parent_id'=>null];
            return isset($this->Storage->find('first', ['conditions' =>$conditionsArray])['Storage']['id']) ? $this->Storage->find('first', ['conditions' =>$conditionsArray])['Storage']['id'] : null;
        }
    }


    public function getMovableStorageById($storageId)
    {
        IF (MODULO_REGISTRATORE_DI_CASSA) {
            $this->Storage = ClassRegistry::init('Storage');
            $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 1, 'Storage.id' => $storageId];
            return isset($this->Storage->find('first', ['conditions' => $conditionsArray])['Storage']) ? $this->Storage->find('first', ['conditions' => $conditionsArray])['Storage'] : null;
        }
    }

    public function getNotMovableStorageById($storageId)
    {
        IF (MODULO_REGISTRATORE_DI_CASSA) {
            $this->Storage = ClassRegistry::init('Storage');
            $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 0, 'Storage.id' => $storageId];
            return isset($this->Storage->find('first', ['conditions' => $conditionsArray])['Storage']) ? $this->Storage->find('first', ['conditions' => $conditionsArray])['Storage'] : null;
        }
    }


    public function getAsusProducts()
    {
        if(MODULO_ASUS)
        {
            $this->Storage = ClassRegistry::init('Storage');
            $fieldsArray = ['Storage.codice','Storage.descrizione','Storage.barcode','Storage.movable','Storage.custom_field','serial_number'];
            $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 1, 'Storage.isasus' => 1];
            return  $this->Storage->find('all',['conditions'=>$conditionsArray,'fields'=>$fieldsArray]);
//            return  $this->Storage->find('all',['conditions'=>$conditionsArray]);
        }
    }

// Resituisce il valore di magazzino di un determinato deposito
    public function getAvailableQuantityValue($storageId, $valueMethod, $depositId = 'all')
    {

        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $depositId == 'all' ? $depositCondition = '1 = 1' : $depositCondition = 'deposit_id = ' . $depositId;

        // Faccio la somma e riaggiungo gli scarichi dovrebbe essere quasi ok
        $totalPurchased = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
        $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity - transfered_quantity) as rimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);

        switch ($valueMethod) {
            case 'CMP':
                $totalPrice = $this->Storagemovement->find('all', ['fields' => ['SUM((quantity) * purchase_price) as totaleRimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
                if ($totalPurchased[0][0]['rimanenze'] != 0) {
                    $totalPrice[0][0]['totaleRimanenze'] = $totalPrice[0][0]['totaleRimanenze'] / $totalPurchased[0][0]['rimanenze'] * $totalQuantity[0][0]['rimanenze'];
                } else {
                    $totalPrice[0][0]['totaleRimanenze'] = $totalPrice[0][0]['totaleRimanenze'] * $totalQuantity[0][0]['rimanenze'];
                }
                break;
            case 'FIFO':
                $totalPrice = $this->Storagemovement->find('all', ['fields' => ['SUM((quantity - transfered_quantity) * purchase_price) as totaleRimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
                break;
            case 'LIFO':
                $totalPrice = $this->Storagemovement->find('all', ['fields' => ['SUM((quantity - transfered_quantity) * purchase_price) as totaleRimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
                break;
            case 'UPA':
                $this->Storage = ClassRegistry::init('Storage');
                $totalPrice = $this->Storagemovement->find('all', ['fields' => ['SUM((quantity)) as totaleRimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
                $conditionsArray = ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY, 'Storage.state' => 1];
                return $totalPrice[0][0]['totaleRimanenze'] * $this->Storage->find('first', ['conditions' => $conditionsArray])['Storage']['last_buy_price'];
                break;
        }

        return is_numeric($totalPrice[0][0]['totaleRimanenze']) ? $totalPrice[0][0]['totaleRimanenze'] : 0;
    }

    public function createNewStorageFromGood($good, $clientId = null, $supplierId = null)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storage = ClassRegistry::init('Storage');
        // Recupero il catalogo
        $this->Catalog = ClassRegistry::init('Catalog');
        // Recupero il catalogo storages
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        if ($clientId != null) {
            // Controllo prima se esiste a catalogo
            $clientCatalog = $this->Catalog->find('first', ['conditions' => ['Catalog.client_id' => $clientId]]);

            // Se esiste un catalogo ed è presente la descrizione nel catalogo
            if (isset($clientCatalog['Catalog']['id'])) {
                $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $good['oggetto'], 'Catalogstorages.catalog_id' => $clientCatalog['Catalog']['id']]]);
            }

            // Se non esiste la descrizione a catalogo
            if (!isset($catalogstorages['Catalogstorages']['description'])) {
                $catalogstorages['Catalogstorages']['description'] = 'nonesistenessunadescrizione||nonesistenessunadescrizione';
            }

            // Se non esiste la descrizione a catalogo
            if (!$existInCatalog = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $catalogstorages['Catalogstorages']['description']]])) {
                // Se esiste già restituisco ciò che trovo a magazzino
                if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['oggetto']]])) {
                    $newStorage = $this->Storage->create();
                    $newStorage['Storage']['company_id'] = MYCOMPANY;
                    isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                    $newStorage['Storage']['descrizione'] = $good['oggetto'];
                    isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                    isset($good['quantita']) ? $newStorage['Storage']['qta'] = 0 : 0; // Sempre nullo tanto non viene considerato
                    isset($good['prezzo']) ? $newStorage['Storage']['prezzo'] = $good['prezzo'] : 0;
                    isset($good['unita']) ? $newStorage['Storage']['unit_id'] = $good['unita'] : null;
                    // Aggiunto salvataggio articoli movimentabili
                    isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                    isset($good['iva_id']) ? $newStorage['Storage']['vat_id'] = $good['iva_id'] : null;

                    // Gestione avanzata di magazzino
                    $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto

                    return $this->Storage->save($newStorage);
                } else {
                    return $oldStorage;
                }
            } else {

                $oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $existInCatalog['Catalogstorages']['storage_id']]]);

                // Questo per correggere un eventuale eliminazione di un articolo di magazzino (presente a catalogo, ma con storage_id mancante nell'elenco degli storage)
                if ($oldStorage == null) {
                    $newStorage = $this->Storage->create();
                    $newStorage['Storage']['company_id'] = MYCOMPANY;

                    // !!!!!!!!!!!!!!! Attenzione questa riga è per il FIX !!!!!!!!!
                    $newStorage['Storage']['id'] = $existInCatalog['Catalogstorages']['storage_id'];
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                    $newStorage['Storage']['descrizione'] = $good['oggetto'];
                    isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                    isset($good['quantita']) ? $newStorage['Storage']['qta'] = 0 : 0; // Sempre nullo tanto non viene considerato
                    isset($good['prezzo']) ? $newStorage['Storage']['prezzo'] = $good['prezzo'] : 0;
                    isset($good['unita']) ? $newStorage['Storage']['unit_id'] = $good['unita'] : null;
                    // Aggiunto salvataggio articoli movimentabili
                    isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                    isset($good['iva_id']) ? $newStorage['Storage']['vat_id'] = $good['iva_id'] : null;

                    $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto

                    return $newStorage;
                } else {
                    return $oldStorage;
                }
            }
        } else {
            // Se esiste già restituisco cioò che trovo a magazzino
            if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['oggetto']]])) {

                $newStorage = $this->Storage->create();
                $newStorage['Storage']['company_id'] = MYCOMPANY;
                isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                $newStorage['Storage']['descrizione'] = $good['oggetto'];
                isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                isset($good['quantita']) ? $newStorage['Storage']['qta'] = 0 : 0; // Sempre nullo tanto non viene considerato
                isset($good['prezzo']) ? $newStorage['Storage']['prezzo'] = $good['prezzo'] : 0;
                $newStorage['Storage']['unit_id'] = $good['unita'];
                // $newStorage['Storage']['withholding_tax'] = isset($good['withholding_tax']) ? $good['withholding_tax'] : null; // Aggiunto ritenuta d'acconto
                isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                isset($good['iva_id']) ? $newStorage['Storage']['vat_id'] = $good['iva_id'] : null;

                // Gestione avanzata di magazzino

                $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto


                return $this->Storage->save($newStorage);
            } else {
                return $oldStorage;
            }
        }
    }

    public function createNewStorageFromLoad($good, $supplierId)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storage = ClassRegistry::init('Storage');
        $this->Supplier = ClassRegistry::init('Supplier');
        $this->Catalog = ClassRegistry::init('Catalog');
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Controllo prima se esiste a catalogo
        $supplierCatalog = $this->Catalog->find('first', ['conditions' => ['Catalog.client_id' => $supplierId]]);

        if (isset($clientCatalog['Catalog']['id'])) {
            $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $good['description'], 'Catalogstorages.catalog_id' => $clientCatalog['Catalog']['id']]]);
        }

        if (!isset($catalogstorages['Catalogstorages']['description'])) {
            $catalogstorages['Catalogstorages']['description'] = 'nonesistenessunadescrizione||nonesistenessunadescrizione';
        }

        if (!$existInCatalog = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $catalogstorages['Catalogstorages']['description']]])) {


            // Se esiste già restituisco cioò che trovo a magazzino
            if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['description']]])) {
                $newStorage = $this->Storage->create();

                $newStorage['Storage']['company_id'] = MYCOMPANY;
                isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                if (isset($supplierId)) {$newStorage['Storage']['supplier_id'] = $supplierId;}

                $newStorage['Storage']['descrizione'] = $good['description'];
                isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                isset($good['quantity']) ? $newStorage['Storage']['qta'] = 0 : 0;

                if (AUTOMATICSELLPRICEINLOADGOOD) {
                    // Se attiva la personalizzazione "boffi" salva in automatico
                    isset($good['load_good_row_price']) ? $newStorage['Storage']['prezzo'] = $good['load_good_row_price'] : 0;
                } else {
                    // Questo è sempre zero quando viene creato da carico merce
                    $newStorage['Storage']['prezzo'] = 0;
                }

                $newStorage['Storage']['unit_id'] = isset($good['unit_of_measure_id']) ? $good['unit_of_measure_id'] : null;
                $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                isset($good['load_good_row_vat_id']) ? $newStorage['Storage']['vat_id'] = $good['load_good_row_vat_id'] : null;

                isset($good['category_id']) ? $newStorage['Storage']['category_id'] = $good['category_id'] : null;
                isset($good['load_good_row_price']) ? $newStorage['Storage']['last_buy_price'] = $good['load_good_row_price'] : 0;
                $newStorage['Storage']['min_quantity'] = 0;

                return $this->Storage->save($newStorage);
            } else {
                return $oldStorage;
            }
        } else {
            $oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $existInCatalog['Catalogstorages']['storage_id']]]);
        }
    }

    // Per ora fa un return zero poi a seconda della valorizzazione di magazzino lo gestisco
    public function getLastBuyPrice($storageId, $purchasePrice = 0)
    {
        // Questa va parametrizzata in base al metodo di valutazione (?)
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        if (isset($this->Storagemovement->find('first', ['conditions' => ['type_of_good_movement' => 'L', 'storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY], 'order' => 'Storagemovement.id desc'])['Storagemovement'])) {
            return $this->Storagemovement->find('first', ['conditions' => ['type_of_good_movement' => 'L', 'storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY], 'order' => 'Storagemovement.id desc'])['Storagemovement']['purchase_price'];
        } else {
            return 0; // Ritorno purchase price di zero (?)
        }
    }

    // aggiorno ultimo prezzo di acquisto sul articolo di magazzino
    public function updateStorageLastPrice($id, $price = 0)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $price == '' ? $price = 0 : null;
        $this->Storage->updateAll(['last_buy_price' => $price], ['Storage.id' => $id]);
    }

    // aggiorno ultimo prezzo su articolo di magazzino
    public function updateStoragePrice($id, $price)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->Storage->updateAll(['prezzo' => $price], ['Storage.id' => $id]);
    }

    // Recupera il metodo di valutaizone a magazzino
    public function getStorageEvaluationMethod()
    {
        $this->Setting = ClassRegistry::init('Setting');
        return $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY], 'fields' => ['Setting.storage_evaluation_method']])['Setting']['storage_evaluation_method'];
    }

    public function isStorageMovable($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');

        if (isset($this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY]])['Storage'])) {
            $isMovable = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY]])['Storage']['movable'];

            if ($isMovable == 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /** Resituisce la quantità totale o in un determinato deposito di un determinato articolo **/
    public function getAvailableQuantity($storageId, $depositId = 'all', $variationId = null)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        // Controllo se è stato definito il filtraggio delle varianti
        if ($depositId == 'all') {
            $conditionsArray = ['Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO];
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => $conditionsArray]);
        } else {
            $conditionsArray = ['Storagemovement.storage_id' => $storageId, 'deposit_id' => $depositId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO];
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => $conditionsArray]);
        }
        return is_numeric($totalQuantity[0][0]['rimanenze']) ? $totalQuantity[0][0]['rimanenze'] : 0;
    }

    /** Resituisce la quantità totale o in un determinato deposito di un determinato articolo - compresi seriali e varianti **/
    public function getAvailableQuantityWithSerialAndVariation($storageId, $depositId = 'all')
    {
        $this->Storage = ClassRegistry::init('Storage');
        /** Recupero le rimanenze dell'articolo */
        $fatherQuantity = $this->Storage->getAvailableQuantity($storageId, $depositId);

        /** Recupero le quantità dei figli i figli */
        $childQuantity = $this->Storage->getChildQuantity($storageId,$depositId);

        return $fatherQuantity + $childQuantity;
    }

    public function getChildQuantity($storageId, $depositId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $conditionsArray = ['Storage.company_id'=>MYCOMPANY, 'Storage.state'=>ATTIVO, 'Storage.parent_id'=>$storageId];
        $children = $this->Storage->find('all',['conditions'=>$conditionsArray]);
        $quantity = 0;
        foreach ($children as $child)
        {
            //         debug($quantity);
            $quantity = $quantity  + $this->Storage->getAvailableQuantity($child['Storage']['id'],$depositId);
            //         debug($quantity);
            $quantity = $quantity + $this->Storage->getChildQuantity($child['Storage']['id'],$depositId);
            //         debug($quantity);
        }
        return $quantity;
    }

    /** Resituisce la quantità di merce ordinata e non ancora arrivata **/
    /** Resituisce la quantità ordinata (?) **/
    public function getOrderedQuantity($storageId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        return $this->Storagemovement->getOrderedDisponible($storageId);
    }

    public function getOrderedQuantityWithVariation($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        /** Recupero le rimanenze dell'articolo */
        $fatherQuantity = $this->Storagemovement->getOrderedDisponible($storageId);
        /** Recupero le quantità dei figli i figli */
        $childrenQuantity = $this->Storage->getChildrenOrderedQuantity($storageId);
        return $fatherQuantity + $childrenQuantity;

        //  $this->Storagemovement = ClassRegistry::init('Storagemovement');
        //  return $this->Storagemovement->getOrderedDisponible($storageId);
    }

    public function  getChildrenOrderedQuantity($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storage.company_id'=>MYCOMPANY, 'Storage.state'=>ATTIVO, 'Storage.parent_id'=>$storageId];
        $children = $this->Storage->find('all',['conditions'=>$conditionsArray]);
        $quantity = 0;
        foreach ($children as $child)
        {
            $quantity = $quantity  + $this->Storagemovement->getOrderedDisponible($child['Storage']['id']);
            $quantity = $quantity + $this->Storage->getChildrenOrderedQuantity($child['Storage']['id']);
        }
        return $quantity;
    }

    /** Resituisce la quantità prenotata (?) **/
    public function getReservedQuantity($storageId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        return $this->Storagemovement->getReservedNotArrived($storageId);
    }

    public function getReservedQuantityWithVariation($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        /** Recupero le rimanenze dell'articolo */
        $fatherQuantity = $this->Storagemovement->getReservedNotArrived($storageId);
        /** Recupero le quantità dei figli i figli */
        $childrenQuantity = $this->Storage->getChildrenReservedNotArrived($storageId);
        return $fatherQuantity + $childrenQuantity;

        //  $this->Storagemovement = ClassRegistry::init('Storagemovement');
        //  return $this->Storagemovement->getOrderedDisponible($storageId);
    }

    public function  getChildrenReservedNotArrived($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $conditionsArray = ['Storage.company_id'=>MYCOMPANY, 'Storage.state'=>ATTIVO, 'Storage.parent_id'=>$storageId];
        $children = $this->Storage->find('all',['conditions'=>$conditionsArray]);
        $quantity = 0;
        foreach ($children as $child)
        {
            $quantity = $quantity  + $this->Storagemovement->getReservedNotArrived($child['Storage']['id']);
            $quantity = $quantity + $this->Storage->getChildrenReservedNotArrived($child['Storage']['id']);
        }
        return $quantity;
    }

    /*public function getVariationAvailableQuantity($storageId, $depositId, $variationId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        if ($variationId == 'novariation') {
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'Storagemovement.deposit_id' => $depositId, 'variation_id' => null, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        } else {
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'Storagemovement.deposit_id' => $depositId, 'variation_id' => $variationId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        }
        return is_numeric($totalQuantity[0][0]['rimanenze']) ? $totalQuantity[0][0]['rimanenze'] : 0;
    }*/

    /* public function getAllVariationAvailableQuantity($storageId, $depositId, $variationId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Variation = ClassRegistry::init('Variation');
        $this->Storage = ClassRegistry::init('Storage');
        // Recupero la variante e prendo il codice
        $thisVariation = $this->Variation->find('first', ['conditions' => ['Variation.id' => $variationId]]);
        // Cerco le foglie con quel codice (i rami avranno quantità nulla quindi ok)
        $thisVariations = $this->Variation->find('all', ['conditions' => ['Variation.parentcode' => $thisVariation['Variation']['code'] . '.']]);
        $arrayOfVariation = [];
        foreach ($thisVariations as $thisVariation) {
            array_push($arrayOfVariation, $thisVariation['Variation']['id']);
        }

        if (sizeof($arrayOfVariation) > 0) { //'NOT'=>['Variation.id'=>$arrayOfId ])
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'deposit_id' => $depositId, 'variation_id IN' => $arrayOfVariation, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        } else {
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'deposit_id' => $depositId, 'variation_id' => $variationId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        }
        //$totalQuantity = $this->Storagemovement->find('all', ['fields'=>[ 'SUM(quantity) as rimanenze'], 'conditions'=>['Storagemovement.storage_id'=>$storageId,'deposit_id' =>$depositId,'parentcode' =>$thisVariation['Variation']['parentcode'], 'company_id'=>MYCOMPANY,'Storagemovement.state'=>ATTIVO]]);
        return is_numeric($totalQuantity[0][0]['rimanenze']) ? $totalQuantity[0][0]['rimanenze'] : 0;
    } */





    //  Restituisce l'elenco degli articoli di magazzino presente in un determinato deposito, o in tutti i depositi
    public function getStorages($depositId = null, $movable = 'all')
    {
        $this->Storage = ClassRegistry::init('Storage');
        switch ($movable) {
            case false:
                $movableconditions = ['Storage.movable' => 0];
                break;
            case true:
                $movableconditions = ['Storage.movable' => 1];
                break;
            case 'all':
                $movableconditions = '1 = 1';
                break;
        }

        if ($depositId != null) {
            $arrayOfStorages = $this->Storage->getStoragesInDeposit($depositId);
            if (count($arrayOfStorages) > 1) {
                $storages = $this->Storage->find('all', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id in ' => $arrayOfStorages, $movableconditions], 'order' => ['Storage.descrizione' => 'asc']]);
            } else {
                $storages = $this->Storage->find('all', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id ' => $arrayOfStorages, $movableconditions], 'order' => ['Storage.descrizione' => 'asc']]);
            }
        } else {
            $storages = $this->Storage->find('all', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, $movableconditions], 'order' => ['Storage.descrizione' => 'asc']]);
        }

        return $storages;
    }

    public function storageHasLeaf($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        if ($this->Storage->find('count', ['conditions' => ['Storage.parent_id' => $storageId, 'Storage.company_id' => MYCOMPANY]]) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getStorageParentId($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY]]);
        return $storage['Storage']['parent_id'];
    }

    public function getStorageParentCodeFromCurrentStorage($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');

        /** Devo prendere il codice del padre, non dello storage id **/
        $storage = $this->Storage->find('first',['conditions'=>['Storage.id'=>$storageId,'Storage.company_id'=>MYCOMPANY]]);
        $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storage['Storage']['parent_id'], 'Storage.company_id' => MYCOMPANY]]);
        return $storage['Storage']['codice'];
    }

    public function getStorageParentDescriptionFromCurrentStorage($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        /** Devo prendere la descrizione del padre, non dello storage id **/
        $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY]]);
        $storage = $this->Storage->find('first',['conditions'=>['Storage.id'=>$storage['Storage']['parent_id'],'Storage.company_id'=>MYCOMPANY]]);
        return $storage['Storage']['descrizione'];
    }

    public function getStoragesWithVariationsNotUnique($depositId = null, $movable = 'all')
    {
        $this->Storage = ClassRegistry::init('Storage');
        return $this->Storage->getStoragesWithVariations($depositId, $movable, true, true);
    }

    /*  Restituisce l'elenco degli articoli di magazzino presente in un determinato deposito (=0), o in tutti i depositi e sue varianti */
    public function getStoragesWithVariations($depositId = null, $movable = 'all', $notShowVariation = 'false', $singleStorageId = null, $onlyExist = false)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->Utilities = ClassRegistry::init('Utilities');

        switch ($movable) {
            case 'false': // Servizi
                $movableconditions = ['Storage.movable' => 0];
                $singleStorageId == true ? $movableconditions = ['Storage.movable' => 0, 'Storage.sn = 0'] : null;
                break;
            case 'true': // Articoli
                $movableconditions = ['Storage.movable' => 1];
                $singleStorageId == true ? $movableconditions = ['Storage.movable' => 1, 'Storage.sn = 0'] : null;
                break;
            case 'all':
                $movableconditions = '1 = 1';
                $singleStorageId == true ? $movableconditions = ['Storage.sn = 0'] : null;
                break;
        }

        $fieldsArray = ['Storage.id', 'Storage.descrizione', 'Storage.prezzo', 'Storage.codice', 'Storage.unit_id', 'Storage.vat_id', 'Storage.barcode', 'Storage.movable', 'Storage.last_buy_price','Storage.category_id','Storage.default_discount'];

        if ($depositId != null) {
            $arrayOfStorages = $this->Storage->getStoragesInDeposit($depositId, $onlyExist);
            if (count($arrayOfStorages) > 1) {
                $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id in ' => $arrayOfStorages, $movableconditions];
                $storages = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
            } else {
                $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id ' => $arrayOfStorages, $movableconditions];
                $storages = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
            }
        } else {
            $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, $movableconditions];
            $storages = $this->Storage->find('all', ['conditions' => $conditionsArray, 'fields' => $fieldsArray, 'order' => ['Storage.descrizione' => 'asc']]);
        }

        $arrayOfStorages = [];
        $newCode = -1;

        foreach ($storages as $storage) {

            //     if (!$this->Storage->storageHasLeaf($storage['Storage']['id'])) {
            $newCode++;

            // Prima passo l'articolo normale senza varianti
            $arrayOfStorages[$newCode]['Storage']['id'] = $storage['Storage']['id'];
            $arrayOfStorages[$newCode]['Storage']['descrizione'] = $storage['Storage']['descrizione'];
            $arrayOfStorages[$newCode]['Storage']['prezzo'] = $storage['Storage']['prezzo'];
            $arrayOfStorages[$newCode]['Storage']['codice'] = $storage['Storage']['codice'];
            $arrayOfStorages[$newCode]['Storage']['unit_id'] = $storage['Storage']['unit_id'];
            $arrayOfStorages[$newCode]['Storage']['vat_id'] = $storage['Storage']['vat_id'];
            $arrayOfStorages[$newCode]['Storage']['barcode'] = $storage['Storage']['barcode'];
            $arrayOfStorages[$newCode]['Storage']['movable'] = $storage['Storage']['movable'];
            $arrayOfStorages[$newCode]['Storage']['last_buy_price'] = $storage['Storage']['last_buy_price'];
            $arrayOfStorages[$newCode]['Storage']['category_id'] = $storage['Storage']['category_id'];
            $arrayOfStorages[$newCode]['Storage']['default_discount'] = $storage['Storage']['default_discount'];
            //    }
        }
        return $arrayOfStorages;
    }

    // Restituisce l’elenco degli articoli di magazzino presente in un deposito con quantità maggiore di zero
    public function getStoragesInDeposit($depositId, $onlyExist = false)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $this->Clients = ClassRegistry::init('Storage');
        $storages = $this->Storage->getStoragesWithVariations();
        $arrayOfStorages = [];
        foreach ($storages as $storage) {
            if ($onlyExist == true) {
                if ($this->Storage->getAvailableQuantity($storage['Storage']['id'], $depositId) > 0) {
                    array_push($arrayOfStorages, $storage['Storage']['id']);
                }
            }

            if ($onlyExist == false) {
                if ($this->Storage->getAvailableQuantity($storage['Storage']['id'], $depositId)) {
                    array_push($arrayOfStorages, $storage['Storage']['id']);
                }
            }
        }

        return $arrayOfStorages;
    }


    public function getStoragesList()
    {
        $this->Storage = ClassRegistry::init('Storage');
        $storages = $this->Storage->find('list', ['fields' => ['Storage.id', 'Storage.descrizione'], 'conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO], 'order' => ['Storage.descrizione' => 'asc']]);
        return $storages;
    }

    // Restituisce l'articolo attivo con il nome corrispondente
    public function getStorageByName($storageDescription)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $return = $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.descrizione' => $storageDescription, 'state' => ATTIVO]]);
    }

    public function notExistInStorage($storageId, $depositId = 'all')
    {
        $this->Storage = ClassRegistry::init('Storage');

        // Se esiste a magazzino per la società
        if ($this->Storage->find('count', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.id' => $storageId]]) == 0) {
            return true;
        } else {
            return true;
        }
    }


    public function checkIfStorageArticleExist($storageName, $clientId = null)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Visualizzo se esiste un catalogo
        $clientCatalog = $this->existClientCatalog($this->getCustomerId($clientId));

        if ($clientCatalog && $clientId != null) {

            // Recupero gli articoli a magazzino
            $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $storageName, 'Catalogstorages.catalog_id' => $clientCatalog['Catalogs']['id']]]);

            // Se non esiste la descrizione a catalogo
            if (!isset($catalogstorages['Catalogstorages']['description'])) {
                // Non esiste a catalogo
                // return false;

                // Se non esiste a catalogo controllo che esista in magazzino o descrizine voci
                $exist = $this->Storage->find('count', ['conditions' => ['descrizione' => $storageName]]);
                if ($exist > 0) {
                    // Esiste in magazzino
                    return true;
                } else {
                    // Non esiste in magazzino
                    return false;
                }
            } else {
                // Esiste a catalogo
                return true;
            }
        } else {
            // Visualizzo se esiste a magazzino
            $existInStorage = $this->Storage->find('count', ['conditions' => ['descrizione' => $storageName, 'Storage.company_id' => MYCOMPANY]]);
            if ($existInStorage > 0) {
                // Esiste in magazzino
                return true;
            } else {
                return false;
            }

            /*else
            {
                $existInVariation = $this->Variation->find('count',['conditions'=>['Variation.description'=>$storageName, 'Variation.company_id'=> MYCOMPANY]]);
                if($existInVariation > 0)
                {
                    return true;
                }
                else
                {
                    // Non esiste in magazzino ne in varianti
                    return false;
                }
            }*/
        }
    }

    public function createNewStorageFromQuote($good, $clientId)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storage = ClassRegistry::init('Storage');
        // Recupero il catalogo
        $this->Catalog = ClassRegistry::init('Catalog');
        // Recupero il catalogo storages
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Controllo prima se esiste a catalogo
        $clientCatalog = $this->Catalog->find('first', ['conditions' => ['Catalog.client_id' => $clientId]]);

        if (isset($clientCatalog['Catalog']['id'])) {
            $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $good['description'], 'Catalogstorages.catalog_id' => $clientCatalog['Catalog']['id']]]);
        }

        if (!isset($catalogstorages['Catalogstorages']['description'])) {
            $catalogstorages['Catalogstorages']['description'] = 'nonesistenessunadescrizione||nonesistenessunadescrizione';
        }

        if (!$existInCatalog = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $catalogstorages['Catalogstorages']['description']]])) {
            // Se esiste già restituisco cioò che trovo a magazzino
            if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['description']]])) {
                $newStorage = $this->Storage->create();
                $newStorage['Storage']['company_id'] = MYCOMPANY;
                isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                $newStorage['Storage']['descrizione'] = $good['description'];
                isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                isset($good['quantity']) ? $newStorage['Storage']['qta'] = 0 : 0;
                isset($good['quote_good_row_price']) ? $newStorage['Storage']['prezzo'] = $good['quote_good_row_price'] : 0;
                $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                isset($good['unit_of_measure_id']) ? $newStorage['Storage']['unit_id'] = $good['unit_of_measure_id'] : null;
                isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                isset($good['quote_good_row_vat_id']) ? $newStorage['Storage']['vat_id'] = $good['quote_good_row_vat_id'] : null;

                return $this->Storage->save($newStorage);
            } else {
                return $oldStorage;
            }
        } else {
            $oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $existInCatalog['Catalogstorages']['storage_id']]]);
            return $oldStorage;
        }
    }

    public function getStorageName($id)
    {
        $this->Storage = ClassRegistry::init('Storage');
        return $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id' => $id], 'fields' => ['Storage.descrizione']])['Storage']['descrizione'];
    }

    public function getStoragesWithCatalog($clientId, $movable = 'all')
    {

        // Il cliente è nuovo non devo recuperare il catalogo
        if ($clientId == null) {
            return null;
        }

        $this->Storage = ClassRegistry::init('Storage');
        $this->Catalogs = ClassRegistry::init('Catalogs');
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Recupero il catalog
        $catalog = $this->Catalogs->find('first', ['conditions' => ['client_id' => $clientId]]);

        // Fix per preventivi dove non vengono mostrati i seriali (non dovrebbero essere tirati nemmeno su perché un seriale non dovrebbe andare nell'eventuale catalogo del cliente ) 2/2
        if(isset($_POST['action']))
        {
            if(($_POST['action'] == 'add' || $_POST['action'] == 'edit' || $_POST['action'] == 'editAdd' || $_POST['action'] == 'editChange') && $_POST['clientInputId'] == "#QuoteClientId") {
                $storages = $this->Storage->getStoragesWithVariationsNotUnique(null, $movable);
            }
            else {
                $storages = $this->Storage->getStoragesWithVariations(null, $movable);
            }}
        else
        {
            $storages = $this->Storage->getStoragesWithVariations(null, $movable);
        }


        $newStorages = [];
        foreach ($storages as $storage) {
            if ($catalog == null) {
                $cs = null;
            } else {
                // Gestisce errore di ricerca articolo nel listino cliente (non presente)
                $cs = $this->Catalogstorages->find('first',
                    [
                        'conditions' => ['storage_id' => $storage['Storage']['id'], 'catalog_id' => $catalog['Catalogs']['id']],
                        'fields' => ['id', 'description', 'catalog_price'],
                    ]);
            }

            $newKey = $storage['Storage']['id'];

            $newStorages[$newKey]['id'] = $storage['Storage']['id'];

            if ($cs != null) {
                $cs['Catalogstorages']['description'] != null ? $newStorages[$newKey]['descrizione'] = $cs['Catalogstorages']['description'] : $newStorages[$newKey]['descrizione'] = $storage['Storage']['descrizione'];
                $cs['Catalogstorages']['catalog_price'] != null ? $newStorages[$newKey]['prezzo'] = $cs['Catalogstorages']['catalog_price'] : $newStorages[$newKey]['prezzo'] = $storage['Storage']['prezzo'];
            } else  // Risolve il problema degli storage senza listino.
            {
                $newStorages[$newKey]['descrizione'] = $storage['Storage']['descrizione'];
                $newStorages[$newKey]['prezzo'] = $storage['Storage']['prezzo'];
            }

            $newStorages[$newKey]['codice'] = $storage['Storage']['codice'];
            $newStorages[$newKey]['unit_id'] = $storage['Storage']['unit_id'];
            $newStorages[$newKey]['vat_id'] = $storage['Storage']['vat_id'];
            $newStorages[$newKey]['movable'] = $storage['Storage']['movable'];
            $newStorages[$newKey]['category_id'] = $storage['Storage']['category_id'];
            $newStorages[$newKey]['default_discount'] = $storage['Storage']['default_discount'];
        }
        return $newStorages;
    }

    public function reserve($storageId,$quantity,$description,$reservationId,$depositId = 0, $dateReservation)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Storagemovement->reserve($storageId,$quantity,$description,$reservationId,$depositId,$dateReservation);
    }

    public function order($storageId, $quantity,$description,$purchaseOrderId,$depositId,$orderDate)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Storagemovement->order($storageId,$quantity,$description,$purchaseOrderId,$depositId,$orderDate);
    }

    public function getDefaultPrice($storageId){
        $this->Storage = ClassRegistry::init('Storage');
        return $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id' => $storageId], 'fields' => ['Storage.prezzo']])['Storage']['prezzo'];
    }


    public function getMarkedUpPrice($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id' => $storageId ];
        $currentStorageId = $this->Storage->find('first', ['conditions' => $conditionsArray]);

        isset($currentStorageId['Storage']['last_buy_price']) ? $lastBuyPrice = $currentStorageId['Storage']['last_buy_price'] : $lastBuyPrice = 0; // Definisce l'ultimo perzzo d'acquisto
        isset($currentStorageId['Productcategory']['mark_up']) ? $markUp = $currentStorageId['Productcategory']['mark_up'] : $markUp = 0; // Definisce il ricarico %
        return $lastBuyPrice * (1+ $markUp/100);
    }

    public function getDefaultVat($storageId){
        $this->Storage = ClassRegistry::init('Storage');
        return $this->Storage->find('first', ['contain' => 'Ivas', 'conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id' => $storageId], 'fields' => ['Ivas.percentuale']])['Ivas']['percentuale'];
    }

    public function getStorageBarcode($storageId){
        $this->Storage = ClassRegistry::init('Storage');
        return $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id' => $storageId], 'fields' => ['Storage.barcode']])['Storage']['barcode'];
    }

    public function adjustInventory($barcode, $quantity)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->User = ClassRegistry::init('User');
        $storageId = $this->Storage->getMovableStorageIdByBarcode($barcode);
        $storageQuantity = $this->Storage->getAvailableQuantityWithSerialAndVariation($storageId, $this->User->getMyDeposit());

        if ($storageQuantity < $quantity) {
            $this->Storagemovement->storageLoad($storageId, $quantity - $storageQuantity, "Aggiustamento inventariale da punto vendita (carico)", -1, $this->User->getMyDeposit(), 1, 'IN');
        }

        if ($storageQuantity > $quantity)
            $this->Storagemovement->storageUnload($storageId,$storageQuantity-$quantity,"Aggiustamento inventariale da punto vendita (scarico)",-1,$this->User->getMyDeposit(),1,'IN');

        return 1;
    }

    public function getDetails($id)
    {
        $this->Storagedetail = ClassRegistry::init('Storagedetail');

        if($id == null)
            return null;

        return $this->Storagedetail->getStorageDetail($id);
    }

    public function  getStorageSerialNumber($storageId)
    {

        $this->Storage = ClassRegistry::init('Storage');
        $conditionsArray = ['Storage.company_id' => MYCOMPANY ,'state'=>1 ,'Storage.parent_id' => $storageId];
        return $this->Storage->find('list',['conditions'=>$conditionsArray]);
    }

    public function getMovements($storageId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        return $this->Storagemovement->getMovements($storageId);
    }

}
