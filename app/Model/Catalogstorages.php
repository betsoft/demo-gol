<?php
App::uses('AppModel', 'Model');

class Catalogstorages extends AppModel {

	public $useTable = 'catalog_storages';

	public $belongsTo = 
	[
		'Storages' => ['className' => 'Storages','foreignKey' => 'storage_id', 'conditions' => '', 'fields' => '', 'order' => '' ],
		'Catalogs' => [ 'className' => 'Catalogs', 'foreignKey' => 'catalog_id', 'conditions' => '','fields' => '','order' => ''],
	]; 

}
