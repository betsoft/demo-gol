<?php
App::uses('AppModel', 'Model');

class Productcategory extends AppModel
{
    public $useTable = 'product_categories';

    public function hide($id)
    {
        return $this->updateAll(['Productcategory.state' => 0, 'Productcategory.company_id' => MYCOMPANY], ['Productcategory.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Productcategory.id' => $id, 'Productcategory.state' => 0]]) != null;
    }

    public function getList()
    {
        $conditionsArray = ['Productcategory.company_id' => MYCOMPANY, 'Productcategory.state' => ATTIVO ];
        return $this->find('list', ['fields' => ['Productcategory.id', 'Productcategory.description'],'conditions'=>$conditionsArray, 'order' => ['Productcategory.description' => 'asc']]);
    }

}
