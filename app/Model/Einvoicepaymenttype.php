<?php

App::uses('AppModel', 'Model');

class Einvoicepaymenttype extends AppModel 
{
	public $useTable = 'einvoice_payment_type';
	public $hasMany = ['Payment' => ['className' => 'Payment','foreignKey' => 'einvoicepaymenttype_id','dependent' => true,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => '']];

	public function findEInvoicePaymentTypeId($code)
	{
	    return $this->find('first',['conditions'=>['code'=>$code]])['Einvoicepaymenttype']['id'];
	}
	
	
}

?>
