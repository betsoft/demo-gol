<?php
App::uses('AppModel', 'Model');

class Maintenanceddt extends AppModel {

    public $useTable = 'maintenance_passiveddtvalues';

    public $belongsTo =
        [
            'Supplier' => ['className' => 'Supplier','foreignKey' => 'supplier_id','conditions' => '','fields' => '','order' => ''],
        ];

    public function hide($id)
    {
        return $this->updateAll(['Maintenanceddt.state' => 0, 'Maintenanceddt.company_id' => MYCOMPANY],['Maintenanceddt.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Maintenanceddt.id'=>$id, 'Maintenanceddt.state' =>0, 'Maintenanceddt.company_id' => MYCOMPANY]]) != null;
    }
}
