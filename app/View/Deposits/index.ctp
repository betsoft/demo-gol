
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Depositi','indexelements' => ['add'=>'Nuovo deposito']]); ?>

<div class="clients index">
	<div class="banks index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				
				<?php if (count($deposits) > 0) 
				{
				?>
				<tr>
					<?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?>
				</tr>
				<?php 
				}
				else
				{
					?><tr><td colspan="5"><center>nessun deposito trovato</center></td></tr><?php				
				}
			?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($deposits as $deposit) 
				{
				?>
					<tr>
						<td><?= h($deposit['Deposit']['code']); ?></td>
						<td><?= h($deposit['Deposit']['deposit_name']); ?></td>
						<td><?= $deposit['Deposit']['main_deposit'] == 0 ? '<i  class="fa fa-star-o" starid="'.$deposit['Deposit']['id'].'" ></i>' : '<i  class="fa fa-star selected " style="color:orange;">' ; ?></td>
						<td class="actions">
							<?=  $this->element('Form/Simplify/Actions/edit',['id' => $deposit['Deposit']['id']]); ?>
							<?php
							if($deposit['Deposit']['main_deposit'] == 0 )
							{
								echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $deposit['Deposit']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler cancellare il deposito ?', $deposit['Deposit']['id']));
							}
							?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
			<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>

<script>
	$(".fa-star-o").click
	(
		function()
		{
			var id = $(this).attr('starid');
			$.ajax({
				method: "POST",
				url: "<?= $this->Html->url(["controller" => "deposits","action" => "setMainDeposit"]) ?>",
				data:
				{
					id:id,
				},
				success: function(data)
				{
					  location.reload();
				}});
		});
</script>