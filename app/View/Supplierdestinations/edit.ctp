<?= $this->element('Form/Components/FilterableSelect/loader'); ?>
<?=  $this->Form->create('Supplierdestination'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica destino'); ?></span>
	 <div class="col-md-12"><hr></div>
  	  <div class="form-group col-md-12">
        <label class="form-margin-top form-label"><strong>Nome</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div>
    
    <div class ="form-group col-md-12">
        <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Indirizzo</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('address', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Cap</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('cap', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Città</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('city', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Provincia</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('province', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
	<label class="form-label form-margin-top"><strong>Nazione</strong></label>
     <div class="form-controls">
		<?= $this->element('Form/Components/FilterableSelect/component', [
			"name" => 'nation_id',
			"aggregator" => '',
			"prefix" => "nation_id",
			"list" => $nations,
			"options" => [ 'multiple' => false,'required'=> false],
			]); 
	?>
   </div>
   </div>    
   </div>  
 <div class="col-md-12"><hr></div>

	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index','passedValue'=>$supplierId]); ?></center>
	<?=  $this->Form->end(); ?>

