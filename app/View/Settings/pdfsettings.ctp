<?php echo $this->Form->create('Setting', ['type' => 'file', 'class' => 'uk-form-horizontal']); ?>
<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Impostazione stampe'); ?></span>
	</div>
         <div class="form-group col-md-12">
          <label class=\"form-margin-top form-label\"><strong>Logo:</strong></label>
          <div class="form-controls">
            <?=  $this->Form->input('header',['div' => false, 'label' => false, 'type'=>'file','class'=>'form-control']); ?>
          </br>
           <label class=\"form-margin-top form-label\"><strong>Logo attuale</strong></label>
           <div class="form-controls">
          <?= $this->Html->image('societa/'.$Setting['Setting']['header'].'', ['style'=>'height:100px; width:auto;max-width:300px;']); ?>
          </div>
          </div>
          <?php if(false) { ?>
            <div class="form-controls">
            <label class=\"form-margin-top form-label\"><strong>COLORE MENU BACKGROUND</strong></label>
            <!--input type="text" name="base_css_fontcolor" id="text-field" class="form-control color-picker-input" value=""-->
            <?=  $this->Form->input('base_css_background',['div' => false, 'label' => false, 'class'=>'form-control color-picker-input']); ?>
            </div>
            <div class="form-controls">
            <label class=\"form-margin-top form-label\"><strong>COLORE MENU SCRITTE</strong></label>
          	<?=  $this->Form->input('base_css_fontcolor',['div' => false, 'label' => false, 'class'=>'form-control color-picker-input']); ?>
          	</div>
        	<?php } ?>
          </br>
      </div>
    
     	<div class="form-group col-md-12">
     		<div class="col-md-2" style="float:left;margin-left:10px;">
         		<label class="form-margin-top form-label"><strong>Allineamento logo</strong></label>
	      	    <div class="form-controls">
	      	      <?=  $this->Form->input('logoalign',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['0'=>'Sinistra','1'=>'Centrato','2'=>'Destra']]); ?>
	       		</div>
        	</div>
        </div>
</div>
<br />
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'pdfsettings']); ?></center>
<?php    echo $this->Form->end(); ?>

