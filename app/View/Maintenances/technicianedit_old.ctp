<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

<!-- Questo è il nuovo autocomplete -->
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?= $this->Form->create('Maintenance'); ?>
    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __("Modifica scheda d'intervento") ?></span>
  	<div class="col-md-12"><hr></div>

    <div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_number', ['label' => false,'class' => 'form-control','required'=>true]); ?>
        </div>
    </div>
    <!--div-- class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Tecnico</strong></label>
        <div class="form-controls">
            <?php
             /*$this->element('Form/Components/FilterableSelect/component', [
                "name" => 'technician_id',
                "aggregator" => '',
                "prefix" => "technician_id",
                "list" => $technicians,
                "options" => [ 'multiple' => false,'required'=> false,'value'=>$this->request->data['Maintenance']['technician_id']],
                ]);*/
                ?>
            </div>
        </div-->

</div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Data scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Maintenance][maintenance_date]" value="<?= date("d-m-Y"); ?>"  required />
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top;" ><strong>Cliente</strong></label>
        <div class="form-controls">
            <?=
            $this->element('Form/Components/FilterableSelect/component',
                [
                    "name" => 'client_id',
                    "aggregator" => '',
                    "prefix" => "client_id_maintenances",
                    "list" => $clients,
                    "options" =>  [  'multiple' => false ,'required'=>false ],
                ]);
            ?>
        </div>
    </div>

    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Cantiere</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'constructionsite_id',
                "aggregator" => '',
                "prefix" => "constructionsite_id",
                "list" => $constructionsites,
                "options" => [ 'multiple' => false,'required'=> false,'value'=>$this->request->data['Maintenance']['constructionsite_id']],
            ]);
            ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Preventivo</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'quote_id',
                "aggregator" => '',
                "prefix" => "quote_id",
                "list" => $quotes,
                "options" => [ 'multiple' => false,'required'=> false,'value'=>$this->request->data['Maintenance']['quote_id']],
            ]);
            ?>
        </div>
    </div>
</div>

<div class ="form-group clientdetails col-md-12">
    <div class="col-md-2" style="float:left">
        <label class="form-label form-margin-top"><strong>Indirizzo cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_address',['label' => false, 'div' =>false, 'class'=>'form-control']);?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>CAP cantiere</strong></label>
        <div class="form-controls">
            <?=  $this->Form->input('maintenance_cap',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici.','minlength'=>5 ]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Città cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_city',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Provincia cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_province',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Nazione cantiere</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'maintenance_nation_id',
                "aggregator" => '',
                "prefix" => "maintenance_nation_id",
                "list" => $nations,
                "options" => [ 'multiple' => false,'required'=> false],
            ]);
            ?>
        </div>
    </div>
</div>

    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left;">
            <label class="form-label form-margin-top"><strong>Inizio intervento</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="time"  class=" segnalazioni-input form-control" name="data[Maintenance][maintenance_hour_from]" value="<?= date("H:i",strtotime($this->request->data['Maintenance']['maintenance_hour_from'])); ?>"  required />
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Fine intervento</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="time"  class="segnalazioni-input form-control" name="data[Maintenance][maintenance_hour_to]" value="<?= date("H:i",strtotime($this->request->data['Maintenance']['maintenance_hour_to'])); ?>"  required />
            </div>
        </div>
        <!-- non cancellare può essere facilmente richiesto -->
        <!--div class="col-md-2" style="float:left;">
            <label class="form-label form-margin-top"><strong>Ore di intervento</strong></label>
            <div class="form-controls">
                <?php // $this->Form->input('maintenance_hour',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
            </div>
        </div-->

    </div>

    <div class="form-group col-md-12">
        <div class="col-md-11" style="float:left;" >
            <label class="form-margin-top form-label"><strong>Descrizione dell'intervento</strong></label>
            <div class="form-controls">
                <?= $this->Form->textarea('intervention_description', ['div' => false, 'label' => false, 'class'=>'form-control','rows'=>5]); ?>
            </div>
        </div>
    </div>

<div class="col-md-12"><hr></div>
<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Materiali utilizzati</div>
    <div class="col-md-12"><hr></div>
    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture" class="col-md-12">
        <?php
        $conto = count($this->request->data['Maintenancerow']) - 1;
        $i = -1;
        ?>
        <script>
            var articoli = setArticles();
            var codici = setCodes();
        </script>
        <?php
        foreach ($this->request->data['Maintenancerow'] as $chiave => $oggetto)
        {
            $i++;
            $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
            $chiave == 0 ? $classe1 = "originale" : $classe1 = '';
        ?>
            <!-- Lunghezza serve per l'editclonable non cancellare -->
        <div class="principale lunghezza contacts_row clonableRow  <?= $classe1 ?>  <?= $classe ?>" id="<?= $chiave ?>">
            <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden ></span>
            <div class="col-md-12">
                <?php
                if(ADVANCED_STORAGE_ENABLED)
                {
                    ?>
                    <div class="col-md-2  jsRowField">
                        <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Maintenancerow.$chiave.codice", ['label' => false, 'class'=>'form-control jsCodice ','div' => false,'required'=>true]); ?>
                    </div>
                    <?php
                }
                else {
                    // Tolgo il jsTipo
                    echo $this->Form->hidden("Maintenancerow.$chiave.tipo", ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]);
                    ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-label jsRowField"><strong>Codice</strong></label>
                        <?= $this->Form->input("Maintenancerow.$chiave.codice", ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'maxlenght' => 11]); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-3 jsRowFieldDescription">
                    <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input("Maintenancerow.$chiave.description", ['label' => false, 'class'=>'form-control jsDescription','div' => false]); ?>
                    <?= $this->Form->hidden("Maintenancerow.$chiave.storage_id"); ?>
                    <?= $this->Form->hidden("Maintenancerow.$chiave.variation_id",['type'=>'text']); ?>
                    <?= $this->Form->hidden("Maintenancerow.$chiave.movable",['class'=>'jsMovable','value'=>1]); ?>
                </div>
                <div class="col-md-3 jsRowField">
                    <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                    <?= $this->Form->input("Maintenancerow.$chiave.customdescription", ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px']); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Quantità <i class="fa fa-warning Transportgood.0.iconwarning" style="color:red;display:none;" ></i></strong><i class="fa fa-asterisk"></i></label>
                    <?=  $this->Form->input("Maintenancerow.$chiave.quantity", ['label' => false, 'required'=>true,  'class'=>'form-control jsQuantity', 'div' => false]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Unità di misura</strong></label>
                    <?= $this->Form->input("Maintenancerow.0.unit_of_measure_id", ['label' => false, 'empty' => 'sel.', 'default' => 'sel.','class'=>'form-control', 'div' => false,'empty'=>true,'type'=>'select','options'=>$units]); ?>
                </div>
            </div>
           <div class="col-md-12"><hr></div>
        </div>
        <?php } ?>
</div>

    <div class="col-md-12">
        <center>
            <img  src="<?=($this->request->data['Maintenance']['signimage']); ?>" >
        </center>
    </div>
    <?php

?>
    <center>
        <?= $this->Form->button(__('Salva'),array('name'=>'redirect', 'value' => 'technicianadd', 'class'=>'btn blue-button new-bill salva', 'id'=>'save','style'=>'background-color:#ea5d0b;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));?>
        <?= $this->Html->link(__('Annulla'), ['action' => 'technicianindex'],array('class'=>'btn blue-button-reverse new-bill cancel', 'escape' =>false,'style'=>'background-color:#dadfe0;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;')); ?>
    </center>

	<?= $this->Form->end(); ?>
<script>
    $(document).ready
    (
        function() {
            var clienti = setClients();
            // Carico il listino passando chiave e id cliente
            loadClientCatalog(0, $("#client_id_maintenances_multiple_select").val(), "#MaintenanceClientId", "Maintenance");
            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#MaintenanceClientId");
            // Abilita il clonable sul cliente del cantiere che è required
            enableCloningedit($("#client_id_maintenances_multiple_select").val(), "#MaintenanceClientId");
            // Aggiungi il rimuovi riga
            addcrossremoving();
        })
</script>
<script>
    /* Aggiorno l'indirizzo sul cantiere */
    $("#constructionsite_id_multiple_select").change(
        function() {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "Constructionsites", "action" => "getAddress"]) ?>",
                data:
                    {
                        constructionsiteId: $(this).val(),
                    },
                success: function (data) {
                    console.log(data);
                    data = JSON.parse(data);
                    $.each(data,
                        function (key, element) {
                            console.log(element);

                            $("#MaintenanceMaintenanceAddress").val(element.address);
                            $("#MaintenanceMaintenanceCity").val(element.city);
                            $("#MaintenanceMaintenanceCap").val(element.cap);
                            $("#MaintenanceMaintenanceProvince").val(element.province);
                            $("#maintenance_nation_id_multiple_select").val(element.nation);
                            $("#maintenance_nation_id_multiple_select").multiselect('rebuild');
                        });
                }
            })
        });
</script>
<?= $this->element('Js/datepickercode'); ?>