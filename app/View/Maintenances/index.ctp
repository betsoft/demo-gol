<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>"Scheda d'intervento",'indexelements' => ['add'=>"Nuova scheda d'intervento"]]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>

				<?php
                    $currentYear = date("Y");
                    if (count($maintenances) > 0) {

                        $options = [
                                0 => 'NO',
                                1 => 'SI',
                                2 => 'Tutti',
                        ];
                    ?>
                    <tr>
                        <?=  $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
                            'htmlElements' => [
                                '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input f datepicker" name="data[filters][date1]" value="01-01-'.$currentYear.'"  bind-filter-event="change"/>'.''.
                                '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input  datepicker" name="data[filters][date2]"  value="31-12-'.$currentYear.'" bind-filter-event="change"/></center>',
                                '<select style="padding:0px;text-align:center;" id="mynet" class="form-control ajax-filter-input" name="data[filters][mynet]" bind-filter-event="change" ><option value=2>Tutti</option><option value=0>No</option><option value=1>Si</option></select>'
                            ]]);
                        ?></tr>
				<?php }
				else {
                    ?>
                    <tr>
                        <td colspan="<?php echo count($filterableFields); ?>" style="text-align: center">nessuna scheda d'intervento trovata</td>
                    </tr><?php
                }
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($maintenances as $maintenance) { ?>
                    <tr>
                        <td><?= h($maintenance['Maintenance']['maintenance_number']); ?></td>
                        <td><?= h($maintenance['Technician']['name'] . ' '. $maintenance['Technician']['surname']); ?></td>
                        <td><?= date("d-m-Y",strtotime($maintenance['Maintenance']['maintenance_date'])); ?></td>
                        <td><?= h($maintenance['Client']['ragionesociale']); ?></td>
                        <td><?= h($maintenance['Constructionsite']['name']); ?></td>
                        <td><?= h($maintenance['Constructionsite']['description']); ?></td>
                        <td><?= strlen($maintenance['Maintenance']['intervention_description']) > 80 ? substr(h($maintenance['Maintenance']['intervention_description']),0,80). '...' : h($maintenance['Maintenance']['intervention_description']); ?></td>
                        <td class="actions">
                            <?php
                            // if($maintenance['Maintenance']['sent'] == 1 || date_diff(date_create($maintenance['Maintenance']['maintenance_date']), date_create(date('Y-m-d')))->format('%d') > 7)
                            if($maintenance['Maintenance']['sent'] == 1)
                            {
                                    echo $iconaModificaOff;
                                }
                            else
                                {
                                    echo $this->Html->link($iconaModifica, ['action' => 'edit', $maintenance['Maintenance']['id']], ['title' => __('Modifica'), 'escape' => false]);
                                }

                                echo $this->Html->link($iconaPdf, ['action' => 'maintenancepdf', $maintenance['Maintenance']['id']], ['target'=>'_blank','title'=>__('Scarica PDF'),'escape' => false]);
                                if(MODULO_CANTIERI) {

                                    if($maintenance['Maintenance']['email'] != null && $maintenance['Maintenance']['email'] != '')
                                    {
                                        if($maintenance['Maintenance']['sent'] == 0)
                                        {
                                            echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#d75a4a;vertical-align: middle;"></i>', ['action' => 'sendMaintenanceMail', $maintenance['Maintenance']['id']], ['target' => '_blank', 'title' => __('Invia la scheda di intervento al seguente indirizzo email: '.$maintenance['Maintenance']['email']), 'escape' => false]);
                                        }
                                        else
                                            {
                                                echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;vertical-align: middle;"></i>',['action' => 'sendMaintenanceMail', $maintenance['Maintenance']['id']], ['title'=>__('Scheda d\'intervento inviata - ( '.$maintenance['Maintenance']['email'].' )'),'escape' => false]);
                                            }
                                    }
                                    else
                                        {
                                            ?>
                                            <i class="fa fa-envelope icon grigio" style="font-size:18px;margin-left:0px;" title="Email mancante nella scheda d'intervento"></i>
                                            <?php
                                        }
                                }
                                echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $maintenance['Maintenance']['id']), array('title' => __('Elimina'), 'escape' => false), __('Sei sicuro di voler cancellare la scheda d\'intervento ?', $maintenance['Maintenance']['id']));
                            ?>
                        </td>
                    </tr>
                    <?php
                }
				?>
			</tbody>
		</table>
		<?= $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
<?=  $this->element('Js/datepickercode'); ?>
