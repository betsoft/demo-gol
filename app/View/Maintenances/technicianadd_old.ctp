<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/clientautocompletefunction'); ?>

<?= $this->Form->create('Maintenances'); ?>
    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __("Nuova scheda d'intervento") ?></span>
  	<div class="col-md-12"><hr></div>

    <div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_number', ['label' => false,'value' => $maintenance_next_number,'class' => 'form-control','required'=>true]); ?>
        </div>
    </div>
        <!--div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Tecnico</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?php
        /*$this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'technician_id',
                    "aggregator" => '',
                    "prefix" => "technician_id",
                    "list" => $technicians,
                    "options" => [ 'multiple' => false,'required'=> true],
                ]);*/
                ?>
            </div>
        </div-->
    </div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Data scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime"  class="datepicker segnalazioni-input form-control" name="data[Maintenances][maintenance_date]" value="<?= date("d-m-Y"); ?>"  required />
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'client_id',
                "aggregator" => '',
                "prefix" => "client_id",
                "list" => $clients,
                "options" => [ 'multiple' => false,'required'=> true],
            ]);
            ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Cantiere</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'constructionsite_id',
                "aggregator" => '',
                "prefix" => "constructionsite_id",
                "list" => $constructionsites,
                "options" => [ 'multiple' => false,'required'=> false],
            ]);
            ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Preventivo</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'quote_id',
                "aggregator" => '',
                "prefix" => "quote_id",
                "list" => $quotes,
                "options" => [ 'multiple' => false,'required'=> false],
            ]);
            ?>
        </div>
    </div>
</div>

<div class ="form-group clientdetails col-md-12">
    <div class="col-md-2" style="float:left">
        <label class="form-label form-margin-top"><strong>Indirizzo cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_address',['label' => false, 'div' =>false, 'class'=>'form-control']);?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>CAP cantiere</strong></label>
        <div class="form-controls">
            <?=  $this->Form->input('maintenance_cap',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici.','minlength'=>5 ]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Città cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_city',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Provincia cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_province',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Nazione cantiere</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'maintenance_nation_id',
                "aggregator" => '',
                "prefix" => "maintenance_nation_id",
                "list" => $nations,
                "options" => [ 'multiple' => false,'required'=> false],
            ]);
            ?>
        </div>
    </div>
</div>

    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left;">
            <label class="form-label form-margin-top"><strong>Inizio intervento</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="time"  class="segnalazioni-input form-control" name="data[Maintenances][maintenance_hour_from]"   required />
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Fine intervento</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="time"  class=" segnalazioni-input form-control" name="data[Maintenances][maintenance_hour_to]" value="<?= date("d-m-Y"); ?>"  required />
            </div>
        </div>
    </div>

    <div class="form-group col-md-12">
        <div class="col-md-11" style="float:left;" >
        <label class="form-margin-top form-label"><strong>Descrizione dell'intervento</strong></label>
        <div class="form-controls">
            <?= $this->Form->textarea('intervention_description', ['div' => false, 'label' => false, 'class'=>'form-control','rows'=>5]); ?>
        </div>
    </div>
    </div>

<div class="col-md-12"><hr></div>
<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Materiali utilizzati</div>
    <div class="col-md-12"><hr></div>

    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture" class="col-md-12">
        <div class="principale contacts_row clonableRow originale ultima_riga">
            <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden ></span>
            <div class="col-md-12">
                <?php
                if(ADVANCED_STORAGE_ENABLED)
                {
                    ?>
                    <div class="col-md-2  jsRowField">
                        <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.codice', ['label' => false, 'class'=>'form-control jsCodice ','div' => false,'required'=>true]); ?>
                    </div>
                    <?php
                }
                else {
                    // Tolgo il jsTipo
                    echo $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]);
                    ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-label jsRowField"><strong>Codice</strong></label>
                        <?= $this->Form->input('Good.0.codice', ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'maxlenght' => 11]); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-3 jsRowFieldDescription">
                    <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.description', ['label' => false, 'class'=>'form-control jsDescription','div' => false]); ?>
                    <?= $this->Form->hidden('Good.0.storage_id'); ?>
                    <?= $this->Form->hidden('Good.0.variation_id',['type'=>'text']); ?>
                    <?= $this->Form->hidden('Good.0.movable',['class'=>'jsMovable','value'=>1]); ?>
                </div>
                <div class="col-md-3 jsRowField">
                    <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                    <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px']); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Quantità <i class="fa fa-warning Transportgood.0.iconwarning" style="color:red;display:none;" ></i></strong><i class="fa fa-asterisk"></i></label>
                    <?=  $this->Form->input('Good.0.quantity', ['label' => false, 'required'=>true,  'class'=>'form-control jsQuantity', 'div' => false]); ?>

                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Unità di misura</strong></label>
                    <?= $this->Form->input('Good.0.unit_of_measure_id', ['label' => false, 'empty' => 'sel.', 'default' => 'sel.','class'=>'form-control', 'div' => false,'empty'=>true,'type'=>'select','options'=>$units]); ?>
                </div>
            </div>
            <div class="col-md-12"><hr></div>
</div>
</div>
<?= $this->element('Js/sketchpad'); ?>
    <center>
       <?= $this->Form->button(__('Salva'),array('name'=>'redirect', 'value' => 'technicianadd', 'class'=>'btn blue-button new-bill salva', 'id'=>'save','style'=>'background-color:#ea5d0b;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));?>
       <?= $this->Html->link(__('Annulla'), ['action' => 'technicianindex'],array('class'=>'btn blue-button-reverse new-bill cancel', 'escape' =>false,'style'=>'background-color:#dadfe0;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;')); ?>
    </center>
    <?php // $this->element('Form/Components/Actions/component',['redirect'=>'technicianindex','template'=>'save']); ?></center>
	<?= $this->Form->end(); ?>
<script>
    $(document).ready
    (
    function() {
        var clienti = setClients();
        var articoli = setArticles();
        var codici = setCodes();
        // Carico il listino passando chiave e id cliente
        loadClientCatalog(0, $("#client_id_multiple_select").val(), "#MaintenanceClientId", "Good");
        // Definisce quel che succede all'autocomplete del cliente
        setClientAutocomplete(clienti, "#MaintenanceClientId");
        // Abilita il clonable sul cliente del cantiere che è required
        enableCloning($("#client_id_multiple_select").val(), "#MaintenanceClientId");
        // Aggiungi il rimuovi riga
        addcrossremoving();
    })
</script>
<script>
    /* Aggiorno l'indirizzo sul cantiere */
    $("#constructionsite_id_multiple_select").change(
        function() {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "Constructionsites", "action" => "getAddress"]) ?>",
                data:
                    {
                        constructionsiteId: $(this).val(),
                    },
                success: function (data) {
                    console.log(data);
                    data = JSON.parse(data);
                    $.each(data,
                        function (key, element) {
                            console.log(element);

                            $("#MaintenancesMaintenanceAddress").val(element.address);
                            $("#MaintenancesMaintenanceCity").val(element.city);
                            $("#MaintenancesMaintenanceCap").val(element.cap);
                            $("#MaintenancesMaintenanceProvince").val(element.province);
                            $("#maintenance_nation_id_multiple_select").val(element.nation);
                            $("#maintenance_nation_id_multiple_select").multiselect('rebuild');
                        });
                }
            })
        });
</script>
<?= $this->element('Js/datepickercode'); ?>