

<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>
<div class="clients index">
    <div class="good index">
        <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Assegnazione categorie commerciali per '.$billType.' numero ' . $billNumber. ' del ' .$billDate); ?></span>
        <?= $this->Html->link(__('indietro'), ['controller'=>'bills' ,'action'=>$billReturnAction], ['class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:10%;text-align:center;']) ?>
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class ="flip-content">

            <?php if (count($goods) > 0) { ?>
            <?php }
            else
            {
                ?><tr><td colspan="5"><center>nessuna articolo trovato</center></td></tr><?php
            }
            ?>
            </thead>
            <tbody class="ajax-filter-table-content">
            <tr>
                <td></td>
                <td></td>
                <td colspan="1" style="text-align:right;font-size:15px;font-weight: bold;">Modificando questo campo verranno modificati tutti i valori dei campi sucessivi. </td>
                <td style="background:#ea5d0b">
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => "category_id_global",
                            "aggregator" => '',
                            "prefix" => "",
                            "list" => $categories,
                            "options" => [ 'multiple' => false,'required'=> false, "class" => "global"],
                        ]);
                        ?>
                    </div>
                </td></tr>
            <tr ><td colspan = "4"><br/></td></tr>
            <?php foreach ($goods as $good)
            {
                if($good['Good']['quantita'] > 0 || ($good['Good']['quantita'] == null && $good['Good']['prezzo'] > 0))
                {
                    ?>
                    <tr>
                        <td><?= h($good['Good']['oggetto']); ?></td>
                        <td style="text-align:right;"><?= $good['Good']['quantita'] == null ? '1,00' : number_format($good['Good']['quantita'],2,',',''); ?></td>
                        <td style="text-align:right;"><?= number_format($good['Good']['prezzo'],2,',',''); ?></td>
                        <td>
                            <div class="form-controls">
                                <?= $this->element('Form/Components/FilterableSelect/component', [
                                    "name" => "category_id_".$good['Good']['id'],
                                    "aggregator" => '',
                                    "prefix" => "category_id",
                                    "list" => $categories,
                                    "options" => ['class'=>'inner', 'multiple' => false,'required'=> false,'goodId'=>$good['Good']['id'],'value'=>$good['Good']['category_id']],
                                ]);
                                ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(".multiple-select.inner").change(
        function()
        {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "goods","action" => "updateCategory"]) ?>",
                data:
                    {
                        categoryValue : $(this).val(),
                        goodId : $(this).attr("goodId"),
                    },
                success: function(data)
                {
                    /* Nothing */
                }
            });
        })

    $(".multiple-select.global").change(function() {
        $(".inner").val($(".multiple-select.global").val());
        $(".inner").multiselect('rebuild');
        $(".inner").change();
    });
</script>