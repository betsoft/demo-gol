<script>
	$(document).ready(
		function() {
			var fornitore = [
			<?php
				foreach($suppliers as $supplier)
				{
					echo '"' . addslashes($supplier) . '",';
				}
			?>];
			
			$( "#fornitore" ).autocomplete({source: fornitore});

		});
</script>
<?= $this->Form->create('Storage', ['class' => 'uk-form uk-form-horizontal' ,'enctype'=>'multipart/form-data']); ?>
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova variante articolo'); ?></span>
		 <div class="col-md-12"><hr></div>
		<?= $this->Form->hidden('movable',['value'=>1]); ?>
		<?= $this->Form->hidden('parent_id',['value'=>$storage['Storage']['id']]); ?>
		<?= $this->Form->hidden('supplier_id',['value'=>$storage['Storage']['supplier_id']]); ?>
		<?= $this->Form->hidden('codice_fornitore',['value'=>$storage['Storage']['codice_fornitore']]); ?>
      <div class="form-group col-md-12">
	  <div class="col-md-3" style="float:left;" > 
		<label class="form-label"><strong>Codice</strong></label>
		<div style="width:100%;margin-top:30px;margin-left:0px;">
			<?= $this->Form->input('parentcode', ['label' => false, 'div'=>false, 'class'=>'','value'=>$storage['Storage']['codice'].'.','style'=>"float:left;background-color:#dedede;width:50%;" ,'readonly'=>true]); ?>
			<?= $this->Form->input('codice', ['label' => false, 'div'=>false,  'required'=>true,'style'=>"width:50%;"]); ?>
		</div>
     </div>
	 <div class="col-md-3" style="float:left;margin-left:10px;" > 
		<label class="form-margin-top form-label"><strong>Descrizione</strong></label>
    	<div style="width:100%;margin-top:30px;margin-left:0px;">
    		<?= $this->Form->input('parentdescription', ['label' => false, 'div'=>false, 'class'=>'','value'=>$storage['Storage']['descrizione'],'style'=>"float:left;background-color:#dedede;width:50%;" ,'readonly'=>true]); ?>
			<?= $this->Form->input('descrizione', ['label' => false, 'div'=>false,  'required'=>true,'style'=>"width:50%;"]); ?>
		</div>
	</div>
	    <div class="col-md-2" style="float:left;margin-left:10px;" > 
			<label class="form-label"><strong>Codice a barre</strong></label>
    		<div class="form-controls">
			<?= $this->Form->input('barcode', ['label' => false, 'div' => false,'value'=>$storage['Storage']['barcode'], 'class'=>'form-control']); ?>
	     </div>
     </div>

</div>
 <?php // $this->Form->hidden('codice_fornitore',['label' => false, 'div' => false,'value'=>$storage['Storage']['supplier_id'],'class'=>'form-control','id'=>'fornitore']); ?>
	<div class="form-group col-md-12">
		<div class="col-md-2" style="float:left;" > 
			<label class="form-margin-top form-label"><strong>Prezzo di vendita predefinito</strong></label>
			<div class="form-controls">
    	<?= $this->Form->input('prezzo',['div' => false, 'label' => false, 'class'=>'form-control','value'=>$storage['Storage']['prezzo']]); ?>
		</div>
		</div>


     <div class="col-md-2" style="float:left;margin-left:10px;" > 
			<label class="form-margin-top form-label"><strong>Iva predefinita</strong></label>
			<div class="form-controls">
    	<?= $this->Form->input('vat_id',['div' => false, 'label' => false, 'class'=>'form-control','options'=>$vats,'empty'=>true,'value'=>$storage['Storage']['vat_id']]); ?>
		</div>
		</div>

		
	 	<div class="col-md-2" style="float:left;margin-left:10px;" > 
			<label class="form-margin-top form-label"><strong>Unità di misura</strong></label>
			<div class="form-controls">
    	<?= $this->Form->input('unit_id',['div' => false, 'label' => false, 'class'=>'form-control','empty'=>true,'value'=>$storage['Storage']['unit_id']]); ?>
		</div>
	</div>
		<div class="col-md-2" style="float:left;margin-left:10px;"> 
	    <label class="form-margin-top form-label"><strong>Variante univoca</strong></label><i class="fa fa-question-circle jsSn" style="color:#589ab8;cursor:pointer;"></i>
		<div class="form-controls">
			<?= $this->Form->input('sn',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['0'=>'No','1'=>'Sì'] ,'type'=>'select','empty'=>false]); ?>
		</div>
    </div>
</div>
    <div class="col-md-12"><hr></div>
 		<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexvariation', 'passedValue'=>$storageId]); ?></center>
	<?= $this->Form->end(); ?>

<script>
	$(".jsSn").click(function()	
	{
		$.alert
    	({
    		icon: 'fa fa-question-circle',
	    	title: '',
    		content: "<?= addslashes($helperMessage); ?>",
        	type: 'blue',
		});
	});
</script>