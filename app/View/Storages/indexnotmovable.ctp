<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Voci descrittive','indexelements' => ['addnotmovable'=>'Nuova voce descittiva'],'xlsLink'=>'indexnotmovable']); ?>
<div class="storages index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
			<?php if (count($storages) > 0) { ?>
			<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
			<?php 
			}
			else
			{
				?><tr><td colspan="7"><center>nessun articolo trovato</center></td></tr><?php				
			}
			?>
		</thead>
		<tbody class="ajax-filter-table-content">
			<?php
			
			foreach ($storages as $storage) { ?>
				<tr>
					<td> <?= h($storage['Storage']['codice']); ?></td>
					<td> <?= h($storage['Storage']['descrizione']); ?></td>
					<td style="text-align:right;"> <?= h(number_format((float)$storage['Storage']['prezzo'], 2, ',', '.')); ?>&nbsp;€</td>
					<td style="text-align:right;"> <?= h($storage['Ivas']['descrizione']); ?></td>
					<td class="actions">
						<?php
							echo $this->Html->link($iconaModifica, array('action' => 'editnotmovable', $storage['Storage']['id']),array('title'=>__('Modifica articolo'),'escape'=>false));
							echo $this->Form->postLink($iconaElimina, array('action' => 'deletenotmovable', $storage['Storage']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare l\' articolo ?', $storage['Storage']['id']));
						?>
					</td>
				
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
</div>
