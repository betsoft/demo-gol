
<script>

	$(document).ready(
		function() {
			var fornitore = [
			<?php
				foreach($suppliers as $supplier)
				{
					echo '"' . addslashes($supplier) . '",';
				}
			?>];
			
			$( "#fornitore" ).autocomplete({source: fornitore});

		});
</script>
<?= $this->Form->create('Storage', array('class' => 'uk-form uk-form-horizontal' ,'enctype'=>'multipart/form-data')); ?>
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova voce descrittiva'); ?></span>
		 <div class="col-md-12"><hr></div>

		<?= $this->Form->hidden('movable',['value'=>0]); ?>

	<div class="form-group col-md-12">
	<div class="col-md-4" style="float:left;">
			<label class="form-label"><strong>Codice</strong></label>
      		<div class="form-controls">
			<?= $this->Form->input('codice', array('label' => false, 'div' => false, 'class'=>'form-control','required'=>false)); ?>
      </div>
      </div>

		
			<div class="col-md-4" >
			<label class="form-margin-top form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
      <div class="form-controls">
			<?= $this->Form->input('descrizione',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'lunghezza' ,'required'=>true]); ?>
		</div>
      </div>

     <div class="col-md-4" >
			<label class="form-margin-top form-label"><strong>Prezzo di vendita predefinito</strong></label>
			<div class="form-controls">
    	<?= $this->Form->input('prezzo',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
		</div>
		</div>


     <div class="col-md-4" >
			<label class="form-margin-top form-label"><strong>Iva predefinita applicata</strong></label>
			<div class="form-controls">
    	<?= $this->Form->input('vat_id',array('div' => false, 'label' => false, 'class'=>'form-control','options'=>$vats,'empty'=>true)); ?>
		</div>
		</div>
		</div>

    <div class="col-md-12"><hr></div>
		<center><?php // $this->Form->submit(__('Salva',true), array('class'=>'btn blue-button new-bill','style'=>'margin-top:2%;')) ?></center>
  			<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexnotmovable']); ?></center>
  			<?= $this->Form->end(); ?>
