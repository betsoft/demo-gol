<script>

	$(document).ready(
		function() {
			var fornitore = [
			<?php
				foreach($suppliers as $supplier)
				{
  					echo '"' . addslashes($supplier) . '",';
				}
			?>];
			$( "#fornitore" ).autocomplete({source: fornitore});
		});
</script>
<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Modifica voce descrittiva</div>
 <div class="col-md-12"><hr></div>
    <?=  $this->Form->create('Storage', array('class' => 'uk-form uk-form-horizontal','enctype'=>'multipart/form-data')); ?>
    <?= $this->Form->input('id',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
    <?= $this->Form->hidden('movable',['value'=>0]); ?>

 	<div class="form-group col-md-12">
		<div class="col-md-4" >
    	<label class="form-label"><strong>Codice</strong></label>
  			<div class="form-controls">
				<?= $this->Form->input('codice',array('div' => false, 'label' => false, 'class'=>'form-control' ,'required'=>false)); ?>
      		</div>
  		</div>

     	<div class="col-md-4" >
		<label class="form-margin-top form-label"><strong>Descrizione<i class="fa fa-asterisk"></i></strong></label>
  			<div class="form-controls">
	 			<?= $this->Form->input('descrizione',array('div' => false, 'label' => false, 'class'=>'form-control' ,'required'=>true)); ?>
      		</div>
  		</div>

	    <div class="col-md-4" >
		<label class="form-margin-top form-label"><strong>Prezzo di vendita predefinito</strong></label>
			<div class="form-controls">
    			<?= $this->Form->input('prezzo',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
			</div>
		</div>

	    <div class="col-md-4" >
    	<label class="form-margin-top form-label"><strong>Iva applicata</strong></label>
			<div class="form-controls">
    	    	<?= $this->Form->input('vat_id',array('div' => false, 'label' => false, 'class'=>'form-control','options'=>$vats,'empty'=>true)); ?>
			</div>
		</div>
	</div>
	 <div class="col-md-12"><hr></div>
	   <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexnotmovable']); ?></center>
    	<?= $this->Form->end(); ?>

