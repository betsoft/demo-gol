<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Valorizzazione rimanenze', 'indexelements' => ['nothing'=>'nothing'], 'xlsLink'=>'indexstoragevalue']); ?>

<div class="storages index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>

			<tr><?= $this->element('Form/Components/AjaxFilter/component', 
			['elements' =>$filterableFields,
			'htmlElements' => [ $this->Form->input('filters.deposit_id', ['div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => $deposits ,  'bind-filter-event'=>"change"])
			]]); ?></tr>
		</thead>
		<tbody class="ajax-filter-table-content">
			<?php

            if (count($storages) > 0)
            {

            }
            else
            {
                ?><tr><td colspan="6" style="text-align: center">nessun articolo trovato</td></tr><?php
            }

			$totaleValorizzazione = 0;
			foreach ($storages as $storage) {?>
				<tr>
					<td><?= h($storage['Storage']['codice']); ?></td>
					<td><?= h($storage['Storage']['descrizione']); ?></td>
					<?php 
						$storageAvailableQuantity = number_format($getAviableQuantity->getAvailableQuantity($storage['Storage']['id'],$depositId),2,',','.'); 
						$storageAvailableQuantity < 0 ?  $tableColor = 'red' : $tableColor = '#6a6a6a' ; 
					?>
					<td style="text-align:right;color:<?= $tableColor ?>">
						<?= $storageAvailableQuantity ?>
					</td>
					<td><?= h($storage['Units']['description'] != null ? $storage['Units']['description'] : '' ); ?></td>
					<td style="text-align:right;">
						<?php
							$aviableValue = $getAviableQuantity->getAvailableQuantityValue($storage['Storage']['id'],$storageEvaluationMethod,$depositId);
							$totaleValorizzazione = $totaleValorizzazione  + $aviableValue;
							echo number_format($aviableValue,2,',','.');	
						?>
					</td>
				</tr>
			<?php } ?>
			<tr><td></td><td></td><td></td><td></td><td style="text-align:right;"><b><?= number_format($totaleValorizzazione,2,',','.'); ?></b></td></tr>		
		</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
</div>