<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Dettaglio magazzino per deposito dell\'articolo ' .$storages['Storage']['descrizione'] ); ?></span>
	</div>
	<div class="tools">
			<?= $this->Html->link('< magazzino', ['controller'=>'storages','action' => 'index'], ['title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;']); ?>		
	</div>
</div>
<div class="storages index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
			<?php if (count($storages) > 0) { ?>
			<tr><?php // $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
			<?php } ?>
		</thead>
		<tbody class="ajax-filter-table-content">
			<?php
			$sumOfQuantity = 0;
			foreach ($deposits as $deposit) { ?>
				<tr>
					<td> <?= h($deposit['description']); ?></td>
					<td style="text-align:right;"> <?= number_format($deposit['quantity'],2,',','.'); ?></td>
					<!--td class="actions"-->
					<?php
					//	if(ADVANCED_STORAGE_ENABLED)
					//	{
							//echo $this->Html->link($iconaDettaglioMagazzino, ['action' => 'depositvariationdetail', $storages['Storage']['id'],$deposit['id']],['style'=>'margin-right:3px;' ,'title'=>__('Dettaglio varianti per deposito'),'escape'=>false]);
					//		echo $this->Html->link($iconaDettaglioMagazzino, ['action' => 'depositdetails', $storage['Storage']['id']],['style'=>'margin-right:3px;' ,'title'=>__('Dettaglio magazzino'),'escape'=>false]);
					//	}																   	
					?>
					<!--/td-->
				</tr>
			<?php } ?>
			
		</tbody>
	</table>
	<?php // $this->element('Form/Components/Paginator/component'); ?>
</div>
