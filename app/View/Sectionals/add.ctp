<?= $this->Form->create('Sectional'); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo sezionale') ?></span>
 <div class="col-md-12"><hr></div>
<div class="form-group col-md-12">
   <div class="col-md-2" style="float:left;">
      <label class="form-margin-top form-label"><strong>Descrizione<i class="fa fa-asterisk"></i></strong></label>
      <div class="form-controls">
         <?= $this->Form->input('description', ['div' => false, 'label' => false, 'class'=>'form-control', 'required'=>true]); ?>
      </div>
   </div>
   <div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Contatore</label><i class="fa fa-asterisk"></i></strong></label>
      <div class="form-controls">
         <?= $this->Form->input('last_number', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>0, 'required'=>true]); ?>
      </div>
   </div>
   <!--div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Prefisso</strong></label>
      <div class="form-controls">
         <?php // $this->Form->input('prefix', ['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[a-zA-Z0-9\-]+"]); ?>
      </div>
   </div-->
   <div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Suffisso</strong></label>
      <div class="form-controls">
         <?= $this->Form->input('suffix', ['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[a-zA-Z0-9\-]+"]); ?>
      </div>
   </div>
    <?php if(MODULE_IXFE) { ?>
    <div class="col-md-2 sectionalixfe" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Sezionale ixfe</strong><i class="fa fa-asterisk"></i></label>
      <div class="form-controls">
         <?= $this->Form->input('ixfe_sectional', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,"pattern"=>"[a-zA-Z0-9\-]+"]); ?>
      </div>
   </div>
   <?php } ?>
   <div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Tipo contatore</strong></label>
      <div class="form-controls">
         <?= $this->Form->input('sectional_type', ['div' => false, 'label' => false, 'class'=>'form-control','option'=>$sectionalTypes,'type'=>'select']); ?>
      </div>
   </div>
</div>
 <div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>

<script>
   $("#SectionalSectionalType").change(function(){ showHideIxfeField(); })
   showHideIxfeField();
   
   function  showHideIxfeField()
    {
       console.log($("#SectionalSectionalType").val());
       if($("#SectionalSectionalType").val() != 'bill_sectional' && $("#SectionalSectionalType").val() != 'creditnote_sectional')
       {
          console.log("A");
          $(".sectionalixfe").find("#SectionalIxfeSectional").attr('required',false);
          $(".sectionalixfe").hide();
       }
       else
       {
          console.log("B");
         $(".sectionalixfe").show();
         $(".sectionalixfe").find("#SectionalIxfeSectional").attr('required',true);
       }
    }
</script>