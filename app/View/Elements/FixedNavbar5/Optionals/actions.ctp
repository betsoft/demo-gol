<div class="topbar-actions">
    <!-- BEGIN GROUP NOTIFICATION -->
    <?php /*<div class="btn-group-notification btn-group" id="header_notification_bar">
        <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="icon-bell"></i>
            <span class="badge">7</span>
        </button>
        <ul class="dropdown-menu-v2">
            <li class="external">
                <h3>
                    <span class="bold">12 pending</span> notifications</h3>
                <a href="#">view all</a>
            </li>
            <li>
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1">
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-success md-skip">
                                    <i class="fa fa-plus"></i>
                                </span> New user registered. </span>
                            <span class="time">just now</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-danger md-skip">
                                    <i class="fa fa-bolt"></i>
                                </span> Server #12 overloaded. </span>
                            <span class="time">3 mins</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-warning md-skip">
                                    <i class="fa fa-bell-o"></i>
                                </span> Server #2 not responding. </span>
                            <span class="time">10 mins</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-info md-skip">
                                    <i class="fa fa-bullhorn"></i>
                                </span> Application error. </span>
                            <span class="time">14 hrs</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-danger md-skip">
                                    <i class="fa fa-bolt"></i>
                                </span> Database overloaded 68%. </span>
                            <span class="time">2 days</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-danger md-skip">
                                    <i class="fa fa-bolt"></i>
                                </span> A user IP blocked. </span>
                            <span class="time">3 days</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-warning md-skip">
                                    <i class="fa fa-bell-o"></i>
                                </span> Storage Server #4 not responding dfdfdfd. </span>
                            <span class="time">4 days</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-info md-skip">
                                    <i class="fa fa-bullhorn"></i>
                                </span> System Error. </span>
                            <span class="time">5 days</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <span class="details">
                                <span class="label label-sm label-icon label-danger md-skip">
                                    <i class="fa fa-bolt"></i>
                                </span> Storage server failed. </span>
                            <span class="time">9 days</span>
                        </a>
                    </li>
                </ul><div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; background: rgb(99, 114, 131);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div></div>
            </li>
        </ul>
    </div>
    <!-- END GROUP NOTIFICATION -->
    <!-- BEGIN GROUP INFORMATION -->
    <div class="btn-group-red btn-group">
        <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="fa fa-plus"></i>
        </button>
        <ul class="dropdown-menu-v2" role="menu">
            <li class="active">
                <a href="#">New Post</a>
            </li>
            <li>
                <a href="#">New Comment</a>
            </li>
            <li>
                <a href="#">Share</a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#">Comments
                    <span class="badge badge-success">4</span>
                </a>
            </li>
            <li>
                <a href="#">Feedbacks
                    <span class="badge badge-danger">2</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- END GROUP INFORMATION -->
    */ ?>
    <!-- BEGIN USER PROFILE -->
    <div class="btn-group user-profile">
        <button type="button" class="btn btn-md md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <span><?= $USER['name'] ?></span>
        </button>
        <ul class="dropdown-menu-v2" role="menu">
            <li>
                <?= $this->Html->link(__('<i class="fa fa-cog"></i> Impostazioni'), ['plugin' => 'UsersManager','controller'=>'users-settings','action' => 'edit', $USER['id']], array('escape' =>false)) ?> 
            </li>
            <li>
                <?= $this->Html->link(__('<i class="fa fa-key"></i> Log Out'), ['plugin' => 'UsersManager','controller'=>'users','action' => 'logout'], array('escape' =>false)) ?> 
            </li>
        </ul>
    </div>
    
    <!-- END USER PROFILE -->
    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
    <?php
    /*<button type="button" class="quick-sidebar-toggler md-skip" data-toggle="collapse">
        <span class="sr-only">Toggle Quick Sidebar</span>
        <i class="icon-logout"></i>
    </button>*/
    ?>
    <!-- END QUICK SIDEBAR TOGGLER -->
    <!--?= $this->Html->link(__('<i class="fa fa-key"></i> Log Out'), ['plugin' => 'UsersManager', 'controller'=>'users','action' => 'logout'], array('class'=>' btn btn-outline btn-sm color-inverted', 'escape' =>false)) ?--> 

</div>