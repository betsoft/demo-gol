<div class="fixed-navbar-5 clearfix navbar-fixed-top">
    <?php if($hamburger !== false): ?>
        <?= $hamburger ?>
    <?php endif; ?>
    
    <!-- BEGIN LOGO -->
    <?php 
    if(isset($COMPANY_LOGO) && $logo): ?>
        <a id="index" class="page-logo" href=<?= $this->Url->Build($USER_HOME_PAGE) ?>>
            <?= $this->Html->image($COMPANY_LOGO) ?>
        </a>
    <?php endif; ?>
    <!-- END LOGO -->

    <!-- BEGIN LANGUAGES -->
    <?php if(isset($languages) && $languages): ?>
        <?= $this->element('Layout/FixedNavbar5/Optionals/languages') ?>
    <?php endif; ?>
    <!-- END LANGUAGES -->

    <!-- BEGIN SEARCH -->
    <?php if($search): ?>
        <?= $this->element('Layout/FixedNavbar5/Optionals/search') ?>
    <?php endif; ?>
    <!-- END SEARCH -->

    <!-- BEGIN TOPBAR ACTIONS -->
    <?php if($actions): ?>
        <?= $this->element('Layout/FixedNavbar5/Optionals/actions') ?>
    <?php endif; ?>
    <!-- END TOPBAR ACTIONS -->
</div>