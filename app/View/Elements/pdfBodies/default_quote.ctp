


<!--table class="items" width="100%" height="50%" style="margin-top:200px;border:0.1px solid black;font-size: 9pt; border-collapse: collapse;" cellpadding="6"-->
<?php
$tableHeaderStyle = "border:1px solid black;font-size:10px;background-color:#dadada;";
$tableHeaderStyleLeft = "border:1px solid black;font-size:10px;background-color:#dadada;text-align:left;padding-left:4px;";
$tableBodyStyle = "border:1px solid black;font-size:10px;";
$tableBodyStyleRight = "border:1px solid black;font-size:10px;text-align:right;";
$tableBodyStyleNoBorder =  "font-size:10px;";
$tableBodyStyleRightNoBorder = "font-size:10px;text-align:right;;";
?>
<!--table-- style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table-->

<table  style="border:0px;margin-top:130px;margin-left:-3px;" ><tbody><tr><td></td></tr></tbody></table>
<?php
    if ((isset($quote['Quote']['cig']) && $quote['Quote']['cig'] != '') || isset($quote['Quote']['cup']) && $quote['Quote']['cup'] != '')
    {
     ?>
        <!--table style="border-spacing: 0;"-->
        <table  style="font-size:12px;width:40px;" >
            <tbody>
                <tr>
        <?php
        if(isset($quote['Quote']['cig']) && $quote['Quote']['cig'] != '')
        { ?>
            <td style="font-size:12px;width:40px;"><?= '<strong>CIG:</strong></td><td style="font-size:12px;width:40px;">' . $quote['Quote']['cig'] ?></td>
        <?php
        }

        if(isset($quote['Quote']['cup']) && $quote['Quote']['cup'] != '')
        {
            ?>
            <td style="font-size:12px;width:40px;"><?= '<strong>CUP:</strong><td style="font-size:12px;width:40px;">' . $quote['Quote']['cup'] ?></td>
        <?php
        }
         ?>
                </tr>
            </tbody>
        </table><table style="min-height:30mm;"><tbody><tr><td></td></tr></tbody></table>
        <table style="min-height:30mm;"><tbody><tr><td></td></tr></tbody></table>
            <?php
    }
    ?>
<?php if(isset($quote['Quote']['note']) && $quote['Quote']['note'] != '') { ?>

<!--table  width="100%" height="50%" style="border:0px;margin-top:200px;margin-left:-3px;font-size: 9pt;" cellpadding="6"-->
    <table style="border-spacing: 0;">
        <tbody>
        <tr><td style="font-size:12px;"><?= $quote['Quote']['note'] ?></td></tr>
        </tbody>
    </table>

<table style="min-height:30mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<?php } ?>
<table style="border-spacing: 0;">


    <thead>
    <tr >
		<th style="width:80mm;<?= $tableHeaderStyle; ?>">Descrizione articolo</th>
		<th style="width:30mm;<?= $tableHeaderStyle; ?>">Quantità</th>
		<th style="width:25mm;<?= $tableHeaderStyle; ?>">U.M.</th>
		<th style="width:25mm;<?= $tableHeaderStyle; ?>">Prezzo unitario</th>
		<th style="width:25mm;<?= $tableHeaderStyle; ?>">Totale</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$totale = 0;
	foreach($quote['Quotegoodrow'] as $numero => $oggetto)
	{
            switch ($oggetto['type_of_line'])
            {
                case null:
                    if(!isset($oggetto['storage_id']))
                    {

                        ?>
                        <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
                        <tr >
                            <td style="<?= $tableBodyStyleNoBorder; ?>"><?= $oggetto['description'] ?></td>
                            <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['quantity']) ? number_format($oggetto['quantity'], 2, ',', '.') : null ?></td>
                            <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['Units']['description']) ? $oggetto['Units']['description'] : null ?></td>
                            <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['quote_good_row_price']) ? number_format($oggetto['quote_good_row_price'], 2, ',', '.') . ' €' : null ?></td>
                            <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['quote_good_row_price']) && isset($oggetto['quantity']) ? number_format($oggetto['quote_good_row_price'] * $oggetto['quantity'], 2, ',', '.') . ' €' : null ?></td>
                        </tr>
                        <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
                        <?php
                    }
                    else {
                        ?>
                        <tr>
                            <td style="<?= $tableBodyStyle; ?>"><?= $oggetto['description'] ?></td>
                            <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['quantity']) ? number_format($oggetto['quantity'], 2, ',', '.') : null ?></td>
                            <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['Units']['description']) ? $oggetto['Units']['description'] : null ?></td>
                            <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['quote_good_row_price']) ? number_format($oggetto['quote_good_row_price'], 2, ',', '.') . ' €' : null ?></td>
                            <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['quote_good_row_price']) && isset($oggetto['quantity']) ? number_format($oggetto['quote_good_row_price'] * $oggetto['quantity'], 2, ',', '.') . ' €' : null ?></td>
                        </tr>
                        <?php
                    }
                    break;
                case 'D':
                    ?>
                    <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
                    <tr >
                        <td style="<?= $tableBodyStyleNoBorder; ?>"><?= $oggetto['description'] ?></td>
                        <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['quantity']) ? number_format($oggetto['quantity'], 2, ',', '.') : null ?></td>
                        <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['Units']['description']) ? $oggetto['Units']['description'] : null ?></td>
                        <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['quote_good_row_price']) ? number_format($oggetto['quote_good_row_price'], 2, ',', '.') . ' €' : null ?></td>
                        <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['quote_good_row_price']) && isset($oggetto['quantity']) ? number_format($oggetto['quote_good_row_price'] * $oggetto['quantity'], 2, ',', '.') . ' €' : null ?></td>
                    </tr>
                    <!--tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr-->
                    <?php
                    break;
                case 'S':
                    ?>
                    <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
                    <tr >
                        <td style="font-size:10px;"><b><?= $oggetto['description'] ?></b></td><td></td><td></td><td></td><td style="text-align:right;font-size:10px;"><b><?= number_format($subtotale,2,',','.'). ' €' ; ?></b></td>
                    </tr>
                    <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
                    <?php
                     $subtotale = 0 ;
                    break;
                case  'O':
                    ?>
            </tbody>
            </table>
            <br/>
            <div><?= $oggetto['description'] ?></div>
            <br/>
            <table style="border-spacing: 0;">
                <thead>
                <tr >
                    <th style="width:80mm;<?= $tableHeaderStyle; ?>">Descrizione articolo</th>
                    <th style="width:30mm;<?= $tableHeaderStyle; ?>">Quantità</th>
                    <th style="width:25mm;<?= $tableHeaderStyle; ?>">U.M.</th>
                    <th style="width:25mm;<?= $tableHeaderStyle; ?>">Prezzo unitario</th>
                    <th style="width:25mm;<?= $tableHeaderStyle; ?>">Totale</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    break;
            }

             $totale += $oggetto['quote_good_row_price'] * $oggetto['quantity'];
            $subtotale += $oggetto['quote_good_row_price'] * $oggetto['quantity'];
    }
		?>

                <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
                <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
		<tr>
			<td style="font-size:12px;"><b><?= isset($settings['Setting']['quoteTotalDescription']) ? $settings['Setting']['quoteTotalDescription'] : 'TOTALE' ?></b></td><td></td><td></td><td></td><td style="text-align:right;font-size:12px;"><b><?= number_format($totale,2,',','.'). ' €' ; ?></b></td>
		</tr>
	</tbody>
</table>
<br/>
<?php
 if(isset($quote['Quote']['elimination']) && $quote['Quote']['elimination'] != '')
 { ?>
<table style="width:100%;border-spacing: 0;">
    <thead>
    <tr>
        <th  style="width:100%;<?= $tableHeaderStyleLeft; ?>">
            <?= isset($settings['Setting']['eliminationdescription']) ? $settings['Setting']['eliminationdescription'] : '';  ?>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr><td>&nbsp;</td></tr>
    <tr><td style="font-size:10px;"><?= nl2br($quote['Quote']['elimination']); ?></td></tr>
    <tr><td>&nbsp;</td></tr>
    </tbody>
</table>
<?php } ?>

<?php

if(isset($quote['Quote']['validity']) || (isset($quote['Quote']['payment']) && $quote['Client']['payment_id'] != null))
{ ?>
<table style="width:100%;border-spacing: 0;">
    <thead>
    <tr>
        <th  style="width:100%;<?= $tableHeaderStyleLeft; ?>">CONDIZIONI COMMERCIALI</th>
    </tr>
    </thead>
    <tbody>
        <tr><td>&nbsp;</td></tr>
        <?php if (isset($quote['Quote']['validity'])) { ?><tr><td style="font-size:10px;">Validità: <?= $quote['Quote']['validity'] ?> </td></tr> <?php } ?>
        <?php if (isset($quote['Quote']['payment']) && $quote['Client']['payment_id'] != null) { ?><tr><td style="font-size:10px;">Pagamento:  <?= $quote['Quote']['payment'] ?> </td></tr><?php } ?>
        <tr><td>&nbsp;</td></tr>
    </tbody>
    </table>
<?php } ?>
<table style="width:100%;border-spacing: 0;">
    <thead>
    <tr>
        <th  style="width:100%;<?= $tableHeaderStyleLeft; ?>">PER CONFERMA</th>
    </tr>
    </thead>
    <tbody>
    <tr><td>&nbsp;</td></tr>
    <tr><td style="font-size:10px;">Data: </td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td style="font-size:10px;">Timbro/Firma: ________________________ </td></tr>
    <tr><td>&nbsp;</td></tr>
    </tbody>
</table>
<table>
    <tbody>
      <tr><td width = "20%" style="font-size:10px;text-align:left;">Grazie e cordiali saluti,</td><td width = "80%"></td></tr>
      <tr><td width = "20%" style="font-size:10px;"><?= $settings['Setting']['name'] ?></td><td width = "80%"></td></tr>
    </tbody>
</table>

<htmlpagefooter name="myfooter"><?= $this->element($pdfFooter); ?></htmlpagefooter>

	