
<label class="form-label form-margin-top">
    <strong><?= $text ?></strong>
    <?=  isset($required) && $required == 'true' ? '<i class="fa fa-asterisk"></i>' : null ?>
</label>