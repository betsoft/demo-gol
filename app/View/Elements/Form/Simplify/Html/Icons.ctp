<?php // $this->webroot; ?>


<?php

        $blue ="#589AB8";
        $orange = "orange";
        $green = "green";
        $grey = '#CECECE';
        $red = 'red';
        $black = 'black';

    switch($Type) {
        // Icone sezione prenotazioni
        case 'iconaChiudiPrenotazione':
            $typeOfIcon = "Metronic";
            $class = "fa fa-folder-open";
            $color = $orange;
            $cursor= "pointer";
            break;

        case 'iconaPrenotazioneChiusa':
            $typeOfIcon = "Metronic";
            $class = "fa fa-folder";
            $color = "green";
            $cursor= "pointer";
            break;
        case 'iconaPrenotazioneNonAnnullabile':
            $typeOfIcon = "Metronic";
            $class = "fa fa-check";
            $color = $grey;
            $cursor= "pointer";
            break;
        case 'iconaAnnullaPrenotazione':
            $typeOfIcon = "Metronic";
            $class = "fa fa-check";
            $color = $green;
            $cursor= "pointer";
            break;
        case 'iconaPrenotazioneAnnullata':
        case 'iconaKo': // Per sezione preventivi
            $typeOfIcon = "Metronic";
            $class = "fa fa-close";
            $color = $red;
             $cursor= "pointer";
            break;
        case 'iconaKoGreen': // Per sezione preventivi
            $typeOfIcon = "Metronic";
            $class = "fa fa-close";
            $color = $green;
            $cursor= "pointer";
            break;
        case 'iconaKoOff': // Per sezione preventivi
            $typeOfIcon = "Metronic";
            $class = "fa fa-close";
            $color = $grey;
            $cursor= "pointer";
            break;
        case 'iconaPrenotazioneNonChiudibile':
            $typeOfIcon = "Metronic";
            $class = "fa fa-folder";
            $color = $grey;
            $cursor= "pointer";
            break;
        // BARCODE
        case 'barcodeIcon':
            $typeOfIcon = "Metronic";
            $class = "fa fa-barcode";
            $color = $black;
            $cursor= "pointer";
            break;
        case 'barcodeIconOff':
            $typeOfIcon = "Metronic";
            $class = "fa fa-barcode";
            $color = $grey;
            $cursor= "not-allowed";
            break;
        // Globali
        case 'iconaModifica':
            $typeOfIcon = "SVG";
            $image = "gestionale-online.net-modifica.svg";
            $cursor= "pointer";
            break;
        case 'iconaModificaOff':
            $typeOfIcon = "SVG";
            $image = "gestionale-online.net-modifica-off.svg";
            $cursor= "not-allowed";
            break;
            // Serial Number
        case 'serialNumber':
            $typeOfIcon = "Metronic";
            $class = "fa fa-code blue";
            $color = $blue;
            $cursor= "pointer";
            break;
        case 'serialNumberOff':
            $typeOfIcon = "Metronic";
            $class = "fa fa-code";
            $color = $grey;
            $cursor= "not-allowed";
            break;
        default:
            $typeOfIcon = null;
            break;
    }

    switch ($typeOfIcon) {
        case "Metronic":
            ?><i class="<?= $class ?> <?= isset($js) ? $js : '' ?>"
                 style="font-size:20px;cursor:<?= $cursor; ?>;vertical-align: middle;color:<?= $color ?>" title="<?= $title ?>"  attrId = "<?= isset($attrId) ? $attrId : '' ?>"></i><?php
            break;
        case "SVG":
            ?>
                <img src="<?= $this->webroot ?>img/gestionaleonlineicon/<?= $image ?>" alt="<?= $title ?>"  class="golIcon <?= isset($js) ? $js : '' ?>"  title = "<?= $title ?>" >
            <?php
            break;
        default:
            break;
    }

?>
