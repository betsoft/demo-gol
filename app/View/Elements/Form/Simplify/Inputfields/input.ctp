
<div class="col-md-<?= $dimension ?>">
    <?= $this->element('Form/Simplify/Html/Label',['text'=>$label]) ?>
    <div class="form-controls">
<?php
    if(isset($value)) {
        echo $this->Form->input($name, ['div' => false, 'label' => false, 'class' => 'form-control', 'value' => $value]);
    }
    else {
        echo $this->Form->input($name, ['div' => false, 'label' => false, 'class' => 'form-control']);
    } ?>
    </div>
</div>