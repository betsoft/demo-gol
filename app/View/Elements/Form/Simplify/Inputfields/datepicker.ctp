<div class="col-md-<?= $dimension ?>">
    <?= $this->element('Form/Simplify/Html/Label',['text'=>$label]) ?>
    <div class="form-controls">
        <input type="datetime" id="<?= $id ?>" class="datepicker form-control" name="<?= $name ?>>"/>
    </div>
</div>