<div class="col-md-<?= $dimension ?>">
    <?= $this->element('Form/Simplify/Html/Label',['text'=>$label, 'required' => 'true']) ?>
    <div class="form-controls">
        <?php if(isset($value)) {
            echo $this->Form->input($name, ['div' => false, 'label' => false, 'class' => 'form-control', 'required' => true, 'value' => $value]);
        }
        else {
            echo $this->Form->input($name, ['div' => false, 'label' => false, 'class' => 'form-control', 'required' => true]);
        }
        ?>
    </div>
</div>