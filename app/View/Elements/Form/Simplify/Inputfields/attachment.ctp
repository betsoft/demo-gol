
<div class="col-md-<?= $dimension ?>">
    <?= $this->element('Form/Simplify/Html/Label',['text'=>$label]) ?>
    <div class="form-controls">
        <?= $this->Form->input($name, ['div' => false, 'label' => false, 'type' => 'file', 'class' => 'form-control attachment']); ?>
    </div>
</div>