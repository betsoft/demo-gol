
<?= $this->Html->script	([ 'plugins/bootstrap/js/bootstrap.js']); ?>
<?php
 if (MODULO_CANTIERI)
        {
            ?>
            <div class="form-group col-md-12">

                <div class="col-md-2" style="float:left;">
                    <label class="form-label form-margin-top">
                        <strong>Cantiere</strong>
                    </label>
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => 'constructionsite_id',
                            "aggregator" => '',
                            "prefix" => "constructionsite_quote",
                            "list" => $constructionsites,
                            "options" => [ 'multiple' => false,'required'=> false],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-2" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top"><strong>Descrizione</strong></label>
                    <div class="form-controls">
                        <?= $this->Form->input('description', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
                    </div>
                </div>
                <div class="col-md-2" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top"><strong>CIG</strong></label>
                    <div class="form-controls">
                        <?= $this->Form->input('cig', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
                    </div>
                </div>
                <div class="col-md-2" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top"><strong>CUP</strong></label>
                    <div class="form-controls">
                        <?= $this->Form->input('cup', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
	
