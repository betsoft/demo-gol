
<script>
    var enhancedDialogsTypes = {
        OK : 0,
        YES : 1,
        NO : 2,
        CANCEL : 3,
        CLOSE : 4,
        SAVE :5,
    }
    
    //define global function dialog()
    var Frizzydialog = _showDialog;
    
    //save ugly alert() and confirm() functions
   // var uglyAlert = alert
    // var uglyConfirm = confirm
    
    //overwrite global function alert() and confirm() 
    var Frizzyalert = _showAlertInterface;
    var Frizzyconfirm = _showConfirmInterface;
    var Frizzysaveconfirm = _showSaveConfirmInterface;
    
    var SHOW_ENHANCED_DIALOG_CLASS = 'enhanced-dialog-container-show';

    function _showAlertInterface(message, title) {
        title = title || "Messaggio"
        message = message || ""
        
        _showDialog(title, message)
    }
    
    function _showConfirmInterface(message, title, callback) {
        title = title || "Attenzione"
        message = message || ""

        _showDialog(title, message, callback, [enhancedDialogsTypes.YES, enhancedDialogsTypes.NO])
    }
    
     function _showSaveConfirmInterface(message, title, callback,html) {
        title = title || "Attenzione"
        message = message || ""

        _showDialog(title, message, callback, [enhancedDialogsTypes.SAVE, enhancedDialogsTypes.CANCEL],html)
    }
    
    function _showDialog(title, message, callback, type, html) {
        title = title || "Finestra di dialogo";
        
        var enhancedDialog = $('#enhanced-dialog');
        if(enhancedDialog.length != 0) {
            var enhancedDialogContainer = enhancedDialog.parents('.enhanced-dialog-container').first();
    
            if(!$.isArray(type))
                type = [enhancedDialogsTypes.OK];
            
            //building enhancedDialog GUI
            enhancedDialog.find('.enhanced-dialog-title-text').text(title);
            enhancedDialog.find('.enhanced-dialog-message-text').html(message);
            
            if(html !== undefined)
            {
                enhancedDialog.find('.enhanced-dialog-message').html(html);
            }
            
            enhancedDialog.find('.enhanced-dialog-buttons button[enhanced-dialog-button-type]').addClass('hidden')
            $.each(type, function(key, element) {
                if($.inArray(element, enhancedDialogsTypes)) {
                    var button = enhancedDialog.find('.enhanced-dialog-buttons button[enhanced-dialog-button-type="' + element + '"]')
                    button.removeClass('hidden')
                }
            })
            
            //binding button's click
            enhancedDialog.find('.enhanced-dialog-buttons button[enhanced-dialog-button-type]').unbind('click')
            enhancedDialog.find('.enhanced-dialog-buttons button[enhanced-dialog-button-type]').click(function() {
                var userChoice = $(this).attr('enhanced-dialog-button-type');
    
    	        //perform callback function call passing user choice as an argument
                if($.isFunction(callback)) {
    		        try {
        		        var shouldHideDialog = callback(userChoice)
                    }
                    catch(err) {
                        console.warn(err)
                    }
                }
                
                if(shouldHideDialog !== false)
                {
                    //hiding enhancedDialog
                    enhancedDialogContainer.removeClass(SHOW_ENHANCED_DIALOG_CLASS)
                }
            })
            
            //making the enhancedDialog not right clickable
            enhancedDialogContainer.bind('contextmenu', function(e) {
                return false;
            })
            
            //removing focus from any input on the page
            $(document.activeElement).blur();
            enhancedDialogContainer.focus();
            
            //showing enhancedDialog
            enhancedDialogContainer.addClass(SHOW_ENHANCED_DIALOG_CLASS);
        }
        else {
            //backup on ugly alert
            if(callback == undefined)
                uglyAlert(message)
            else {
                var response = uglyConfirm(message)
                callback(response ? enhancedDialogsTypes.YES : enhancedDialogsTypes.NO)
            }
            console.warn('Falling back to default\'s dialog boxes due to EnhancedDialogs being not correctly loaded or dialog() function being called too early. To fix this issue be sure to wait until every DOM element is loaded into the page (e.g. after window.onload() or $(document).ready events)')
        }
    }
</script>

<?= $this->Html->css('EnhancedDialogs/default') ?>
<?= $this->Html->css('EnhancedDialogs/custom') ?>

