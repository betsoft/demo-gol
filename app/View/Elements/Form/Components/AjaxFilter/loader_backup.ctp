<script>
	
	addLoadEvent(function() {
		initializeAjaxFilter()
	}, 'ajaxFilterLoader')
	
	function initializeAjaxFilter() {
		var filterInput = $(".ajax-filter-input");
		
		var event = filterInput.attr('bind-filter-event');
		filterInput.on(event, function() {
			ajaxFilterPerform("<?=  Router::url( $this->here, true ); ?>");
		});
		
		//add ajax on paginator link's click
		initializeAjaxPaginator(function(link, href) {
			ajaxFilterPerform(href);
		});
	}
	
	function ajaxFilterGetParameters() {
	    return $('.ajax-filter-input').serialize();
	} 
	
	var lastAjax = null;
	function ajaxFilterPerform(url) {
		
		if(lastAjax != null) {
			lastAjax.abort();
		}

		if(!isLoading)
			ajaxFilterStartLoading();
		
        var beforeAjaxFilterCallback = window['beforeAjaxFilterCallback'];
		if($.isFunction(beforeAjaxFilterCallback)) {
	        try {
		        beforeAjaxFilterCallback(url)
            }
            catch(err) {
                console.warn(err)
            }
        }
		
		lastAjax = $.ajax({
			url: url,
			type: "POST",
			data: ajaxFilterGetParameters(),
			success:function(data) {
				$(".ajax-filter-table-content").html($(data).find(".ajax-filter-table-content").html());
				$(".paginator").html($(data).find(".paginator").html());
				
				initializeAjaxPaginator(function(link, href) {
        			ajaxFilterPerform(href);
        		});
        		
				ajaxFilterStopLoading();

				console.log(window['afterAjaxFilterCallback']);
				
				var afterAjaxFilterCallback = window['afterAjaxFilterCallback'];
				if($.isFunction(afterAjaxFilterCallback)) {
			        try {
				        afterAjaxFilterCallback(data)
		            }
		            catch(err) {
		                console.warn(err)
		            }
		        }
			},
			error: function(request, error)  {},
			complete: function(request, error) {
				lastAjax = null;
			}
		});
	}
	
	var isLoading = false;
	function ajaxFilterStartLoading() {
		isLoading = true;
		$('.ajax-filter-loading-container').remove();
		
		var loading = $('<?= addslashes(str_replace(["\n","\r"], '', $this->element('Form/Components/AjaxFilter/animation'))) ?>');
		var loadingContainer = $("<div>", {"class":"ajax-filter-loading-container"});
		
		loadingContainer.append(loading);
		
		$(".ajax-filter-table-content").parents('table').first().css('position','relative').append(loadingContainer);
		$(".ajax-filter-table-content").addClass('ajax-filter-loading-background');
	}
	
	function ajaxFilterStopLoading() {
		$('.ajax-filter-loading-container').remove();
		$(".ajax-filter-table-content").removeClass('ajax-filter-loading-background');
		isLoading = false;
	}

</script>

<?= $this->Html->css('AjaxFilter/default') ?>
<?= $this->Html->css('AjaxFilter/custom') ?>