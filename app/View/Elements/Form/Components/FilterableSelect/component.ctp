<?php
	/*
	$name:
		nome dell'input
	$aggregator:
		prefisso che verrà usato per aggregare nel medesimo array i campi indirizzo
		appartenenti allo stesso form
	$prefix:
		prefisso che verrà usato per garantire l'univocità di ciascun id
		anche in presenza di più elementi di questo stesso tipo all'interno della stessa pagina
	$list:
		list of values to fill the select with
	$options[
		'multiple':
			indica se la select accetta selezioni multiple
		'label': 
			true/false indica se è visibile la lable 
		'required':
			corrisponde a 'required' se il campo è necessario ai fini della validazione
	]
	*/
	
	if(!isset($list))
		$list = [];
	
	$defaultOptions = [
		'multiple' => 'multiple',
		'label' => false,
		'required' => false,
		'empty' => 'Seleziona un valore',
		'default' => null,
		'class' => '',
		'style' => 'opacity:0',
		'required' => false,
		'value' => null
	];
	
	if(!isset($options))
		$options = [];
	
	if(!isset($actions)) //array di elementi html che verranno wrappati in un elemento <button>
		$actions = [];
		
	if(!isset($class))
		$class = '';
	
	if(!isset($id))
	{
		if(!isset($prefix))
			$prefix = uniqid();
			
		$id = $prefix.'_multiple_select';
	}
	
	if(!isset($autoload))
		$autoload = 'true';
		
	if(isset($autoload) && $autoload === false)
		$autoload = 'false';
	
	$options = array_merge($defaultOptions, $options);
	$options['id'] = $id;
	$options['class'] = 'multiple-select ' . $options['class'];
	$options['autoload'] = $autoload;
	$options['required'] &= count($list) > 0;
	$options['default-id'] = isset($options['default']) ? $options['default'] : null;
	
	if(isset($url))
	{
		$options['url'] = $url;
		$options['empty'] = "Inserisci almeno tre caratteri";
		
		if(!is_array($list))
			$list = $list->toArray();
		
		if($url && !isset($list[$options['default']]))
			$list = [];
	}	
	else
		$url = null;
?>

<div class="input-group multiple-select-container <?= $class ?> <?= count($actions) > 0 ? 'has-actions' : '' ?>">
    <?= $this->Form->select($aggregator.$name, $list, $options); ?>
    <?php if(count($actions) > 0): ?>
	    <span class="input-group-btn multiple-select-actions">
	   		<?php foreach($actions as $action): ?>
	   	        <button class="btn default multiple-select-action "  style="background-color:#589AB8 !important" type="button">
		   			<?= $action ?>
   		        </button>
	   		<?php endforeach; ?>
	    </span>
	<?php endif; ?>
</div>


<style>
	a.fa.fa-plus.mega-form-button
	{
		color:#ffffff  !important;
		padding: 7px 10px 7px 0px !important;
	}
</style>