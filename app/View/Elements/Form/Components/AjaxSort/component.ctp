<?php
    /*
	*/
?>

<?php foreach($elements as $key => $element): ?>
	<?php if($element[0] == "#actions"): ?>
		<th class="ajax-sort actions"><?= isset($element[1]) ? $element[1] : 'Azioni' ?></th>
	<?php else: ?>
	    <th class="ajax-sort">
			<?php if($element != null): ?>
				<?php if(is_array($element)): ?>
					<?php
						$field = $element[0];
						$alias = $element[1];
						if(isset($element[2]) && is_array($element[2]))
							$options = $element[2];
						else
							$options = [];
					?>
					<?php
						if($field == null)
						{
							echo $alias;
						}
						else
						{
							echo $this->Paginator->sort($field, $alias, $options) ;
						}
					?>
				<?php endif; ?>
			<?php endif; ?>
	    </th>
	<?php endif; ?>
<?php endforeach; ?>
