<script>
    addLoadEvent(function() {
        
        initializeAjaxPaginator(function() {
            console.warn('No ajax callback set for this paginator item');
            return false;
        })
    }, 'paginatorLoader');
    
    function initializeAjaxPaginator(callback) {
        $('.paginator-ajax span:not(.disabled) a').each(function() {
            var link = $(this);
            var href = link.attr('href');
            
            link.unbind('click.AjaxPaginator');
            link.on('click.AjaxPaginator', function() {
                if($.isFunction(callback)) {
    		        try {
                        callback(link, href)
                    }
                    catch(err) {
                        console.warn(err)
                    }
		        }
                return false;
            });
        })
    }
</script>

<?= $this->Html->css('Paginator/default') ?>
<?= $this->Html->css('Paginator/custom') ?>