<?php
    //se true genera soltanto il bottone del megaform
    if(!isset($buttonOnly))
        $buttonOnly = false;
        
    //se true genera soltanto il form del megaform
    if(!isset($formOnly))
        $formOnly = false;
        
    if(!isset($megaFormButtonTitle))
        $megaFormButtonTitle = '';
        
    if(!isset($url))
        $url = $this->Html->url(null);
    
    if(!isset($fields))
    	$fields = 'id,name';
    	
    if(!isset($hidden))
    	$hidden = false;
    
    if($hidden)
    	$style = 'width:0px;height:0px;padding:0px;border:0px;margin:0px';
    else
    	$style = "";
    
    if(!isset($fadingAnimation))
        $fadingAnimation = true;
    
    if(!isset($linkClass))
    	$linkClass = 'btn btn-outline btn-sm color';
    	
    if(!isset($class))
        $class = '';
        
    if(!isset($breadcrumbs))
        $breadcrumbs = true;

    //nome della funzione di callback da chiamare prima di mostrare/nascondere il form
    if(!isset($beforePerformShowOrHideCallback))
    	$beforePerformShowOrHideCallback = false;
    $beforePerformShowOrHideCallback = !$beforePerformShowOrHideCallback || !is_string($beforePerformShowOrHideCallback) ? '' : 'before-perform-show-or-hide-callback="' . $beforePerformShowOrHideCallback . '"';
    
    //nome della funzione di callback da chiamare prima dell'ajax di caricamento del form 
    if(!isset($beforeAjaxCallback))
    	$beforeAjaxCallback = false;
    $beforeAjaxCallback = !$beforeAjaxCallback || !is_string($beforeAjaxCallback) ? '' : 'before-ajax-callback="' . $beforeAjaxCallback . '"';
    
    //nome della funzione di callback da chiamare dopo l'ajax di caricamento del form 
    if(!isset($afterAjaxCallback))
    	$afterAjaxCallback = false;
    $afterAjaxCallback = !$afterAjaxCallback || !is_string($afterAjaxCallback) ? '' : 'after-ajax-callback="' . $afterAjaxCallback . '"';
    
    //nome dalla funzione di callback da chiamare dopo il salvataggio dei dati alla pressione del bottone "salva"
    if(!isset($afterSaveCallback))
    	$afterSaveCallback = false;
    $afterSaveCallback = !$afterSaveCallback || !is_string($afterSaveCallback) ? '' : 'after-save-callback="' . $afterSaveCallback . '"';
    
    //nome dalla funzione di callback da chiamare dopo la pressione del bottone "annulla"
    if(!isset($afterCancelCallback))
    	$afterCancelCallback = false;
    $afterCancelCallback = !$afterCancelCallback || !is_string($afterCancelCallback) ? '' : 'after-cancel-callback="' . $afterCancelCallback . '"';
    
    //nome dalla funzione di callback da chiamare all'inizializzazione di cascun megaform figlio del mega from corrente (verra sovrascritta in caso il megaform figlio abbia esplicitato questo setsso parametro)
    if(!isset($initializedInnerMegaFormCallback))
    	$initializedInnerMegaFormCallback = false;
    $initializedInnerMegaFormCallback = !$initializedInnerMegaFormCallback || !is_string($initializedInnerMegaFormCallback) ? '' : 'initialized-inner-mega-form-callback="' . $initializedInnerMegaFormCallback . '"';
?>

<?php if(!$formOnly): ?>
    <a class="<?= $linkClass ?> mega-form-button" style="<?= $style ?>" data-toggle="modal" href="#mega_form_<?= $megaFormIdSuffix . ($AJAX ? '_ajax' : '') ?>"><?= $megaFormButtonTitle ?></a>
<?php endif; ?>
<?php if(!$buttonOnly): ?>
    <div id="mega_form_<?= $megaFormIdSuffix . ($AJAX ? '_ajax' : '') ?>" class="modal <?= $fadingAnimation ? 'fade' : '' ?> mega-form <?= $class ?>" tabindex="-1" style="display: none;" url="<?= $url ?>" fields="<?= $fields ?>" <?= $beforeAjaxCallback ?> <?= $afterAjaxCallback ?> <?= $afterSaveCallback ?> <?= $afterCancelCallback ?> <?= $initializedInnerMegaFormCallback ?> <?= $beforePerformShowOrHideCallback ?>>
    	<div class="modal-dialog">
    		<div class="modal-content" >
    			<div class="modal-header">
                    <?php if($breadcrumbs): ?>
                        <div class="mega-form-breadcrumbs breadcrumbs"></div>
                    <?php endif; ?>
                    <h4 class="modal-title"><?= $megaFormButtonTitle ?></h4>
    			</div>
    			<div class="modal-body">
                	<div class="slim-scroll-div">
                    	<div data-always-visible="1" data-rail-visible1="1" data-initialized="1">
    			  	        <div class="mega-form-data-container"></div>
    			  		</div>
    			  	</div>
    			</div>
    		</div>
    	</div>
    </div>
<?php endif; ?>