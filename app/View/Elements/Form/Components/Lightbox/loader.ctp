<script>
    addLoadEvent(function() {
        $('.lightbox').bind('contextmenu', function() {
            return false;
        })
    }, 'lightBoxLoader')

    function showLightBox() {
        $('.lightbox').addClass('lightbox-show');
    }
    
    function hideLightBox() {
        $('.lightbox').removeClass('lightbox-show');
    }
</script>

<?= $this->Html->css('Lightbox/default') ?>
<?= $this->Html->css('Lightbox/custom') ?>