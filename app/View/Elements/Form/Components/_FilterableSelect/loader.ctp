<!-- SCRIPT -->
<?= $this->Html->script('../metronic/assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect') ?>

<script>
    addLoadEvent(function() {
        console.log("A");
        initializeFilterableSelects()
    }, 'FilterableSelectLoader');
    
    function initializeFilterableSelects(id) {
        console.log("B");
        $('.multiple-select').each(function() {
            console.log("C");
            var select = $(this);
            if(id == undefined || select.attr('id') == id) {
                if(select.attr('autoload') !== "false" || id !== undefined) {
                    var selectContainer = select.parents('.multiple-select-container').first();
                    var searchInput = select.next('.btn-group').find('.multiselect-search');
                    var prefix = select.attr('id').split('_multiple_select')[0];
                    console.log(select);
                    select.multiselect({
                        //css
                        buttonWidth: '100%',
                        
                        //selection
                        includeSelectAllOption: true,
                        selectAllText: 'Seleziona tutti',
                        allSelectedText: 'Tutti',
                        nonSelectedText: 'Nessuno',
                        numberDisplayed: 1,
                        
                        //filtering
                        enableFiltering: true,
                        filterPlaceholder: 'Cerca',
                        enableCaseInsensitiveFiltering: true,
                        
                        //option groups
                        enableClickableOptGroups: false,
                        enableCollapsibleOptGroups: true,
                        
                        //callbacks
                        onDropdownShown: function(event) {
                            selectContainer.find('.multiselect-container.dropdown-menu .form-control.multiselect-search').focus();
                        }
                    });
                    
                    select.unbind('focus.FilterableSelect');
                    select.on('focus.FilterableSelect', function() {
                        selectContainer.find('.multiselect.dropdown-toggle').click();
                        selectContainer.find('.multiselect-container.dropdown-menu .form-control.multiselect-search').focus();
                    });
                    
                    selectContainer.find('.multiple-select-actions .multiple-select-action a, .multiselect-clear-filter').each(function() {
                        $(this).attr('tabindex', '-1');
                    });
                    
                    filterableSelectBindFilterUtilities(select.attr('id'));
                }
            }
        });
    }
    
    var lastAjax = null;
    function filterableSelectBindFilterUtilities(id) {
        var select = $('[id=' + id + ']');
        var searchInput = select.next('.btn-group').find('.multiselect-search');
        
        searchInput.unbind('keyup');
        searchInput.on('keyup', function (e) {
            if (e.keyCode == 13) {
                var displayedSelect = $(this).parents('.multiselect-container.dropdown-menu').first();
                var hiddenSelect = $(this).parents('.btn-group.open').first().prev('select');

                var lastItem;
                var optionsCount = 0;
                displayedSelect.find('li').each(function() {
                    if($(this).css('display') != 'none' && !$(this).hasClass('multiselect-filter')) {
                        optionsCount += 1;
                        lastItem = $(this);
                    }
                })
                
                if(optionsCount == 1) {
                    if(hiddenSelect.attr('multiple')) {
                        var oldValues = hiddenSelect.val();
                        var newValues = oldValues.push(lastItem.find('input').val())
                        hiddenSelect.val(newValues)
                    }
                    else {
                        hiddenSelect.val(lastItem.find('input').val())
                    }
                    
                    hiddenSelect.change();
                    hiddenSelect.multiselect('refresh');
                }
                
                console.log("trigger tab")
                $('input, select, textarea')
                [$('input,select,textarea').index(this) + 4].focus();
            }
            
            if(select.attr('url') && searchInput.val().length >= 3) {
                var url = select.attr('url')
                
                if(lastAjax != null) {
        			lastAjax.abort();
        		}
                
                lastAjax = $.ajax({
        			url: url,
        			type: "POST",
        			data: {
        			    filters: {
        			        list_value: searchInput.val()
        			    }
        			},
        			success:function(data) {
        				options = JSON.parse(data)

        				if(Object.keys(options).length > 0) {
        				    select.find('option').remove();
            				$.each(options, function(id, value) {
            				    select.append($('<option>').attr('value', id).text(value))
            				})
            				select.multiselect('rebuild');
            				initializeFilterableSelects(select.attr('id'))
        			        
        			        select.next('.btn-group').find('.multiselect-search').val(searchInput.val()).focus();
        				}
    			        select.change()
        			},
        			error: function(request, error)  {},
        			complete: function(request, error) {
        				lastAjax = null;
        		        
        		        return false;
        			}
        		});
            }
        });
    }
</script>

<!-- CSS -->
<?= $this->Html->css('../metronic/assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect') ?>

<?= $this->Html->css('FilterableSelect/default') ?>
<?= $this->Html->css('FilterableSelect/custom') ?>
