<div class="signature-pad" id="signature-pad-div">
    <?= $this->Form->input("signimage", array('type' => 'hidden', 'id' => 'signature', 'value' => '')); ?>
    <label class="form-label"><strong>Firma Cliente</strong><i class="fa fa-asterisk"></i></label>
    <canvas name="signature" id="signature-pad" class="signature-pad-canvas" style="border: black solid 1px;" height="300"></canvas>
    <span class="blue-button btn-outline dropdown-toggle" style="padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'" data-action="erase-signature-pad" id="clear">
        <i class="fa fa-refresh"></i>
    </span>
</div>