<?= $this->Html->script('signature_pad.min.js') ?>

<script>
    addLoadEvent(function () {
        initializeSignaturePad()
    }, 'initializeSignaturePad');

    var signaturePad;

    function initializeSignaturePad() {
        var canvas =  document.getElementById('signature-pad');
        var signatureDiv =  document.getElementById('signature-pad-div');
        var cancelButton = signatureDiv.querySelector("[data-action=erase-signature-pad]");

        signaturePad = new SignaturePad(canvas, {
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(0, 0, 0)',
            onEnd: () => {
                $('#signature').val(signaturePad.toDataURL());
            }
        });

        if($('#signature').val().search('data:image/png;base64') !== -1){
            signaturePad.fromDataURL($('#signature').val());
        }

        cancelButton.addEventListener('click', function (event) {
            signaturePad.clear();
            return;
        });
        resizeCanvas();
        window.addEventListener("resize", resizeCanvas);
    }

    function resizeCanvas() {
        var canvas =  document.getElementById('signature-pad');
        var ratio =  1;
        var signatureDiv =  document.getElementById('signature-pad-div');

        canvas.width = (signatureDiv.offsetWidth) * ratio;
        canvas.height = canvas.offsetHeight;
        canvas.getContext("2d").scale(ratio, ratio);
        signaturePad.clear();
    }

</script>

