<?php $paddingWidth =  "padding-top:7px;margin-right:10px;background-color:#589AB8;color:#fff;width:100%;min-height:60px;font-size:15pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;text-align:center;vertical-align:middle;float:left;padding-left:0px;padding-right:0px;"?>
<!--div class="form-group col-md-3 createReceipt"><div  style="<?= $paddingWidth ?>"><?= 'GENERA SCONTRINO' ?></div></div-->
<div class="form-group col-md-3 createReceipt" ><div  style="<?= $paddingWidth ?>"><?= 'GENERA SCONTRINO FISCALE' ?></div></div>
<div class="form-group col-md-3" onclick="singleSendToPrinter('printerNonFiscalReceipt')"  ><div  style="<?= $paddingWidth ?>"><?= 'GENERA SCONTRINO NON FISCALE' ?></div></div>
<div class="form-group col-md-3"  onclick="singleSendToPrinter('openDrawer')" id="singleSendButton" ><div  style="<?= $paddingWidth ?>"><?= 'APERTURA CASSETTO' ?></div></div>
<div class="form-group col-md-3"  onclick="singleSendToPrinter('printZReport')" id="singleSendButton" ><div  style="<?= $paddingWidth ?>"><?= 'CHIUSURA FISCALE' ?></div></div>


<!--input name="singleSendButton" id="singleSendButton" type="button" value="Single Send" onclick="singleSendToPrinter()" -->
<div id="blocco_receive" class="col-md-5" style="border:1px solid black;min-height:50px;">
    <div id="displayURL"></div>
    <div id="blocco_receive_result"> </div>
    <div id="blocco_receive_add_info">   </div>
</div>
<printRecItem  operator="1" description="articolo 1" quantity="1" unitPrice="25" department="1" justification="1" />
<?=  $this->Html->script(['fiscalprint.js']) ?>
<script>


   // $(document).ready(
   //     function() {
    // Pulisce il div di risposta
    function clearResponseMessages()
    {
        document.getElementById("blocco_receive_result").innerHTML = "";
        document.getElementById("blocco_receive_add_info").innerHTML = "";
    }



    function opaticize(opaticize,isReceipt) {
        if (opaticize == true) {
            $("#content").css('pointer-events', 'none');
            $("#content").css('opacity', '0.4');
            if(isReceipt == true) {
                $(".sending").show();
            }
        }
        else
            {

            }
    }

    function multiSendToPrinter(typeOfCommand)
    {
       var newMulti = 0;
       sendInProgress=true;
        singleSendToPrinter(typeOfCommand);
      //  $("#content").css('opacity','1');

        /* Non cancellare */
       /* var myVar = self.setInterval(function () {
            if(newMulti > 2)
            {
                clearInterval(myVar);
                alert("Tre tentativi falliti");
            }
            else {
                singleSendToPrinter(typeOfCommand);
                newMulti++;
                console.log(newMulti);
            }
        }, 10000);
*/



    }




    function singleSendToPrinter(typeOfCommand)
    {

        clearResponseMessages();
        var epos = new epson.fiscalPrint();
        epos.onreceive = function (result, tag_names_array, add_info, res_add)
        {
            console.log(result);

            var add_info_text = "";
            var lowPaper = 0;
            if (tag_names_array.length > 0)
            {
                add_info_text = "Additional Information<br>";

                for (var i = 0; i < tag_names_array.length; i++)
                {
                    if (tag_names_array[i] == "printerStatus" && add_info[tag_names_array[i]].substring(0, 1) == "2")
                        lowPaper = 1;
                    add_info_text =	add_info_text + tag_names_array[i] + " = ";
                    var tag_names_array_respaced = "";
                    for (var k = 0; k < add_info[tag_names_array[i]].length; k++)
                    {
                        if (add_info[tag_names_array[i]].substring(k, k + 1) == " ")
                        {
                            tag_names_array_respaced = tag_names_array_respaced + "&nbsp;";
                        }
                        else
                        {
                            tag_names_array_respaced = tag_names_array_respaced + add_info[tag_names_array[i]].substring(k, k + 1);
                        }
                    } // end for (var k = 0; k < add_info[tag_names_array[i]].length; k++)

                    add_info_text =	add_info_text + tag_names_array_respaced + "<br>";
                    if (tag_names_array[i] == "lineCount" || tag_names_array[i] == "deviceCount")
                    {
                        var node2;
                        var node_child2;
                        var node_val2;
                        var node_val2_respaced = "";
                        if (tag_names_array[i] == "lineCount")
                        {
                            for (var j = 1; j <= add_info[tag_names_array[i]]; j++)  // lineCount or deviceCount value
                            {
                                node2 = res_add[0].getElementsByTagName("lineNumber" + j)[0];
                                node_child2 = node2.childNodes[0];
                                try
                                {
                                    node_val2 = node_child2.nodeValue;
                                }
                                catch(err) // Blank lines generate exceptions
                                {
                                    // Add 46 spaces to make blank line
                                    node_val2 = "                                              ";
                                }

                                for (var k = 0; k < node_val2.length; k++)
                                {
                                    if (node_val2.substring(k, k + 1) == " ")
                                    {
                                        node_val2_respaced = node_val2_respaced + "&nbsp;";
                                    }
                                    else
                                    {
                                        node_val2_respaced = node_val2_respaced + node_val2.substring(k, k + 1);
                                    }
                                } // end for (var k = 0; k < node_val2.length; k++)

                                add_info_text = add_info_text + "&nbsp;&nbsp;&nbsp;" + node_val2_respaced + "<BR>";
                                node_val2 = "";
                                node_val2_respaced = "";
                            } // End for (var j = 1; j <= add_info[tag_names_array[i]]; j++)
                            add_info_text = '<font face="courier">' + add_info_text + '</font>'; // need to move to css
                        } // End if (tag_names_array[i] == "lineCount")

                        else if (tag_names_array[i] == "deviceCount")
                        {
                            var deviceAttributes = [
                                "deviceId",
                                "deviceIp",
                                "deviceType",
                                "deviceModel",
                                "deviceRetry"
                            ];
                            for (var j = 1; j <= add_info[tag_names_array[i]]; j++)  // deviceCount value
                            {
                                add_info_text = add_info_text + "&nbsp;&nbsp;&nbsp;Device " + j + " has the following settings:<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                for (var k = 0; k < deviceAttributes.length; k++)
                                {
                                    try
                                    {
                                        node2 = res_add[0].getElementsByTagName("deviceNumber" + j)[0].attributes;
                                        node_val2 = node2.getNamedItem(deviceAttributes[k]).nodeValue;
                                    }
                                    catch(err)
                                    {
                                        alert(err);
                                    }

                                    add_info_text = add_info_text + deviceAttributes[k] + " = " + node_val2 + "&nbsp;";
                                    node_val2 = "";
                                } // End for (var k = 0; k < deviceAttributes.length; k++)

                                add_info_text = add_info_text + "<p>";
                            } // End for (var j = 1; j <= add_info[tag_names_array[i]]; j++)
                        } // End else if (tag_names_array[i] == "deviceCount"
                    } // End if (tag_names_array[i] == "lineCount" || tag_names_array[i] == "deviceCount")
                } // End for (var i = 0; i < tag_names_array.length; i++)
            }
            else {
                add_info_text = "No Additional Information";
            } // End if (tag_names_array.length > 0)

            document.getElementById("blocco_receive_result").innerHTML =
                "Success = " + result.success + "<br>" +
                "Code = " + result.code + "<br>" +
                "Status = " + result.status;

            if (result.success == 0 && document.getElementById('multiSendButton').value == "Cancel")
            {
                document.printerSelectionForm.multiSendErrors.value++;
            }
            document.getElementById("blocco_receive_add_info").innerHTML = add_info_text;

            if (lowPaper == 1)
            {
                // document.getElementById('multiSendButton').value = "Multi Send";
                //document.getElementById('multiSendButton').title = multiSendStartTitle;
                //multi=window.clearInterval(multi);
            }

            opaticize(false, true);
            $("#content").css('pointer-events','auto');
            $("#content").css('opacity','1');
            $(".sending").hide();
            /*
            document.getElementById("valore2").innerHTML = res.code;
            document.getElementById("valore3").innerHTML = res.status;
            document.getElementById("valore4").innerHTML = add_info.cpuRel;
            document.getElementById("valore5").innerHTML = add_info.mfRel;
            document.getElementById("valore6").innerHTML = add_info.mfStatus;
            */
            // clearXML();
            // clearMyAttributes();
        } // End epos.onreceive = function (result, tag_names_array, add_info, res_add)

        epos.onerror = function (result)
        {
            console.log(result);
            $.alert
             ({
                 icon: 'fa fa-warning',
                 title: 'MANCATO INVIO',
                 content: "Attenzione, errore durante la trasmissione dei dati.",
                 type: 'orange',
             });
            document.getElementById("blocco_receive_result").innerHTML =
                "Success = " + result.success + "<br>" +
                "Code = " + result.code + "<br>" +
                "Status = " + + result.status;
            $("#content").css('pointer-events','auto');
            $("#content").css('opacity','1');
            $(".sending").hide();

        } // End epos.onerror = function (result)

        // Root of singleSendToPrinter() function
        var query_string = "";

        if (query_string == "")
        {
            (query_string = "?");
        }
        else
        {
            query_string = query_string + "&";
        }
        query_string = query_string + "timeout=" + 6000;




        switch(typeOfCommand)
        {
            case 'printZReport':
                opaticize(true, false);
                var data_to_send = "";
                var fileType = "printerFiscalReport"; // Apertura cassetto
                var cumulative_XML_Lines = '<printZRport  operator="1"  timeout="30"/>\n';
                data_to_send = "<" + fileType + ">\n" + cumulative_XML_Lines + "</" + fileType + ">\n";
                /*
                var xmlHTTPRequestDestination = "";
                xmlHTTPRequestDestination = "<?= $userCashRegisterAddress; ?>";
                var xmlHTTPRequestURL = window.location.protocol + "//" + xmlHTTPRequestDestination + "/cgi-bin/fpmate.cgi" + query_string;
                document.getElementById("displayURL").innerHTML = "URL = " + xmlHTTPRequestURL;
                epos.send(xmlHTTPRequestURL, data_to_send, 6000);
                console.log(data_to_send);
                data_to_send = ""; // Just in case
                */

                break;
            case 'openDrawer':
                opaticize(true, false);
                var data_to_send = "";
                var fileType = "printerCommand"; // Apertura cassetto
                var cumulative_XML_Lines = '<openDrawer  operator="1" />\n'
                data_to_send = "<" + fileType + ">\n" + cumulative_XML_Lines + "</" + fileType + ">\n";
                //console.log(data_to_send);
                //console.log(xmlHTTPRequestDestination);
                // console.log(xmlHTTPRequestURL);
                /*var xmlHTTPRequestDestination = "";
                xmlHTTPRequestDestination = "<?= $userCashRegisterAddress; ?>"
                var xmlHTTPRequestURL = window.location.protocol + "//" + xmlHTTPRequestDestination + "/cgi-bin/fpmate.cgi" + query_string;
                document.getElementById("displayURL").innerHTML = "URL = " + xmlHTTPRequestURL;
                epos.send(xmlHTTPRequestURL, data_to_send, 6000);
                data_to_send = ""; // Just in case*/

             break;
            case 'createReceipt':
                opaticize(true, true);
                var data_to_send = "";
                var fileType = "printerFiscalReceipt"; // Apertura cassetto
                var cumulative_XML_Lines = '<beginFiscalReceipt  operator="1" />\n'
                /* Vendite */
                //$(".clonedRow").each(
                $(".recrow").each(
                    function() {
                        if($(this).hasClass('clonedRow')) {
                            var description = $(this).find('.name').html();
                            var quantity = $(this).find('.quantity').html();
                            var price = $(this).find('.price').html();
                            if(price >= 0) {
                                cumulative_XML_Lines += '<printRecItem  operator="1" description="' + description + '" quantity="' + quantity + '" unitPrice="' + (price / quantity).toFixed(2) + '" department="1" justification="1" />\n';
                            }else
                                {
                                    cumulative_XML_Lines += '<printRecRefund  operator="1" description="' + description + '" quantity="' + quantity + '" unitPrice="' + (-1 * price / quantity).toFixed(2) + '" department="1" justification="1" />\n';
                                }
                        }
                        if($(this).hasClass('clonedDiscountRow')) {
                            var description = $(this).find('.name').html();
                            var quantity = $(this).find('.quantity').html();
                            var rowDiscount = $(this).find('.rowDiscount').html();
                            console.log(rowDiscount);
                            console.log(quantity);
                            console.log(amount);
                            if(rowDiscount < 0) {
                                var amount = Math.abs(+quantity * +rowDiscount);
                                cumulative_XML_Lines += '<printRecItemAdjustment  operator="1"  description="' + description + '" adjustmentType="0" amount="' + (+amount).toFixed(2) + '" department="1" justification="1" />\n';
                            }
                        }
                    });

                /** Nel caso sia visibile il subtotale, allora viene agigunto */

                if($(".rowsubtotal").is(":visible")) {
                    cumulative_XML_Lines += '<printRecSubtotal operator="1" option="1"/>\n'; // option --> Only Print
                }

                /** Sconto sul subtotale dello scontrino **/

                if($(".rowReceiptDiscountFixed").is(":visible")) {
                    cumulative_XML_Lines += '<printRecSubtotalAdjustment  operator="1"  description="Sconto applicato al subtotale" adjustmentType="1" amount="' +  $(".rowReceiptDiscountFixed").find('.rowDiscountFixed').html() + '"  justification="1" />\n';
                }

                /** Definizione pagamenti **/

                if($(".cash").css('background-color') == 'rgb(0, 128, 0)' ) { payment =  2; }
                if($(".electronic").css('background-color') == 'rgb(0, 128, 0)' ) { payment =  0; }
                console.log(payment);
                console.log($(".cash").css('background-color'));
                console.log($(".electronic").css('background-color'));
                if(payment == 0) {
           //         cumulative_XML_Lines += '<printRecTotal  operator="1" description="Contanti" payment="' + '0,00' + '" paymentType="0"  justification="1" />';
                    cumulative_XML_Lines += '<printRecTotal  operator="1" description="Elettronico" payment="' + $(".totale").html() + '" paymentType="2"  justification="1" />';
                } else if(payment == 2)
                {
                    cumulative_XML_Lines += '<printRecTotal  operator="1" description="Contanti" payment="' +  $(".totale").html() +  '" paymentType="0"  justification="1" />';
             //       cumulative_XML_Lines += '<printRecTotal  operator="1" description="Elettronico" payment="' + '0,00' + '" paymentType="2"  justification="1" />';
                }

                cumulative_XML_Lines += '<endFiscalReceipt  operator="1" />';
                data_to_send = "<" + fileType + ">\n" + cumulative_XML_Lines + "</" + fileType + ">\n";
                break;
            case 'printerNonFiscalReceipt':
                opaticize(true, true);
                var data_to_send = "";
                var fileType = "printerNonFiscal"; // Apertura cassetto
                var cumulative_XML_Lines = '<beginNonFiscal  operator="1" />\n';
                        cumulative_XML_Lines +=  '<printNormal  operator="1"  font="1" data="prova di stampa." />\n';
       //                 cumulative_XML_Lines +=  '<printNormal  operator="1" font="2"  data ="test di comunicazione con la stampante fiscale."/>\n';
                cumulative_XML_Lines += '<endNonFiscal  operator="1" />\n';
                data_to_send = "<" + fileType + ">\n" + cumulative_XML_Lines + "</" + fileType + ">\n";
                break;
        }

        var xmlHTTPRequestDestination = "";
        xmlHTTPRequestDestination = "<?= $userCashRegisterAddress; ?>";
        var xmlHTTPRequestURL = window.location.protocol + "//" + xmlHTTPRequestDestination + "/cgi-bin/fpmate.cgi" + query_string;
        document.getElementById("displayURL").innerHTML = "URL = " + xmlHTTPRequestURL;
        epos.send(xmlHTTPRequestURL, data_to_send, 6000);
        console.log(data_to_send);
        data_to_send = ""; // Just in case


    }


        //}
</script>

<!--response success="true" code = "" status = "x">
    <addInfo>
       <elementList>lastCommand,printerStatus,fiscalReceiptNumber,fiscalReceiptAmount,fiscalReceiptDate,fiscalReceiptTime,zRepNumber</elementList>
       <lastCommand>74</lastCommand>
        <printerStatus>20010</printerStatus>
        <fiscalReceiptNumber>1234</fiscalReceiptNumber>
        <fiscalReceiptAmount>12345,22</fiscalReceiptAmount>
        <fiscalReceiptDate>17/06/2020</fiscalReceiptDate>
        <fiscalReceiptTime>00:22</fiscalReceiptTime>
        <zRepNumber>ZZZ</zRepNumber>
    </addInfo>
</response-->