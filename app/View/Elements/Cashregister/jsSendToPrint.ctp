<script>

    function sendReceipt()
    {
        //git print();
    }

    function cbConnect(data) {
        if (data == 'OK' || data == 'SSL_CONNECT_OK') {
            ePosDev.createDevice('local_printer', ePosDev.DEVICE_TYPE_PRINTER,
                { 'crypto': false, 'buffer': false }, cbCreateDevice_printer);
        } else {
            alert('Attenzione stampante non connessa');
        }
    }

    //TIMEOUT DI CONNESSIONE
    function cbCreateDevice_printer(devobj, retcode) {
        if (retcode == 'OK') {
            printer = devobj;
            printer.timeout = 60000;
            printer.onreceive = function (res) { alert(res.success); };
            printer.oncoveropen = function () { alert('coveropen'); };
            print();
        } else {
            alert(retcode);
        }
    }

    //SCONTRINO DA STAMPARE
   /* function print() {
        printer.addTextAlign(printer.ALIGN_CENTER);
        printer.brightness = 1.0;
        printer.halftone = printer.HALFTONE_ERROR_DIFFUSION;
        printer.addImage(context, 0, 0, 454, 165, printer.COLOR_1, printer.MODE_MONO);
        printer.addText('\n');
        printer.addTextPosition(5);
        printer.addText('#N 00001');
        printer.addTextPosition(10);
        printer.addTextPosition(390);
        printer.addText('27/03/2020\n\n\n');
        printer.addTextPosition(1);
        printer.addText('0001');
        printer.addTextPosition(100);
        printer.addText('Articolo 1');
        printer.addTextPosition(395);
        printer.addText('€ 100,00   \n');
        printer.addTextPosition(1);
        printer.addText('0015');
        printer.addTextPosition(100);
        printer.addText('Articolo 15');
        printer.addTextPosition(395);
        printer.addText('€ 18,95    \n');
        printer.addTextPosition(1);
        printer.addText('0245');
        printer.addTextPosition(100);
        printer.addText('Articolo 245');
        printer.addTextPosition(395);
        printer.addText('€ 1000,00  \n');
        printer.addText('\n');
        printer.addTextLineSpace(50);
        printer.addTextPosition(10);
        printer.addText('TOTALE');
        printer.addTextPosition(395);
        printer.addText('€ 1118,95  \n\n');
        printer.addTextAlign(printer.ALIGN_CENTER);
        printer.addText('Gestionale Online\n');
        printer.addText('Viale stelvio 172/F\n');
        printer.addText('Tel: +39 03421590108\n');
        printer.addText('Grazie e arrivederci\n');
        printer.addCut(printer.CUT_FEED);
        printer.addTextPosition(0);
        printer.send();
    }*/
</script>

<script>
    function singleSendToPrinter()
    {
        clearResponseMessages();
        document.getElementById("displayURL").innerHTML = "";
        var epos = new epson.fiscalPrint();
        epos.onreceive = function (result, tag_names_array, add_info, res_add)
        {
            var add_info_text = "";
            var lowPaper = 0;
            if (tag_names_array.length > 0)
            {
                add_info_text = "Additional Information<br>";

                for (var i = 0; i < tag_names_array.length; i++)
                {
                    if (tag_names_array[i] == "printerStatus" && add_info[tag_names_array[i]].substring(0, 1) == "2")
                        lowPaper = 1;
                    add_info_text =	add_info_text + tag_names_array[i] + " = ";
                    var tag_names_array_respaced = "";
                    for (var k = 0; k < add_info[tag_names_array[i]].length; k++)
                    {
                        if (add_info[tag_names_array[i]].substring(k, k + 1) == " ")
                        {
                            tag_names_array_respaced = tag_names_array_respaced + "&nbsp;";
                        }
                        else
                        {
                            tag_names_array_respaced = tag_names_array_respaced + add_info[tag_names_array[i]].substring(k, k + 1);
                        }
                    } // end for (var k = 0; k < add_info[tag_names_array[i]].length; k++)

                    add_info_text =	add_info_text + tag_names_array_respaced + "<br>";
                    if (tag_names_array[i] == "lineCount" || tag_names_array[i] == "deviceCount")
                    {
                        var node2;
                        var node_child2;
                        var node_val2;
                        var node_val2_respaced = "";
                        if (tag_names_array[i] == "lineCount")
                        {
                            for (var j = 1; j <= add_info[tag_names_array[i]]; j++)  // lineCount or deviceCount value
                            {
                                node2 = res_add[0].getElementsByTagName("lineNumber" + j)[0];
                                node_child2 = node2.childNodes[0];
                                try
                                {
                                    node_val2 = node_child2.nodeValue;
                                }
                                catch(err) // Blank lines generate exceptions
                                {
                                    // Add 46 spaces to make blank line
                                    node_val2 = "                                              ";
                                }

                                for (var k = 0; k < node_val2.length; k++)
                                {
                                    if (node_val2.substring(k, k + 1) == " ")
                                    {
                                        node_val2_respaced = node_val2_respaced + "&nbsp;";
                                    }
                                    else
                                    {
                                        node_val2_respaced = node_val2_respaced + node_val2.substring(k, k + 1);
                                    }
                                } // end for (var k = 0; k < node_val2.length; k++)

                                add_info_text = add_info_text + "&nbsp;&nbsp;&nbsp;" + node_val2_respaced + "<BR>";
                                node_val2 = "";
                                node_val2_respaced = "";
                            } // End for (var j = 1; j <= add_info[tag_names_array[i]]; j++)
                            add_info_text = '<font face="courier">' + add_info_text + '</font>'; // need to move to css
                        } // End if (tag_names_array[i] == "lineCount")

                        else if (tag_names_array[i] == "deviceCount")
                        {
                            var deviceAttributes = [
                                "deviceId",
                                "deviceIp",
                                "deviceType",
                                "deviceModel",
                                "deviceRetry"
                            ];
                            for (var j = 1; j <= add_info[tag_names_array[i]]; j++)  // deviceCount value
                            {
                                add_info_text = add_info_text + "&nbsp;&nbsp;&nbsp;Device " + j + " has the following settings:<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                for (var k = 0; k < deviceAttributes.length; k++)
                                {
                                    try
                                    {
                                        node2 = res_add[0].getElementsByTagName("deviceNumber" + j)[0].attributes;
                                        node_val2 = node2.getNamedItem(deviceAttributes[k]).nodeValue;
                                    }
                                    catch(err)
                                    {
                                        alert(err);
                                    }

                                    add_info_text = add_info_text + deviceAttributes[k] + " = " + node_val2 + "&nbsp;";
                                    node_val2 = "";
                                } // End for (var k = 0; k < deviceAttributes.length; k++)

                                add_info_text = add_info_text + "<p>";
                            } // End for (var j = 1; j <= add_info[tag_names_array[i]]; j++)
                        } // End else if (tag_names_array[i] == "deviceCount"
                    } // End if (tag_names_array[i] == "lineCount" || tag_names_array[i] == "deviceCount")
                } // End for (var i = 0; i < tag_names_array.length; i++)
            }
            else {
                add_info_text = "No Additional Information";
            } // End if (tag_names_array.length > 0)

            document.getElementById("blocco_receive_result").innerHTML =
                "Success = " + result.success + "<br>" +
                "Code = " + result.code + "<br>" +
                "Status = " + result.status;

            if (result.success == 0 && document.getElementById('multiSendButton').value == "Cancel")
            {
                document.printerSelectionForm.multiSendErrors.value++;
            }
            document.getElementById("blocco_receive_add_info").innerHTML = add_info_text;

            if (lowPaper == 1)
            {
                document.getElementById('multiSendButton').value = "Multi Send";
                document.getElementById('multiSendButton').title = multiSendStartTitle;
                multi=window.clearInterval(multi);
            }

            /*
            document.getElementById("valore2").innerHTML = res.code;
            document.getElementById("valore3").innerHTML = res.status;
            document.getElementById("valore4").innerHTML = add_info.cpuRel;
            document.getElementById("valore5").innerHTML = add_info.mfRel;
            document.getElementById("valore6").innerHTML = add_info.mfStatus;
            */
            // clearXML();
            // clearMyAttributes();
        } // End epos.onreceive = function (result, tag_names_array, add_info, res_add)

        epos.onerror = function (result)
        {

            document.getElementById("blocco_receive_result").innerHTML =
                "Success = " + result.success + "<br>" +
                "Code = " + result.code + "<br>" +
                "Status = " + result.status;
            // clearXML();
            // clearMyAttributes();
        } // End epos.onerror = function (result)

        // Root of singleSendToPrinter() function
        var query_string = "";
        if (document.printerSelectionForm.localPrinterCheckBox.checked == false)
        {
            if (document.printerSelectionForm.devIdValue.value.length == 0)
            {
                alert("Please fill-in the Device ID field");
                return;
            }
            else
            {
                query_string = query_string + "?devid=" + document.printerSelectionForm.devIdValue.value;
            }
        }
        if (document.printerSelectionForm.fpmateCgiTimeout.value == "")
        {
            // Add nothing
        }
        else if (document.printerSelectionForm.fpmateCgiTimeout.value < 6000 && EFTPOS_Timeout == false)
        {
            alert("Timeout value below minimum, must be at least 6000");
            return;
        }
        else if (document.printerSelectionForm.fpmateCgiTimeout.value < 60000 && EFTPOS_Timeout == true)
        {
            alert("Timeout value below minimum, must be at least 60000 when authorizeSales element used");
            return;
        }
        else // Timeout checks complete - you can proceed
        {
            if (query_string == "")
            {
                (query_string = "?");
            }
            else
            {
                query_string = query_string + "&";
            }
            query_string = query_string + "timeout=" + document.printerSelectionForm.fpmateCgiTimeout.value;
        }

        var data_to_send = "";
        if (XMLmode == "createXML") {
            data_to_send = "<" + fileType + ">\n" + cumulative_XML_Lines + "</" + fileType + ">\n";
            // alert (data_to_send);
        }
        else {
            data_to_send = document.getElementById("pasteXMLWindow").value;
        }
        // alert(data_to_send);
        var xmlHTTPRequestDestination = "";
        if (document.crossDomainForm.crossDomainCheckBox.checked == false) {
            xmlHTTPRequestDestination = window.location.hostname;
        }
        else {
            xmlHTTPRequestDestination = document.crossDomainForm.ipAddress1.value + "." +
                document.crossDomainForm.ipAddress2.value + "." +
                document.crossDomainForm.ipAddress3.value + "." +
                document.crossDomainForm.ipAddress4.value;
        }
        var xmlHTTPRequestURL = window.location.protocol + "//" + xmlHTTPRequestDestination +
            "/cgi-bin/fpmate.cgi" + query_string;
        document.getElementById("displayURL").innerHTML = "URL = " + xmlHTTPRequestURL;
        epos.send(xmlHTTPRequestURL, data_to_send, document.printerSelectionForm.libraryTimeout.value);
        data_to_send = ""; // Just in case
    }

</script>