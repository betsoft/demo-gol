<?php $styleBlue =  "padding-top:7px;margin-right:10px;background-color:#589AB8;color:#fff;max-width:70px;min-width:50px;min-height:70px;font-size:15pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;text-align:center;vertical-align:middle;float:left;"?>
<?php $styleRed =  "padding-top:7px;margin-right:10px;background-color:red;color:#fff;max-width:70px;min-width:50px;min-height:70px;font-size:15pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;text-align:center;vertical-align:middle;float:left;"?>
<?php $styleGreen =  "padding-top:7px;margin-right:10px;background-color:green;color:#fff;max-width:70px;min-width:50px;min-height:70px;font-size:15pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;text-align:center;vertical-align:middle;float:left;"?>
<?php $styleGrey =  "padding-top:7px;margin-right:10px;background-color:grey;color:#fff;max-width:70px;min-width:70px;min-height:70px;font-size:15pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;text-align:center;vertical-align:middle;float:left;"?>
<div class="col-md-12"  id="schermo"  style="margin-bottom:10px;height:50px;border: 1px solid #e5e5e5;padding:5px;font-size:18px" ></div>
    <div class="form-group col-md-12">
        <div class="col-md-2" onclick="calculate(7)" style="<?= $styleBlue ?>"><?= '7' ?></div>
        <div class="col-md-2" onclick="calculate(8)" style="<?= $styleBlue ?>"><?= '8' ?></div>
        <div class="col-md-2" onclick="calculate(9)" style="<?= $styleBlue ?>"><?= '9' ?></div>
        <div class="col-md-2" onclick="calculate('C')" style="<?= $styleRed ?>"><?= 'C' ?></div>
        <div class="col-md-2"  onclick="calculate('%')" style="<?= $styleGrey ?>"><?= '%' ?></div>
    </div>

    <div class="form-group col-md-12">
        <div class="col-md-2" onclick="calculate(4)" style="<?= $styleBlue ?>"><?= '4' ?></div>
        <div class="col-md-2" onclick="calculate(5)" style="<?= $styleBlue ?>"><?= '5' ?></div>
        <div class="col-md-2" onclick="calculate(6)" style="<?= $styleBlue ?>"><?= '6' ?></div>
        <div class="col-md-2" onclick="calculate('+')" style="<?= $styleRed ?>"><?= '+' ?></div>
        <div class="col-md-2" onclick="calculate('X')" style="<?= $styleGrey ?>"><?= 'X' ?></div>
    </div>
    <div class="form-group col-md-12">
        <div class="col-md-2" onclick="calculate(1)" style="<?= $styleBlue ?>"><?= '1' ?></div>
        <div class="col-md-2" onclick="calculate(2)" style="<?= $styleBlue ?>"><?= '2' ?></div>
        <div class="col-md-2" onclick="calculate(3)" style="<?= $styleBlue ?>"><?= '3' ?></div>
        <div class="col-md-2" onclick="calculate('-')" style="<?= $styleRed ?>"><?= '-' ?></div>
        <div class="col-md-2"  onclick="calculate('=')" style="<?= $styleGreen ?>"><?= '=' ?></div>
    </div>
    <div class="form-group col-md-12">
        <div class="col-md-2" onclick="calculate('.')" style="<?= $styleRed ?>"><?= '.' ?></div>
        <div class="col-md-2" onclick="calculate(0)" style="<?= $styleBlue ?>"><?= '0' ?></div>
        <div class="col-md-2" onclick="calculate('00')" style="<?= $styleRed ?>"><?= '00' ?></div>
        <div class="col-md-2" onclick="calculate('000')" style="<?= $styleRed ?>"><?= '000' ?></div>
        <div class="col-md-2" onclick="calculate('OK')" style="<?= $styleGreen ?>"><?= 'OK' ?></div>
    </div>
    <div class="form-group col-md-12">

        <div class="col-md-5" onclick="calculate('D')" style="<?= $styleRed ?>;max-width: 190px"><?= 'SCONTO SUL TOTALE €' ?></div>
        <div class="col-md-5" onclick="calculate('CS')" style="<?= $styleRed ?>;max-width: 190px"><?= 'CANCELLA SCONTRINO' ?></div>
    </div>
