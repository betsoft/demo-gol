<script>
    function setCodbarFocused() {
        $(".jsBarcode").focus();
        $(".jsBarcode").val('');
    }

    function setQuantityFocus() {
        $("#quantity").focus();
    }

    function setTotalDiscount(discount) {
        if (parseFloat(discount) < 0) {
            console.log($('.containerReceipt').children().last());
            if ($('.containerReceipt').children().last().hasClass('clonedDiscountRow')) {
                $.alert
                ({
                    icon: 'fa fa-question-circle',
                    title: 'ERRORE INSERIMENTO',
                    content: "È già stato applicato uno sconto a questo articolo.",
                    type: 'red',
                });
            } else {
                var quantity = parseInt($('.containerReceipt').children().last().find('.quantity').text());
                var price = parseFloat($('.containerReceipt').children().last().find('.price').text());
                var vat = vats[parseInt($('.containerReceipt').children().last().find('.vat').text())];
                var newDiscountRow = $(".rowReceiptDiscount").clone();
                $(newDiscountRow).removeClass("rowReceiptDiscount");
                $(newDiscountRow).addClass("clonedDiscountRow");
                $(newDiscountRow).appendTo(".containerReceipt");
                $(newDiscountRow).find('.vatvalue').html((Math.abs(discount)).toFixed(2) + '%');
                $(newDiscountRow).find('.quantity').html(quantity);
                console.log("QUANTITA: " + quantity);
                console.log("PREZZO: " + price);
                console.log("IVA: " + vat);
                $(newDiscountRow).find('.rowDiscount').html((price * parseFloat(discount) / 100).toFixed(2));
                $(newDiscountRow).show();
                console.log($(newDiscountRow).find('.rowDiscount').html());
                $(".totale").val(+$(".totale").val() + parseFloat($(newDiscountRow).find('.rowDiscount').html()));
                $(".totale").html((+$(".totale").val()).toFixed(2));
            }
        } else {
            $.alert
            ({
                icon: 'fa fa-question-circle',
                title: '',
                content: "Inserire uno sconto negativo.",
                type: 'red',
            });
        }
    }

    function addDiscount(discount) {

        console.log(" ___ PER CALCOLI START ___");
        console.log(Math.abs($(".scontototale").val()));
        console.log(parseFloat(discount));
        console.log(Math.abs($(".rowReceiptDiscountFixed").find('.rowDiscountFixed').html()));
        console.log(" ___ PER CALCOLI FINE___");


        if($(".rowsubtotal").is(":visible")) {
            var oldDiscount = $(".rowReceiptDiscountFixed").find(".rowDiscountFixed").val();
            var oldSubtotal = $(".rowsubtotal").find(".subtotale").val();
            $(".rowsubtotal").find(".subtotale").val(oldSubtotal + oldDiscount);
            $(".rowsubtotal").find(".subtotale").html(oldSubtotal + oldDiscount);
            console.log("SUBTOTATLE");
            console.log(oldSubtotal);
            console.log(oldDiscount);
            console.log(oldSubtotal + oldDiscount);
            $(".scontototale").val(Math.abs($(".scontototale").val()) - oldDiscount + Math.abs(parseFloat(discount)));
            $(".scontototale").html((-$(".scontototale").val()).toFixed(2));
            console.log($(".scontototale").val());
            console.log(+$(".totale").val());
            $(".totale").val(+$(".totale").val() + +oldDiscount - +Math.abs(parseFloat(discount)));
            console.log(+$(".totale").val());
            $(".totale").html((+$(".totale").val()).toFixed(2));
        }
        else {
            $(".scontototale").val(Math.abs($(".scontototale").val()) + Math.abs(parseFloat(discount)));
            $(".scontototale").html((-$(".scontototale").val()).toFixed(2));
            $(".totale").val(+$(".totale").val() - Math.abs(parseFloat(discount)));
            $(".totale").html((+$(".totale").val()).toFixed(2));
        }
    }

    function resetDiscount() {
        $(".totale").val(+$(".totale").val() + Math.abs(parseFloat($(".scontototale").val())));
        $(".totale").html((+$(".totale").val()).toFixed(2));
        $(".scontototale").val('0.00');
        $(".scontototale").html($(".scontototale").val());
    }


    function addNewRowToReceipt(returned) {

        if ($('#vat_id').val() == '') {
            $.ajax
            ({
                method: "POST",
                async: false,
                url: "<?= $this->Html->url(["controller" => "ivas", "action" => "getDefaultReceiptIva"]) ?>",
                success: function (data) {
                    data = JSON.parse(data);
                    $("#vat_id").val(data.Iva.id);
                }
            })
        }

        if ($("#barcode").val() != '' || $("#storage_id_multiple_select").val() != '' || $("#notmovable_id_multiple_select").val() != '') {
            // Copio la riga
            var newRow = $(".rowReceipt").clone();
            $(newRow).removeClass("rowReceipt");
            $(newRow).addClass("clonedRow");
            $(newRow).appendTo(".containerReceipt");
            var element = $("#storage_id_multiple_select");
            if ($("#notmovable_id_multiple_select").val() > 0) {
                element = $("#notmovable_id_multiple_select");
            }

            $(newRow).find('.name').html($(element).parent().find(".multiselect-selected-text").html());
            $(newRow).find('.barcode').html($("#barcode").val());
            $(newRow).find('.quantity').html($("#quantity").val());
            $(newRow).find('.vat').html($("#vat_id").val());
            $(newRow).find('.price').html($("#price").val());
            $(newRow).find('.vatvalue').html(vats[$("#vat_id").val()] + '%');
            $(newRow).find('.movable').html($("#movable").val());
            $(newRow).find('.storage_id').html($("#storage_id").val());
            $(newRow).find('.discount').html($("#discount").val());
            $(newRow).show();

            // Se lo sconto è maggiore di zero e non è un reso
            if ($("#discount").val() > 0 ) {
                var newDiscountRow = $(".rowReceiptDiscount").clone();
                $(newDiscountRow).removeClass("rowReceiptDiscount");
                $(newDiscountRow).addClass("clonedDiscountRow");
                $(newDiscountRow).appendTo(".containerReceipt");
                $(newDiscountRow).find('.vatvalue').html($("#discount").val() + '%');
                $(newDiscountRow).find('.quantity').html($("#quantity").val());
                $(newDiscountRow).find('.rowDiscount').html((-1 * $("#price").val() * (1 + (vats[$("#vat_id").val()] / 100)) * $("#discount").val() / 100 * $("#quantity").val()).toFixed(2));
                $(newDiscountRow).show();
            }

            $(newRow).find('.price').html(($("#price").val() * (1 + (vats[$("#vat_id").val()] / 100)) * $("#quantity").val()).toFixed(2));

            $(".totale").val(+$(".totale").val() + +(($("#price").val() * (1 + (vats[$("#vat_id").val()] / 100)) * $("#quantity").val())).toFixed(2));
            $(".totale").html((+$(".totale").val()).toFixed(2));

            // Se lo sconto è maggiore di zero
            if ($("#discount").val() > 0) {
                $(".totale").val(+$(".totale").val() + +(-1 * $("#price").val() * (1 + (vats[$("#vat_id").val()] / 100)) * $("#discount").val() / 100 * $("#quantity").val()).toFixed(2));
                $(".totale").html((+$(".totale").val()).toFixed(2));
            }

            $('.fa-trash').unbind('click');
            $('.fa-trash').click(function () {
                $(this).parent().parent().find(".price").html();

                // ** ** //
                console.clear();
                console.log(+$(".totale").val()); // Totale
                console.log($(this).parent().parent().find(".price").html()); // Prezzo
                console.log($(this).parents('.clonedRow').next().find(".rowDiscount").html()); // Sconto

                var price = $(this).parent().parent().find(".price").html();
                var discount = $(this).parents('.clonedRow').next().find(".rowDiscount").html();

                if (discount == undefined)
                    discount = 0;

                $(".totale").val(+$(".totale").val() + (-price + -discount));
                //$(".totale").html((+$(".totale").val()).toFixed(2).replace("-", ""));
                $(".totale").html((+$(".totale").val()).toFixed(2));
                $(this).parents('.clonedRow').next().remove();
                $(this).parent().parent().remove();

            });

            setCodbarFocused();
            $("#notmovable_id_multiple_select").val(null);
            $("#notmovable_id_multiple_select").multiselect('rebuild');
            $("#storage_id_multiple_select").val(null);
            $("#storage_id_multiple_select").multiselect('rebuild');
            $("#vat_id").val('');
            $("#movable").val('');
            $("#storage_id").val('');
            $("#discount").val('');
            $("#price").val('');
            $("#quantity").val(1);

        }
        else {
            $.alert
            ({
                icon: 'fa fa-warning',
                title: 'Cassa',
                content: "<?= addslashes('Attenzione, non è stato selezionato correttamente nessun articolo'); ?>",
                type: 'orange',
            });
        }
    }
</script>
