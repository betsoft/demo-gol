<script>

    function getMovable(element) {

        $("#barcode").css('border-color', '#e5e5e5');
        $("#barcodemessage").hide();
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "storages", "action" => "getMovableStorageByBarcode"]) ?>",
            data:
                {
                    barcode: $(element).val(),
                },
            success: function (data) {
                data = JSON.parse(data);
                //    $("#storage_id_multiple_select").val(13);
                if (data == null) {
                    $("#barcode").css('border-color', 'red');
                    $("#quantity").css('border-color', '#e5e5e5');
                    setCodbarFocused();
                    $(".multiselect-native-select").click();
                    $("#barcodemessage").show();
                    $("#storage_id").val('');
                    $("#movable").val('');
                    $("#vat_id").val('');
                    $("#discount").val('');
                } else {
                    $("#storage_id_multiple_select").val(data.id);
                    $("#storage_id_multiple_select").multiselect('rebuild');
                    $("#notmovable_id_multiple_select").val(null);
                    $("#notmovable_id_multiple_select").multiselect('rebuild');
                    $("#storage_id").val(data.id);
                    $("#movable").val(data.movable);
                    $("#vat_id").val(data.vat_id);
                    $("#price").val(data.prezzo);
                    $("#discount").val(data.default_discount);
                    $("#quantity").focus();
                }
            }
        })


    }
</script>