<span class="caption-subject bold subTitle"><?= __('Scontrino'); ?></span>
<?= $HR12 ?>
<div class='col-md-12'   style="margin-bottom:10px;" >
    <div class='col-md-3 name' > Descrizione</div>
    <div class='col-md-3 barcode' > Codice a barre</div>
    <div class='col-md-1 vat' hidden> Iva</div>
    <div class='col-md-1 vatvalue'>  Iva </div>
    <div class='col-md-1 quantity'> QT </div>
    <div class='col-md-2 price'> Prezzo (€)</div>
    <div class='col-md-1' ></div>
    <div class='col-md-1 movable' hidden > movable </div>
    <div class='col-md-1 storage_id' hidden>  storageid </div>
    <div class='col-md-1 storage_id' hidden>  discount </div>
</div>
<div class='col-md-12 rowReceipt recrow'  style="margin-bottom:10px;" hidden>
    <div class='col-md-3 name' style="text-align: left;margin-left:0px;padding-left:0px; "> Nome</div>
    <div class='col-md-3 barcode' style="text-align: left;margin-left:0px;padding-left:0px; "> codiceabarre</div>
    <div class='col-md-1 vat' hidden> Iva</div>
    <div class='col-md-1 vatvalue' style="text-align: right;margin-right:0px;padding-left:0px; ">  Iva </div>
    <div class='col-md-1 quantity' style="text-align: right;margin-right:0px;padding-left:0px; "> N° </div>
    <div class='col-md-2 price' style="text-align: right;margin-right:0px;padding-left:0px; "> Prezzo </div>
    <div class='col-md-1' ><i class='fa fa-trash'></i></div>
    <div class='col-md-1 movable' hidden> movable </div>
    <div class='col-md-1 storage_id' hidden>  storageid </div>
    <div class='col-md-1 discount' hidden>  discount  </div>
</div>
<div class='col-md-12 rowReceiptDiscount recrow'  style="margin-bottom:10px;" hidden>
    <div class='col-md-2 name' style="text-align: left;margin-left:0px;padding-left:0px; ">SCONTO</div>
    <div class='col-md-1 vatvalue' style="text-align: right;margin-right:0px;padding-left:0px; ">  Iva </div>
    <div class='col-md-4' style="text-align: right;margin-right:0px;padding-left:0px; "></div>
    <div class='col-md-1 quantity' style="text-align: right;margin-right:0px;padding-left:0px; "> N° </div>
    <div class='col-md-2 rowDiscount' style="text-align: right;margin-right:0px;padding-left:0px; "> Prezzo </div>
</div>


<div class="col-md-12 containerReceipt"></div>
<div class='col-md-12'></div>
<div class='col-md-12'>
    <div class='col-md-12 rowsubtotal' hidden style="margin-bottom:10px;" hidden>
        <div class='col-md-6' style="text-align:left;margin-right:0px;padding-left:0px;" ><b>SUBTOTALE:</b></div>
        <div class='col-md-1 ' style="text-align: right;margin-right:0px;padding-left:0px; ">&nbsp;</div>
        <div class='col-md-1 ' style="text-align: right;margin-right:0px;padding-left:0px; ">&nbsp;</div>
        <div class='col-md-2 subtotale' style="text-align:right;font-weight:bold;"></div>
    </div>
    <div class='col-md-12 rowReceiptDiscountFixed '  style="margin-bottom:10px;" hidden>
        <div class='col-md-6 name' style="text-align: left;margin-left:0px;padding-left:0px; ">SCONTO SU SUBTOTALE</div>
        <div class='col-md-1 ' style="text-align: right;margin-right:0px;padding-left:0px; ">&nbsp;</div>
        <div class='col-md-1 ' style="text-align: right;margin-right:0px;padding-left:0px; ">&nbsp;</div>
        <div class='col-md-2 rowDiscountFixed' style="text-align: right;margin-right:0px;padding-left:0px; ">&nbsp;</div>
    </div>
    <div class='col-md-12' hidden> <!-- non eliminare viene usato  -->
        <div class='col-md-6' style="text-align:left;margin-left:0px;padding-left:0px;" ><b>SCONTO TOTALE:</b></div>
        <div class='col-md-1' ></div>
        <div class='col-md-1' ></div>
        <div class='col-md-2 scontototale' style="text-align:right;font-weight:bold;"></div>
    </div>
    <div class='col-md-12'>
        <div class='col-md-6' style="text-align:left;margin-left:0px;padding-left:0px;" ><b>TOTALE:</b></div>
        <div class='col-md-1' ></div>
        <div class='col-md-1' ></div>
        <div class='col-md-2 totale' style="text-align:right;font-weight:bold;"></div>
    </div>
</div>