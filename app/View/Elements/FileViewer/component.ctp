<?php
    /*
	$url
		url della risorsa da visualizzare
	*/
	
	if(!isset($id))
		$id = null;
		
	if(!isset($url)) 
		$url = null;
		
	if(!isset($downloadable))
		$downloadable = true;
		
	if(!isset($expandible))
		$expandible = false;

	if(!isset($fileName))
		$fileName = "download";
?>

<span class="file-viewer" id="<?= $id ?>">
	<?php if($downloadable): ?>
		<a for="<?= $id ?>" class="download" href="<?= $url ?>" download="<?= $fileName ?>">
	<?php endif; ?>
			<img for="<?= $id ?>" class="preview" src="<?= $url ?>" <?= $expandible ? "expandible='true'" : null ?>></img>
	<?php if($downloadable): ?>
		</a>
	<?php endif; ?>
</span>