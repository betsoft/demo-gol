<script>
	addLoadEvent(function() {
		initializeFileUploader()
	},'initializeFileUploader')
	
	function initializeFileUploader() {
		$('.file-uploader').each(function() {
			documentFileChange($(this))
		})
	}
	
	function documentFileChange(fileUploader) {
		fileUploader.find(".file-upload-input").unbind('change.addGoodPageScript')
		fileUploader.find(".file-upload-input").on('change.addGoodPageScript', function() {
            fileUploaderLoadPreview(this);
        });
	}
	
	function fileUploaderLoadPreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).parents('.file-uploader').find('label.filename[for="' + $(input).attr('id') + '"]').text(input.files[0].name);
                $(input).parents('.file-uploader').find('img.preview[for="' + $(input).attr('id') + '"]').attr('src', e.target.result);
                $(input).parents('.file-uploader').find('a.download[for="' + $(input).attr('id') + '"]').attr('href', e.target.result);
                $(input).parents('.file-uploader').find('a.download[for="' + $(input).attr('id') + '"]').attr('download', input.files[0].name);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<?= $this->Html->css('FileViewer/default') ?>
<?= $this->Html->css('FileViewer/custom') ?>