
<nav id="vladhmenu" class="uk-navbar dark" >
	<ul class="uk-navbar-nav">
		<li class="logo">
			<?php
				if(isset($LOGOMENU[0]))
				{
					if($LOGOMENU[0]['Setting']['header'] == '' || $LOGOMENU[0]['Setting']['header'] == null)
						{
							echo $this->Html->image('gestionale-online.png', ["class" => "uk-container-center"]); 
						}
						else
						{
							echo $this->Html->image('societa/'.$LOGOMENU[0]['Setting']['header'], ["class" => "uk-container-center",'style'=>"max-height: 80px;height:100px;max-width:500px;"]); 
						}
				}
				else
				{
					echo $this->Html->image('gestionale-online.png', ["class" => "uk-container-center"]); 
				}
				?>
		</li>
    </ul>
</nav>
	
<style>
	#vladhmenu 
	{
		background-color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;
		color:<?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?>;
	}
	
	.footer.uk-text-center
	{
		background-color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;
		color:<?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?>;
	}
	
	.level-zero
	{
		background-color:white !important;
	}
	
	.uk-navbar-flip
	{
		background-color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;
	}
	
	.menu-voice
	{
		border-bottom: 5px solid <?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?>;
	}
	
	.attivo>a
	{
		border-bottom: 5px solid <?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?> !important;
		color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?> !important;
	}
	
	.menu-voice>a:hover
	{
		border-bottom: 5px solid <?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?> !important;
		color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?> !important;
	}
	
	
	.blue-button
	{
		background-color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;
		color:<?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?>;
		border: 1px solid <?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;
	}
	
	.blue-button:focus
	{
		background-color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;
		color:<?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?>;
		border: 1px solid <?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;	
	}
	
	.blue-button:hover
	{
		background-color:<?= $LOGOMENU[0]['Setting']['base_css_background'] ?>;
		color:<?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?> !important;
		border : 1px solid <?= $LOGOMENU[0]['Setting']['base_css_background'] ?> !important;
	}
	
	.h1
	{
	/*	color:<?php //$LOGOMENU[0]['Settings']['base_css_fontcolor'] ?> !important; */
	}
	
	.tools .icon
	{
			color:<?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?> !important;
	}
	
	.accordion-toggle
	{
			color:<?= $LOGOMENU[0]['Setting']['base_css_fontcolor'] ?> !important;
	}
</style>
