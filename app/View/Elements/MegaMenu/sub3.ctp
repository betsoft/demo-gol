<?php
    /*
        'name':
            nome della voce di menu
        'subs':
            array contenente le sottovoci di menu
    */
    if(isset($subs['controller']) && isset($subs['action']))
    {

        list($plugin, $controller) = split('\.', $subs['controller']);
        $action = $subs['action'];
        
        $link = $this->Html->Url(["plugin" => $plugin,"controller" => $controller,"action" => $action]);
?>
        <li>
            <a href="<?= $link ?>"> <?= $name ?> </a>
        </li>
<?php
    }
?>