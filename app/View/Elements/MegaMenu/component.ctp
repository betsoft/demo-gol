<!-- BEGIN HEADER -->
<header class="page-header">
    <nav class="navbar mega-menu" role="navigation">
        <div class="container-fluid" style="background-color:#dadfe0;" >
            <?= 
                $this->element('FixedNavbar5/navbar', [
                    'hamburger' => $this->element('MegaMenu/toggler'),
                    'logo' => true,
                    'search' => false,
                    'actions' => false,
                ]);
            ?>
            <?= $this->element('MegaMenu/main') ?>
        </div>
    </nav>
</header>
