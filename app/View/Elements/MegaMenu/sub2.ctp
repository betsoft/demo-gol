<?php
    /*
        'subId':
            id univoco della voce di menu
        'name':
            nome della voce di menu
        'subs':
            array contenente le sottovoci di menu
    */
    
    if(isset($_SESSION['Auth']['User']['activeMenuItemParentId']))
        $activeMenuItemParentId = $_SESSION['Auth']['User']['activeMenuItemParentId'];
    else
        $activeMenuItemParentId = -1;    
    
    if(isset($_SESSION['Auth']['User']['activeMenuItemId']))
        $activeMenuItemId = $_SESSION['Auth']['User']['activeMenuItemId'];
    else
        $activeMenuItemId = -1;
        
    $link = $this->Html->Url(["plugin" => $subs['plugin_name'],"controller" => $subs['controller'],"action" => $subs['action']]); 
?>
    <li  id="<?= $subId ?>" class="level-uno <?= $mainId == $activeMenuItemParentId && $subId == $activeMenuItemId ? 'active' : '' ?>   ">
        <a href="<?= $link ?>"><?= $name ?> </a>
    </li>



