<?php
    /*
        'mainId':
            indice univoco della voce di menu
        'name':
            nome della voce di menu
        'subs':
            array contenente le sottovoci di menu
    */
    if(isset($_SESSION['Auth']['User']['activeMenuItemParentId']))
        $activeMenuItemParentId = $_SESSION['Auth']['User']['activeMenuItemParentId'];
    else
        $activeMenuItemParentId = -1;
?>



<li id="<?= $mainId ?>" class=" <?= $final == 'final' ? 'final' : '' ?> menu-voice dropdown dropdown-fw dropdown-fw-disabled level-zero <?= $mainId == $activeMenuItemParentId  ? 'open' : '' ?>">
    
    
    <?php if(count($subs) > 0): ?>
        <a href="javascript:;" >
             <?= $name; ?>
        </a> <!-- Stampa il livello 0  -->
        <ul class="dropdown-menu dropdown-menu-fw container-level-uno">
            <?php
                $c = 0;
                foreach($subs as $key => $item)
                {
                    echo $this->element('MegaMenu/sub2',[
                        'mainId' => $mainId,
                        'subId' => 'sub_' . $c,
                        'name' => $key,
                        'subs' => $item
                    ]);
                    $c += 1;
                }
            ?>
        </ul>
    <?php else: ?>
        <a href="<?= $this->Html->url(["plugin" => $plugin, "controller" => $controller, "action"=> $action], ['fullBase' => true]) ?>" >
             <?= $name; ?>
        </a>
    <?php endif; ?>
</li>
