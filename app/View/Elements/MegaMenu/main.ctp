    <div style="float:left;width:150px;padding:10px;background-color:#dadfe0;" >
    	<?= $this->Html->image('gestionale-online.png', ["class" => "uk-container-center",'style'=>"max-height: 50px;height:50px;max-width:500px;padding-right:30px;float:left;"]); ?>
    </div>
    <div id="mega_menu" class="vladhmenu nav-collapse collapse navbar-collapse navbar-responsive-collapse " style="float:left;margin-left:30px; font-family: 'Barlow Semi Condensed' !important;font-size: 9pt;text-transform:uppercase">
        <ul class="nav navbar-nav" style="padding-top:34px;" >
            <?php
                $c = 0;

            if($_SESSION['Auth']['User']['group_id'] != 20) {

                if (isset($PLUGINS_DB_MENU)) {
                    foreach ($PLUGINS_DB_MENU as $key => $plugin) {

                        echo $this->element('MegaMenu/sub1', [
                            'mainId' => 'main_' . $c,
                            'name' => $key,
                            'subs' => isset($plugin['subs']) ? $plugin['subs'] : [],
                            'plugin' => null,
                            'controller' => $plugin['controller'],
                            'action' => $plugin['action'],
                            'final' => (count($PLUGINS_DB_MENU) == ($c + 1)) ? 'final' : '',
                        ]);
                        $c += 1;
                    }
                }
            }
            else
            {
                    if (isset($PLUGINS_DB_MENU)) {
                    foreach ($PLUGINS_DB_MENU as $key => $plugin) {

                        ?>
                        <a href ="<?= $this->webroot.$plugin['controller'] ?>/<?= $plugin['action'] ?>"><?= $key ?></a>
                        <?php
                        $c += 1;
                        }
                 }
             }
            ?>
        </ul>
    </div>
    <?php
    if(true) {
    	?>
    <div class="page-actions" style="padding-top:30px;">
        	<div class="btn-group ">
            <button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true" id="buttoned">
            	<?php if($_SESSION['Auth']['User']['group_id'] != 20){ ?>
                <span class="hidden-sm" style="font-size:15px;"><?= $_SESSION['companyName']; ?></span>
                <?php
            	}
                else
                    {
                ?>
                    <span class="hidden-sm" style="font-size:15px;"><?= $_SESSION['companyName'] . ' - '.$tennichianname ?></span>
                <?php
                    }
                ?>
                <?php if(count($settingsList) >1) { ?>
                    <i class="fa fa-angle-down"></i>
                <?php } ?>
                </button>
                <?php if(count($settingsList) >1) { ?>
                <ul class="dropdown-menu" role="menu" id="subbuttoned">
                    <?php foreach($settingsList as $settings)
                    {
                        ?>
                            <li><?= $this->Html->link($settings['Setting']['name'], ['controller' => 'settings', 'action' => 'changeCurrentCompany', '?' => [ $settings['Setting']['company_id'], $this->request->params['controller'] , $this->request->params['action']]]); ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <?php } ?>
			</div>
        </div>
        <?php } ?>

<script>
    $("#buttoned").click(
        function()
        {
            $("#subbuttoned").toggle();
        }
    )
</script>
