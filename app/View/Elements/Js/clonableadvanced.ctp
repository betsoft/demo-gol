 <?= $this->element('Js/addcrossremoving'); ?>
 <?= $this->element('Js/addcrossremovingddt'); ?>
 <?= $this->element('Js/addcrossremovinghour'); ?>
 <script>
     jQuery(function($){
         $.datepicker.regional['it'] = {
             closeText: 'Chiudi',
             prevText: '&#x3c;Prec',
             nextText: 'Succ&#x3e;',
             currentText: 'Oggi',
             monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
                 'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
             monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
                 'Lug','Ago','Set','Ott','Nov','Dic'],
             dayNames: ['Domenica','Luned&#236','Marted&#236','Mercoled&#236','Gioved&#236','Venerd&#236','Sabato'],
             dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
             dayNamesMin: ['Do','Lu','Ma','Me','Gio','Ve','Sa'],
             dateFormat: 'dd/mm/yy', firstDay: 1,
             isRTL: false};
         $.datepicker.setDefaults($.datepicker.regional['it']);
     });

    function clonableadvanced(addRowButtonId,fields, label, newlinevalue, nomeOggetti,functionToExecute, functionparameters,codici)
    {
        var clonato;
        var nuova_chiave = 0 ;
        $(addRowButtonId).click
        (
            function()
            {
                nuova_chiave = nuova_chiave + 1;
 
                clonato = $('.originale').clone();
                $(clonato).removeClass("originale");
                $(clonato).addClass("clonato");
                $(".ultima_riga").removeClass("ultima_riga");
             
                $(clonato).insertAfter($(".clonableRow").last());

                for (i = 0; i<fields.length; i++)
                {
                   $(".clonato #"+nomeOggetti+"0"+fields[i]).attr('name', "data["+nomeOggetti+"][" + nuova_chiave + "]["+label[i]+"]");
                   $(".clonato #"+nomeOggetti+"0"+fields[i]).attr('id', nomeOggetti + nuova_chiave + fields[i]); 
                   $(".clonato #"+nomeOggetti+ nuova_chiave + fields[i]).val(newlinevalue[i]);
                }
                   
                $(".clonato .rimuoviRigaIcon").show();
                
                $(clonato).addClass("ultima_riga");
                $(clonato).removeClass("clonato");
            
                // Aggiungo il rimuovi icona
                addcrossremoving() 

                switch(functionToExecute)
                {
                    // Trasferimento tra depositi
                    case "indexdepositmovementclonable":
                        indexdepositmovementclonable(nuova_chiave,functionparameters,codici);
                    break;
                }
            });
    }

    function clonableadvancedDdt(addRowButtonId,fields, label, newlinevalue, nomeOggetti)
    {
        var clonato;
        var nuova_chiave = 0;
        jQuery("#Ddt0Ddtdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});
        $(addRowButtonId).click
        (
            function () {
                nuova_chiave = nuova_chiave + 1;

                jQuery("#Ddt0Ddtdate").datepicker("destroy");
                clonato = $('.originaleddt').clone();
                $(clonato).removeClass("originaleddt");
                $(clonato).addClass("clonatoddt");
                $(".ultima_rigaddt").removeClass("ultima_rigaddt");

                $(clonato).insertAfter($(".clonableRowddt").last());

                for (i = 0; i < fields.length; i++) {
                    switch (fields[i]) {
                        case 'ddtdate':
                            $(".clonatoddt #" + nomeOggetti + "0" + 'Ddtdate').attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #" + nomeOggetti + "0" + 'Ddtdate').attr('id', nomeOggetti + nuova_chiave + fields[i]);

                            $(".clonatoddt #" + nomeOggetti + nuova_chiave + 'Ddtdate').val(newlinevalue[i]);


                            jQuery(".clonatoddt #" + nomeOggetti + nuova_chiave + "ddtdate").datepicker({
                                "dateFormat": "dd-mm-yy",
                                "showAnim": "slideDown"
                            });
                            jQuery("#Ddt0Ddtdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});
                            break;
                        case 'supplier_id_multiple_select':

/*						$(".clonatoddt #supplier_id_multiple_select").attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #supplier_id_multiple_select").attr('id', "supplier_id_multiple_select" + nuova_chiave);
                            var selectHtml3 = $("#supplier_id_multiple_select" + nuova_chiave).parent().html();
                            var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                            $("#supplier_id_multiple_select" + nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                            $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').remove();
                            $("#supplier_id_multiple_select" + nuova_chiave).multiselect('rebuild');
                            $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                            $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
  */
						$(".clonatoddt #supplier_id_multiple_select").attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                        $(".clonatoddt #supplier_id_multiple_select").attr('id', "supplier_id_multiple_select" + nuova_chiave);

                        //var id = $(this).find('select').attr('id');
                        var id = $(clonato).find('.multiselect-native-select').find('select').attr('id');
                        initializeFilterableSelects(id);
                        $(clonato).find('.multiselect-native-select').find('.btn-group')[1].remove();  

						break;
                        default:
                            $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('id', nomeOggetti + nuova_chiave + fields[i]);
                            $(".clonatoddt #" + nomeOggetti + nuova_chiave + fields[i]).val(newlinevalue[i]);
                            break;
                    }
                }

                $(".clonatoddt .rimuoviRigaIconddt").show();

                $(clonato).addClass("ultima_rigaddt");
                $(clonato).removeClass("clonatoddt");

                // Aggiungo il rimuovi icona
                addcrossremovingddt();

            });
    }

    function clonableadvancedEditDdt(addRowButtonId,fields, label, newlinevalue, nomeOggetti)
    {
        var clonato;
        // var nuova_chiave = 0;
        jQuery("#Ddt0Ddtdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});
        $(addRowButtonId).click
        (
            function () {
                //nuova_chiave = nuova_chiave + 1;
                var nuova_chiave = $(".lunghezzaddt").length;

                jQuery("#Ddt0Ddtdate").datepicker("destroy");
                clonato = $('.originaleddt').clone();
                $(clonato).removeClass("originaleddt");
                $(clonato).addClass("clonatoddt");
                $(".ultima_rigaddt").removeClass("ultima_rigaddt");

                $(clonato).insertAfter($(".clonableRowddt").last());

                for (i = 0; i < fields.length; i++) {
                    switch (fields[i]) {
                        case 'ddtdate':
                            $(".clonatoddt #" + nomeOggetti + "0" + 'Ddtdate').attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #" + nomeOggetti + "0" + 'Ddtdate').attr('id', nomeOggetti + nuova_chiave + fields[i]);

                            $(".clonatoddt #" + nomeOggetti + nuova_chiave + 'Ddtdate').val(newlinevalue[i]);


                            jQuery(".clonatoddt #" + nomeOggetti + nuova_chiave + "ddtdate").datepicker({
                                "dateFormat": "dd-mm-yy",
                                "showAnim": "slideDown"
                            });
                            jQuery("#Ddt0Ddtdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});
                            break;
                        case 'supplier_id_multiple_select':


                           $(".clonatoddt #supplier_id_multiple_select").attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #supplier_id_multiple_select").attr('id', "supplier_id_multiple_select" + nuova_chiave);
                        /*    var selectHtml3 = $("#supplier_id_multiple_select" + nuova_chiave).parent().html();
                            var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                            $("#supplier_id_multiple_select" + nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                            $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').remove();
                            $("#supplier_id_multiple_select" + nuova_chiave).val('empty');
                            $("#supplier_id_multiple_select" + nuova_chiave).multiselect('rebuild');
                            $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                            $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
*/
							var id = $(clonato).find('.multiselect-native-select').find('select').attr('id');
                            initializeFilterableSelects(id);
                            $(clonato).find('.multiselect-native-select').find('.btn-group')[1].remove();
                            $("#supplier_id_multiple_select" + nuova_chiave).val('empty');
                            $("#supplier_id_multiple_select" + nuova_chiave).multiselect('rebuild');


                            break;
                        default:
                            $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('id', nomeOggetti + nuova_chiave + fields[i]);
                            $(".clonatoddt #" + nomeOggetti + nuova_chiave + fields[i]).val(newlinevalue[i]);
                            break;
                    }
                }

                $(".clonatoddt .rimuoviRigaIconddt").show();

                $(clonato).addClass("ultima_rigaddt");
                $(clonato).removeClass("clonatoddt");

                // Aggiungo il rimuovi icona
                addcrossremovingddt();

            });
    }


     function clonableadvancedHour(addRowButtonId,fields, label, newlinevalue, nomeOggetti)
     {
         var clonato;
         var nuova_chiave = 0;
     //    jQuery("#Ddt0Ddtdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});
         $(addRowButtonId).click
         (
             function () {
                 nuova_chiave = nuova_chiave + 1;

                 jQuery("#Ddt0Ddtdate").datepicker("destroy");
                 clonato = $('.originaleddt').clone();
                 $(clonato).removeClass("originalehour");
                 $(clonato).addClass("clonatohour");
                 $(".ultima_rigahour").removeClass("ultima_rigaddt");

                 $(clonato).insertAfter($(".clonableRowhour").last());

                 for (i = 0; i < fields.length; i++) {
                     switch (fields[i]) {
                         /*case 'hour_from':
                             $(".clonatohour #" + nomeOggetti + "0" + 'Hour_from').attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                             $(".clonatohour #" + nomeOggetti + "0" + 'Hour_from').attr('id', nomeOggetti + nuova_chiave + fields[i]);

                             $(".clonatoddt #" + nomeOggetti + nuova_chiave + 'Ddtdate').val(newlinevalue[i]);


                             jQuery(".clonatoddt #" + nomeOggetti + nuova_chiave + "ddtdate").datepicker({
                                 "dateFormat": "dd-mm-yy",
                                 "showAnim": "slideDown"
                             });
                             jQuery("#Ddt0hourdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});
                             break;*/
                         /*case 'hour_to':
                             $(".clonatoddt #supplier_id_multiple_select").attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                             $(".clonatoddt #supplier_id_multiple_select").attr('id', "supplier_id_multiple_select" + nuova_chiave);
                             var selectHtml3 = $("#supplier_id_multiple_select" + nuova_chiave).parent().html();
                             var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                             $("#supplier_id_multiple_select" + nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                             $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').remove();
                             $("#supplier_id_multiple_select" + nuova_chiave).multiselect('rebuild');
                             $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                             $("#supplier_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
                             break;*/
                         case 'person':
                             $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                             $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('id', nomeOggetti + nuova_chiave + fields[i]);
                             $(".clonatoddt #" + nomeOggetti + nuova_chiave + fields[i]).val(newlinevalue[i]);
                             break;
                     }
                 }

                 $(".clonatohour .rimuoviRigaIconhour").show();

                 $(clonato).addClass("ultima_rigahour");
                 $(clonato).removeClass("clonatohour");

                 // Aggiungo il rimuovi icona
                 addcrossremovinghour();

             });
     }
</script>
