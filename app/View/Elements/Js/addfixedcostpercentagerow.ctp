<script>
    function addFixedCostPercentageRow(buttonSource,buttonDestiny)
    {
        // Clicco sul pulsante che duplica la riga
        $(buttonSource).click(function()
        {
            var prezzoTotale = 0;
             $(".clonableRow").each(
                 function()
                 {
                     prezzo = $(this).find('.jsPrice').val();
                     quantity = $(this).find('.jsQuantity').val();
                     prezzoTotale = +prezzoTotale + +prezzo * +quantity;
                 });
             
             $(buttonDestiny).click();
            
             addWaitPointer();    
                
             $.ajax
             ({
    		    method: "POST",
    			url: "<?= $this->Html->url(["controller" => "bills","action" => "calculateFixedTotalPercentage"]) ?>",
    			data: 
    			{
    			    totale : prezzoTotale,
    			},
    			success: function(data)
    			{
    			    var percentuale = data;  
                    $.ajax
                    ({
    		            method: "POST",
    			        url: "<?= $this->Html->url(["controller" => "bills","action" => "getFixedTotalPercentageDescription"]) ?>",
    			        success: function(data)
    			        {
                            $(".ultima_riga").find('.jsDescription').val(data);
                            $(".ultima_riga").find('.jsPrice').val(percentuale);
                            removeWaitPointer();    
        		        }
    		        });
                    
        		}
    		});

         
        });
    }
</script>