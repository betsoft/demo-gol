<script>
function syncro(classe)
{	
	$(classe).click(
		function()
		{
			addWaitPointer();
			var me = $(this);
			$.ajax({
				method: "POST",
				url: "<?= $this->Html->url(["controller" => "bills","action" => "ixfesyncro"]) ?>",
				async : false,
				data:
				{
					billId :  $(me).attr('refreshid'), 
				},
				success: function(data)
				{
					removeWaitPointer();
					location.reload();
				},
				error: function(data)
				{
					removeWaitPointer();
					location.reload();
				}
		    })
        })		
}
</script>