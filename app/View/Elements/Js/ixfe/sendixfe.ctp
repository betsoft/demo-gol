<script>
    function sendixfe(classe,message)
    {
    	$(classe).click(function()
    	{
    		var me = $(this);
    		event.preventDefault();
    		$.confirm
    		({
        		title: 'IXFE',
    			content: message,
    			type: 'blue',
    			buttons:
    			{
                	Ok: function ()
                	{
                    	action:
                        {
                        	$(me).unbind('click').click();
                        }
                	},
                    annulla: function ()
                    {
                    	action:
                        {
                        	event.preventDefault();
                        }
                    },
                }
             })
    	});
    }
</script>