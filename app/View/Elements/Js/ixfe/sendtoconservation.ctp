<script>
function sendtoconservation(classe)
{
		
		$(classe).click(
			function()
			{
				addWaitPointer();
				var me = $(this);
					$.ajax({
					method: "POST",
					url: "<?= $this->Html->url(["controller" => "bills","action" => "resendInConservation"]) ?>",
					async : false,
					data:
					{
						billId :  $(me).attr('resendId'),
					},
					success: function(data)
					{
						 location.reload();
					},
					error: function(data)
					{
						location.reload();
					}
				})
		})
		removeWaitPointer();
}
</script>