<script>
    function addcrossremovinghour() {
        $(".rimuoviRigaIconhour").unbind('click');
        $(".rimuoviRigaIconhour").click(function () {
            if ($(".rimuoviRigaIconhour").length > 1) {
                if ($(this).parent('.principalehour').hasClass('ultima_riga')) {
                    $(this).parent('.principalehour').remove();
                    $('.principalehour').last().addClass('ultima_riga');
                } else {
                    $(this).parent('.principalehour').remove();
                }
                addcrossremoving();
            } else {
                $.alert
                ({
                    icon: 'fa fa-warning',
                    title: '',
                    content: 'Attenzione, deve essere sempre presente almeno una riga.',
                    type: 'orange',
                });
            }
        });
    }
</script>