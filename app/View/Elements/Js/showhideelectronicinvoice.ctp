 <script>
  function showHideElectronicInvoice()
  {
      
      
    if($("#BillElectronicInvoice").prop("checked") == true)
    {
        $(".jsEsigibilityVat").show();
       
       // $("#BillAccompagnatoria").prop('disabled',true);

        $("#BillEinvoicevatesigibility").attr("required", "true");
       
        if($("#BillSplitPayment").prop("checked") == true)
        {
            $("#BillEinvoicevatesigibility").val("S");
        }
        
       loadclientispa("#BillClientId");
    }
    else
    {
        $(".jsEsigibilityVat").hide(); 
    //  $("#BillAccompagnatoria").prop('disabled',false);
        $("#BillEinvoicevatesigibility").removeAttr("required");
        
        
        loadclientispa("#BillClientId");
    }
      
        
    // Se fatttura elettronica selezionato mostra i dati per fattura elettronica */
    $("#BillElectronicInvoice").click(function()
    {
        if($(this).prop("checked") == true)
        {
           $(".jsEsigibilityVat").show();
            // $("#BillAccompagnatoria").prop('disabled',true);
            $("#BillEinvoicevatesigibility").attr("required", "true");
            if($("#BillSplitPayment").prop("checked") == true)
            {
                $("#BillEinvoicevatesigibility").val("S");
            }
            
            // Carico il metodo di pagamento del cliente
		    loadclientispa("#BillClientId");
        }
        else
        {

            $(".jsEsigibilityVat").hide();
    //        $("#BillAccompagnatoria").prop('disabled',false);
            $("#BillEinvoicevatesigibility").removeAttr("required");
            

            loadclientispa("#BillClientId");
        }
        
    });
  }
</script>