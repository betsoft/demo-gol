<script>
/* Recupero i dati del cliente */
function loadClientSplitPayment(clientInputId)
{
    $.ajax
    ({
	    method: "POST",
	    url: "<?= $this->Html->url(["controller" => "clients","action" => "getClientSplitPayment"]) ?>",
	    data: 
        {
		    //clientId : $("#BillClientId").val(),
		    clientId : $(clientInputId).val(),
		},
		success : function(data)
		{
    	    if(data == '1')
    	    {
    	    	if($("#BillSplitPayment").prop("checked") == false)
    	    	{
    	    		$("#BillSplitPayment").click();
    	    	}
    	    }
    	    else
    	    {
    	    	if($("#BillSplitPayment").prop("checked") == true)
    	    	{
    	    		$("#BillSplitPayment").click();
    	    	}	
    	    }
	}});
}
</script>