<script>
    //function setMaxStorageQuantity(depositId,storageId,nomeOggetti,classePerIncremento)
    function setMaxStorageQuantity(depositId,storageId,quantityCellId,currentDocumentArticleQuantity)
    {
         $.ajax
         ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "storages","action" => "getAvailableQuantity"]) ?>",
            data: 
            {
                deposit : depositId,
                storageId : storageId,
            },
            success: function(data)
            {
                // Aggiorno il valore max quantity
                if(data <0 ){ data = 0;}
                data = +data + +currentDocumentArticleQuantity; 
                $(quantityCellId).attr('maxquantity',data); 	   
            }
        })
    }
    
</script>