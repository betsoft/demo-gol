<script>
   
    function setArticles()
    {
        var articoli =
        [
            <?php
            foreach ($magazzini as $magazzino)
            {
                $_id      = $magazzino['Storages']['id'];
                $_article =  $magazzino['Storages']['descrizione'];
                $_price   = $magazzino['Storages']['prezzo'];
                $_codice  = $magazzino['Storages']['codice'];
                $_unit  = $magazzino['Storages']['unit_id'];
                $_vat  = $magazzino['Storages']['vat_id'];
                $_barcode  = $magazzino['Storages']['barcode'];
                $_movable = $magazzino['Storages']['movable'];
                $_last_buy_price = $magazzino['Storages']['last_buy_price'];
            ?>
                {
                    label: <?=  '"' . addslashes(str_replace(["\n","\r"], '', $_article)) . '"'; ?>,
                    descrizione: <?= '"' . addslashes(str_replace(["\n","\r"], '',$_article)) . '"'; ?>,
                    value: <?= '"' . $_id . '"'; ?>,
                    prezzo: <?= '"' . $_price . '"'; ?>,
                    codice: <?= '"' . $_codice . '"'; ?>,
                    unit: <?= '"' . $_unit . '"'; ?>,
                    vat: <?= '"' . $_vat . '"'; ?>,
                    barcode: '<?php '"' . addslashes(str_replace(["\n","\r"], '',$_barcode)) . '"'; ?>',
                    movable: <?= '"' . $_movable . '"'; ?>,
                    prezzoacquisto: <?= '"' . $_last_buy_price . '"'; ?>,
                },
            <?php
             } 
            ?>
        ];   
        return articoli;
    }

    function setCodes()
    {
        var codici =
        [
            <?php
            foreach ($magazzini as $magazzino)
            {
                $_id      = $magazzino['Storages']['id'];
                $_article = $magazzino['Storages']['descrizione'];
                $_price   = $magazzino['Storages']['prezzo'];
                $_codice  = $magazzino['Storages']['codice'];
                $_unit  = $magazzino['Storages']['unit_id'];
                $_vat  = $magazzino['Storages']['vat_id'];
                $_barcode  = $magazzino['Storages']['barcode'];
                $_movable = $magazzino['Storages']['movable'];
                $_last_buy_price = $magazzino['Storages']['last_buy_price'];
            ?>
                {
                    label: <?= '"' .  addslashes(str_replace(["\n","\r"], '',$_codice)) . '"'; ?>,
                    value: <?= '"' . $_id . '"'; ?>,
                    prezzo: <?= '"' . $_price . '"'; ?>,
                    codice: <?= '"' . addslashes(str_replace(["\n","\r"], '',$_codice)) . '"'; ?>,
                    descrizione: <?= '"' . addslashes(str_replace(["\n","\r"], '',$_article)) . '"'; ?>,
                    unit: <?= '"' . $_unit . '"'; ?>,
                    vat: <?= '"' . $_vat . '"'; ?>,
                    barcode: '<?php '"' . addslashes(str_replace(["\n","\r"], '',$_barcode)) . '"'; ?>',
                    movable: <?= '"' . $_movable . '"'; ?>,
                    prezzoacquisto: <?= '"' . $_last_buy_price . '"'; ?>,
                },
            <?php
             } 
            ?>
        ];   
        return codici;
    }

    function setArticlesNew(magazzini)
    {
        var articoli = [];
        $(magazzini).each(function(key, magazzino)
        {
            articoli.push({
                label: magazzino.Storages.descrizione,
                descrizione: magazzino.Storages.descrizione,    
                value: magazzino.Storages.id,
                prezzo: magazzino.Storages.prezzo || "",
                codice: magazzino.Storages.codice,
                unit: magazzino.Storages.unit_id,
                vat: magazzino.Storages.vat_id,
                barcode: magazzino.Storages.barcode,
                codice: magazzino.Storages.codice,
                movable: magazzino.Storages.movable,
                prezzoacquisto: magazzino.Storages.last_buy_price,
            })
        });
        return articoli;
    }
    
    function setCodesNew(magazzini)
    {
        var codici = [];
        $(magazzini).each(function(key, magazzino)
        {
            codici.push({
                //label: magazzino.Storages.codice,
                codice: magazzino.Storages.codice,    
                value: magazzino.Storages.id,
                prezzo: magazzino.Storages.prezzo || "",
                codice: magazzino.Storages.codice,
                descrizione: magazzino.Storages.descrizione,
                unit: magazzino.Storages.unit_id,
                vat: magazzino.Storages.vat_id,
                barcode: magazzino.Storages.barcode,
                movable: magazzino.Storages.movable,
                prezzoacquisto: magazzino.Storages.last_buy_price,
            })
        });
        return codici;
    }
   
    

    function setClients()
    {
        <?php if(isset($clients)) { ?>
        var clienti =
        [
            <?php
            foreach ($clients as $client)
            {
                echo '"' . addslashes($client) . '",';
            }
            ?>
        ];
        
        return clienti;
        
        <?php } ?>
    }
    
    
    function setSuppliers()
    {
         <?php if(isset($suppliers)) { ?>
        
         var suppliers = [
            <?php
            
            foreach ($suppliers as $supplier)
              {
                echo '"' . addslashes($supplier) . '",';
              }
            ?>
           ];
          
         return suppliers;
         
        <?php } ?>
    }
</script>