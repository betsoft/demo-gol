 
 <script>
 function loadSupplierDifferentAddress(supplierInuptId,sezioneDiProvenienza)
 {
 	
	 /* Vado a prendere il listino passando eventuali destini diversi passando dalla ragione sociale */
    $.ajax
    ({
	    method: "POST",
		url: "<?= $this->Html->url(["controller" => "bills","action" => "getSupplierDifferentAddress"]) ?>",
		data: 
		{
		    supplierId : supplierInuptId,
		},
		success: function(data)
		{
		    
		    data = JSON.parse(data);
		    console.log(supplierInuptId);
		    console.log(sezioneDiProvenienza);
		    console.log(data);
		    
		    switch(sezioneDiProvenienza)
		    {
				case 'fatture':
					alternativeAddress = 'exit';
				break;
				case  'ddt':
					alternativeAddress = '#TransportSupplierAlternativeaddressId';
				break;
				case 'scontrino':
					alternativeAddress = 'exit';
				break;
				case 'preventivi':
					alternativeAddress = 'exit';
				break;
		    }
		    
		    if(alternativeAddress != 'exit' )
		    {
			    $(alternativeAddress).find('option').remove().end();
			    
				if(data != null && data.length != 0)
				{
				    
	
					$(alternativeAddress).append($("<option></option>").attr("value",'empty').text(""));
	
					$.each(data,
					    function(key, element)
						{
						    if(element.nation != null)
							{
						    	var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province + '  ' + element.nation;
							}
							else
							{
								var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province;
							}
						    $(alternativeAddress).append($("<option></option>").attr("value",element.id).text(indirizzo));             
						})
					console.log("A");
					$("#differentSupplierAddress").show();
				}
				else
				{
				    $(alternativeAddress).val('');
					$("#differentSupplierAddress").hide();
				}
		    }
		}
    });
 }
 </script>