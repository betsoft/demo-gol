<script>
    function addcrossremovingddt() {
        $(".rimuoviRigaIconddt").unbind('click');
        $(".rimuoviRigaIconddt").click(function () {
            if ($(".rimuoviRigaIconddt").length > 1) {
                if ($(this).parent('.principaleddt').hasClass('ultima_riga')) {
                    $(this).parent('.principaleddt').remove();
                    $('.principaleddt').last().addClass('ultima_riga');
                } else {
                    $(this).parent('.principaleddt').remove();
                }
                addcrossremoving();
            } else {
                $.alert
                ({
                    icon: 'fa fa-warning',
                    title: '',
                    content: 'Attenzione, deve essere sempre presente almeno una riga.',
                    type: 'orange',
                });
            }
        });
    }
</script>