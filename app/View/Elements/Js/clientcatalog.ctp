<?= $this->element('Js/clientvat'); ?>
<?= $this->element('Js/setmaxstoragequantity'); ?>
<script>

var magazzinoCliente = null;

function loadClientCatalog(classePerIncremento, clientId, clientInputId, nomeOggetti)
{
    
    if(magazzinoCliente == null) {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills", "action" => "getClientCatalog"]) ?>",
            async: false,
            data:
                {
                    clientId: clientId,
                    clientInputId: clientInputId,
                    action: '<?= $this->action; ?>',
                },
            success: function (data) {
                magazzinoCliente = data;
                setDataToRow(classePerIncremento, clientId, clientInputId, nomeOggetti);
            }
        });
    }
    else {
        setDataToRow(classePerIncremento, clientId, clientInputId, nomeOggetti);
    }

     function setDataToRow(classePerIncremento, clientId, clientInputId, nomeOggetti) {
         var articoli = JSON.parse(magazzinoCliente);
         var codici = JSON.parse(magazzinoCliente);

         $(codici).each(function (key, val) {
             val.label = val.codice;
         })

         switch (clientInputId) {
             case '#QuoteClientId':
                 sezioneDiProvenienza = 'preventivi';
                 campoDescrizione = "Description";
                 campoCodice = "Codice";
                 break;
             case '#BillClientId':
                 sezioneDiProvenienza = 'fatture';
                 campoDescrizione = "Oggetto";
                 campoCodice = "Codice";
                 break;
             case '#ReceiptClientId':
                 sezioneDiProvenienza = 'scontrino';
                 campoDescrizione = "Oggetto";
                 campoCodice = "Codice";
                 break;
             case '#TransportClientId':
                 sezioneDiProvenienza = 'ddt';
                 campoDescrizione = "Oggetto";
                 campoCodice = "Codice";
                 break;
             case '#MaintenanceClientId':
                 sezioneDiProvenienza = 'schedaintervento';
                 campoDescrizione = "Description";
                 campoCodice = "Codice";
                 break;
         }

         $("#" + nomeOggetti + classePerIncremento + campoDescrizione).autocomplete
         (
             {
                 focus: function (event, ui) {
                     event.preventDefault();
                     return false;
                 },
                 select: function (event, ui) {
                     event.preventDefault();
                     autocompleteItem(sezioneDiProvenienza, classePerIncremento, ui.item, 'descrizione');
                 },
                 change: function (event, ui) // Fix per rimuovere i value vuoti quando non autocomplete
                 {
                     // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
                     if (ui.item == null) {
                         setReadonlyValues('descrizione', this, articoli, classePerIncremento, sezioneDiProvenienza);
                     }
                 },
                 source: articoli
             }
         );


         $("#" + nomeOggetti + classePerIncremento + campoCodice).autocomplete
         ({
             focus: function (event, ui) {
                 event.preventDefault();
                 return false;
             },
             select: function (event, ui) {
                 event.preventDefault();
                 autocompleteItem(sezioneDiProvenienza, classePerIncremento, ui.item, 'codice');
             },
             change: function (event, ui) // Fix per rimuovere i value vuoti quando non autocomplete
             {
                 // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
                 if (ui.item == null) {
                     setReadonlyValues('codice', this, codici, classePerIncremento, sezioneDiProvenienza);
                 }
             },
             source: codici
         });
     }
	
	// Funzione per il settaggi odei readonly
	function setReadonlyValues(typeOfAutocomplete,thisElement,valori,classePerIncremento,sezioneDiProvenienza)
	{
	    
	    var rowDescription  = "#" + nomeOggetti + classePerIncremento;
	    
	    switch(sezioneDiProvenienza)
        {
            case 'ddt':
                var fieldDescription = rowDescription + "Oggetto";
                var removeMaxQuantity = true;
            break;
            case 'fatture':
                var fieldDescription = rowDescription + "Oggetto";
                var removeMaxQuantity = true;
            break;
             case 'scontrino':
                var fieldDescription = rowDescription + "Oggetto";
                var removeMaxQuantity = true;
            break;
            case 'preventivi':
                var fieldDescription = rowDescription + "Description";
                var fieldQuantity =  rowDescription + "Quantity";
                var removeMaxQuantity = false;
            break;
            case 'schedaintervento':
                var fieldDescription = rowDescription + "Description";
                var fieldQuantity =  rowDescription + "Quantity";
                break;
        }

	    var fieldCode = rowDescription + "Codice"; 
	    var fieldStorageId = rowDescription + "StorageId";
	    var fieldBarcode =  rowDescription + "Barcode";
	    var fieldType =  rowDescription + "Tipo";
	    var fieldMovable =  rowDescription + "Movable";
	    var fieldQuantity =  rowDescription + "Quantita";
	    var fieldImporto =  rowDescription + "Importo";
	    
	    var txtValue = $(thisElement).val();
        var forceAutocomplete = false;
        var selectedItem = null;

	    // Change della descrizione
	    if(typeOfAutocomplete == 'descrizione')
	    {
	       // I valori erano solo codici
            $(valori).each(
                function()
                {
                    if(this.descrizione != null)
                    {
                        if(this.descrizione.toLowerCase() == txtValue.toLowerCase())
                        {
                            forceAutocomplete =  true;
                            selectedItem = this;
                        }
                        else
                        {
                        }
                    }
                }
            )
	        
	        // Se c'è un forceautocomplete
            if(forceAutocomplete)
            {
                resetFields(fieldCode,fieldDescription);
                // verrà ricompilato dall'autocomplete
                $(fieldDescription).on( "autocompleteselect", autocompleteItem(sezioneDiProvenienza, classePerIncremento, selectedItem,'descrizione'));
            }
            else
            {
                manageFields(fieldType,fieldCode);
            }
	         
	         if($(fieldType).val() == '' || $(fieldType).val() === undefined || $(fieldType).val() == null )
	         {
	            $(fieldMovable).val('');
	            $(fieldCode).parent('div').find('.fa-asterisk').hide();
	         }
	        
	        switch(sezioneDiProvenienza)
	        {
	           case 'ddt': var removeMaxQuantity = true; break;
	           case 'fatture': var removeMaxQuantity = true; break;
	           case 'scontrino': var removeMaxQuantity = true; break;
	           case 'preventivi': var removeMaxQuantity = false; break;
                case 'schedaintervento': var removeMaxQuantity = true; break;
	        }

            if(removeMaxQuantity == true)                                
            {
                $(fieldStorageId).removeAttr('value');
                $(fieldQuantity).removeAttr('maxquantity');
            }
	    }
	    
	    // Change del codice
	    if(typeOfAutocomplete == 'codice')
	    {
          
            // I valori erano solo codici
            $(valori).each(
                function()
                {
                    if(this.codice != null)
                    {
                        if(this.codice.toLowerCase() == txtValue.toLowerCase())
                        {
                            forceAutocomplete =  true;
                            selectedItem = this;
                            return;
                        }
                        else
                        {
                        }
                    }
                }
            )

            // Se c'è un forceautocomplete
            if(forceAutocomplete)
            {
                // Resetto i campi
                resetFields(fieldCode,fieldDescription); 
                // Forzo l'autocomplete
                $(fieldCode).on( "autocompleteselect", autocompleteItem(sezioneDiProvenienza, classePerIncremento, selectedItem,'codice'));
            }
            else
            {
                 manageFields(fieldType,fieldDescription);
            }

             if($(fieldType).val() == '' || $(fieldType).val() === undefined || $(fieldType).val() == null )
	         {
	            $(fieldMovable).val('');
	            $(fieldCode).parent('div').find('.fa-asterisk').hide();
	         }

            switch(sezioneDiProvenienza)
	        {
	           case 'ddt': var removeMaxQuantity = true; break;
	           case 'fatture': var removeMaxQuantity = true; break;
	           case 'scontrino': var removeMaxQuantity = true; break;
	           case 'preventivi': var removeMaxQuantity = false; break;
	           case 'schedaintervento': var removeMaxQuantity = true; break;
	        }

            if(removeMaxQuantity == true)
            {
                $(fieldStorageId).removeAttr('value');
                $(fieldQuantity).removeAttr('maxquantity');
            }
	    }
	}
	
	
	 function resetFields(fieldCode,fieldType)
	{
	    // Resetto il campo codice 
        $(fieldCode).removeAttr('readonly');
        $(fieldCode).css("background", "#ffffff");
        $(fieldCode).val('');
        
        // Resetto il field Type
        $(fieldType).val('');
        $(fieldType).removeAttr('readonly');
        $(fieldType).css("background", "#ffffff");
	}
	
	
	 function manageFields(fieldType,fieldDescriptionOrCode)
	{

	    if($(fieldType).attr('disabled') !== undefined)
        {
            $(fieldType).val('');
            $(fieldType).removeAttr('disabled'); 
            $(fieldType).css("background", "#ffffff");
        }
                
        // Resetto il campo codice
        if($(fieldDescriptionOrCode).attr('readonly') !== undefined)
        {
            $(fieldDescriptionOrCode).val(''); 
            $(fieldDescriptionOrCode).removeAttr('readonly');
            $(fieldDescriptionOrCode).css("background", "#ffffff");
        } 
	}
	
	/** Codice che viene esegueti all'autocomplete **/
	function autocompleteItem(sezioneDiProvenienza, classePerIncremento, item, typeOfAutocomplete)
	{
	    // Ripristino eventuale errore
	    $('.fastErroreMessage').remove();
	    $('.jsQuantity').css('border-color','#e5e5e5');
	    
	    var rowDescription  = "#" + nomeOggetti + classePerIncremento;
	    
	    switch(sezioneDiProvenienza)
	    {
	        case 'fatture':
	            var fieldDescription =  rowDescription + "Oggetto";
	            var fieldPrice = rowDescription + "Prezzo";
	            var fieldUnitOfMeasure =rowDescription + "Unita";
	            var fieldVat = rowDescription + 'IvaId';
	            var fieldQuantity =  rowDescription + "Quantita";
	        break;
	         case 'scontrino':
	            var fieldDescription =  rowDescription + "Oggetto";
	            var fieldPrice = rowDescription + "Prezzo";
	            var fieldUnitOfMeasure =rowDescription + "Unita";
	            var fieldVat = rowDescription + 'IvaId';
	            var fieldQuantity =  rowDescription + "Quantita";
	        break;
	        case 'preventivi':
	            var fieldDescription = rowDescription + "Description";
	            var fieldPrice = rowDescription + "QuoteGoodRowPrice";
	            var fieldUnitOfMeasure = rowDescription +"UnitOfMeasureId";
	            var fieldVat = rowDescription + 'QuoteGoodRowVatId';
	            var fieldQuantity =  rowDescription + "Quantity";
	       break;
	        case 'ddt':
	            var fieldDescription = rowDescription + "Oggetto";
	            var fieldPrice = rowDescription + "Prezzo";
	            var fieldUnitOfMeasure = rowDescription + "Unita";
	            var fieldVat = rowDescription +'IvaId';
	            var fieldQuantity =  rowDescription + "Quantita";
	        break;
            case 'schedaintervento':
                var fieldDescription = rowDescription + "Description";
           //     var fieldPrice = rowDescription + "Prezzo";
                var fieldUnitOfMeasure = rowDescription + "UnitOfMeasureId";
           //     var fieldVat = rowDescription +'IvaId';
                var fieldQuantity =  rowDescription + "Quantity";
           break;
	    }

	    var fieldCode = rowDescription + "Codice";
	    var fieldStorageId = rowDescription + "StorageId";
	    var fieldBarcode =  rowDescription + "Barcode";
	    var fieldType =  rowDescription + "Tipo";
	    var fieldMovable =  rowDescription + "Movable";
	    var fieldImporto =  rowDescription + "Importo";
	    
	    if(typeOfAutocomplete == 'descrizione')
	    {
            //$(fieldCode).attr("readonly", true);
            //$(fieldCode).css("background", "#f7f7f7");
            $(fieldDescription).attr("readonly", false);
            $(fieldDescription).css("background", "#ffffff");
            $(fieldDescription).val(item.descrizione); //<-- OK
	     }
	    
	    if(typeOfAutocomplete == 'codice')
	     {
	        //$(fieldDescription).attr("readonly", true);
            //$(fieldDescription).css("background", "#f7f7f7");
            $(fieldCode).attr("readonly", false);
            $(fieldCode).css("background", "#ffffff");
            $(fieldDescription).val(item.descrizione);
	     }

         $(fieldPrice).val(item.prezzo);
         
         <?php if(MULTICURRENCY)
         { ?>
            $(rowDescription+"Currencyprice").val((item.prezzo * $("#BillChangevalue").val()).toFixed(2));
         <?php } ?>
         
         $(fieldVat).val(item.vat);
         $(fieldStorageId).val(item.value);
         $(fieldCode).val(item.codice);
         $(fieldUnitOfMeasure).val(item.unit);
         $(fieldBarcode).val(item.barcode);
         

         // A Seconda della tipologia modifico il tipo e metto disabled
         $(fieldType).val(item.movable);
         $(fieldType).attr("disabled", true);
         $(fieldMovable).val(item.movable);

	    // Ridondo il required
        /*if(item.vat == null)
        {
            // Carico l'iva del cliente
            loadClientVat(clientId,"#"+nomeOggetti+ classePerIncremento +fieldVat);   
        }
        else 
        {
            $("#"+nomeOggetti + classePerIncremento + fieldVat).val(item.vat);
        }*/
        
         loadClientVat(clientId,null,
         function(data,fieldToFill)
         {
            if(data == "" || data == 0)
			{
			}
			else
			{	
			    loadClientVat(clientId,fieldVat);  
			}
         });
        
        if(item.movable == 1)
        { 
            $(fieldCode).attr("required", true);
            $(fieldCode).parent(".col-md-2").find(".fa.fa-asterisk").show();
        }
        
        if(item.movable == 0)
        { 
            $(fieldCode).removeAttr("required");
            $(fieldCode).parents(".col-md-2").find(".fa.fa-asterisk").hide();
        }

        // Metto il maxquantity a fatture
        if(sezioneDiProvenienza == 'fatture')
        {
             <?php 
                if(isset($bills['Bill']['accompagnatoria']) && $bills['Bill']['accompagnatoria'] == 1)
                { ?>
                    setMaxStorageQuantity($("#BillDepositId").val(),item.value,fieldQuantity,0); 
                <?php 
                }
                else
                { ?>
                    if($("#BillAccompagnatoria").prop("checked") == true)
                    {
                        setMaxStorageQuantity($("#BillDepositId").val(),item.value,fieldQuantity,0); 
                    }
            <?php } ?>
        }
        
        
        // metto il maxquantity a bolle
        if(sezioneDiProvenienza == 'ddt' )
        {
            // Recupero massimo dell'articolo a deposito
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "storages","action" => "getAvailableQuantity"]) ?>",
                data: 
                {
                    deposit : $("#TransportDepositId").val(),
                    storageId : item.value,
                },
                success: function(data)
                {
                    // Aggiorno il valore max quantity
                    $(fieldQuantity).attr('maxquantity',data); 	   
                }
            })
        }
        
        
        // metto il maxquantity gli scontrini
        if(sezioneDiProvenienza == 'scontrino' )
        {
            // Recupero massimo dell'articolo a deposito
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "storages","action" => "getAvailableQuantity"]) ?>",
                data: 
                {
                    deposit : $("#BillDepositId").val(),
                    storageId : item.value,
                },
                success: function(data)
                {
                    // Aggiorno il valore max quantity
                    $(fieldQuantity).attr('maxquantity',data); 	   
                }
            })
        }
        
        // Calcolo l'importo
        var quantity =  $(fieldQuantity).val();
        var prezzo  =  $(fieldPrice).val();
        var importo = quantity * prezzo;
        $(fieldImporto).val(importo.toFixed(2));
	}
        
}
</script>

      
                   