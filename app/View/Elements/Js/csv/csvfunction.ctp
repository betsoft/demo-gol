<script>
    function createAccountantCsv(itemId,bakValue)
    {
    	$(itemId).click(
	    	function()
		    {
    			if(bakValue == '')
	    		{
		    		$.alert
    	            ({
    			    	icon: 'fa fa-warning',
    	    			title: '',
        				content: 'Nessuna banca selezionata, selezionare la banca per cui fare il flusso riba.',
            			type: 'orange',
			    	});
    			}
	    		else
		    	{
			    	$.ajax({
    					method: "POST",
	    				url: "<?= $this->Html->url(["controller" => "bills","action" => "createAccountantCsv",null]) ?>",
		    			data:
			    		{
				    		bank : bakValue,
					    },
    					success: function(data)
	    				{
		    				csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
			    			var a = document.createElement('A');
				    		a.href = csvData;
					    	a.download = 'IM000000.D';
						    document.body.appendChild(a);
    						a.click();
	    					document.body.removeChild(a);
		    			},
			    		error: function(data)
				    	{
					    	// Error on csv creation
					    }
    				})
	    		}
            });
    }
</script>