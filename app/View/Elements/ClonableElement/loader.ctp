<script>
    addLoadEvent(function() {

        clonableInitializeClonables();

    }, 'ClonableElementLoader');

    //Utilities functions
    function clonableInitializeClonables() {
        var clonableElements = ["input","select","checkbox"];

        initilizeClonables();

        function initilizeClonables() {

            $(".clone").unbind('click.ClonableElementLoader');
            $(".copy").unbind('click.ClonableElementLoader');
            $(".clear").unbind('click.ClonableElementLoader');
            $(".remove").unbind('click.ClonableElementLoader');
            $(".remove-all").unbind('click.ClonableElementLoader');
            $(".moveup").unbind('click.ClonableElementLoader');
            $(".movedown").unbind('click.ClonableElementLoader');

            buildIndexes();

            $(".clone").on('click.ClonableElementLoader', add);
            $(".copy").on('click.ClonableElementLoader', copy);
            $(".clear").on('click.ClonableElementLoader', clear);
            $(".remove").on('click.ClonableElementLoader', remove);
            $(".remove-all").on('click.ClonableElementLoader', removeAll);
            $(".moveup").on('click.ClonableElementLoader', moveUp);
            $(".movedown").on('click.ClonableElementLoader', moveDown);

            var clonables = $(".clonable-element .clonable")
            if(clonables.length == 1) {
                var clonable = clonables.first();
                if(isHiddenClonable(clonable))
                    hideClonable(clonable);
            }
        }

        function add() {
            var clonableElement = $(this).parents(".clonable-element").first();
            var clonable = clonableElement.find(".clonable").last();

            // Se la prima riga è nascosta <default> allora la mostro
            if(isHiddenClonable(clonable))
            {
                showClonable(clonable);
                hideEmptyMessage(clonableElement);
            }
            else // Altrimenti clono la riga precedente
            {
                var newClonable = clonable.clone(true, true);
                newClonable.removeClass('from-db')

                clonable.find(".clone").css('visibility','hidden');

                if(hasRemoveIcon(clonable))
                    addRemoveIcon(newClonable);

                if(hasCopyIcon(clonable))
                    addCopyIcon(newClonable);

                if(hasClearIcon(clonable))
                    addClearIcon(newClonable);

                if(hasMoveUpIcon(clonable))
                    addMoveUpIcon(newClonable);

                if(hasMoveDownIcon(clonable))
                    addMoveDownIcon(newClonable);

                emptyValues(newClonable);

                clonable.after(newClonable);

                refreshIndexes();

                setCorrectIcon(clonableElement);

                newClonable.find('.multiselect-native-select').each(function() {
                    var id = $(this).find('select').attr('id')

                    var a = $(this).find('select').clone(false, false);
                    a.find('option').remove()
                    $(this).replaceWith(a)

                    initializeFilterableSelects(id);
                })
            }
        }

        function copy() {
            var clonableElement = $(this).parents(".clonable-element").first();
            var clonable = $(this).parents(".clonable").first();

            var newClonable = clonable.clone(true, true);
            newClonable.removeClass('from-db')

            cloneValues(clonable, newClonable);

            if(hasRemoveIcon(clonable))
                addRemoveIcon(newClonable);

            if(hasCopyIcon(clonable))
                addCopyIcon(newClonable);

            if(hasClearIcon(clonable))
                addClearIcon(newClonable);

            if(hasMoveUpIcon(clonable))
                addMoveUpIcon(newClonable);

            if(hasMoveDownIcon(clonable))
                addMoveDownIcon(newClonable);

            clonable.after(newClonable);

            setCorrectIcon(clonableElement);

            refreshIndexes();
        }

        function clear() {
            var clonable = $(this).parents(".clonable").first();
            emptyValues(clonable);
        }

        function moveUp() {
            var clonableElement = $(this).parents(".clonable-element").first();
            var clonable = $(this).parents(".clonable").first();
            clonable.prev(".clonable").before(clonable);

            setCorrectIcon(clonableElement);
        }

        function moveDown() {
            var clonableElement = $(this).parents(".clonable-element").first();
            var clonable = $(this).parents(".clonable").first();
            clonable.next(".clonable").after(clonable);

            setCorrectIcon(clonableElement);
        }

        function remove() {
            var clonableElement = $(this).parents(".clonable-element").first();
            var clonable = $(this).parents('.clonable').first();
            var clonablesCount = clonable.nextAll('.clonable').length + clonable.prevAll('.clonable').length + 1;

            if(clonablesCount > 1)
                clonable.remove();
            else {

                hideClonable(clonable);

                emptyValues(clonable);

                showEmptyMessage(clonableElement);
            }

            clonableElement.find(".clonable").last().find(".clone").css('visibility', 'visible');

            setCorrectIcon(clonableElement);
        }

        function removeAll() {
            var clonableElement = $(this).parents(".clonable-element").first();

            clonableElement.find('.clonable').each(function() {

                var clonable = $(this);
                var clonablesCount = clonable.nextAll('.clonable').length + clonable.prevAll('.clonable').length + 1;

                if(clonablesCount > 1)
                    clonable.remove();
                else {

                    hideClonable(clonable);

                    emptyValues(clonable);

                    showEmptyMessage(clonableElement);
                }

                clonableElement.find(".clonable").last().find(".clone").css('visibility', 'visible');

                setCorrectIcon(clonableElement);
            })
        }

        function emptyValues(clonable) {
            clonableElements.forEach(function(item, index) {
                clonable.find(item).each(function() {
                    if(item == "input") {
                        $(this).val('');
                    }
                    else if(item == "select") {
                        $(this).find(":selected").each(function() {
                            $(this).attr('selected', false);
                        });
                    }
                    else if(item == "checkbox") {
                        $(this).prop('checked', false);
                    }
                });
            });
            clonable.find('.clonable-title').html('');
            clonable.find('.clonable-message').html('');
        }

        function cloneValues(original, copy) {
            clonableElements.forEach(function(item, index) {
                original.find(item).each(function() {
                    if(item == "select") {
                        var originalSelect = $(this);
                        var originalSelectedOption = originalSelect.find(":selected");
                        var originalValue = originalSelectedOption.val();
                        var originalName = originalSelect.attr('name');
                        var copiedSelect = copy.find(item + '[name="' + originalName + '"]');

                        copiedSelect.val(originalSelect.val());

                        /*if(originalValue != undefined && originalValue != '')
                            copiedSelect.find("option[value=" + originalValue + "]").attr('selected', true);
                        else
                            copiedSelect.find("option:selected").attr("selected",false);*/
                    }
                });
            });
        }

        function addCopyIcon(newClonable) {
            newClonable.find(".copy-container").html('<i class="fa fa-copy copy">');
            newClonable.find(".copy").click(copy);
        }

        function addClearIcon(newClonable) {
            newClonable.find(".clear-container").html('<i class="fa fa-eraser clear">');
            newClonable.find(".clear").click(clear);
        }

        function addMoveUpIcon(newClonable) {
            newClonable.find(".moveup-container").html('<i class="fa fa-sort-asc moveup"></i>');
            newClonable.find(".moveup").click(moveUp);
        }

        function addMoveDownIcon(newClonable) {
            newClonable.find(".movedown-container").html('<i class="fa fa-sort-desc movedown"></i>');
            newClonable.find(".movedown").click(moveDown);
        }

        function addRemoveIcon(newClonable) {
            newClonable.find(".remove-container").html('<i class="fa fa-trash remove">');
            newClonable.find(".remove").click(remove);
        }

        function hasCopyIcon(clonable) {
            return clonable.find(".copy-container").length > 0;
        }

        function hasClearIcon(clonable) {
            return clonable.find(".clear-container").length > 0;
        }

        function hasMoveUpIcon(clonable) {
            return clonable.find(".moveup-container").length > 0;
        }

        function hasMoveDownIcon(clonable) {
            return clonable.find(".movedown-container").length > 0;
        }

        function hasRemoveIcon(clonable) {
            return clonable.find(".remove-container").length > 0;
        }

        function showEmptyMessage(clonableElement) {
            var emptyMessageElement = clonableElement.find('.clonable-head .header-empty-message');

            if(emptyMessageElement.css('visibility') == 'hidden') {
                emptyMessageElement.css('visibility','visible').css('height','40px');
            }
        }

        function hideEmptyMessage(clonableElement) {
            var emptyMessageElement = clonableElement.find('.clonable-head .header-empty-message');

            if(emptyMessageElement.css('visibility') == 'visible') {
                emptyMessageElement.css('visibility','hidden').css('height','0');;
            }
        }

        function showClonable(clonable) {
            clonable.find('[required-when-visible]').each(function() {
                $(this).removeAttr('required-when-visible');
                $(this).prop('required',true);
            });
            clonable.find('[disabled]').each(function() {
                $(this).removeAttr('disabled');
            });
            clonable.find('[readonly]').each(function() {
                if($(this).parents().hasClass("changeReadOnlyToDisabled"))
                {
                    $(this).removeAttr('readonly');
                    $(this).attr('disabled','disabled');
                }
            });

            clonable.show();
        }

        function hideClonable(clonable) {
            clonable.find('[required]').each(function() {
                $(this).attr('required-when-visible','');
                $(this).prop('required',false);
            });

            clonable.find(getClonableElementsSelector()).each(function() {
                $(this).attr('disabled','disabled');

                /* if($(this).attr('required') != undefined) {
                    $(this).attr('required-when-visible','');
                    $(this).prop('required',false);
                } */
            });
            clonable.hide();
        }

        function isHiddenClonable(clonable) {
            return clonable.css('display') == 'none';
        }

        function setCorrectIcon(clonableElement) {
            clonableElement.find(".fa-trash").show() ;
            clonableElement.find(".clonable-actions .moveup").show();
            clonableElement.find(".clonable").first().find(".clonable-actions .moveup").hide();
            clonableElement.find(".clonable-actions .movedown").show();
            clonableElement.find(".clonable").last().find(".clonable-actions .movedown").hide();
        }

        function buildIndexes() {
            $(".clonable").each(function(index) {
                var clonable = $(this);
                var aggregator = $(this).parents('.clonable-element').first().attr('aggregator');
                var onBuildIndexCallback = clonable.parents('.clonable-element').first().attr('on-build-index-callback');

                clonable.find(getClonableElementsSelector()).each(function() {
                    var input = $(this);

                    if(input.attr('name') != undefined) {
                        var name = input.attr('name').split('[').pop().split(']')[0];
                        var uniqueName = aggregator + '[' + index + ']' + '[' + name + ']';
                        input.attr('name', uniqueName);

                        if(!input.hasClass('form-control-no-border')) {
                            input.addClass('form-control-no-border');

                            if(input.parents('.clonable-element-input-container').length == 0)

                                input.parents('.clonable .content div').last().addClass('clonable-element-input-container')

                            if(input.siblings().length == 0)
                                input.css('width','100%').css('padding-right','10px');
                        }

                        if(isHiddenClonable(clonable) && input.attr('required') == 'required') {
                            input.attr('required-when-visible','');
                            input.prop('required', false);
                        }
                    }

                    if($.isFunction(window[onBuildIndexCallback])) {
                        window[onBuildIndexCallback](input, index)
                    }
                });
            });
        }

        function refreshIndexes() {
            $(".clonable").each(function(index) {
                var clonable = $(this);
                var aggregator = $(this).parents('.clonable-element').first().attr('aggregator');
                var onRefreshIndexCallback = clonable.parents('.clonable-element').first().attr('on-refresh-index-callback');

                clonable.find(getClonableElementsSelector()).each(function() {
                    var input = $(this);

                    if(input.attr('name') != undefined) {
                        var name = input.attr('name');
                        //get the name without the part coming after the first square bracket
                        var nonIndexedName = name.substring(name.lastIndexOf("["), name.length);
                        var uniqueName = aggregator + '[' + index + ']' + nonIndexedName;
                        input.attr('name', uniqueName);

                        try {
                            if(input.hasClass('date-picker'))
                                datepickerRestore(input);

                            if(input.hasClass('timepicker'))
                                timepickerRestore(input);
                        }
                        catch(err){}
                    }

                    if($.isFunction(window[onRefreshIndexCallback])) {
                        window[onRefreshIndexCallback](input, index)
                    }
                });
            });
        }

        function getClonableElementsSelector() {
            var string = "";
            clonableElements.forEach(function(item, index) {
                string += ".content " + item;
                if(index != clonableElements.length - 1)
                    string += ",";
            });
            return string;
        }
    }

    function clonableGetFirstRow(clonableContainer) {
        return clonableContainer.find('.clonable').first();
    }

    function clonableGetLastRow(clonableContainer) {
        return clonableContainer.find('.clonable').last();
    }

    function clonableGetCloneButton(clonableContainer) {
        return clonableContainer.find('.clone').first();
    }

    function clonableGetRemoveButton(clonableContainer) {
        return clonableContainer.find('.remove-all').first();
    }

</script>

<?= $this->Html->css('ClonableElement/default') ?>
<?= $this->Html->css('ClonableElement/custom') ?>
