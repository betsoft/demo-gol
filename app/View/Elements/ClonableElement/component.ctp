<?php
/*
    $aggregator:
        prefisso che verrà usato per aggregare nel medesimo array i campi data
		appartenenti allo stesso form
	$title:
	    titolo blocco
	$description:
	    descrizione blocco
	$content:
	    contenuto HTML del blocco clonabile
	$header:
	    contenuto HTML della riga di intestazione
	$headerActions:
	    elementi extra da aggiungere nella barra delle azioni
	$actions:
	    array delle azioni attive per l'elemento clonabile, alla destra del singolo elemento
*/
/*
$template = [
    [
        'name'=>'input1',
        'type' => 'Form/Components/DatePicker/component',
        'options' => [
            "aggregator" => 'ciao',
            "prefix" => "ciao_birthdate",
            "input" => [
                "options" => [
                    "label" => false,
                    "required" => true,
                ]
            ]
        ],
        'arrayPaths' => [
            //'key dell'array datas: value | options | checked | ... ' => 'path in cui salvare il valore di data nell'array options del template'
            'value' => ['input','options','value'],
            'name' => ['name']
        ]
    ],
    [
        'name'=>'select1',
        'type' => 'select',
        'options' => [
            'class'=>'form-control ',
            'label'=>false ,
            'required'=>true,
            'templates' => [
                'inputContainer' => '<div style="display:inline-block" class="col-md-3">{{content}}</div>'
            ]
        ]
    ],
];
$datas = [
    [
        ['value'=>'10-01-2016'],
        ['value'=>'2', 'options'=>['ciao','ciao2','ciao3']],
    ],
    [
        ['value'=>'20-12-2010'],
        ['value'=>'1', 'options'=>['ciao4','ciao5','ciao6']],
    ],
];
*/

if(!isset($aggregator))
    $aggregator = '';

if(!isset($title))
    $title = '';

if(!isset($descriptionLong))
    $descriptionLong = '';

if(!isset($description))
    $description = '';

if(!isset($content))
    $content = '';

if(is_array($content))
    $content = implode('',$content);

if(!isset($header))
    $header = '';

if(is_array($header))
    $header = implode('',$header);

if(!isset($actions))
    $actions = [
        'copy',
        'clear',
        'move',
        'remove',
    ];

if(!isset($onBuildIndexCallback))
    $onBuildIndexCallback = false;

if(!isset($onRefreshIndexCallback))
    $onRefreshIndexCallback = false;

if(!isset($headerActions['default']))
    $headerActions['default'] = true;

if(!function_exists ("insert_using_keys"))
{
    function insert_using_keys($arr, $keys, $value) {
        // we're modifying a copy of $arr, but here
        // we obtain a reference to it. we move the
        // reference in order to set the values.
        $a = &$arr;

        while( count($keys) > 0 ){
            // get next first key
            $k = array_shift($keys);

            // if $a isn't an array already, make it one
            if(!is_array($a)){
                $a = array();
            }

            // move the reference deeper
            $a = &$a[$k];
        }
        $a = $value;

        // return a copy of $arr with the value set
        return $arr;
    }
}
?>

<div class="col-md-12 clonable-element portlet light " aggregator="<?= __($aggregator) ?>" <?= $onBuildIndexCallback ? "on-build-index-callback='$onBuildIndexCallback'" : '' ?> <?= $onRefreshIndexCallback ? "on-refresh-index-callback='$onRefreshIndexCallback'" : '' ?>>
    <div class="form-group title">
        <div>
            <label class="" style="font-size:14px;"><b><?= __($title) ?></b></label>
        </div>
    </div>
    <div class="form-group title">
        <label>
            <span class="btn btn-outline btn-sm color clone" style="padding:5px; <?= !$headerActions['default'] ? 'display:none' :'' ?>"><?= __($description) ?></span>
            <span class="btn btn-outline btn-sm color remove-all" style="padding:5px"><?= __('Rimuovi tutti') ?></span>
            <?php unset($headerActions['default']); ?>
            <?php foreach($headerActions as $headerAction): ?>
                <?= $headerAction ?>
            <?php endforeach; ?>
        </label>
    </div>
    <div class="form-group clonable-head">
        <div class="col-md-10 header-content">
            <b><?= $header ?></b>
        </div>
        <div class="col-md-2 header-content"><center><b>Azioni</b></center></div>
        <div class="col-md-12 header-empty-message" style="<?= (!isset($template) && !isset($datas)) || count($datas) == 0 ? 'visibility:visible; height:40px' : 'visibility:hidden; height:0px' ?>">
            Nessun elemento aggiunto
        </div>
    </div>
    <div class="clonables-container col-md-12">
        <?php if(isset($template) && isset($datas)): ?>
            <?php foreach($datas as $index => $data): ?>
                <div class="form-group clonable <?= isset($data['disabled']) && $data['disabled'] ? 'clonable-disabled' : '' ?>">
                    <div class="content col-md-12 clonable-title"><?= isset($data['title']) ? $data['title'] : '' ?></div>
                    <div class="content col-md-10">
                        <?php foreach($template as $key => $element): ?>
                            <?php
                            if($element != '')
                            {
                                try
                                {
                                    //provo a creare l'element usando il type se fallisce lo tratto come un input normale
                                    if(!isset($element['arrayPaths']))
                                        throw new Exception();
                                    $arrayPaths = $element['arrayPaths'];
                                    foreach($arrayPaths as $propertyName => $arrayPath)
                                    {
                                        if(isset($element['name']) && $propertyName == 'name')
                                            $element['options'] = insert_using_keys($element['options'], $arrayPath, $element['name']);
                                        else
                                        {
                                            if(isset($data[$key][$propertyName]) || $data[$key][$propertyName] == null)
                                                $element['options'] = insert_using_keys($element['options'], $arrayPath, $data[$key][$propertyName]);
                                            else
                                                throw new Exception();
                                        }
                                    }

                                    echo $this->element($element['type'], $element['options']);
                                }
                                catch(Exception $ex)
                                {
                                    $name = $element['name'];
                                    $options = $element['options'];

                                    if(isset($element['type']))
                                        $options['type'] = $element['type'];

                                    $options['value'] = $data[$key]['value'];

                                    switch ($options['type'])
                                    {
                                        case 'select'     : $options['options'] = $data[$key]['options']; break;
                                        case 'checkbox '  : $options['checked'] = $data[$key]['checked']; break;
                                        case 'input'      : throw new Exception('Eccezione interna: inserito element con type input'); break;
                                    }

                                    echo $this->Form->input($name, $options);
                                }
                            }
                            ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-2 clonable-actions">
                        <center>
                            <?php foreach($actions as $action): ?>
                                <?php
                                $display = $index == 0 ? "display:none" : "";
                                ?>
                                <?php if($action == 'copy'): ?>
                                    <span class="copy-container">
                          		    <i class="fa fa-copy copy"></i>
                          		</span>
                                <?php endif; ?>

                                <?php if($action == 'clear'): ?>
                                    <span class="clear-container">
                          		    <i class="fa fa-eraser clear"></i>
                          		</span>
                                <?php endif; ?>

                                <?php if($action == 'move'): ?>
                                    <span class="moveup-container">
                               	    <i class="fa fa-sort-asc moveup" style="<?= $display ?>"></i>
                               	</span>
                                    <span class="movedown-container">
                               	    <i class="fa fa-sort-desc movedown" style="<?= $display ?>"></i>
                               	</span>
                                <?php endif; ?>

                                <?php if($action == 'remove'): ?>
                                    <span class="remove-container">
                                    <i class="fa fa-trash remove"></i>
                                </span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </center>
                    </div>
                    <div class="content col-md-12 clonable-message"></div>
                </div>
            <?php endforeach; ?>

            <?php if(count($datas) == 0): ?>
                <div class="form-group clonable" style="display:none">
                    <div class="content col-md-12 clonable-title"></div>
                    <div class="content col-md-10">
                        <?php foreach($template as $key => $element): ?>
                            <?php
                            if($element != '')
                            {
                                try
                                {
                                    //provo a creare l'element usando il type se fallisce lo tratto come un input normale
                                    if(!isset($element['arrayPaths']))
                                        throw new Exception();

                                    $arrayPaths = $element['arrayPaths'];
                                    foreach($arrayPaths as $propertyName => $arrayPath)
                                    {
                                        if($propertyName == 'name')
                                            $element['options'] = insert_using_keys($element['options'], $arrayPath, $element['name']);
                                    }
                                    echo $this->element($element['type'], $element['options']);
                                }
                                catch(Exception $ex)
                                {
                                    $name = $element['name'];
                                    $options = $element['options'];

                                    if(isset($element['type']))
                                        $options['type'] = $element['type'];

                                    echo $this->Form->input($name,$options);
                                }
                            }
                            ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-2 clonable-actions">
                        <center>
                            <?php foreach($actions as $action): ?>
                                <?php
                                $display = "display:none";
                                ?>
                                <?php if($action == 'copy'): ?>
                                    <span class="copy-container">
                          		    <i class="fa fa-copy copy"></i>
                          		</span>
                                <?php endif; ?>

                                <?php if($action == 'clear'): ?>
                                    <span class="clear-container">
                          		    <i class="fa fa-eraser clear"></i>
                          		</span>
                                <?php endif; ?>

                                <?php if($action == 'move'): ?>
                                    <span class="moveup-container">
                               	    <i class="fa fa-sort-asc moveup" style="<?= $display ?>"></i>
                               	</span>
                                    <span class="movedown-container">
                               	    <i class="fa fa-sort-desc movedown" style="<?= $display ?>"></i>
                               	</span>
                                <?php endif; ?>

                                <?php if($action == 'remove'): ?>
                                    <span class="remove-container">
                                    <i class="fa fa-trash remove"></i>
                                </span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </center>
                    </div>
                    <div class="content col-md-12 clonable-message"></div>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <div class="form-group clonable" style="display:none">
                <div class="content col-md-10">
                    <?= $content ?>
                </div>
                <div class="col-md-2 clonable-actions">
                    <center>
                        <?php foreach($actions as $action): ?>
                            <?php if($action == 'copy'): ?>
                                <span class="copy-container">
                      		    <i class="fa fa-copy copy"></i>
                      		</span>
                            <?php endif; ?>

                            <?php if($action == 'clear'): ?>
                                <span class="clear-container">
                      		    <i class="fa fa-eraser clear"></i>
                      		</span>
                            <?php endif; ?>

                            <?php if($action == 'move'): ?>
                                <span class="moveup-container">
                           	    <i class="fa fa-sort-asc moveup" style="display:none"></i>
                           	</span>
                                <span class="movedown-container">
                           	    <i class="fa fa-sort-desc movedown" style="display:none"></i>
                           	</span>
                            <?php endif; ?>

                            <?php if($action == 'remove'): ?>
                                <span class="remove-container">
                                <i class="fa fa-trash remove"></i>
                            </span>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </center>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
