<td style="width:85mm;border: 0.2mm solid #000000; padding-left:10px;padding-top:5px;"><span style="font-size: 8pt; color: #555555; font-family: sans;">Destinatario fattura </span>
    <br />
	<?php
	    // Se è abilitata la gestione dei rivenditori aggiungo questo if
		if($bill['Bill']['referredclient_id'] > 0)
		{
		    echo $this->element('pdfHeaders/default_bill_referredclient_info');
		}
		else
		{		
            if($bill['Bill']['alternativeaddress_id'] > 0)
			{
			    echo $this->element('pdfHeaders/default_bill_client_alternative_address');
            }
		}
	?>
</td>