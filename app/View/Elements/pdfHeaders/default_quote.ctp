<?= $this->element('pdfHeaders/logoeintestazione'); ?>

<!-- INTESTAZIONI -->
<table style ="width:100%;border-collapse: collapse;" >
	<tbody>
		<tr>
            <td style="width:10mm;"></td>
            <td style="width:100mm; padding-left:10px;padding-top:5px;">
			<td style="width:50mm; padding-left:10px;padding-top:5px;"><span style="font-size: 8pt; color: #555555; font-family: sans;">Spett.le:</span><br />
			<strong><?= $quote['Client']['ragionesociale']; ?></strong><br />
            <?= $quote['Quote']['client_address']; ?><br />
            <?= $quote['Quote']['client_cap'] . ' ' . $quote['Quote']['client_city'] . ' ' . $quote['Quote']['client_province']; ?>
			<?= $quote['Nation']['name']; ?>
			</td>

			
			</td>
		</tr>
	</tbody>
</table>
<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<span style="font-size: 12px;">Preventivo N. <?= $quote['Quote']['quote_number'],'/'. $this->Time->format('Y', $quote['Quote']['quote_date']) .' del '. $this->Time->format('d/m/Y',$quote['Quote']['quote_date']); ?></span>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
