
<?php 
    
    isset($bill['Payment']['metodo']) ?  $metodoDiPagamento  = $bill['Payment']['metodo'] : $metodoDiPagamento = '';
   
    if($bill['Payment']['riba'] == 1) 
	{
	    $title1 = 'ABI';
	    $title2 = 'CAB';
	    $value1 = isset($bill['Client']['Bank']['abi']) ? $bill['Client']['Bank']['abi'] : '<br/>';
	    $value2 = isset($bill['Client']['Bank']['cab']) ? $bill['Client']['Bank']['cab'] : '<br/>';
	}
	else
	{
	    $title1 = 'Banca bonifico';
	    $title2 = 'Iban';
	    $value1 = isset($bill['Payment']['Bank']['description']) ? $bill['Payment']['Bank']['description'] : '<br/>';
	    $value2 = isset($bill['Payment']['Bank']['iban']) ? $bill['Payment']['Bank']['iban'] : '<br/>';
	    
	    if(strlen($value1) > 34)
	    {
	    	$title1 = $title1 . ': <b>' . substr($value1,0,14).'</b>';
	    	$value1 = substr($value1,15,strlen($value1));
	    }
	    
	}
?>
<table style=" border-collapse: collapse;">
	<tbody>
		<tr>
			<td style="width:71mm;border-top:1px solid black;border-left:1px solid black;text-align:center;">Pagamento</td>
			<td style="width:70mm;border-top:1px solid black;border-left:1px solid black;text-align:center;"><?= strtolower($title1) ?></td>
			<td style="width:70mm;border-top:1px solid black;border-left:1px solid black;border-right:1px solid black;text-align:center;"><?= strtolower($title2) ?></td>
		</tr>
		<tr>
			<td style="width:71mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;padding-right:5px;">
			    <span style="font-size:13px;"><?= strtolower($metodoDiPagamento); ?></span>
			</td>
			<td  style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;"><?= strtolower($value1)  ?></td>
			<td style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;border-right:1px solid black;"><?=  strtoupper($value2)  ?></td>
		</tr>
	</tbody>
</table>

