<!-- NUMERO FATTURA con sezionali -->
<?php

//	isset($bill['Sectional']['prefix']) ? $prefissoSezionale = $bill['Sectional']['prefix'] : $prefissoSezionale = '';
	isset($bill['Sectional']['suffix']) ? $suffissoSezionale = $bill['Sectional']['suffix'] : $suffissoSezionale = '';

?>
<table style=" border-collapse: collapse;">
	<tbody>
		<tr>
			<td style="width:60mm;border-top:1px solid black;border-left:1px solid black;text-align:center;">&nbsp;<?= $billTitle ?> numero</td>
			<!--td style="width:4mm;"></td-->
			<td style="width:30mm;border-top:1px solid black;border-left:1px solid black;text-align:center;">Data</td>
			<td style="width:43mm;border-top:1px solid black;border-left:1px solid black;text-align:center;">Partita Iva</td>
			<td style="width:43mm;border-top:1px solid black;border-left:1px solid black;text-align:center;">Codice fiscale</td>
			<!--td style="width:4mm;"></td-->
			<td style="width:25mm;border-top:1px solid black;border-left:1px solid black;border-right:1px solid black;text-align:center;">PAG. </td>
		</tr>
		<tr>
			<td style="width:60mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;padding-right:5px;">
			    <span style="font-size:13px;">
			        <?= $bill['Bill']['numero_fattura'] . $suffissoSezionale;  ?>
			    </span>
			</td>
			<td style="width:30mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;"><?= $this->Time->format('d/m/Y', $bill['Bill']['date']);  ?></td>
			<td style="width:43mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;"><?=  $bill['Client']['piva']  ?></td>
			<td style="width:43mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;"><?=  $bill['Client']['cf'];  ?></td>
			<td style="width:25mm;border-right:1px solid black;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;"><?= '{PAGENO} / {nbpg}' ?></td>
				
		</tr>

	</tbody>
</table>