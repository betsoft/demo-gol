<?php
		$articolo = $prezzo_articoli = $prezzo_articoli_ivato = $prezzo_pagato = $prezzo_da_pagare = $prezzo_non_pagato = '';
		$totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = 0;
?>
<div class="portlet-title">
		<div class="caption">
			<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Elenco fatture per flusso riba'); ?></span>
		</div>
		<div class=" tools">
		    <div id='createRibaCsv' title="Genera flusso riba della banca visualizzata" style="float:right;"><i class="uk-icon-file-text-o icon"></i></div>
		</div>
	</div>
	<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
	<thead class ="flip-content">
		<tr>
			<th><?php echo $this->Paginator->sort('numero_fattura', 'N° Fattura'); ?></th>
			<th><?php echo $this->Paginator->sort('date', 'Data'); ?></th>
			<th><?php echo $this->Paginator->sort('client_id','Cliente'); ?></th>
			<th><?php echo $this->Paginator->sort('banca','Banca'); ?></th>
			<th>Importo</th>
			<th>Importo IVA compr.</th>
			<!--th class="actions"><?php echo __('Azioni'); ?></th-->
			</thead>
		</tr>
		<?php
		foreach ($bills as $bill): ?>
		<tr>
			<td><?php echo $bill['Bill']['numero_fattura']; ?> / <?php echo date("Y"); ?></td>
			<td><?php echo $this->Time->format('d-m-Y', $bill['Bill']['date']); ?></td>
			<td class="table-max-width uk-text-truncate">
			<?php echo $this->Html->link($bill['Client']['ragionesociale'], array('controller' => 'clients', 'action' => 'edit', $bill['Client']['id'])); ?>
		</td>
		<td>
			<?= $bill['Payment']['Bank']['description']; ?> 
		</td>
		<td>
			<?php

		
				foreach($bill['Good'] as $articolo) {

				  $prezzo_articoli += $articolo['prezzo'] * $articolo['quantita'];
	
					if($bill['Bill']['pagato'] == 1)
					{
						$prezzo_pagato += $articolo['prezzo']* $articolo['quantita'];
					}
					if($bill['Bill']['pagato'] == 0)
					{
						$prezzo_non_pagato += $articolo['prezzo']* $articolo['quantita'];
					}

					if($bill['Bill']['pagato'] == 2)
					{
					$prezzo_da_pagare += $articolo['prezzo']* $articolo['quantita'];
						$prezzo_pagato_parziale = $bill['Bill']['pagamento_parziale'];
					}
				}
				$totale_fatturato += $prezzo_articoli;
				$totale_fatturato_pagato += $prezzo_pagato;
				$totale_fatturato_da_pagare += $prezzo_da_pagare;
				$totale_fatturato_pagato_parzialmente += $prezzo_pagato_parziale;
				$totale_fatturato_non_pagato += $prezzo_non_pagato;

				echo number_format($prezzo_articoli, 2, ',', '.'); echo'<br/>';

			?>
		</td>
		<td>
			<?php
			$articolo = '';
			$prezzo_articoli_ivato = '';
			foreach($bill['Good'] as $articolo) {
				
				//$prezzo_articoli_ivato += ($articolo['prezzo'] * $articolo['quantita']) + (($articolo['prezzo'] * $articolo['quantita']) * $articolo['Iva']['percentuale']) / 100;
				
				// Fix eventuali errori di mancanto inserimento iva
				if(isset($articolo['Iva']['percentuale']))
				{
					$prezzo_articoli_ivato += ($articolo['prezzo'] * $articolo['quantita']) + (($articolo['prezzo'] * $articolo['quantita']) * $articolo['Iva']['percentuale']) / 100;
				}
				else 
				{
					$prezzo_articoli_ivato += ($articolo['prezzo'] * $articolo['quantita']) + ($articolo['prezzo'] * $articolo['quantita']);
				}
			}
			echo number_format($prezzo_articoli_ivato, 2, ',', '.');
			$totale_ivato += $prezzo_articoli_ivato;
			?>
		</td>
			<?=  $this->element('Form/Simplify/Actions/edit',['id' => $bill['Bill']['id']]); ?>
			<?php echo $this->Html->link('<i class="uk-icon-file-code-o icon"></i>', array('action' => 'fattura_xml', $bill['Client']['ragionesociale'] . '-' . $bill['Bill']['numero_fattura'] . '_' . $this->Time->format('d_m_Y', $bill['Bill']['date']), $bill['Bill']['id']), array('title'=>__('Genera Fattura Elettronica'),'escape' => false)); ?>
			<?php

			if($bill['Client']['mail'])
			{

				echo $this->Html->link('<i class="uk-icon-envelope icon"></i>', array('action' => 'fatturapdf_mail', $bill['Bill']['id'],str_replace(' ', '_', $bill['Client']['ragionesociale'])."_". $bill['Bill']['numero_fattura'] . '_' . $this->Time->format('d-m-Y', $bill['Bill']['date'])), array('title'=>__('Invia Mail'),'escape' => false));
			}
			?>
			
			<?php echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $bill['Bill']['id']), array('title'=>__('Elimina'),'escape' => false), __('Siete sicuri di voler eliminare questo documento?', $bill['Bill']['id'])); ?>
		</td-->
	</tr>
<?php endforeach; ?>

	</table>
	<p class="uk-text-center">
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Pagina {:page} di {:pages}')
	));
	?>	</p>

	<div class="paging uk-text-center">
	<?php
		echo $this->Paginator->prev('<i class="fa fa-arrow-circle-o-left"></i> &nbsp;', array('escape'=>false,'class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' - '));
		echo $this->Paginator->next('&nbsp; <i class="fa fa-arrow-circle-o-right"></i>', array('escape'=>false,'class' => 'next disabled'));
	?>
	</div>
	<div id="element"></div>
	<!--a id="exportsags">questo è il link</div-->

<script>

	$('#createRibaCsv').click(
		function() 
		{
			if($("#selectBank").val() == '')
			{
				$.alert({
    				icon: 'fa fa-warning',
	    			title: 'Creazione flusso riba',
    				content: 'Nessuna banca selezionata, selezionare la banca per cui fare il flusso riba.',
    				type: 'orange',
				});
			}
			else
			{
				$.ajax({
					method: "POST",
					url: "<?= $this->Html->url(["controller" => "bills","action" => "createAccountantCsv",null]) ?>",
					data: 
					{
						bank : $("#selectBank").val(),
					},
					success: function(data)
					{
						csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
						var a = document.createElement('A');
						a.href = csvData;
						a.download = 'flusso_commercialista.txt';
						document.body.appendChild(a);
						a.click();
						document.body.removeChild(a);
					},
					error: function(data)
					{
					}
				})
			}
		});
			
</script>