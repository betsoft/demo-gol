
<div>Riepilogo registrazioni vendite dal <?= $dateFrom ?> al <?= $dateTo ?></div>

<table style="font-size:10px;margin-top:30px;">
    <thead>
    <tr>
        <th width="40%" style="text-align: left;" >Documento</th>
        <th width="40%" style="text-align: center;">Conto</th>
        <th width="20%" style="text-align: right;">Imponibile</th>
    </tr>
    </thead>
<tbody>
<tr><td colspan="3"></td></tr>

<?php

$arrayConti = [];
foreach($bills as $bill) {
    if (count($bill['Good']) > 0) {
        $bill['Bill']['tipologia'] == 1 ? $coefficiente = 1 : $coefficiente = -1;
        switch ($bill['Bill']['tipologia']) {
            case 1:
                $type = 'Fattura';
                $clientOrSupplierData = 'Cliente: ' . $bill['Bill']['client_name'];
                break;
            case 2:
                $type = 'Fattura';
                $clientOrSupplierData = 'Fornitore: ' . $bill['Bill']['supplier_name'];
                break;
            case 3:
                $type = 'Nota di credito';
                $clientOrSupplierData = 'Cliente: ' . $bill['Bill']['client_name'];
                break;
        }
        ?>
        <tr style="width:100%;"><?php
        $i = 0;

        foreach ($bill['Good'] as $good) {
            $i++;
            if ($good['prezzo'] != null) {
                ?>
                <tr>
                    <?php
                    if ($i > 1) {

                    } else {
                        ?>
                        <td width="100%" rowspan="<?= count($bill['Good']); ?>">
                            <?= $type . ' numero ' . $bill['Bill']['numero_fattura'] . ' del ' . date("d-m-Y", strtotime($bill['Bill']['date'])) . '<br/>' . $clientOrSupplierData; ?>
                        </td>
                        <?php
                    }

                    ($good['discount'] != null) ? $discount = (1 - $good['discount'] / 100) : $discount = 1;
                    $price = $coefficiente * $good['prezzo'] * $good['quantita'] * $discount . ' €';

                    if ($good['category_id'] != null) {
                        isset($arrayConti[$good['Category']['description']]['totale']) ? $arrayConti[$good['Category']['description']]['totale'] += $price : $arrayConti[$good['Category']['description']]['totale'] = $price;

                        ?>
                        <td><?= $good['Category']['description']; ?></td><?php
                    } else {
                        isset($arrayConti['no_category']['totale']) ? $arrayConti['no_category']['totale'] += $price : $arrayConti['no_category']['totale'] = $price;
                        ?>
                        <td>(categoria non inserita)</td><?php
                    }

                    ?>
                    <td style="text-align: right;"><?= number_format($price, 2, ',', '') . ' € '; ?></td>
                </tr>
                <?php
            } else {
            }
        }
        ?>
        </tr>
        <tr>
            <td colspan="3">
                <hr>
            </td>
        </tr>
    <?php }
}
?>
    </tbody>
</table>
<div style="text-align: center; margin-top:40px; margin-bottom:40px;" >Riepilogo complessivo per conto</div>
<table width="100%" style="font-size:10px;">
    <thead>
    <tr>
        <th width="60%" style="text-align: left;">Conto</th>
        <th width="40%">Imponibile</th>
    </tr>
    </thead>
    <tdoby>
        <?php
        foreach($arrayConti as $key => $riepilogo)
        {
            ?>
            <tr width="100%">
                <td width="60%"><?= $key == 'no_category' ? '(categoria non inserita)' : $key; ?> </td>
                <td width="40%" style="text-align:right;"><?= number_format($riepilogo['totale'],2,',','.'). ' €'?></td>
            </tr>
            <?php
        }
        ?>
    </tdoby>
</table>
