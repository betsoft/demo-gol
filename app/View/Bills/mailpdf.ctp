<?php if($bill['Client']['divisa'] == 0) { $divisa = 'EURO'; } else { $divisa = 'CHF'; } ?>
<html>
<head>
<style>
body {font-family: sans-serif;
    font-size: 10pt;
}
p {    margin: 0pt;
}
td { vertical-align: top; }
.items td {
    border-left: 0.1mm solid #000000;
    border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
    text-align: center;
    border: 0.1mm solid #000000;
}
.items td.blanktotal {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-top: 0.1mm solid #000000;
    border-right: 0.1mm solid #000000;
}
.items td.totals {
    text-align: right;
    border: 0.1mm solid #000000;
}
.items td.totals-none {
    text-align: left;
    border: 0.1mm solid #000000;
}
.items td.white {

    border: 0.0mm solid #ffffff;
}
.items{
  position: absolute;
     bottom: 0;
     top: 0;
     left: 0;
     right: 0;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr>                                                                    echo $this->Html->image('societa/'.$setting['Setting']['header'].'',array('style'=>'width:50px; height:auto;'));
<td width="50%" style="color:#000;"><span style="font-weight: bold; font-size: 14pt;"><?php if($impostazioni['Setting']['header'] !="" && $impostazioni['Setting']['company_id'] !="3"){echo $this->Html->image('societa/'.$impostazioni['Setting']['header'].'',array('style'=>'width:15%; height:auto;'));}else{} ?></span><br /><br /><?php if($impostazioni['Setting']['company_id'] =="3"){
?>
<center style="font-size:15px; color: #3a3a3a;"><b>VALTFONDI SNC</b><br/>
DI MOTTA MARCO & C.<br/>
via merlina 40<br/>
23011 Ardenno - SO<br/>
P.IVA 00728700147 TEL. 348-7817606<br/>
E-MAIL: valtfondi@live.it<br/>
PEC. valtfondisnc@smart-cert.it</center>
<?php
} else{ ?><h3><?php echo $impostazioni['Setting']['name']; ?></h3><br /><h5>Città: <?php echo $impostazioni['Setting']['citta']; ?>  <?php echo $impostazioni['Setting']['prov']; ?>  <?php echo $impostazioni['Setting']['indirizzo']; ?></h5><br /><h5>CAP: <?php echo $impostazioni['Setting']['cap']; ?>  &nbsp;&nbsp; TEL: <?php echo $impostazioni['Setting']['tel']; ?></h5><br /><h5>E-MAIL: <?php echo $impostazioni['Setting']['email']; ?></h5><?php } ?></td>

<td width="24%" style="text-align: center;"><span style="font-size: 7pt; color: #555555; font-family: sans;">Fattura n.</span><br /><?php echo $bill['Bill']['numero_fattura']; ?> / <?php echo date("Y"); ?></td>
<td width="24%" style="text-align: center;"><span style="font-size: 7pt; color: #555555; font-family: sans;">Data documento:</span><br/> <?php echo $this->Time->format('d/m/y', $bill['Bill']['date']); ?></td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Fattura n. <?php echo $bill['Bill']['numero_fattura']; ?> del <?php echo $this->Time->format('d/m/y', $bill['Bill']['date']); ?>. Pagina {PAGENO} di {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
<br><br/><br/>
<br />
<table width="100%" style="font-family: serif;" cellpadding="10">
  <tr>
  <td width="45%" style="border: 0mm solid #000;">&nbsp;</td>
  <td width="10%">&nbsp;</td>
  <td width="45%" style="border: 0.1mm solid #888888;"><span style="font-size: 7pt; color: #555555; font-family: sans;">Codice Fiscale / Partita Iva:</span><center><span style="font-size: 9pt;"> <br/>&nbsp;&nbsp;<?php if(empty($bill['Client']['piva'])){echo "";} else{echo"P.IVA: ".$bill['Client']['piva'];} ?><br/>
  &nbsp;&nbsp;<?php if(empty($bill['Client']['cf'])){echo "";} else{echo"CF: ".$bill['Client']['cf'];} ?><br /><br /></span></center></td>
  </tr>
<tr>
<td width="45%" style="border: 0mm solid #000;">&nbsp;</td>
<td width="10%">&nbsp;</td>
<td width="45%" style="border: 0.1mm solid #888888;"><span style="font-size: 7pt; color: #555555; font-family: sans; text-align:left !important;">Spett.le:</span><br /><center><?php echo $bill['Client']['ragionesociale']; ?><br /><?php echo $bill['Client']['indirizzo']; ?><br />
  <?php echo $bill['Client']['cap'] . ' ' . $bill['Client']['citta'] . ' ' . $bill['Client']['provincia']; ?></center><br /></td>
</tr>
</table>
<br/>




<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="5">
<thead>
<tr>
<td width="50%">DESCRIZIONE</td>
<td width="15%">QUANTITA'</td>
<td width="15%">PREZZO</td>
<td width="20%">TOT</td>
<!--td width="15%">IVA</td-->
</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
<?php
	foreach($bill['Good'] as $numero => $oggetto)
	{
		$prezzo_riga = $oggetto['prezzo'] *  $oggetto['quantita'];
		$classe='';
		if ($numero % 2 != 0)
		{
			$classe = 'style="background-color: #eee;"';
		}
		?>
		<tr <?php echo $classe; ?>>
			<td><?php echo $oggetto['oggetto']; ?></td>
			<td align="center"><?php echo number_format($oggetto['quantita'], 2,",","."); ?> &nbsp; <?php echo $oggetto['unita']; ?></td>
			<td align="right"><?php echo number_format($oggetto['prezzo'] , 2,",","."); ?> &nbsp; €</td>
			<td align="right"><?php echo number_format($prezzo_riga, 2,",","."); ?> &nbsp; €</td>
			<!--td align="right"><?php echo $oggetto['Iva']['percentuale'] . '%'; ?></td-->
		</tr>
	<?php

		$importo_iva += ($prezzo_riga/100) * $ivaSelect['Iva']['percentuale'];
		$imponibile_iva += $prezzo_riga;
	}
	$totale = $importo_iva + $imponibile_iva;
?>
<!-- END ITEMS HERE -->
<tr>
<td class="totals" style="text-align:left;" colspan="2" rowspan="5"><em>Note:</em><br/><?php echo $ivaSelect['Iva']['descrizione']; ?><hr/>
<em>Condizioni di pagamento:</em><br/>
<?php echo $payment['Payment']['metodo']; ?>
<?php
if(empty($payment['Payment']['banca'])){}else{echo"<br/>".$payment['Payment']['banca'];}
if(empty($payment['Payment']['iban'])){}else{echo"<br/>".$payment['Payment']['iban'];}
 ?>
</td>
<td class="totals">Totale imponibile :</td>
<td class="totals"><?php echo number_format($imponibile_iva, 2,",","."); ?> &nbsp; € </td>
<td class="white" style="border-top: 0.1mm solid #000000;"></td>
</tr>
<tr>
<td class="totals">Totale IVA :</td>
<td class="totals"><?php if($importo_iva != 0) {echo number_format($importo_iva, 2,",",".");} ?> &nbsp; € <br/><?php echo $ivaSelect['Iva']['percentuale']; ?> &nbsp; %</td>
<td class="white"></td>
</tr>
<tr>

<td class="totals"><b>TOTALE DOCUMENTO:</b></td>
<td class="totals"><b><?php echo number_format($totale, 2,",","."); ?> &nbsp; € </b></td>
<td class="white"></td>
</tr>
</tbody>
</table>
<br />
<?php
if(empty($bill['Bill']['note']))
{}
else{ ?>
<div style="text-align: left; font-style: italic;">Note: </strong><?php echo $bill['Bill']['note']; ?></div><?php } ?>
<!--div style="text-align: center; font-style: italic;"><?php echo $impostazioni['Setting']['divisa']; ?> - Ns. Banca <?php echo $impostazioni['Setting']['banca']; ?> IBAN <?php echo $impostazioni['Setting']['iban']; ?></div-->
<!--<div style="text-align: center; font-style: italic;">Note: </strong><?php echo $bill['Bill']['note']; ?></div>
<div style="text-align: center; font-style: italic;">Ns. Banca <?php echo $impostazioni['Setting']['banca']; ?></div>
<div style="text-align: center; font-style: italic;">IBAN <?php echo $impostazioni['Setting']['iban']; ?></div>-->
</body>
</html>
