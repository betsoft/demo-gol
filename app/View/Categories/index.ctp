
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>

<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Categorie Merceologiche','indexelements' => ['add'=>'Nuova categoria merceologica']]); ?>
<div class="clients index">
	<div class="banks index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($categories) > 0) { ?>
				<tr>
					<?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?>
				</tr>
				<?php }
					else
					{
						?><tr><td colspan="5"><center>nessuna categoria trovata</center></td></tr><?php
					}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($categories as $category)
				{
				?>
					<tr>
                        <td><?php echo h($category['Category']['code']); ?></td>
                        <td><?php echo h($category['Category']['description']); ?></td>
						<td class="actions">
							<?=	$this->element('Form/Simplify/Actions/edit',['id' => $category['Category']['id']]); ?>
							<?= $this->element('Form/Simplify/Actions/delete',['id' =>$category['Category']['id'],'message' => 'Sei sicuro di voler eliminare la banca ?']);  ?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
			<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
