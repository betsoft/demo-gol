<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<?= $this->Html->link('indietro', ['controller' => 'constructionsites', 'action' => 'index'], ['title' => __('Indietro'), 'escape' => false, 'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style' => 'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:10%;text-align:center;']); ?>

<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Schede d\'intervento tecnico' . ' - ' . $constructionSiteName]); ?>
</br>

<b>PREVENTIVI</b>
<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr>
                <td>Numero Preventivo</td>
                <td>Data preventivo</td>
                <td>Descrizione</td>
            </tr>
            </thead>
            <tbody class="ajax-filter-table-content">

            <?php
            foreach ($constructionSiteQuote as $quote) {
                ?>
                <tr>
                    <td><?= $quote['Quote']['quote_number']; ?></td>
                    <td><?= date("d-m-Y", strtotime($quote['Quote']['quote_date'])); ?></td>
                    <td><?= $quote['Quote']['description']; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<b>ORE TECNICI</b>
<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr>
                <td style="max-width: 60px;">Numero scheda d'intervento</td>
                <td>Data scheda d'intervento</td>
                <td>Descrizione</td>
                <td>Tecnico</td>
                <td>Ora inizio</td>
                <td>Ora Fine</td>
                <td>Totale</td>
                <td>Weekend</td>
                <td>Notturno</td>
                <td>Costo tecnico</td>
                <td>Costo notturno</td>
                <td>Costo weekend</td>
                <td>Costo totale</td>
                <td>Preventivo</td>
            </tr>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php
            $totalDayMinutes = $totalNightMinutes = $finalTotal = $totaleRow = 0;
            foreach ($constructionSiteMaintenance as $maintenance) {
                foreach ($maintenance['Maintenancehour'] as $maintenancehour) {
                    if($maintenancehour['partecipating']) {
                        $quoteNumber = isset($maintenance['Technician']['quote_number']) && isset($maintenance['Technician']['date']) ? ($maintenance['Technician']['quote_number'] . ' del ' . date("Y-m-d", strtotime($maintenance['Quote']['date']))) : '';
                        $qutoeDescription = ' - ' . isset($maintenance['Quote']['description']) ? $maintenance['Quote']['description'] : '';
                        $technicianCost = $maintenance['Technician']['labour_cost'];
                        $technicianNightCost = $maintenance['Technician']['labour_cost_night'];
                        $technicianWeekendCost = $maintenance['Technician']['labour_cost_weekend'];

                        $dayMinutes = $utilities->differencesInMinutes(date("H:i", strtotime($maintenancehour['hour_from'])), date("H:i", strtotime($maintenancehour['hour_to'])), false) ;
                        $dayHours = $dayMinutes / 60;
                        $totalCost = 0;
                        $weekendCost = 0;

                        if (isset($maintenancehour['hour_from']) && isset($maintenancehour['hour_to'])) {
                            $nightMinutes = $utilities->getNightTimeHourAndMinutes($maintenancehour['hour_from'], $maintenancehour['hour_to']);
                        } else {
                            $nightMinutes = '0';
                        }

                        $nightHours = $nightMinutes / 60;

                        if ($utilities->isWeekend($maintenance['Maintenance']['maintenance_date'])) {
                            $weekendCost = $dayHours * $technicianWeekendCost;
                            $totalCost += $weekendCost;
                        } else {
                            $nightCost = $nightHours * $technicianNightCost;
                            $totalCost += $nightCost;
                        }

                        $dayCost = $technicianCost * ($dayHours - $nightHours);

                        $totalCost += $dayCost;

                        $totalDayMinutes += $dayMinutes;
                        $totalNightMinutes += $nightMinutes;

                        $finalTotal += $totalCost;

                    ?>
                    <tr>
                        <td><?= $maintenance['Maintenance']['maintenance_number']; ?></td>
                        <td style="text-align: center;"><?= date("d-m-Y", strtotime($maintenance['Maintenance']['maintenance_date'])); ?></td>
                        <td><?= $maintenance['Maintenance']['intervention_description']; ?></td>
                        <td><?= $maintenance['Technician']['surname'] . ' ' . $maintenance['Technician']['name']; ?></td>
                        <td style="text-align: center;"><?= isset($maintenancehour['hour_from']) ? $maintenancehour['hour_from'] : null; ?></td>
                        <td style="text-align: center;"><?= isset($maintenancehour['hour_to']) ? $maintenancehour['hour_to'] : null; ?></td>
                        <td class="right"><?= $utilities->formatTimeInHourAndMinutes($dayMinutes); ?></td>
                        <td style="text-align: center;"> <?= $utilities->isWeekend($maintenance['Maintenance']['maintenance_date']) ? '' : 'NO'; ?></td>
                        <td class="right"><?= $utilities->formatTimeInHourAndMinutes($nightMinutes); ?></td>
                        <td class="right"><?= number_format($dayCost, 2, ',', '.'); ?></td>
                        <td class="right"><?= number_format($nightCost, 2, ',', '.'); ?></td>
                        <td class="right"><?= number_format($weekendCost, 2, ',', '.'); ?></td>
                        <td class="right"><?= number_format($totalCost, 2, ',', '.'); ?></td>
                        <td><?= $quoteNumber . $qutoeDescription; ?></td>
                    </tr>
                    <?php
                    }
                }
            }
            ?>
            <tr>
                <td colspan="6"><b>TOTALE :</b></td>
                <td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalDayMinutes); ?></b></td>
                <td></td>
                <td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalNightMinutes) ?></b></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="right"><b><?= number_format($finalTotal, 2, ',', '') ?></b></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<span>
<b>MATERIALI UTILIZZATI</b>
<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr>
                <td style="max-width: 60px;">Numero scheda d'intervento</td>
                <td>Data scheda d'intervento</td>
                <td>Descrizione materiale</td>
                <td>Descrizione personalizzata</td>
                <td>Quantità</td>
                <td>Unità di misura</td>
            </tr>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php
            foreach ($constructionSiteMaintenance as $maintenance) {
                foreach ($maintenance['Maintenancerow'] as $row) {
                    if($row['state'] == 1 && $row['description'] != '') {
                        ?>
                        <tr>
                    <td><?= $maintenance['Maintenance']['maintenance_number'] ?></td>
                    <td><?= date('d-m-Y', strtotime($maintenance['Maintenance']['maintenance_date'])); ?></td>
                    <td><?= $row['description']; ?></td>
                    <td><?= $row['customdescription']; ?></td>
                    <td><?= $row['quantity']; ?></td>
                    <td><?= $units->getName($row['unit_of_measure_id']) ?></td>
                </tr>
                        <?php
                    }
                }
            }
            /*
                $arrayOfArticle[$row['storage_id']]['description'] = $row['description'];
                isset($arrayOfArticle[$row['storage_id']]['quantity']) ?  $arrayOfArticle[$row['storage_id']]['quantity'] += $row['description']: $arrayOfArticle[$row['storage_id']]['quantity'] = $row['quantity'];
            }
            */
            ?>
            </tbody>
        </table>
    </div>
</div>
</span>

<span>
<b>VALORIZZAZIONE BOLLE MERCE</b>
<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr>
                <td style="max-width: 60px;">Numero scheda d'intervento</td>
                <td>Data scheda d'intervento</td>
                <td>Numero bolla</td>
                <td>Data bolla</td>
                <td>Fornitore</td>
                <td>Valorizzazione</td>
            </tr>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php
            foreach ($constructionSiteMaintenance as $maintenance) {
                foreach ($maintenance['Maintenanceddt'] as $row) {
                    if($row['state'] == 1 && $row['number'] != '') {
                        ?>
                        <tr>
                        <td><?= $maintenance['Maintenance']['maintenance_number'] ?></td>
                        <td><?= date('d-m-Y', strtotime($maintenance['Maintenance']['maintenance_date'])); ?></td>
                        <td><?= $row['number']; ?></td>
                       <td><?=  date('d-m-Y', strtotime($row['date'])); ?></td>
                        <td><?= isset($row['Supplier']['name']) ? $row['Supplier']['name'] : null; ?></td>
                        <td class="right"><?= number_format($row['value'], 2, ',', '.'); ?></td>
                    </tr>
                        <?php
					$totaleRow += $row['value'];                   
				   }
                }
            }
            ?>
            <tr><td Colspan="5"></td><td class="right"><b><?= number_format($totaleRow, 2, ',', '.') ?></b></td></tr>
            </tbody>
        </table>
    </div>
</div>
</span>

<b>ORE OPERATORI ESTERNI</b>
<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr>
                <td style="max-width: 60px;">Numero scheda d'intervento</td>
                <td>Data scheda d'intervento</td>
                <td>Descrizione</td>
                <td>Tecnico</td>
                <td>Ora inizio</td>
                <td>Ora Fine</td>
                <td>Totale</td>
                <td>Weekend</td>
                <td>Notturno</td>
                <td>Costo tecnico</td>
                <td>Costo notturno</td>
                <td>Costo weekend</td>
                <td>Costo totale</td>
                <td>Preventivo</td>
            </tr>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php
            $totalDayMinutes = $totalNightMinutes = $finalTotal = $totaleRow = 0;
            foreach ($constructionSiteMaintenance as $maintenance) {
                foreach ($maintenance['Maintenancehour'] as $maintenancehour) {
                    foreach ($maintenancehour['Maintenancehoursoutsideoperator'] as $maintenancehourOperator) {
                        $quoteNumber = isset($maintenance['Technician']['quote_number']) && isset($maintenance['Technician']['date']) ? ($maintenance['Technician']['quote_number'] . ' del ' . date("Y-m-d", strtotime($maintenance['Quote']['date']))) : '';
                        $qutoeDescription = ' - ' . isset($maintenance['Quote']['description']) ? $maintenance['Quote']['description'] : '';
                        $technicianCost = $maintenancehourOperator['Outsideoperator']['labour_cost'];
                        $technicianNightCost = $maintenancehourOperator['Outsideoperator']['labour_cost_night'];
                        $technicianWeekendCost = $maintenancehourOperator['Outsideoperator']['labour_cost_weekend'];

                        $dayMinutes = $utilities->differencesInMinutes(date("H:i", strtotime($maintenancehour['hour_from'])), date("H:i", strtotime($maintenancehour['hour_to'])), false);
                        $dayHours = $dayMinutes / 60;
                        $totalCost = 0;
                        $weekendCost = 0;

                        if (isset($maintenancehour['hour_from']) && isset($maintenancehour['hour_to'])) {
                            $nightMinutes = $utilities->getNightTimeHourAndMinutes($maintenancehour['hour_from'], $maintenancehour['hour_to']);
                        } else {
                            $nightMinutes = '0';
                        }

                        $nightHours = $nightMinutes / 60;

                        if ($utilities->isWeekend($maintenance['Maintenance']['maintenance_date'])) {
                            $weekendCost = $dayHours * $technicianWeekendCost;
                            $totalCost += $weekendCost;
                        } else {
                            $nightCost = $nightHours * $technicianNightCost;
                            $totalCost += $nightCost;
                        }

                        $dayCost = $technicianCost * ($dayHours - $nightHours);

                        $totalCost += $dayCost;

                        $totalDayMinutes += $dayMinutes;
                        $totalNightMinutes += $nightMinutes;

                        $finalTotal += $totalCost;

                        ?>
                        <tr>
                            <td><?= $maintenance['Maintenance']['maintenance_number']; ?></td>
                            <td style="text-align: center;"><?= date("d-m-Y", strtotime($maintenance['Maintenance']['maintenance_date'])); ?></td>
                            <td><?= $maintenance['Maintenance']['intervention_description']; ?></td>
                            <td><?= $maintenancehourOperator['Outsideoperator']['surname'] . ' ' . $maintenancehourOperator['Outsideoperator']['name']; ?></td>
                            <td style="text-align: center;"><?= isset($maintenancehour['hour_from']) ? $maintenancehour['hour_from'] : null; ?></td>
                            <td style="text-align: center;"><?= isset($maintenancehour['hour_to']) ? $maintenancehour['hour_to'] : null; ?></td>
                            <td class="right"><?= $utilities->formatTimeInHourAndMinutes($dayMinutes); ?></td>
                            <td style="text-align: center;"> <?= $utilities->isWeekend($maintenance['Maintenance']['maintenance_date']) ? '' : 'NO'; ?></td>
                            <td class="right"><?= $utilities->formatTimeInHourAndMinutes($nightMinutes); ?></td>
                            <td class="right"><?= number_format($dayCost, 2, ',', '.'); ?></td>
                            <td class="right"><?= number_format($nightCost, 2, ',', '.'); ?></td>
                            <td class="right"><?= number_format($weekendCost, 2, ',', '.'); ?></td>
                            <td class="right"><?= number_format($totalCost, 2, ',', '.'); ?></td>
                            <td><?= $quoteNumber . $qutoeDescription; ?></td>
                        </tr>
                        <?php
                    }
                }
            }
            ?>
            <tr>
                <td colspan="6"><b>TOTALE :</b></td>
                <td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalDayMinutes); ?></b></td>
                <td></td>
                <td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalNightMinutes) ?></b></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="right"><b><?= number_format($finalTotal, 2, ',', '') ?></b></td>
                <td></td>

            </tr>
            </tbody>
        </table>
    </div>
</div>
