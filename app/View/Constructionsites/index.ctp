
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Cantieri','indexelements' => ['add'=>'Nuovo cantiere']]); ?>


<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
                <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields, 'afterAjaxfilterCallback'=>'constructionSiteJsFunction']); ?></tr>
                <?php
                if (count($constructionsites) < 1)
				{
					?><tr><td colspan="<?= count($sortableFields) ?>"  style="text-align: center">nessun cantiere trovato</td></tr><?php
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">

            <?php foreach ($constructionsites as $constructionsite)
				{
                    if($constructionsite['Constructionsite']['state'] > 1)
                    {
                        $displayClose = 'none';
                        $displayOpen = 'inline';
                    }
                    else {
                        $displayClose = 'inline';
                        $displayOpen = 'none';
                    }
				?>
					<tr>
                        <td><?= h($constructionsite['Client']['ragionesociale']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['name']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['description']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['address']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['city']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['cap']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['province']); ?></td>
						<td class="actions">
							<?php
								echo $this->Html->link($iconaModifica, ['action' => 'edit', $constructionsite['Constructionsite']['id']],['title'=>__('Modifica'),'escape'=>false]);
                                ?><i class="fa fa-unlock jsConstructionsiteClose" id="jsOpen<?=  $constructionsite["Constructionsite"]['id'] ; ?>" constructionsiteId = "<?=  $constructionsite["Constructionsite"]['id'] ; ?>" style="display:<?= $displayClose  ?>;font-size:20px;vertical-align: middle;color:#577400;cursor:pointer;" title="Chiudi cantiere"></i><?php
                                ?><i class="fa fa-lock jsConstructionsiteOpen" id="jsClose<?=  $constructionsite["Constructionsite"]['id'] ; ?>" constructionsiteId = "<?=  $constructionsite["Constructionsite"]['id'] ; ?>" style="display:<?= $displayOpen  ?>;font-size:20px;vertical-align: middle;margin-right:5px;color:#EA5D0B;cursor:pointer;" title="Riapri cantiere"></i><?php
                                echo $this->Html->link('<i class="fa fa-list  verde" style="vertical-align:middle;font-size:20px;" constructionsiteId = "'. $constructionsite["Constructionsite"]['id'] .'" ></i>', ['action' => 'constructionsitedetail','controller'=>'constructionsites', $constructionsite["Constructionsite"]['id']],['title'=>__('Visualizza riepilogativo cantiere'),'escape'=>false]);

                                if($constructionsite['Constructionsite']["state"] == 3) {
                                ?>
                                <i class="fa fa-home jsConstructionsiteAuthorize" style="font-size:20px;vertical-align: middle;color:#ea5d0b;cursor:pointer;" title="Conferma il cantiere" constructionsiteid="<?= $constructionsite['Constructionsite']['id'] ?>"></i>
                                <?php
                            }
                            else
                            {
                                ?>
                                <i class="fa fa-home" style="font-size:20px;color:#577400;vertical-align: middle;" constructionsiteid="<?= $constructionsite["Constructionsite"]['id']  ?>"></i>
                                <?php
                            }

                                echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $constructionsite['Constructionsite']['id']]   , ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler eliminare il cantiere ?'        , $constructionsite['Constructionsite']['id']));
								?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=   $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
<script>

    addLoadEvent(function()
    {
        constructionSiteJsFunction();
    })

    function constructionSiteJsFunction()
    {
        $(".jsConstructionsiteClose").click(function () {
            var constructionsite = $(this);
            $.confirm({
                title: 'Abilitazione cantieri.',
                content: 'Abilitare il cantiere selezionato ?',
                type: 'blue',
                buttons: {
                    Ok: function () {
                        action:
                        {
                            $.ajax
                            ({
                                method: "POST",
                                url: "<?= $this->Html->url(["controller" => "constructionsites", "action" => "closeConstructionsite"]) ?>",
                                data:
                                    {
                                        constructionsiteId: $(constructionsite).attr("constructionsiteid"),
                                    },
                                success: function (data) {
                                    $("#jsOpen" + data).hide();
                                    $("#jsClose" + data).show();
                                }
                            });
                        }
                    },
                    annulla: function () {
                        action:
                        {
                            // Nothing
                        }
                    },
                }
            });
        });

        $(".jsConstructionsiteOpen").click(function () {
            var constructionsite = $(this);
            $.confirm({
                title: 'Abilitazione cantieri.',
                content: 'Abilitare il cantiere selezionato ?',
                type: 'blue',
                buttons: {
                    Ok: function () {
                        action:
                        {
                            $.ajax
                            ({
                                method: "POST",
                                url: "<?= $this->Html->url(["controller" => "constructionsites", "action" => "openConstructionsite"]) ?>",
                                data:
                                    {
                                        constructionsiteId: $(constructionsite).attr("constructionsiteid"),
                                    },
                                success: function (data) {
                                    $("#jsOpen" + data).show();
                                    $("#jsClose" + data).hide();
                                }
                            });
                        }
                    },
                    annulla: function () {
                        action:
                        {
                            // Nothing
                        }
                    },
                }

            });
        });

    $(".jsConstructionsiteAuthorize").click(function () {
        var constructionsite = $(this);
        $.confirm({
            title: 'Abilitazione cantieri.',
            content: 'Abilitare il cantiere selezionato ?',
            type: 'blue',
            buttons: {
                Ok: function () {
                    action:
                    {
                        $.ajax
                        ({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "constructionsites", "action" => "enable"]) ?>",
                            data:
                                {
                                    constructionsiteId: $(constructionsite).attr("constructionsiteid"),
                                },
                            success: function (data) {
                                $(constructionsite).css("color", "#577400");
                                $(constructionsite).parent().find(".jsConstructionsiteOpen").css("color", "#577400");
                            }
                        });
                    }
                },
                annulla: function () {
                    action:
                    {
                        // Nothing
                    }
                },
            }
        });
    });
   }
</script>