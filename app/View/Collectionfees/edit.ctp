<?=  $this->Form->create('Collectionfee'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica Vettore:'); ?></span>
 	 <div class="col-md-12"><hr></div>
    <div class="form-group col-md-12">
        <label class="form-margin-top form-label"><strong>Importo da</strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('from_amount', ['div' => false, 'label' => false, 'class'=>'form-control', 'required'=>true,'type'=>'number']); ?>
   	 </div></div>
    	<div class="form-group col-md-12">
  		<label class="form-margin-top form-label"><strong>Importo a</strong></label>
  		<div class="form-controls">
			<?= $this->Form->input('to_amount',['div' => false, 'label' => false, 'class'=>'form-control', 'required'=>true,'type'=>'number']); ?>
		</div>
	</div>
	<div class="form-group col-md-12">
  		<label class="form-margin-top form-label"><strong>Importo spese di incasso</strong></label>
  		<div class="form-controls">
			<?= $this->Form->input('fee',['div' => false, 'label' => false, 'class'=>'form-control', 'required'=>true,'type'=>'number']); ?>
		</div>
	</div>
    <div class="col-md-12"><hr></div>
	<center><?= $this->Form->submit(__('Salva',true), ['class'=>'btn blue-button new-bill']); ?></center>
<?=  $this->Form->end(); ?>

