<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Gruppi di utenti'); ?></span>
	</div>
	<?= $this->element('Form/Simplify/topaddaction',['action'=>'add' ,'testo'=>'Creazione nuovo gruppo']); ?>
</div>
<div class="clients index">
	<div class="ivas index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th><?php echo $this->Paginator->sort('description'); ?></th>
					<th class="actions"><?php echo __('Azioni'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($groups as $group) { ?>
					<tr>
						<td><?php echo h($group['Group']['description']); ?></td>
						<td class="actions">
							<?php
								echo $this->Html->link($iconaModifica, array('action' => 'edit', $group['Group']['id']),array('title'=>__('View'),'escape'=>false));
							?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<p class="uk-text-center">
			<?php echo $this->Paginator->counter(array('format' => __('Pagina {:page} di {:pages}'))); ?>
		</p>
		<div class="paging uk-text-center">
			<?php
				echo $this->Paginator->prev('<i class="fa fa-arrow-circle-o-left"></i> &nbsp;', array('escape'=>false,'class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ' - '));
				echo $this->Paginator->next('&nbsp; <i class="fa fa-arrow-circle-o-right"></i>', array('escape'=>false,'class' => 'next disabled'));
			?>
		</div>
	</div>
</div>
