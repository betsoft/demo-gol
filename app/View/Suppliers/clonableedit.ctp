 <script>
     
 function enableCloningedit(clientName, clientInputId)
 {
     
            // Sbindo
            $("#aggiungi_riga").unbind('click');

            var clonato;
            var nuova_chiave = $(".lunghezza").length -1;
            
            /** Identifico quale è la tipologia chiamante  */
            switch(clientInputId)
			{
			    case '#QuoteClientId':
				    sezioneDiProvenienza = 'preventivi';    
					campoDescrizione =  "Description";
                    nomeOggetti = "Quotegoodrow";					
				break;
				case '#BillClientId':
				    sezioneDiProvenienza = 'fatture';    // E note di credito // E scontrini // Entrata merce
					campoDescrizione =  "Oggetto";
					nomeOggetti = "Good";
				break;
				case '#TransportClientId':
				    sezioneDiProvenienza = 'ddt';    
					campoDescrizione =  "Oggetto";
					nomeOggetti = "Transportgood";
				break;
				case '#LoadgoodSupplierId':
				    sezioneDiProvenienza = 'entratamerce';    
					campoDescrizione =  "Description";
					nomeOggetti = "Loadgoodrow";
				break;
			}

            $("#"+nomeOggetti+"0"+campoDescrizione).prop('required',true);

            $(".originale .remove").hide();

            $("#aggiungi_riga").click
            (
                function()
                {

                  if(sezioneDiProvenienza == 'preventivi')
                  {
                      var array = $.parseJSON("<?= isset($this->data['Quotegoodrow']) ?  addslashes(json_encode($this->data['Quotegoodrow'])) : null; ?>");
                  }

                  if(sezioneDiProvenienza == 'entratamerce')
                  {
                      var array = $.parseJSON("<?= isset($this->data['Loadgoodrow']) ? addslashes(json_encode($this->data['Loadgoodrow'])) : null; ?>");
                  }

                  if(sezioneDiProvenienza == 'ddt' )
                  {
                      var array = $.parseJSON("<?= isset($this->data['Transportgood']) ? addslashes(json_encode($this->data['Transportgood'])) : null; ?>");
                  }

                  if(sezioneDiProvenienza == 'fatture' )
                  {
                       var array = $.parseJSON("<?= isset($this->data['Good']) ? addslashes(json_encode($this->data['Good'])) : null; ?>");
                  }
                  
                   var firstId=array[0]['id'];

                   var newId=firstId+$(".lunghezza").length;
                   nuova_chiave = nuova_chiave+ 1; //$(".lunghezza").length;

                   var clonableRowDescription = ".clonato #"+nomeOggetti+ nuova_chiave;
                   
                   var clonableFieldType = clonableRowDescription + "Tipo";
                   var clonableFieldCode = clonableRowDescription + "Codice";
                   var clonableFieldCode = clonableRowDescription + "Oggetto";

                   clonato = $('.originale').clone();
                   $(clonato).removeClass("originale");
                   $(clonato).addClass("clonato");
                   $(clonato).addClass("ultima_riga");
                   $(clonato).insertAfter(".ultima_riga");
                   $(clonato).removeClass("principale"+firstId);
                   $(clonato).addClass("principale"+newId);
                   $(".ultima_riga").removeClass("ultima_riga");

                    if(sezioneDiProvenienza == 'fatture' )
                    {
                        var Campi = ['Codice','Oggetto','Quantita','Prezzo','Discount','IvaId','Unita','Movable' ,'StorageId','Customdescription','TransportId','Id','VariationId','Tipo','Importo'];
                        var Label = ['codice','oggetto','quantita','prezzo','discount','iva_id','unita','movable','storage_id','customdescription','transport_id','id','variation_id','tipo','importo'];
                        var newLineValue = ['','',1,'','','','','','','','',newId,'','',''] ;
                        
                        for (i = 0; i<Campi.length; i++)
                        {
                            $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('name', "data["+nomeOggetti+"][" + nuova_chiave + "]["+Label[i]+"]");
                            $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]); 
                            $(".clonato #"+nomeOggetti+ nuova_chiave + Campi[i]).val(newLineValue[i]);
                            if(sezioneDiProvenienza == 'fatture')
                            { 
                                $(".clonato #"+nomeOggetti+ nuova_chiave + Campi[i]).removeAttr('readonly');
                            }
                        }
                        
                        $(clonableFieldType).removeAttr("disabled");
                        $(clonableFieldCode).removeAttr("readonly");
                        $(clonableFieldCode).css("background", "#ffffff");
                        $(clonableFieldCode).removeAttr("required");
                        $(clonableFieldDescription).removeAttr("readonly");
                        $(clonableFieldDescription).css("background", "#ffffff");
                        $(clonableFieldDescription).prop('required',true);
                        
                        $(".clonato .rimuoviRigaIcon").css('color','red');
                        $(".clonato .rimuoviRigaIcon").attr('title','Rimuovi riga');
                        $(".clonato .remove").show();
                    }
                    
                     if(sezioneDiProvenienza == 'ddt')
                    {
                        var Campi = ['Codice','Oggetto','Quantita','Prezzo','Discount','IvaId','Unita','Movable' ,'StorageId','Customdescription','TransportId','Id','VariationId','Tipo','Importo'];
                        var Label = ['codice','oggetto','quantita','prezzo','discount','iva_id','unita','movable','storage_id','customdescription','transport_id','id','variation_id','tipo','importo'];
                        var newLineValue = ['','',1,'','','','','','','','',newId,'','',''] ;
                        
                        for (i = 0; i<Campi.length; i++)
                        {
                            $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('name', "data["+nomeOggetti+"][" + nuova_chiave + "]["+Label[i]+"]");
                            $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]); 
                            $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                        }
                        
                        $(clonableFieldType).removeAttr("disabled");
                        $(clonableFieldCode).removeAttr("readonly");
                        $(clonableFieldCode).css("background", "#ffffff");
                        $(clonableFieldCode).removeAttr("required");
                        $(clonableFieldDescription).removeAttr("readonly");
                        $(clonableFieldDescription).css("background", "#ffffff");
                        $(clonableFieldDescription).prop('required',true);
                        
                        $(".clonato .rimuoviRigaIcon").css('color','red');
                        $(".clonato .rimuoviRigaIcon").attr('title','Rimuovi riga');
                        $(".clonato .remove").show();
                    }
                    
                    if(sezioneDiProvenienza == 'entratamerce')
                    {
                        
                        var clonableFieldCode = clonableRowDescription + "Description";
                        
                       var Campi = ['Codice','Description','UnitOfMeasureId','Quantity','LoadGoodRowPrice','LoadGoodRowVatId','Barcode','Movable','StorageId','Tipo','Importo'];
                       var Label = ['codice','description','unit_of_measure_id','quantity','load_good_row_price','load_good_row_vat_id','barcode','movable','storage_id','tipo','importo'];
                       var newLineValue = ['','','',1,'','','','','','','','',''] ;
                       
                       for (i = 0; i<Campi.length; i++)
                       {
                          $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('name', "data["+nomeOggetti+"][" + nuova_chiave + "]["+Label[i]+"]");
                          $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]); 
                          $(clonableRowDescription+ Campi[i]).val(newLineValue[i]);
                       }
                        
                        $(clonableFieldType).removeAttr("disabled");
                        $(clonableFieldCode).removeAttr("readonly");
                        $(clonableFieldCode).css("background", "#ffffff");
                        $(clonableFieldCode).removeAttr("required");
                        $(clonableFieldDescription).removeAttr("readonly");    
                        $(clonableFieldDescription).css("background", "#ffffff");
                        $(clonableFieldDescription).prop('required',true);
                        $(".clonato .remove").show();
                    }
                    
                    if(sezioneDiProvenienza == 'preventivi')
                    {
                       var Campi = ['Codice','Description','UnitOfMeasureId','Quantity','QuoteGoodRowPrice','QuoteGoodRowVatId','Movable','StorageId','VariationId','Tipo','Importo'];
                       var Label = ['codice','description','unit_of_measure_id','quantity','quote_good_row_price','quote_good_row_vat_id','movable','storage_id','variation_id','Tipo','Importo'];
                       var newLineValue = ['','','',1,'','','','','','',''];
                       
                       var clonableFieldCode = clonableRowDescription + "Description";
                       
                       for (i = 0; i<Campi.length; i++)
                       {
                          $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('name', "data["+nomeOggetti+"][" + nuova_chiave + "]["+Label[i]+"]");
                          $(".clonato #"+nomeOggetti+"0"+Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]); 
                          $(".clonato #"+nomeOggetti+ nuova_chiave + Campi[i]).val(newLineValue[i]);
                       }
                    
                       $(clonableFieldType).removeAttr("disabled");
                       $(clonableFieldCode).removeAttr("readonly");
                       $(clonableFieldCode).css("background", "#ffffff");
                       $(clonableFieldCode).removeAttr("required");
                       $(clonableFieldDescription).removeAttr("readonly");        
                       $(clonableFieldDescription).css("background", "#ffffff");
                       $(clonableFieldDescription).prop('required',true);
                       
                       $(".clonato .remove").show();
                    }
                    
                    $(clonato).addClass("ultima_riga");
                    $(clonato).attr('id',  nuova_chiave );
                    $(clonato).removeClass("clonato");

                    // Prendo la x che cancella la riga (per i campi aggiunti dinamicamente)
					var temp = $(".ultima_riga .rimuoviRigaIcon");

					// E ne aggiungo l'evento click che elimina la riga
					$(temp).first(".rimuoviRigaIcon").click
					(
						function()
						{
							if($(this).parent().hasClass('ultima_riga'))
							{
								$(this).parent().prev('.lunghezza').addClass("ultima_riga");
								$(this).parent().remove();
							}
							else
							{
								$(this).parent().remove();
							}
						}
					)				

                    if(clientName != null) // Il catalogo è solo per clienti
                    {
                        loadClientCatalog(nuova_chiave,clientName,clientInputId,nomeOggetti);
                    }
                    else
                    {
                        var articoli = setArticles();
                        $(codici).each(function(key, val)
					    {
					        val.label = val.codice;
					    })
                      //  loadSupplierCatalog(articoli,nomeOggetti,nuova_chiave,campoDescrizione,sezioneDiProvenienza,codici);
                        loadSupplierCatalog(articoli,nomeOggetti,nuova_chiave,sezioneDiProvenienza,codici); 
                    }
                    
                    // Aggiungo il calcolo degli importi
                    $(".jsPrice").change(function(){setImporto(this);});
                    $(".jsQuantity").change(function(){setImporto(this);});
                    $(".jsTipo").change(function(){setCodeRequired(this);});   
                }
            );
            
            $('.contacts_row .clonato').removeClass("clonato");
    }
</script>