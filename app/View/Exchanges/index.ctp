<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>


<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Storico cambio','indexelements' => ['add'=>'Nuova cambio'],'postValue' => $currencyId]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($exchanges) > 0) { ?>
				<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
				<?php 
				}
				else
				{
					?><tr><td colspan="3"><center>nessun cambio trovata</center></td></tr><?php				
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($exchanges as $exchange) 
				{
				?>
					<tr>
						<!--d><?php // h($exchange['Exchange']['currency_id']); ?></td-->
						<td><?= date('d-m-Y', strtotime($exchange['Exchange']['start_date'])); ?></td>
						<td><?= h($exchange['Exchange']['rate']); ?></td>
						<td class="actions">
							<?=  $this->element('Form/Simplify/Actions/edit',['id' => $exchange['Exchange']['id']]); ?>
							<?=  $this->Form->postLink($iconaElimina, ['action' => 'delete', $exchange['Exchange']['id']], ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler cancellare il cambio ?',  $exchange['Exchange']['id'])); ?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
