<?php // $this->Html->script(['epos-2.12.0.js']) ?>

<?= $this->element('Form/Components/MegaForm/loader'); ?>
<?= $this->element('Cashregister/jsCashregisterFunctions'); ?>
<?= $this->element('Cashregister/jsGetMovable'); ?>
<?= $this->element('Cashregister/jsSendToPrint'); ?>
<?php //$this->element('Js/variables'); ?>

<script>
    var vats = <?= json_encode($vats); ?>;
</script>
<?= $this->element('Form/formelements/indextitle', ['indextitle' => 'Dashboard - Punto vendita']); ?>
<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

<?php $paddingWidth = "padding-top:7px;margin-right:10px;background-color:#589AB8;color:#fff;width:100%;min-height:60px;font-size:15pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;text-align:center;vertical-align:middle;float:left;padding-left:0px;padding-right:0px;" ?>

<?= $HR12 ?>
<label class="sending" style="color:red;background-color:white;" hidden><h1>INVIO SCONTRINO IN CORSO ...</h1></label>
<script>
$(".sending").css("position","absolute");
$(".sending").css("top", Math.max(0, (($(window).height() - $(".sending").outerHeight()) / 3) + $(window).scrollTop()) + "px");
$(".sending").css("left", Math.max(0, (($(window).width() - $(".sending").outerWidth()) / 2) + $(window).scrollLeft()) + "px");
$(".sending").css("z-index",1000);
$(".sending").css("z-index",1000);
</script>

<div class="col-md-12">
    <div class="col-md-6 input-info">
        <span class="caption-subject bold subTitle"><?= __('Inserisci i dati'); ?></span>

        <?= $HR12 ?>

        <!--div class="form-group col-md-12">
            <div class="col-md-6" style="float:left;">
                <label class="form-label"><strong>Numero Scontrino</strong></label>
                <div class="form-controls">
                    <?php // $this->Form->input('receipt_number', ['label' => false, 'value' => '', 'class' => 'form-control', "pattern" => "[0-9]+", 'title' => 'Il campo può contenere solo caratteri numerici','value'=>$currentReceiptNumber,'readonly'=>true]); ?>
                </div>
            </div>
            <div class="col-md-6" style="float:left;">
                <label class="form-label"><strong>Data Scontrino</strong></label>
                <div class="form-controls">
                    <?php // $this->Form->input('receipt_date', ['label' => false, 'value' => '', 'class' => 'form-control', "pattern" => "[0-9]+", 'title' => 'Il campo può contenere solo caratteri numerici','value'=>date("d-m-Y") ,'readonly'=>true]); ?>
                </div>
            </div>
            <div class="col-md-6" style="float:left;">
                <label class="form-label"><strong>Data</strong></label>
                <div class="form-controls">
                    <input type="datetime" id="datepicker" class=" datepicker segnalazioni-input form-control"
                           name="data[Bill][date]" value="<?= date("d-m-Y") ?>" required/>
                </div>
            </div-->
        <!--/div-->

        <!--div class="col-md-12"><hr></div-->


        <!--div class="form-group col-md-12">
            <div class="col-md-12 col-sm-12" style="float:left;">
                <label class="form-label"><strong>Articolo da magazzino</strong></label>
            </div>
            <div class="form-controls col-md-12 col-sm-6">
                <?php // $this->Form->input('storage_id', ['label' => false, "prefix" => "storage_id", 'div' => false, 'class' => 'form-control', 'options' => $storages, 'empty' => true, 'required' => true]); ?>
            </div>
        </div-->



        <div class="form-group col-md-12">
            <div class="col-md-12 col-sm-12" style="float:left;">
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Articolo da magazzino']) ?>
            </div>
            <div class="col-md-12 col-sm-6">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'storage_id',
                    "aggregator" => '',
                    "prefix" => "storage_id",
                    "list" => $storages,
                    "options" => ['multiple' => false, 'required' => true],
                ]); ?>
            </div>

        </div>
        <!--script> $( "#storage_id" ).autocomplete({source: articoli}); </script-->

        <!--div class="form-group col-md-12">
            <div class="col-md-12 col-sm-12" style="float:left;">
                <label class="form-label"><strong>Voce descrittiva</strong></label>
            </div>
            <div class="form-controls col-md-12 col-sm-6">
                <?php // $this->Form->input('notmovable_id', ['label' => false, "prefix" => "notmovable_id", 'div' => false, 'class' => 'form-control', 'options' => $notmovables, 'empty' => true, 'required' => true]); ?>
            </div>
        </div-->

        <div class="form-group col-md-12">
            <div class="col-md-12 col-sm-12" style="float:left;">
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Servizio']) ?>
            </div>
            <div class="col-md-12 col-sm-6">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'notmovable_id',
                    "aggregator" => '',
                    "prefix" => "notmovable_id",
                    "list" => $notmovables,
                    "options" => ['multiple' => false, 'required' => true],
                ]); ?>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-12" style="float:left;">
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Codice a barre']) ?>
                <div class="form-controls">
                    <?= $this->Form->input('barcode', ['div' => false, 'label' => false, 'class' => 'form-control jsBarcode']); ?>
                    <span id="barcodemessage" style="color:red;" hidden>Articolo inesistente</span>
                </div>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-4" style="float:left;" hidden>
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Quantità']) ?>
                <div class="form-controls">
                    <?= $this->Form->input('quantity', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number', 'default' => 1]); ?>
                </div>
            </div>
            <div class="col-md-4" style="float:left;" hidden>
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Prezzo']) ?>
                <div class="form-controls">
                    <?= $this->Form->input('price', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number']); ?>
                </div>
            </div>
            <div class="col-md-4" style="float:left;" hidden>
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Sconto']) ?>
                <div class="form-controls">
                    <?= $this->Form->input('discount', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number']); ?>
                </div>
            </div>
            <!--div class="col-md-3 col-sm-12" style="float:left;">
                <label class="form-label"><strong>Iva</strong></label>
            </div-->
            <!--div class="col-md-3 col-sm-6">
                    <?php /* $this->element('Form/Components/FilterableSelect/component', [
                        "name" => 'notmovable_id',
                        "aggregator" => '',
                        "prefix" => "notmovable_id",
                        "list" => $vats,
                        "options" => ['multiple' => false, 'required' => true],
                    ]); */ ?>
                </div-->

            <div class="col-md-6" style="float:left;" hidden>
                <label class="form-label"><strong></strong></label>
                <div class="form-controls">
                    <?= $this->Form->input('storage_id', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number']); ?>
                </div>
            </div>
            <div class="col-md-6" style="float:left;" hidden>
                <label class="form-label"><strong></strong></label>
                <div class="form-controls">
                    <?= $this->Form->input('movable', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number']); ?>
                </div>
            </div>
            <div class="col-md-6" style="float:left;" hidden>
                <label class="form-label"><strong></strong></label>
                <div class="form-controls">
                    <?= $this->Form->input('vat_id', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'number']); ?>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <!--  NON CANCELLARE *** Nascosti perché non posso mandarli alla stampante NON CANCELLARE ***-->
            <!--div class="col-md-4" style="float:left;margin-bottom:10px;">
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Codice fiscale']) ?>
                <div class="form-controls">
                    <?= $this->Form->input('cf', ['div' => false, 'label' => false, 'class' => 'form-control jsFiscalCode', 'maxlength' => 11]); ?>
                </div>
            </div>
            <div-- class="col-md-4" style="float:left;margin-bottom:10px;">
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Codice lotteria']) ?>
                <div class="form-controls">
                    <?= $this->Form->input('lottery_code', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                </div>
            </div-->

            <div class="col-md-12" style="float:left;margin-bottom:20px;">
                <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Carta fedeltà']) ?>
                <div class="form-controls">
                    <?= $this->Form->input('fidelity_card', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                </div>
            </div>
        </div>

        <div class="form-group col-md-12 calculator">
            <div class="form-group col-md-12">
                <!--div class="form-group col-md-12"><div onclick="calculate('ENTER')" style="<?= $paddingWidth ?>"><?= 'ENTER' ?></div></div-->
                <!--div class="col-md-12"  id="schermo"  style="margin-bottom:10px;height:50px;border: 1px solid #e5e5e5;padding:5px;font-size:18px" ></div-->
                <?= $this->element('Cashregister/paymentmethod'); ?>
            </div>
        </div>
        <!-- TODO? div class="col-md-12" style="float:left;margin-bottom:20px;" >
            <?php // $this->element('Form/Simplify/Html/Label', ['text' => 'TOTALE PAGATO']) ?>
            <div class="form-controls">
                <?php // $this->Form->input('payed', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
            </div>
        </div-->
        <div class="col-md-12">
            <hr class="invisible">
        </div>
        <?= $this->element('Cashregister/receipt'); ?>

    </div>
    <div class="col-md-6">
        <center>
            <div class="col-md-12">
                <?= $this->element('Cashregister/keyboard'); ?>
            </div>
        </center>
        <?= $this->element('Cashregister/command'); ?>
    </div>

    <div class="form-group col-md-6">
        <div class="col-md-12" style="float:right;">
            <?= $this->element('Form/Simplify/Html/Label', ['text' => 'Inserire codice a barre per le operazioni sottostanti']) ?>
            <div class="form-controls"><?= $this->Form->input('check_barcode', ['div' => false, 'label' => false, 'class' => 'form-control checkBarcode']); ?>
                <!--span id="barcodemessage2" style="color:red;" hidden >Articolo inesistente</span-->
            </div>
        </div>

        <div class="col-md-12 col-sm-12" style="margin-top:30px;">
            <!--a class="btn blue-button btn-outline mega-form-button barcodeMegaform" title=""
               style="min-width:150px;width: auto;font-weight:normal;padding:5px;text-transform:none !important;font-family: 'Barlow Semi Condensed' !important;font-weight: normal;background-color:#589AB8;"
               data-toggle="modal" href="#mega_form_index_resume" tabindex="-1">Visualizza giacenza</a-->
            <a class="btn blue-button btn-outline mega-form-button wherehousing" title=""
               style="min-width:150px;width: auto;font-weight:normal;padding:5px;text-transform:none !important;font-family: 'Barlow Semi Condensed' !important;font-weight: normal;background-color:#589AB8;"
               data-toggle="modal" tabindex="-1">Visualizza giacenza</a>
            <a class="btn blue-button btn-outline mega-form-button barcodeFastInventory" title=""
               style="min-width:150px;width: auto;font-weight:normal;padding:5px;text-transform:none !important;font-family: 'Barlow Semi Condensed' !important;font-weight: normal;background-color:#589AB8;"
               data-toggle="modal" tabindex="-1">Aggiusta inventario del deposito</a>
            <span class="btn blue-button btn-outline mega-form-button" onclick="calculate('R')" title=""
                  style="min-width:150px;width: auto;font-weight:normal;padding:5px;text-transform:none !important;font-family: 'Barlow Semi Condensed' !important;font-weight: normal;background-color:#589AB8;"
                  data-toggle="modal" tabindex="-1">Effettua reso</span>

        </div>
        <div class="col-md-12 col-sm-12" style="margin-top:30px;"><?= $HR12 ?></div>

        <div class="form-group col-md-12 WH" style="border-top 1px solid black;" hidden>
            <div class="col-md-12" style="float:right;">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">&nbsp;</div>

                <div class="col-md-12" style="margin-right:15px;color:#589AB8"><b>GIACENZA DI MAGAZZINO: </b></div>

                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-8">&nbsp;</div>

                <div class="col-md-2" style="margin-right:15px;"><b>Articolo: </b></div>
                <div class="jsName col-md-6" style="padding-left:10px;"></div>
                <div class="col-md-3">&nbsp;</div>

                <div class="col-md-4" style="margin-right:15px;">&nbsp;</div>
                <div class="col-md-7">&nbsp;</div>

                <div class="col-md-2" style="margin-right:15px;"><b>Giacenza: </b></div>
                <div class="jsWherehousing col-md-6" style="text-align:right;"></div>
                <div class="col-md-3">&nbsp;</div>

                <div class="col-md-2" style="margin-right:15px;"><b>Ordinati: </b></div>
                <div class="jsOrdered col-md-6" style="text-align:right;"></div>
                <div class="col-md-3">&nbsp;</div>


                <div class="col-md-2" style="margin-right:15px;"><b>Impegnati: </b></div>
                <div class="jsReserved col-md-6" style="text-align:right;"></div>
                <div class="col-md-3">&nbsp;</div>
            </div>
        </div>
    </div>


    <script>

        $(document).ready(function () {
            <?php if(!isset($defaultIva['Iva'])){ ?>
            /*$.alert
            ({
                icon: 'fa fa-warning',
                title: 'IVA STANDARD NON CONFIGURATA',
                content: "Attenzione non è stata impostata nessuna aliquota iva predefinita per gli scontrini. Prima di proseguire contattare l'amministrazione.",
                type: 'orange',
            });*/
            $("#content").html('<center><b><h1>Non è stata impostata nessuna aliquota iva predefinita per gli scontrini</br><br/>Per poter utilizzare il registratore di cassa configurare tale valore.</br></br>Contattare l\'amministrazione</br></h1></b></center>');
            <?php } ?>
        });

        /* $('.checkBarcode').on('input', function (x) {
           /*  console.log(x);
             var e = jQuery.Event("keyup");
             e.keyCode = 13;
             $(this).trigger(e);*/
        //$(".WH").hide();
        // });*/

        /*$('.checkBarcode').on('keyup', function (e) {
            if (e.keyCode == 13) {
                $('.barcodeMegaform').click();
            }
        });*/

        $(".createReceipt").click(function () {
            console.log('start save receipt');
            var receiptRowArray = [];
            $(".clonedRow").each(
                function () {

                    var name = $(this).find('.name').html();
                    var barcode = $(this).find('.barcode').html();
                    var vat = $(this).find('.vat').html();
                    var vatvalue = $(this).find('.vatvalue').html();
                    var quantity = $(this).find('.quantity').html();
                    var price = $(this).find('.price').html();
                    var movable = $(this).find('.movable').html();
                    var storage_id = $(this).find('.storage_id').html();
                    var discount = $(this).find('.discount').html();

                    receiptRowArray.push({
                        description: name,
                        barcode: barcode,
                        vat: vat,
                        vatvalue: vatvalue,
                        quantity: quantity,
                        price: price,
                        movable: movable,
                        storage_id: storage_id,
                        discount: discount,
                    });

                });


            var paymentMethod = null;
            // Controllo selezione metodo di pagamento
            if ($(".cash").css("background-color") == "rgb(0, 128, 0)") {
                paymentMethod = 1000;
                errore = 0;
                if (receiptRowArray.length == 0) {
                    errore = 2;
                } else {
                }
            } else if ($(".electronic").css("background-color") == "rgb(0, 128, 0)") {
                paymentMethod = 2000;
                errore = 0;
                if (receiptRowArray.length == 0) {
                    errore = 2;
                } else {
                }
            } else if ($(".notpayed").css("background-color") == "rgb(0, 128, 0)") {
                paymentMethod = 3000;
                errore = 0;
                if (receiptRowArray.length == 0) {
                    errore = 2;
                } else {
                }
            } else if($(".totale").val() < 0)
            {
                errore = 3;
            }
            else {
                errore = 1;
            }

            // Controllo scontrinato
            console.log(receiptRowArray);
            console.log(errore);

            switch (errore) {
                case 0:
                    /** Mando al registatore di cassa */
                    multiSendToPrinter('createReceipt');

                    $(".cash").css('background-color', 'grey');
                    $(".electronic").css('background-color', 'grey');
                    $(".notpayed").css('background-color', 'grey');
                    $(".clonedRow").remove();
                    $(".clonedDiscountRow").remove();
                    $(".totale").val('0.00');
                    $(".totale").html($(".totale").val());
                    $(".scontototale").val('0.00');
                    $(".scontototale").html($(".scontototale").val());
                    $("#price").val(0);
                    $("#discount").val(0);
                    // New
                    $(".rowsubtotal").hide();
                    $(".rowsubtotal").find(".subtotale").html('');
                    $(".rowsubtotal").find(".subtotale").val('0.00');

                    $(".rowReceiptDiscountFixed").hide();
                    $(".rowReceiptDiscountFixed").find(".subtotale").html('');
                    $(".rowReceiptDiscountFixed").find(".subtotale").val('0.00');



                    // questa va spostata perché i dati come numero scontrino e data li prendo dalla risposta del registratore di cassa
                   /* $.ajax
                    ({
                        method: "POST",
                        url: "<?php // $this->Html->url(["controller" => "receipts", "action" => "createReceiptFromCashRegister"]) ?>",
                        data:
                            {
                                receiptRows: receiptRowArray,
                                clientFiscalCode: $(".jsFiscalCode").val(),
                                paymentMethod: paymentMethod,
                            },
                        success: function (data) {
                            $(".cash").css('background-color', 'grey');
                            $(".electronic").css('background-color', 'grey');
                            $(".notpayed").css('background-color', 'grey');
                            $(".clonedRow").remove();
                            $(".clonedDiscountRow").remove();
                            $(".totale").val('0.00');
                            $(".totale").html($(".totale").val());
                            $(".scontototale").val('0.00');
                            $(".scontototale").html($(".scontototale").val());
                            $("#price").val(0);
                            $("#discount").val(0);
                            // Mostro lo scontrino

                            //sendReceipt();

                            /*$.ajax
                            ({
                                method: "POST",
                                url: "<?php // $this->Html->url(["controller" => "receipts", "action" => "getNextReceiptNumber"]) ?>",
                        data:
                            {},
                        success: function (data) {
                            $("#receipt_number").val(data);
                        }
                    })*/
                   /*     },
                        error: function (data) {
                            console.log(data);
                            $.alert
                            ({
                                icon: 'fa fa-warning',
                                title: 'Punto vendita',
                                content: "<?php // addslashes('Errore'); ?>",
                                type: 'orange',
                            });
                            $(".cash").css('background-color', 'grey');
                            $(".electronic").css('background-color', 'grey');
                            $(".notpayed").css('background-color', 'grey');
                        }
                    }) */
                    break;
                case 1:
                    $.alert
                    ({
                        icon: 'fa fa-warning',
                        title: 'Punto vendita',
                        content: "Selezionare un metodo di pagamento: contanti o elettronico",
                        type: 'orange',
                    });
                    break;
                case 2:
                    $.alert
                    ({
                        icon: 'fa fa-warning',
                        title: 'Punto vendita',
                        content: "Attenzione - scontrino vuoto",
                        type: 'orange',
                    });
                    break;
                case 3:
                    $.alert
                    ({
                        icon: 'fa fa-warning',
                        title: 'Punto vendita',
                        content: "Attenzione - l'importo dello scontrino non può essere negativo",
                        type: 'orange',
                    });
                    break;
            }
        })

        // && $("#storage_id").change(function()
        $("#storage_id_multiple_select").change(function () {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "storages", "action" => "getMovableStorageById"]) ?>",
                data:
                    {
                        storageId: $(this).val(),
                    },
                success: function (data) {
                    data = JSON.parse(data);
                    console.log("changed!!!!!!");
                    console.log(data);
                    console.log(data.barcode);
                    $("#barcode").val(data.barcode);
                    $("#price").val(data.prezzo);
                    $("#discount").val(data.discount_default);
                    $("#notmovable_id_multiple_select").val(null);
                    $("#notmovable_id_multiple_select").multiselect('rebuild');
                    $("#storage_id").val(data.id);
                    $("#movable").val(data.movable);
                    $("#vat_id").val(data.vat_id);
                    $("#discount").val(data.default_discount);
                    addNewRowToReceipt(0);
                }
            })

        })


        $("#notmovable_id_multiple_select").change(function () {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "storages", "action" => "getNotMovableStorageById"]) ?>",
                data:
                    {
                        storageId: $(this).val(),
                    },
                success: function (data) {
                    data = JSON.parse(data);
                    console.log(data);
                    $("#storage_id_multiple_select").val(null);
                    $("#storage_id_multiple_select").multiselect('rebuild');
                    $("#barcode").val(data.barcode);
                    $("#price").val(data.prezzo);
                    $("#discount").val(data.discount);
                    $("#storage_id").val(data.id);
                    $("#movable").val(data.movable);
                    $("#vat_id").val(data.vat_id);
                    addNewRowToReceipt(0);
                }
            })
        })


        $("#barcode").change(function () {
            getMovable($(this));
        });

        $("#fidelity_card").on('keyup', function () {
            if ($(this).val() != '') {
                $.ajax
                ({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "Clients", "action" => "getClientByFidelitycard"]) ?>",
                    data:
                        {
                            fidelitycard: $(this).val()
                        },
                    success: function (data) {
                        data = JSON.parse(data);
                        if(data == null){
                            $.alert
                            ({
                                icon: 'fa fa-warning',
                                title: 'Punto vendita',
                                content: "Carta fedeltà non riconosciuta.",
                                type: 'red',
                            });
                        }
                    },
                    error: function (data) {
                    }
                });
            }
        });


        $("#quantity").focus(function () {
            $(this).css('border-color', 'yellow');
            $("#price").css('border-color', '#e5e5e5');
            $(".calculator").attr("currentField", "quantity");
            $("#schermo").val('');
            $("#schermo").html('');
        });


        $("#price").focus(function () {
            $(this).css('border-color', 'yellow');
            $("#quantity").css('border-color', '#e5e5e5');
            $(".calculator").attr("currentField", "price");
            $("#schermo").val('');
            $("#schermo").html('');
        });

        $("#barcode").on('keyup', function (e) {
            if (e.keyCode == 13) {
                calculate('ENTER');
            }
        })

    </script>

    <script>
        function calculate(value, r = null) {

            var valore = $('#schermo').html();
            // if ($('.calculator').attr('currentField') != "price" && $('.calculator').attr('currentField') != "quantity" && value != 'ENTER' ) {
            //          if( value != 'ENTER'){

            //setQuantityFocus();
            //calculate(value);
            /*$.alert
            ({
                icon: 'fa fa-warning',
                title: 'Cassa',
                content: "<?php // addslashes('Selezionare il campo quantità o prezzo'); ?>",
                    type: 'orange',
                });*/
//            } else {
            switch (value) {
                case 'C':
                    $("#schermo").html('');
                    $("#schermo").val('');
                    break;
                case '.':
                    $("#schermo").val($("#schermo").val() + value);
                    $("#schermo").html($("#schermo").val());
                    break;
                case '00':
                    $("#schermo").val($("#schermo").val() + value);
                    $("#schermo").html($("#schermo").val());
                    break;
                case '000':
                    $("#schermo").val($("#schermo").val() + value);
                    $("#schermo").html($("#schermo").val());
                    break;
                case '+':
                    $("#schermo").val($("#schermo").val() + value);
                    $("#schermo").html($("#schermo").val());
                    if ($('.calculator').attr('currentField') == "price") {
                        $("#quantity").val($("#price").val() + $("#schermo").val());
                    }
                    if ($('.calculator').attr('currentField') == "quantity") {
                        $("#quantity").val($("#quantity").val() + $("#schermo").val());
                    }
                    break;
                case '-':
                    $("#schermo").val($("#schermo").val() + value);
                    $("#schermo").html($("#schermo").val());
                    break;
                case '=':
                    $("#schermo").html(eval($("#schermo").val()));
                    $("#schermo").val(eval($("#schermo").val()));
                    break;
                case '%':
                    setTotalDiscount($("#schermo").html());
                    $("#schermo").html('');
                    $("#schermo").val('');
                    break;
                case 'D':
                    var importoScontato = (+$("#schermo").val()).toFixed(2);
                    addDiscount(importoScontato);
                    // importoScontato = $(+importoScontato).toFixed(2);
                    var importoTotale = $(".totale").html();

                    $("#schermo").html('');
                    $("#schermo").val('');
                    $(".rowReceiptDiscountFixed").show();
                    $(".rowsubtotal").show();
                    console.log($(".totale").val());
                    $(".subtotale").val(importoTotale);
                    $(".subtotale").html(importoTotale);
                    $(".rowReceiptDiscountFixed").find('.rowDiscountFixed').val(importoScontato) ;
                    $(".rowReceiptDiscountFixed").find('.rowDiscountFixed').html(importoScontato);

                    break;
                case 'CS':
                    $(".cash").css('background-color', 'grey');
                    $(".electronic").css('background-color', 'grey');

                    $(".clonedRow").remove();
                    $(".clonedDiscountRow").remove();
                    $(".totale").val('0.00');
                    $(".totale").html($(".totale").val());
                    $(".scontototale").html('0.00');
                    $("#price").val(0);
                    $("#discount").val(0);

                    // New subtotale
                    $(".rowsubtotal").hide();
                    $(".rowsubtotal").find(".subtotale").html('');
                    $(".rowsubtotal").find(".subtotale").val('0.00');

                    $(".rowReceiptDiscountFixed").hide();
                    $(".rowReceiptDiscountFixed").find(".subtotale").html('');
                    $(".rowReceiptDiscountFixed").find(".subtotale").val('0.00');
                    break;
                case 'R':
                    calculate('ENTER', 1);
                    $("#schermo").val('');
                    $("#schermo").html('');
                    break;
                case 'OK':
                    /*  if ($('.calculator').attr('currentField') == "price") {
                          $("#price").val($("#schermo").val());
                      }
                      if ($('.calculator').attr('currentField') == "quantity") {
                          $("#quantity").val($("#schermo").val());
                      } */
                    calculate('ENTER');
                    $("#quantity").val(1);
                    $("#schermo").val('');
                    $("#schermo").html('');
                    break;
                case 'X':
                    setCodbarFocused();
                    if ($("#schermo").html() != '') {
                        $("#quantity").val(($("#schermo").html()))
                        $("#schermo").html('');
                    } else {
                        null;
                    }
                    break;
                case 'ENTER':
                    if (r == 1) {
                        var barcode = $($("#check_barcode")).val();
                    } else {
                        var barcode = $($("#barcode")).val();
                    }

                    if (barcode == '' || barcode === undefined) {
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Barcode assente',
                            content: 'Attenzione non è stato inserito alcun barcode',
                            type: 'orange'
                        });
                        /* $.confirm({
                            icon: 'fa fa-warning',
                            title: 'Barcode assente',
                            content: 'Attenzione non è stato inserito alcun barcode',
                            type: 'orange',
                            buttons: {
                                Ok: function () {
                                    action:
                                    {
                                        getMovableByBarcode(barcode, r);
                                    }
                                }, annulla: function () {
                                    action:
                                    {
                                        // Nothing
                                    }
                                },
                            }
                        });*/
                    } else {
                        getMovableByBarcode(barcode, r);
                    }
                    break;
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    if($("#payed").is(":focus"))
                    {
                        $("#payed").val($("#payed").val() + value);
                        $("#payed").html($("#payed").html() + value);
                    }
                    else
                        {
                            $("#schermo").val($("#schermo").val() + value);
                            $("#schermo").html($("#schermo").html() + value);
                        }

                    //$("#schermo").html();
                    break;
            }
        }

        function getMovableByBarcode(barcode, returned) {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "storages", "action" => "getMovableStorageByBarcode"]) ?>",
                data:
                    {
                        barcode: barcode,
                    },
                success: function (data) {
                    console.log(data);
                    data = JSON.parse(data);

                    /** NEW **/
                    $.ajax
                    ({
                        method: "POST",
                        async: false,
                        url: "<?= $this->Html->url(["controller" => "Storages", "action" => "sellerindexresume"]) ?>",
                        data:
                            {
                                barcode: barcode,
                                deposit: true,
                            },
                        success: function (data) {
                            data = JSON.parse(data);
                           /* if(data.wherehousing < $("#quantity").val()){
                                $.alert
                                ({
                                    icon: 'fa fa-warning',
                                    title: 'Disponibilità articolo',
                                    content: 'Attenzione, non è disponibile la quantità richiesta per l\'articolo selezionato.',
                                    type: 'orange',
                                });
                            }*/

                        },
                        error: function (data) {
                        }
                    });
                    /** NEW **/


                    if (data == null) {
                        $("#barcode").css('border-color', 'red');
                        $("#quantity").css('border-color', '#e5e5e5');
                        setCodbarFocused();
                        $(".multiselect-native-select").click();
                        $("#barcodemessage").show();
                        $("#storage_id").val('');
                        $("#movable").val('');
                        $("#vat_id").val('');
                        $("#discount").val('');
                    } else {
                        console.log(data);


                        $("#storage_id_multiple_select").val(data.id);
                        console.log(data.id);
                        $("#storage_id_multiple_select").multiselect('rebuild');
                        $("#notmovable_id_multiple_select").val(null);
                        $("#notmovable_id_multiple_select").multiselect('rebuild');
                        $("#storage_id").val(data.id);
                        $("#movable").val(data.movable);
                        $("#vat_id").val(data.vat_id);
                        if (returned !== null)
                            $("#price").val(-1 * data.prezzo);
                        else
                            $("#price").val(data.prezzo);
                        $("#discount").val(data.default_discount);
                        $("#quantity").focus();
                        addNewRowToReceipt(returned);
                        console.log(data);

                        $("#schermo").val('');
                        $("#schermo").html('');
                    }
                }

            });
        }

        function changePayment(type) {
            $(".cash").css('background-color', 'grey');
            $(".electronic").css('background-color', 'grey');
            $("." + type).css('background-color', 'green');
        }

        function sendReceipt() {
            alert("CODICE SCONTRINO DA ULTIMARE");
        }


        $(document).ready(function () {
            setCodbarFocused()


            $('.checkBarcode').on('keyup', function (e) {
                $(".WH").hide();
            });


            $(".wherehousing").click(function () {
                var barcodeValue = $("#check_barcode").val();
                if (barcodeValue == '') {
                    $.alert
                    ({
                        icon: 'fa fa-warning',
                        title: 'Visualizzazione giacenze',
                        content: 'Attenzione non è stato selezionato alcun barcode',
                        type: 'orange',
                    });
                } else {
                    $.ajax
                    ({
                        method: "POST",
                        url: "<?= $this->Html->url(["controller" => "storages", "action" => "getMovableStorageByBarcode"]) ?>",
                        data:
                            {
                                barcode: barcodeValue,
                            },
                        success: function (data) {
                            data = JSON.parse(data);
                            if (data == null) {
                                $.alert
                                ({
                                    icon: 'fa fa-warning',
                                    title: 'Inventario veloce',
                                    content: 'Barcode inesistente, codificare l\' articolo a magazzino. Inventario veloce non applicabile.',
                                    type: 'orange',
                                });
                            } else {
                                $.ajax
                                ({
                                    method: "POST",
                                    url: "<?= $this->Html->url(["controller" => "Storages", "action" => "sellerindexresume"]) ?>",
                                    data:
                                        {
                                            barcode: barcodeValue,
                                            deposit: true,
                                        },
                                    success: function (data) {
                                        data = JSON.parse(data);
                                        $(".WH").show();
                                        $(".jsName").html(data.name);
                                        $(".jsWherehousing").html(data.wherehousing);
                                        $(".jsOrdered").html(data.ordered);
                                        $(".jsReserved").html(data.reserved);
                                        console.log(data);
                                        console.log(data.name);

                                    },
                                    error: function (data) {
                                    }
                                });
                            }
                        }
                    })
                }
            })

            // fix

            $(".barcodeFastInventory").click(function () {
                var quoteIdAttribute = $(this).attr("quoteid");


                console.log($.isNumeric($("#schermo").val()));

                var barcodeValue = $("#check_barcode").val();
                var monitorValue = $("#schermo").val();
                if (barcodeValue == '') {
                    $.alert
                    ({
                        icon: 'fa fa-warning',
                        title: 'Inventario veloce',
                        content: 'Attenzione non è stata selezionato alcun barcode',
                        type: 'orange',
                    });
                } else if (!($.isNumeric(monitorValue))) {
                    $.alert
                    ({
                        icon: 'fa fa-warning',
                        title: 'Inventario veloce',
                        content: 'Attenzione non è stata inserita una quantità inventariale corretta',
                        type: 'orange',
                    });
                } else {
                    $.confirm({
                        title: 'Inventario veloce',
                        content: 'Attenzione si sta procedendo alla sistemazione dell\'inventario del punto vendita con una quantità pari a ' + $("#schermo").val() + ', continuare ?',
                        type: 'blue',
                        buttons: {
                            Ok: function () {
                                action:
                                {
                                    $.ajax
                                    ({
                                        method: "POST",
                                        url: "<?= $this->Html->url(["controller" => "storages", "action" => "getMovableStorageByBarcode"]) ?>",
                                        data:
                                            {
                                                barcode: barcodeValue,
                                            },
                                        success: function (data) {
                                            data = JSON.parse(data);
                                            if (data == null) {
                                                $.alert
                                                ({
                                                    icon: 'fa fa-warning',
                                                    title: 'Inventario veloce',
                                                    content: 'Barcode inesistente, codificare l\' articolo a magazzino. Inventario veloce non applicabile.',
                                                    type: 'orange',
                                                });
                                            } else {
                                                $.ajax
                                                ({
                                                    method: "POST",
                                                    url: "<?= $this->Html->url(["controller" => "storages", "action" => "fastInventory"]) ?>",
                                                    data:
                                                        {
                                                            barcode: barcodeValue,
                                                            quantity: monitorValue,
                                                        },
                                                    success: function () {
                                                        $(".wherehousing").click();
                                                        $.alert
                                                        ({
                                                            icon: 'fa fa-warning',
                                                            title: 'Inventario veloce',
                                                            content: 'Inventario eseguito correttamente.',
                                                            type: 'blue',
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    })
                                }
                            },
                            annulla: function () {
                                action:
                                {
                                    //alert('no');
                                    // Nothing
                                    //$(".enhanced-dialog-container").show();
                                }
                            },
                        }
                    })
                }
            });
        });
    </script>




