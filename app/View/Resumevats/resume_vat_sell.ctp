<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Registro IVA vendite'); ?></span>
	</div>
</div>
<div>
	<br/>
</div>
<?= $this->Form->create(null, ['url' => ['action' => 'CreateResumeVatSell', 'controller' => 'resumevats'], 'class' => 'uk-form uk-form-horizontal']); ?>

<div>
<table style="width:100%">
		<tr style="width:100%">
			<td style="width:10%;vertical-align:middle  !important;padding:5px;"><label><b>Fatture dal</b></label></td>
			<td style="width:20%;"><input  type="datetime" id="datepicker" class="form-control  datepicker" name="data[dateFrom]"  required/></td>
			<td style="width:5%;vertical-align:middle !important;padding-left:1%;"><label><b>al</b></label></td>
			<td style="width:20%;"><input type="datetime" id="datepicker2" class="form-control datepicker" name="data[dateTo]"   required/></td>
            <td style="width:5%;"></td>
            <td style="width:40%;"><input class="btn blue-button new-bill" type="submit" style="margin-top:0px;min-width:50%;max-width: 50%" value="Genera Pdf registro iva" ></td>
        </tr>
</table>
<br/>
<div class="submit" style="margin-left:2%;">

</div>
  <?= $this->Form->end();  ?>
</div>
<?= $this->element('Js/datepickercode'); ?>
