<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>


<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Valute','indexelements' => ['add'=>'Nuova valuta']]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($currencies) > 0) { ?>
				<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
				<?php }
					else
			{
				?><tr><td colspan="3"><center>nessuna valuta trovata</center></td></tr><?php				
			}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($currencies as $currency) 
				{
				?>
					<tr>
						<td><?php echo h($currency['Currency']['description']); ?></td>
						<td><?php echo h($currency['Currency']['symbol']); ?></td>
						<td><?php echo h($currency['Currency']['isocode']); ?></td>
					
						<td class="actions">
							<?php if(strtoupper($currency['Currency']['isocode']) != 'EUR') { ?>
								<?=  $this->element('Form/Simplify/Actions/edit',['id' => $currency['Currency']['id']]); ?>
								<?= $this->Html->link($iconaLisitini, array('controller' => 'exchanges' , 'action' => 'index', $currency['Currency']['id']),array('title'=>__('Storico cambio'),'escape'=>false)); ?>
								<?=  $this->Form->postLink($iconaElimina, array('action' => 'delete', $currency['Currency']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler cancellare la valuta ?',  $currency['Currency']['id'])); ?>
							<?php } ?>
						</td>
						
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
