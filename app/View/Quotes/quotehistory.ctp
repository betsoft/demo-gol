<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

	<?php //	$this->element('Form/formelements/indextitle',['indextitle'=>'Preventivi','indexelements' => ['add'=>'Nuovo preventivo']]); ?>

    <?= $this->Html->link('indietro', ['controller'=>'quotes','action' => 'index'], ['title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:10%;text-align:center;']); ?>

	<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
		</thead>
		
		<?php $currentYear = date("Y"); ?>
		
		<tr>
			<?=  $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields, 
			'htmlElements' => [
				'<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="01-01-'.$currentYear.'"  bind-filter-event="change"/>'.''.
				'<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="31-12-'.$currentYear.'" bind-filter-event="change"/></center>'
				]]); 
			?></tr>

		<?php 
		if (count($quotes) == 0) {
            if (MODULO_CANTIERI) {
                ?>
                <tr>
                    <td colspan="8">
                        <center>nessun preventivo trovato</center>
                    </td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td colspan="7">
                        <center>nessun preventivo trovato</center>
                    </td>
                </tr><?php
            }
        }
		
		?>
		<tbody class="ajax-filter-table-content">
		<?php
		$totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = 0;

		foreach ($quotes as $quote)
		{
		    ?>
			<tr>
				<td><?= $quote['Quote']['quote_number']; ?> / <?= substr($quote['Quote']['quote_date'],0,4); ?></td>
				<td style="text-align:center;"><?= $this->Time->format('d-m-Y', $quote['Quote']['quote_date']); ?></td>
				<td class="table-max-width uk-text-truncate">
					<?php echo $quote['Client']['ragionesociale']; ?>
				</td>
                <?php
                if (MODULO_CANTIERI) {
                   //  debug($quote);
                    ?>
                    <td><?= $quote['Constructionsite']['name']; ?></td>
                    <td>
                        <?php
                        switch ($quote['Quote']['type_of_variation']) {
                            case 'MO':
                                echo 'Modifica a preventivo accettato';
                                break;
                            case 'VA':
                                echo 'Variazione';
                                break;
                        }
                        ?>
                    </td>
                    <?php
                }
                ?>

			<td style="text-align:right;">
			<?php
					if(isset($quote['Quotegoodrow'])) {
                        // Importo
                        $articolo = '';
                        $prezzo_articoli = $prezzo_articoli_ivato = $prezzo_pagato = $prezzo_da_pagare = $prezzo_non_pagato = $prezzo_scontato = $ritenutaAcconto = $importo = 0;

                        // Per ogni articolo
                        foreach ($quote['Quotegoodrow'] as $articolo) {

                            isset($articolo['Iva']['percentuale']) ? $definedPercentage = (1 + $articolo['Iva']['percentuale'] / 100) : $definedPercentage = 1;

                            $totaleRiga = $articolo['quote_good_row_price'] * $articolo['quantity'];
                            $totaleRigaScontato = $totaleRiga;
                            $prezzo_articoli += $totaleRigaScontato * $definedPercentage;
                            $prezzo_scontato += $totaleRigaScontato;
                        }

                        $totale_fatturato += $prezzo_articoli;
                        $totale_fatturato_pagato += $prezzo_pagato;
                        $totale_fatturato_da_pagare += $prezzo_da_pagare;
                        $totale_fatturato_pagato_parzialmente += $prezzo_pagato_parziale;
                        $totale_fatturato_non_pagato += $prezzo_non_pagato;

                        $importo += $prezzo_scontato;

                        if ($prezzo_articoli == '') {
                        } else {
                            echo number_format($prezzo_scontato, 2, ',', '.');
                        }
                        echo '<br/>';
                    }
			?>
		</td>
		<td style="text-align:right;">
			<?php
				if(isset($quote['Quotegoodrow'])) {
                    $articolo = $prezzo_articoli_ivato = '';
                    foreach ($quote['Quotegoodrow'] as $articolo) {
                        isset($articolo['Iva']['percentuale']) ? $definedPercentage = ($articolo['Iva']['percentuale'] / 100) : $definedPercentage = 1;
                        $totaleRiga = $articolo['quote_good_row_price'] * $articolo['quantity'];
                        $totaleRigaScontato = $totaleRiga;
                        $prezzo_articoli += $totaleRigaScontato * $definedPercentage;
                        $prezzo_scontato += $totaleRigaScontato;
                        $prezzo_articoli_ivato += $totaleRigaScontato * $definedPercentage;
                    }

                    if (isset($prezzo_articoli_ivato)) {
                        if ($prezzo_articoli_ivato != '') {
                            echo number_format($prezzo_articoli_ivato, 2, ',', '.');
                        } else {
                            // Nothing
                        }
                    } else {
                        // Nothing
                    }
                }
			?>
		</td>
		<td style="text-align:right;"> 
			<?php 
					$articolo = $prezzo_articoli_ivato = '';
			if(isset($quote['Quotegoodrow'])) {
                foreach ($quote['Quotegoodrow'] as $articolo) {
                    isset($articolo['Iva']['percentuale']) ? $definedPercentage = (1 + $articolo['Iva']['percentuale'] / 100) : $definedPercentage = 1;
                    $totaleRiga = $articolo['quote_good_row_price'] * $articolo['quantity'];
                    $totaleRigaScontato = $totaleRiga;
                    $prezzo_articoli += $totaleRigaScontato * $definedPercentage;
                    $prezzo_scontato += $totaleRigaScontato;

                    $prezzo_articoli_ivato += $totaleRigaScontato * $definedPercentage;
                }

                if (isset($prezzo_articoli_ivato)) {
                    if ($prezzo_articoli_ivato != '') {
                        echo number_format($prezzo_articoli_ivato, 2, ',', '.');
                    } else {
                        // Nothing
                    }
                } else {
                    // Nothing
                }
            }
			?>
		</td>
		<td class="actions" style="text-align:left;">
			<?php
				if($quote['Quote']['bill_created'] == 0)
				{
                    echo $this->Html->link($iconaPdf, ['action' => 'quotespdf', $quote['Client']['ragionesociale'] . '-' . str_replace("/", "", $quote['Quote']['quote_number']) . '_' . $this->Time->format('d_m_Y', $quote['Quote']['quote_date']), $quote['Quote']['id']], ['target' => '_blank', 'title' => __('Scarica PDF'), 'escape' => false]);
                }
				else {
                    echo $this->Html->link($iconaPdf, array('action' => 'quotespdf', $quote['Client']['ragionesociale'] . '-' . str_replace("/", "", $quote['Quote']['quote_number']) . '_' . $this->Time->format('d_m_Y', $quote['Quote']['quote_date']), $quote['Quote']['id']), ['target' => '_blank', 'title' => __('Scarica PDF'), 'escape' => false]);
                }
			?>
		</td>
	</tr>
<?php } ?>

</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
	<div id="element"></div>

<?=  $this->element('Js/datepickercode'); ?>

<script>
    $(".jsQuoteAdd").click(function(){
        alert("Modifica preventivo");
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quoteadd"]) ?>",
            data:
                {
                    quoteid: $(this).attr("quoteid"),
                },
            success: function (data) {
                $(this).attr("color", "red");
            }
        });
    });
    $(".jsQuoteChange").click(function(){
        alert("Modifica preventivo");
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quotechange"]) ?>",
            data:
                {
                    quoteid: $(this).attr("quoteid"),
                },
            success: function (data) {
                $(this).attr("color", "red");
            }
        });
    });
    $(".jsQuoteStory").click(function(){
        alert("Modifica preventivo");
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quotestory"]) ?>",
            data:
                {
                    quoteid: $(this).attr("quoteid"),
                },
            success: function (data) {
                $(this).attr("color", "red");
            }
        });
    });
    $(".jsQuoteChange").click(function(){alert('vario preventivo esistente')});
    $(".jsQuoteStory").click(function(){alert('storia preventivo esistente')});
</script>
