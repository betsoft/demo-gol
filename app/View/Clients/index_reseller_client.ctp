<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Clienti del rivenditore ' . $resellerName); ?></span>
		<div class=" -badge tools">
			<?= $this->Html->link('<i class="uk-icon-plus-circle icon"></i>', array('action' => 'addNewResellerClient',$resellerId),array('title'=>__('Nuovo Cliente'),'escape'=>false)); ?>
		</div>
	</div>
</div>
<div class="clients index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr>
				<th><?= $this->Paginator->sort('ragsociale', 'Ragione sociale'); ?></th>
				<th><?= $this->Paginator->sort('indirizzo'); ?></th>
				<th><?= $this->Paginator->sort('cap', 'CAP'); ?></th>
				<th><?= $this->Paginator->sort('citta', 'Città'); ?></th>
				<th><?= $this->Paginator->sort('provincia'); ?></th>
				<th><?= $this->Paginator->sort('nazione'); ?></th>
			</tr>
			<?php if (count($clients) > 0) { ?>
			<tr>
				<?php // $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?>
			</tr>
			<?php } ?>
		</thead>
		<tbody class="ajax-filter-table-content">
			<?php foreach ($clients as $client)
			{ 
			?>
				<tr>
					<td class="table-max-width uk-text-truncate"><?= h($client['Client']['ragionesociale']); ?></td>
					<td><?= h($client['Client']['indirizzo']); ?></td>
					<td><?= h($client['Client']['cap']); ?></td>
					<td><?= h($client['Client']['citta']); ?></td>
					<td><?= h($client['Client']['provincia']); ?></td>
					<td><?= isset($client['Client']['Nation']['name']) ?  h($client['Client']['Nation']['name']) : ''; ?></td>
				</tr>
			
			<?php } ?>
		</tbody>
	</table>
	<?php  // $this->element('Form/Components/Paginator/component'); ?>
</div>
