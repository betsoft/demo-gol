<?= $this->Form->create(); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo cliente') ?></span>
<div class="col-md-12 col-sm-12">
    <hr>
</div>

<div class="form-group col-md-12">
    <div class="col-md-9 ">
        <label class="form-label "><strong>Cognome Nome / Ragione sociale<i class="fa fa-asterisk"></i></strong></label>
        <div class="form-controls">
            <?= $this->Form->input('ragionesociale', ['label' => false, 'div' => false, 'class' => 'form-control', 'required' => true, 'pattern' => PATTERNBASICLATIN]); ?>
        </div>
    </div>
    <div class="col-md-3">
        <label class="form-label "><strong>Telefono</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('telefono', ['label' => false, 'div' => false, 'class' => 'form-control', 'required' => true]); ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-4">
        <label class="form-label "><strong>Indirizzo</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('indirizzo', ['label' => false, 'div' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2">
        <label class="form-label"><strong>CAP</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('cap', ['label' => false, 'div' => false, 'class' => 'form-control', "pattern" => "[0-9]+", 'title' => 'Il campo può contenere solo caratteri numerici.', 'minlength' => 5]); ?>
        </div>
    </div>
    <div class="col-md-2">
        <label class="form-label "><strong>Città</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('citta', ['label' => false, 'div' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2">
        <label class="form-label "><strong>Provincia</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('provincia', ['label' => false, 'div' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2">
        <label class="form-label "><strong>Nazione</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'nation_id',
                "aggregator" => '',
                "prefix" => "nation_id",
                "list" => $nations,
                "options" => ['multiple' => false, 'required' => false,
                    'default' => '106']
            ]); ?>
        </div>
    </div>
</div>

<center><?= $this->element('Form/Components/Actions/component', ['redirect' => 'index']); ?></center>
<?= $this->Form->end(); ?>
<script>
    // Chiamata quando viene aperta dal megarform
    function addFastClientPageCustomLoaders(htmlContent, megaFormButton, megaForm) {
        initializeFilterableSelects();
    }

    // Quando premo salva aggiorno il valore sulla cellachiamata
    function addFastClientMegaFormsAfterSaveCallback(entityData, saveButton, megaFormButton, megaForm) {
        var changedSelect = megaFormButton.parents('.multiple-select-container').find('[id$="_multiple_select"]').first();
        var val = $.isNumeric(changedSelect.val()) || !$.isArray(changedSelect.val()) ? [changedSelect.val] : changedSelect.val();
        changedSelect.append($('<option>', {
            value: entityData.id,
            text: entityData.fullName ? entityData.fullName : entityData.ragionesociale
        }));
        changedSelect.val($.merge(val, ["" + entityData.id])).multiselect('rebuild');

        // Personalizzazione per megaform del cantiere

        $("#client_megaform_id_multiple_select").append($('<option>',
            {
                value: entityData.id,
                text: entityData.fullName ? entityData.fullName : entityData.ragionesociale
            }));

        $("#client_megaform_id_multiple_select").multiselect('rebuild');
        $("#client_id_multiple_select").trigger("change");

    }

    // Quando premo salva aggiorno il valore sulla cellachiamata
    function addFastClientMegaFormsAfterSaveCallback2(entityData, saveButton, megaFormButton, megaForm) {
        var changedSelect = megaFormButton.parents('.multiple-select-container').find('[id$="_multiple_select"]').first();
        var val = $.isNumeric(changedSelect.val()) || !$.isArray(changedSelect.val()) ? [changedSelect.val] : changedSelect.val();
        changedSelect.append($('<option>', {
            value: entityData.id,
            text: entityData.fullName ? entityData.fullName : entityData.ragionesociale
        }));
        changedSelect.val($.merge(val, ["" + entityData.id])).multiselect('rebuild');

        $("#client_megaform_id2_multiple_select").append($('<option>',
            {
                value: entityData.id,
                text: entityData.fullName ? entityData.fullName : entityData.ragionesociale
            }));
        $("#client_megaform_id2_multiple_select").multiselect('rebuild');


        // Personalizzazione per megaform del cantiere
        $("#client_id_multiple_select").append($('<option>',
            {
                value: entityData.id,
                text:  entityData.ragionesociale
            }));

        $("#client_id_multiple_select").val(entityData.id).multiselect('rebuild');





    }
</script>


	
