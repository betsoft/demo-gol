<!-- loader -->
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>


<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Clienti','indexelements' => ['add'=>'Nuovo cliente']]); ?>
<div class="clients index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
			<?php //if (count($clients) > 0) { ?>
			<tr>
				<?php
				 // Se abilitata la gestione dei rivenditori
				/* if(RESELLER_ENABLED)
				 {
				 	echo $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
					'htmlElements'=> 
					  [$this->Form->input('filters.reseller', [  'div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => ['1'=>'Sì','0'=>'No'] , 'empty'=>'Tutti', 'bind-filter-event'=>"change"])]
					  ]); 
				 }
				 else
				 { */
				 	echo $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); 
				 /* } */
				?>
			</tr>
			<?php // }
			//else
			//{
			
			//}
			?>
		</thead>
		<tbody class="ajax-filter-table-content">
				
			<?php 
			if (count($clients) == 0) 
			{
				?><tr><td colspan="11"><center>Nessun cliente trovato</center></td></tr><?php				
			}
			foreach ($clients as $client)
			{ ?>
				<tr>
					<td><?= h($client['Client']['code']); ?></td>
					<td class="table-max-width uk-text-truncate"><?php echo h($client['Client']['ragionesociale']); ?></td>
					<td><?= h($client['Client']['indirizzo']); ?></td>
					<td><?= h($client['Client']['cap']); ?></td>
					<td><?= h($client['Client']['citta']); ?></td>
					<td><?= h($client['Client']['provincia']); ?></td>
					<td><?= h($client['Client']['telefono']); ?></td>
					<td><?= h($client['Client']['mail']); ?></td>
					<td><?= h($client['Client']['piva']); ?></td>
					<td><?= h($client['Client']['cf']); ?></td>
					<!--td><?php // echo h($client['Nation']['name']); ?>&nbsp;</td-->
					<?php /* if(RESELLER_ENABLED) { ?><td><?= $client['Client']['reseller'] == 0? 'NO':'SI'; ?></td><?php } */?>
					<td class="actions">
						<?=  $this->element('Form/Simplify/Actions/edit',['id' => $client['Client']['id']]); ?>
						<?php
							
							echo $this->Html->link($iconaDestini, array('controller'=>'Clientdestinations','action' => 'index', $client['Client']['id']),array('title'=>__('Gestione destini diversi'),'escape'=>false));
							if(ADVANCED_STORAGE_ENABLED)
							{
								echo $this->Html->link($iconaLisitini, array('controller' => 'catalogs' , 'action' => 'index', $client['Client']['id'], 'cliente'),array('title'=>__('Listino'),'escape'=>false));
							}
						    if(MODULO_CANTIERI)
                            {
                                if($client['Client']["state"] == 2) {
                                    ?>
                                    <i class="fa fa-user jsEnableClient" style="font-size:20px;vertical-align: middle;color:#ea5d0b;cursor:pointer;" title="Abilita il cliente" clientid="<?= $client['Client']['id'] ?>"></i>
                                    <?php
                                }
                                else
                                    {
                                        ?>
                                        <i class="fa fa-user" style="font-size:20px;color:#23a24d;vertical-align: middle;" clientid="<?= $client['Client']['id'] ?>"></i>
                                        <?php
                                    }
                            }
						?>
						<?= $this->element('Form/Simplify/Actions/delete',['id' => $client['Client']['id'],'message' => 'Sei sicuro di voler eliminare il cliente ?']); ?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
</div>

<script>

    $(".jsEnableClient").click(function()
    {
        var client =  $(this);
        $.confirm({
            title: 'Abilitazione clienti.',
            content: 'Abilitare il cliente selezionato ?',
            type: 'blue',
            buttons: {
                Ok: function () {
                    action:
                    {
                        $.ajax
                        ({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "clients", "action" => "enable"]) ?>",
                            data:
                                {
                                    clientId: $(client).attr("clientid"),
                                },
                            success: function (data)
                            {
                                $(client).css("color", "#23a24d");
                            }
                        });
                    }
                },
                annulla: function () {
                    action:
                    {
                        // Nothing
                    }
                },
            }
        });
});

</script>


