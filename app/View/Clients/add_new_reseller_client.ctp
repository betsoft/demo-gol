<?= $this->element('Form/Components/FilterableSelect/loader'); ?>
<?= $this->Form->create('Client',['url'=>'saveclientsReferent']); ?>
<div class="caption">
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Aggiunta nuovi cliente per rivenditore: ' . $resellerName); ?></span>
	 <div class="col-md-12"><hr></div>
</div>
<?= $this->Form->hidden('resellerId',['value' => $resellerId]); ?>
	<label><strong>clienti del rivenditore:</strong></label>
	<?= $this->element('Form/Components/FilterableSelect/component', ["name" => 'client_id',"aggregator" => '',"prefix" => "client_id","list" => $clients,"options" => [ 'multiple' => true,'required'=> false,'default'=> $checkedArray,],]); ?>
	<div><br/></div><div><br/></div><div><br/></div><div><br/></div><div><br/></div><div><br/></div><div><br/></div>
	<center><?= $this->Form->submit(__('Salva',true), ['class'=>'btn blue-button new-bill']); ?></center>
<?= $this->Form->end(); ?>

