<?= $this->element('Form/Components/Paginator/loader'); ?><?= $this->element('Form/Components/AjaxFilter/loader') ?>

<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Vettori','indexelements' => ['add'=>'Nuovo vettore']]); ?>
<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
					<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php //if (count($carrier) > 0) { ?>
					<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php 
				if (count($carrier) == 0) 
				{
					?><tr><td colspan="4"><center>nessun vettore trovato</center></td></tr><?php				
				}
				foreach ($carrier as $carrier) 
				{
				?>
					<tr>
						<td><?php echo h($carrier['Carrier']['name']); ?></td>
						<td><?php echo h($carrier['Carrier']['piva']); ?></td>
						<td><?php echo h($carrier['Carrier']['cf']); ?></td>
						<td class="actions">
							<?=  $this->element('Form/Simplify/Actions/edit',['id' => $carrier['Carrier']['id']]); ?>
							<?php
								echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $carrier['Carrier']['id']], ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler cancellare il vettore ?', $carrier['Carrier']['id']));
							?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
