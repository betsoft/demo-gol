<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Vettore'); ?></span>
	</div>
	<div class="tools">
		<?php echo $this->Html->link('<i class="uk-icon-plus-circle icon"></i>', ['action' => 'add'],array('title'=>__('Nuovo camion vettore'),'escape'=>false)); ?>
	</div>
</div>
<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th><?php echo $this->Paginator->sort('Targa camion'); ?></th>
					<th><?php echo $this->Paginator->sort('Descrizione'); ?></th>
					<th class="actions"><?php echo __('Azioni'); ?></th>
				</tr>
			</thead>
			<tbody>
				
				<?php foreach ($trucks as $truck) 
				{
				?>
					<tr>
						<td><?php echo h($carrier['Carrier']['description']); ?></td>
						<td class="actions">
							<?=  $this->element('Form/Simplify/Actions/edit',['id' => $carrier['Carrier']['id']]); ?>
							<?=  $this->Html->link('<i class="fa fa-truck" style="padding-right:2%;font-size:140%"></i>', array('action' => 'trucks', $carrier['Carrier']['id']),array('title'=>__('Gestione camion'),'escape'=>false)); ?>
							<?=  $this->Form->postLink($iconaElimina, array('action' => 'delete', $carrier['Carrier']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler cancellare il vettore ?', $carrier['Carrier']['id']));	?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<p class="uk-text-center">
			<?php echo $this->Paginator->counter(array('format' => __('Pagina {:page} di {:pages}'))); ?>
		</p>
		<div class="paging uk-text-center">
			<?php
				echo $this->Paginator->prev('<i class="fa fa-arrow-circle-o-left"></i> &nbsp;', array('escape'=>false,'class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ' - '));
				echo $this->Paginator->next('&nbsp; <i class="fa fa-arrow-circle-o-right"></i>', array('escape'=>false,'class' => 'next disabled'));
			?>
		</div>
	</div>
</div>
