     <?= $this->Form->create('Units'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova unità di misura') ?></span>
  	 <div class="col-md-12"><hr></div>
    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('description', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>10]); ?>
         </div>
         </div>
    </div>
     <div class="col-md-12"><hr></div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>

