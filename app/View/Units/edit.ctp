<?=  $this->Form->create('Units'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica unità di misura'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',['class'=>'form-control']); ?>
  	
  	<div class="form-group col-md-12">
  		<div class="col-md-2" style="float:left">
  		<label class="form-margin-top form-label"><strong>Descrizione<i class="fa fa-asterisk"></i></strong></label>
  		<div class="form-controls ">
			<?= $this->Form->input('description',['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>10]); ?>
		</div>
		</div>
	</div>
	 <div class="col-md-12"><hr></div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?=  $this->Form->end(); ?>

