<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Unità di misura','indexelements' => ['add'=>'Nuova unità di misura']]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($units) > 0) { ?>
				<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
				<?php }
				else
				{
					?><tr><td colspan="2"><center>nessun unità di misura trovata</center></td></tr><?php				
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($units as $unit) 
				{
				?>
					<tr>
						<td><?php echo h($unit['Unit']['description']); ?></td>
						<td class="actions">
							<?php
								echo $this->Html->link($iconaModifica, array('action' => 'edit', $unit['Unit']['id']),array('title'=>__('Modifica'),'escape'=>false));
								echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $unit['Unit']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler cancellare l\' unità di misura ?', $unit['Unit']['id']));
							?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
