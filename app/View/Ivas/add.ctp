
<?= $this->Form->create(); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo regime iva') ?></span>
 <div class="col-md-12"><hr></div>
    
    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label> 
           <div class="form-controls">
	           <?= $this->Form->input('codice', ['div'=>false, 'label' => false, 'class'=>'form-control','maxlength'=>5,'required'=>true]); ?>
	       </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-margin-top form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label> 
            <div class="form-controls">
	            <?= $this->Form->input('descrizione', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
	        </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
	        <label class="form-label"><strong>Percentuale</strong><i class="fa fa-asterisk"></i></label> 
            <div class="form-controls">
                <?= $this->Form->input('percentuale', ['div'=>false, 'label' => false, 'class'=>'form-control', 'min' => '0', 'max' => '100','required'=>true]); ?>
	         </div>
        </div>
    </div>
    <div class="form-group col-md-12">
   <div class="col-md-12" style="float:left;">
       <label class="form-margin-top"><strong>Riferimento normativo (per fatturazione elettronica)</strong></label> 
       <div class="form-controls">
    	<?= $this->Form->input('einvoicenature_id', ['div'=>false, 'label' => false, 'class'=>'form-control', 'type' => 'select', 'options'=>$einvoicenatures, 'empty'=>true]); ?>
    	 </div>
     </div>
     </div>
   <div class="col-md-12"><hr></div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?=  $this->Form->end(); ?>

    <script>
        $("#IvaAddForm").on('submit.default',function(ev) 
        {
        });
            
        $("#IvaAddForm").on('submit.validation',function(ev) 
        {              
            ev.preventDefault();
            /* Validations go here */
            
            if($("#IvaPercentuale").val() > 0 && $("#IvaEinvoicenatureId").val() != '')
            {
                $.alert({
        		    icon: 'fa fa-warning',
	        		title: 'Creazione regime iva',
    	    		content: 'Attenzione, se la percentuale è maggiore di zero il riferimento normativo non deve essere indicato.',
    		    	type: 'orange',
			    });
			
                return false;               
            }
			
			if($("#IvaPercentuale").val() == 0 && $("#IvaEinvoicenatureId").val() == '')
            {
                $.alert({
        		    icon: 'fa fa-warning',
	        		title: 'Creazione regime iva',
    		    	content: 'Attenzione, se la percentuale è nulla deve essere obbligatoriamente indicato il riferimento normativo.',
    			    type: 'orange',
    			});
	    		return false
            }
            
             $("#IvaAddForm").trigger('submit.default');
        });
    </script>