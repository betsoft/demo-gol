
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>

<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Causali','indexelements' => ['add'=>'Nuova causale']]); ?>
<div class="clients index">
	<div class="causals index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($causals) > 0) { ?>
				<tr>
					<?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?>
				</tr>
				<?php }
					else
					{
						?><tr><td colspan="5"><center>nessuna causale trovata</center></td></tr><?php				
					}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($causals as $causal) 
				{
				?>
					<tr>
						<td><?php echo h($causal['Causal']['name']); ?></td>
						<td><?=  $causal['Causal']['clfo'] == 'C' ? 'Cliente' : 'Fornitore';  ?></td>
						<td class="actions">
							<?=	$this->element('Form/Simplify/Actions/edit',['id' => $causal['Causal']['id']]); ?>
							<?= $this->element('Form/Simplify/Actions/delete',['id' =>$causal['Causal']['id'],'message' => 'Sei sicuro di voler eliminare la causale ?']);  ?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
			<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
