<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

	<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Ordini d\'acquisto','indexelements' => ['add'=>'Nuovo ordine d\'acquisto']]); ?>
	<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
		</thead>
		<?php $currentYear = date("Y"); ?>
		<tr>
			<?=  $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields, 
			'htmlElements' => [
				'<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="01-01-'.$currentYear.'"  bind-filter-event="change"/>'.''.
				'<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="31-12-'.$currentYear.'" bind-filter-event="change"/></center>'
				]]); 
			?></tr>
		<?php
		if (count($purchaseorders) == 0)
        {
            ?>
            <tr>
                <td colspan="8">
                    <center>nessun ordine d'acquisto trovato</center>
                </td>
            </tr>
            <?php
        }
		?>
		<tbody class="ajax-filter-table-content">
		<?php
		$totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = 0;
		// debug($purchaseorders );
		foreach ($purchaseorders as $purchaseorder)
        { ?>
			<tr>
				<td><?= $purchaseorder['Purchaseorder']['number']; ?> / <?= substr($purchaseorder['Purchaseorder']['date'],0,4); ?></td>
				<td style="text-align:center;"><?= $this->Time->format('d-m-Y', $purchaseorder['Purchaseorder']['date']); ?></td>
				<td class="table-max-width uk-text-truncate"><?= $purchaseorder['Supplier']['name']; ?></td>
                <!--td><?php // $purchaseorder['Constructionsite']['name']; ?></td-->
		<td class="actions" style="text-align:left;">
            <?php
                if($purchaseorder['Purchaseorder']['loadgoodcreated'] == 0) {
                    echo $this->Html->link($iconaModifica, ['action' => 'edit', $purchaseorder['Purchaseorder']['id']], ['title'=>__('Modifica ordine acquisto'),'escape' => false]);
                    echo $this->Form->postLink($iconaGeneraEntrataMerce, ['controller' => 'Loadgoods', 'action' => 'createLoadgoodFromPurchaseOrder', $purchaseorder['Purchaseorder']['id']], ['title' => __('Genera entrata merce'), 'escape' => false], __("Generare entrata merce da ordine d'acquisto ? ", $purchaseorder['Purchaseorder']['id']));
                }
                else
                    {
                        echo $iconaModificaOff;
                        echo $iconaGeneraEntrataMerceOff;

                    }
                echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $purchaseorder['Purchaseorder']['id']], ['title'=>__("Elimina ordine d'acquisto"),'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $purchaseorder['Purchaseorder']['id']));
            ?>
		</td>
	</tr>
<?php } ?>

</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
	<div id="element"></div>

<?=  $this->element('Js/datepickercode'); ?>


