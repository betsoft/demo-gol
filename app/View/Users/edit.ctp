	<?= $this->Form->create('User'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Modifica utente'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',['class'=>'form-control']); ?>
	</br>
	<label><strong>Username</strong></label>
	<?= $this->Form->input('username',['div' => false, 'label' => false, 'class'=>'form-control','disabled'=>'disabled']); ?>
	<label class="form-margin-top"><strong>Password</strong></label>
	<?= $this->Form->input('password',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
	<label class="form-margin-top"><strong>Conferma password</strong></label>
	<?= $this->Form->input('password_confirm', ['div' => false, 'label' => false, 'type' => 'password','class'=>'form-control','id'=>'lunghezza']); ?>
	<div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>
