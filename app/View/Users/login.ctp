<link href='https://fonts.googleapis.com/css?family=Barlow Semi Condensed' rel='stylesheet'>
<div class="contenitore" style="width:100%" >
		<div style="width:100%">
		<center>
			<?= $this->Form->create('User');?>
			<?= $this->Session->flash('auth'); ?>
			<div style="color: #589AB8;font-family: 'Barlow Semi Condensed';font-size: 16pt;">Inserisci username e password per accedere</div>
			<div class="portlet-body form col-md-12">
				<div class="form-body col-md-12">
					<div class="row col-md-12">
						<div class="col-md-offset-5 col-md-2">
							<input type="text" placeholder="Username" name="data[User][username]"  style="font-family: 'Barlow Semi Condensed';font-size: 12pt; width:100%" autofocus>
						</div>
					</div>
					<div class="row col-md-12">
						<div class="col-md-offset-5 col-md-2">
							<input type="password" placeholder="Password" name="data[User][password]" style="font-family: 'Barlow Semi Condensed';font-size: 12pt;width:100%">
						</div>
					</div>
					<div class="flash form-margin-top col-md-offset-4 col-md-4" >
						<?= $this->Session->flash() ?>
					</div>
					<div class="row col-md-12">
					<div class="form-actions col-md-offset-4 col-md-12" >
						<?php
							echo $this->Form->submit(__('Login',true), ['class'=>'btn blue-button','style'=>"font-family: 'Barlow Semi Condensed' !important;font-size: 10pt;"]);
		    				echo $this->Form->end();
		    			?>
		    			<br/>
					</div>
					</div>
				</div>
			</div>
		</center>
	</div>
</div>

