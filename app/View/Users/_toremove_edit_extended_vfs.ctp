	<?php echo $this->Form->create('User'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Modifica utente'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',array('class'=>'form-control')); ?>
	<label><strong>Username</strong></label>
	<?= $this->Form->input('username',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
	<label class="form-margin-top"><strong>Password</strong></label>
	<?= $this->Form->input('password',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
	<label class="form-margin-top"><strong>Conferma password</strong></label>
	<?= $this->Form->input('password_confirm', array('div' => false, 'label' => false, 'type' => 'password','class'=>'form-control','id'=>'lunghezza')); ?>
	 <div class="col-md-12"><hr></div>
	<center><?= $this->Form->submit(__('Salva',true), array('class'=>'btn blue-button new-bill')) ?></center>
	<?=  $this->Form->end(); ?>
