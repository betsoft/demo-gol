<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase"><?php echo __('User'); ?></span>
							</div>
							<div class="tools">
								<?php echo $this->Html->link('<i class="fa fa-arrow-left"></i>', ['action' => 'index'],array('title'=>__('List User'),'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-plus-circle"></i>', ['action' => 'add'],array('title'=>__('New User'),'escape'=>false)); ?>
							    <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $user['User']['id']),array('title'=>__('Edit'),'escape'=>false)); ?>
							   <?php echo $this->Form->postLink('<i class="fa fa-times"></i>', array('action' => 'delete', $user['User']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare l\' utente ?', $user['User']['id'])); ?>
								
							</div>
						</div>
						<div class="portlet-body flip-scroll">
						
							
						</div>
<table class="table table-bordered table-striped table-condensed flip-content">
	<tr>				
		<dl>
			<td>	<dt><?php echo __('Id'); ?></dt></td>
			<td>	<dd><?php echo h($user['User']['id']); ?>	&nbsp;	</dd></td>
    </tr>
	<tr>
		   <td><dt><?php echo __('Username'); ?></dt></td>
		   <td><dd><?php echo h($user['User']['username']); ?>&nbsp;</dd></td>
    </tr>
    <tr>
		   <td><dt><?php echo __('Password'); ?></dt></td>
		   <td><dd><?php echo h($user['User']['password']); ?>&nbsp;</dd></td>
    </tr>
   
   </dl>
	
</table>	
</div>