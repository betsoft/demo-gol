<?= $this->element('Form/Components/Paginator/loader'); ?><?= $this->element('Form/Components/AjaxFilter/loader') ?>

<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Utenti'); ?></span>
	</div>
	<?= $this->element('Form/Simplify/topaddaction',['action'=>'addExtended' ,'testo'=>'Creazione nuovo utente']); ?>
</div>
<table class="table table-bordered table-striped table-condensed flip-content">
	<thead class ="flip-content">
		<tr>
			<th><?php echo $this->Paginator->sort('Nome utente'); ?></th>
			<th><?php echo $this->Paginator->sort('Gruppo'); ?></th>
			<th class="actions" style="width:10%;"><?php echo __('Azioni'); ?></th>
			<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
		</thead>
		<tbody class="ajax-filter-table-content">
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
				<td><?php echo h($user['Groups']['description']); ?>&nbsp;</td>
				<td class="actions">
					<?php
						echo $this->Html->link($iconaModifica, array('action' => 'editExtended', $user['User']['id']),array('title'=>__('Edit'),'escape'=>false));
						echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $user['User']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare  l\'utente ?', $user['User']['id']));
					?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
