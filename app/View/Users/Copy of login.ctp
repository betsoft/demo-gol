<div class="col-md-6">
	<?php echo $this->Session->flash('auth'); ?>
    <?php echo $this->Form->create('User'); ?>

					<!-- BEGIN SAMPLE FORM PORTLET-->
					
					
					<div class="portlet light" id="sign-up">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase"><?php echo __('Si prega di inserire username e password'); ?></span>
							</div>
							
						</div>
						
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
									<div class="form-group">
										<label></label>
										<div class="input-group">
											
											
										<?php
											echo $this->Form->input('username',array('class'=>'form-control'));
											echo $this->Form->input('password',array('class'=>'form-control'));
										?>
										</div>
									</div>
									<div class="form-actions">
									<?php echo $this->Form->submit(__('Login',true), array('class'=>'btn blue')); 
    									echo $this->Form->end(); ?>
									
									 <p><a href="#"><?php echo __("I forgot my password"); ?></a></p>
								</div>
	

</div>
</div>
</div>
<div id="element"></div>
</div>
  