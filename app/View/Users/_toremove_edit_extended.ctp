	<?php echo $this->Form->create('User'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Modifica utente'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',array('class'=>'form-control')); ?>
	<label><strong>Username</strong></label>
	<?= $this->Form->input('username',array('div' => false, 'label' => false, 'class'=>'form-control','disabled'=>'disabled')); ?>
	<label class="form-margin-top"><strong>Password</strong></label>
	<?= $this->Form->input('password',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
	<label class="form-margin-top"><strong>Conferma password</strong></label>
	<?= $this->Form->input('password_confirm', array('div' => false, 'label' => false, 'type' => 'password','class'=>'form-control','id'=>'lunghezza')); ?>
    <label id="UserTareLabel"><strong>Tara del camion</strong></label>
	<?= $this->Form->input('tare',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
	 <div class="col-md-12"><hr></div>
	<center><?= $this->Form->submit(__('Salva',true), array('class'=>'btn blue-button new-bill')) ?></center>
	<?=  $this->Form->end(); ?>
	
	<script>
			// Se l'utente è un camion mostro il campo tara
			if($("#UserGroupId").val() == '4')
				{
					$("#UserTare").show();
					$("#UserTareLabel").show();
				}
	
				
			// Questo nel caso rimettessi la possibilità di cambiare gruppo d'utenza	
			$("#UserGroupId").change(function()
			{
				if($(this).val() == '4')
				{
					$("#UserTare").show();
					$("#UserTareLabel").show();
				}
				else
				{
					$("#UserTare").hide();
					$("#UserTareLabel").hide();
				}
			})
		</script>