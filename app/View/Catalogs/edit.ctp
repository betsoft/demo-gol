    <?=  $this->Form->create('Catalogs', ['class' => 'uk-form uk-form-horizontal']); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica listino per il cliente: ' . $customerName) ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',['class'=>'form-control']); ?>
  	

  	    <div class="form-group col-md-12">
        <label class="form-margin-top form-label"><strong>Descrizione aggiuntiva</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('description', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>
    <!-- Non cancellare va rimesso -->
    <!--div class="form-group">
        <label class="form-margin-top form-label"><strong>Valuta</strong></label>
         <div class="form-controls">
	           <?php // $this->Form->input('currency_id', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div-->
     <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index','passedValue'=>$clientId]); ?></center>
	<?=  $this->Form->end(); ?>

