   
    <?=  $this->Form->create('Catalogstorages', ['class' => 'uk-form uk-form-horizontal']); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica listino corrente per il prodotto - ' . $storageName) ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',['class'=>'form-control','value'=>$id]); ?>
  	

  	    <!--div class="form-group">
        <label class="form-margin-top form-label"><strong>Descrizione modificata</strong></label>
         <div class="form-controls">
	           <?php // $this->Form->input('description', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div-->
    <div class="form-group col-md-12">
        <label class="form-margin-top form-label"><strong>Prezzo listino</strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('catalog_price', ['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
    </div>
     <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'storagesList'.'/'.$idCatalog.'/'. $currentCatalog]); ?></center>
	<?=  $this->Form->end(); ?>

