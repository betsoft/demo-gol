<?= $this->element('Form/Components/Paginator/loader'); ?><?= $this->element('Form/Components/AjaxFilter/loader') ?>
<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Listini del cliente: ' . $customerName); ?></span>
	<!--/div>
	<div class=" -badge tools"-->
		<?php 
			// Per ora gestisco un solo catalogo per cliente
			if($catalogs == null)
			{ 
		?>
			<?php // $this->Html->link('< elenco clienti', array('controller'=>'clients','action' => 'index'), array('title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;')); ?>
			<?php // $this->Html->link('Nuovo listino', ['action' => 'add',$id], ['title'=>__('Nuovo Listino'),'escape' => false,'class' => "blue-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;color:#555555 !imporant;margin-right:20px;']); ?>
            <?= $this->Html->link('elenco clienti', array('controller'=>'clients','action' => 'index'), array('title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:10%;text-align:center;')); ?>
            <?= $this->Html->link('Nuovo listino', ['action' => 'add',$id], ['title'=>__('Nuovo Listino'),'escape' => false,'class' => "blue-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;color:#555555 !imporant;margin-right:20px;float:right;width:10%;text-align:center;']); ?>
                <?php
			}
			else
			{
//				echo $this->Html->link('< elenco clienti', array('controller'=>'clients','action' => 'index'), array('title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;'));
                echo $this->Html->link('elenco clienti', array('controller'=>'clients','action' => 'index'), array('title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:10%;text-align:center;'));
			}
		?>
	</div>
</div>
<div class="clients index">
	<div class="ivas index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th><?= $this->Paginator->sort('Descrizione'); ?></th>
					<th><?= $this->Paginator->sort('Valuta'); ?></th>
					<th class="actions"><?php echo __('Azioni'); ?></th>
				</tr>
				<?php if (count($catalogs) > 0) { ?>
				<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
				<?php } ?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php
					foreach ($catalogs as $catalog)
					{
					?>
					<tr>
						<td><?php echo h($catalog['Catalog']['description']); ?></td>
						<td><?php echo h($catalog['Currencies']['symbol']); ?></td>
						<td class="actions">
							<?php
								echo $this->Html->link($iconaModifica, array('action' => 'edit',$catalog['Catalog']['id'],$id),array('title'=>__('Modifica'),'escape'=>false));
								echo $this->Html->link($iconaDettaglioMagazzino, array('action' => 'storagesList',$catalog['Catalog']['id'],$id),array('title'=>__('Visualizza prodotti listino'),'escape'=>false));
								echo $this->Html->link($iconaElimina, array('action' => 'delete', $catalog['Catalog']['id'],$id), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare il listino  ?', $catalog['Catalog']['id']));
							?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
