    <?= $this->Form->create('Catalogs'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo Listino per il cliente - ' . $customerName) ?></span>
  	 <div class="col-md-12"><hr></div>
    <div class="form-group col-md-12">
        <label class="form-margin-top form-label"><strong>Descrizione listino</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('description', array('div' => false, 'label' => false, 'class'=>'form-control','required'=>true)); ?>
         </div>
    </div>
    
    <!-- Non cancellare va rimesso -->
    <!--div class="form-group">
        <label class="form-margin-top form-label"><strong>Valuta</strong></label>
         <div class="form-controls">
	           <?php // $this->Form->input('currency_id', array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
         </div>
    </div-->
     <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index','passedValue'=>$clientId]); ?></center>
	<?= $this->Form->end(); ?>
