<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'movimenti di magazzino','indexelements' => ['add'=>'Nuova unità di misura']]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($storagemovements) > 0) { ?>
				<!--tr><?php // $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr-->
				<?php }
				else
				{
					?><tr><td colspan="2"><center>nessun movimento trovato</center></td></tr><?php				
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($storagemovements as $storagemovement)
				{
					$storagemovement['Storagemovement']['description'] = str_replace('[TR_DEP_SC]','',$storagemovement['Storagemovement']['description']);
                    $storagemovement['Storagemovement']['description'] = str_replace('[TR_DEP_CA]','',$storagemovement['Storagemovement']['description']);
				?>
					<tr>
						<td><?= h($storagemovement['Storagemovement']['description']); ?></td>
                        <td style="text-align:center;"><?= h(date("d-m-Y H:i:s",strtotime($storagemovement['Storagemovement']['movement_time']))); ?></td>
						<td><?= h($storagemovement['Deposit']['deposit_name']); ?></td>
						<td class="right"><?= h(number_format($storagemovement['Storagemovement']['quantity'],2,',','')); ?></td>
						<!--td class="actions">
							<?php
								// TO DO echo $this->Html->link($iconaModifica, array('action' => 'edit', $storagemovement['Storagemovement']['id']),array('title'=>__('Modifica'),'escape'=>false));
							    // TO DO	echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $storagemovement['Storagemovement']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler cancellare l\' unità di misura ?', $storagemovement['Storagemovement']['id']));
							?>
						</td-->
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
