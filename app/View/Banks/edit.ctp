
<?=  $this->Form->create(); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica Banca'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',array('class'=>'form-control','id'=>'lunghezza2')); ?>
  	
  	 <div class="form-group col-md-12">
  	   <div class="col-md-6">
        <label class="form-margin-top form-label"><strong>Descrizione banca / filiale</label><i class="fa fa-asterisk"></i></strong>
         <div class="form-controls">
	           <?= $this->Form->input('description', array('div' => false, 'label' => false, 'class'=>'form-control','required'=>true)); ?>
         </div>
       </div>
       <div class="form-group col-md-2">
        <label class="form-margin-top form-label"><strong>Abi</strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('abi', array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
         </div>
      </div>
      <div class="form-group col-md-2">
        <label class="form-margin-top form-label"><strong>Cab</strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('cab', array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
         </div>
      </div>
    </div>

    <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Dati per bonifici in ingresso</div>
    
   <div class="form-group col-md-12">
      <div class="col-md-4">
         <label class="form-margin-top "><strong>Nostro codice iban</strong><i class="fa fa-question-circle jsBankIban" style="color:#589AB8;cursor:pointer;"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('iban', array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
         </div>
      </div>
   </div>
 
 <?php   // Gestione dei flussi riba
    if(RIBA_FLOW) { ?>
      
      <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Dati per generazione flusso riba</div>
     
     <div class="form-group col-md-12">
      <div class=" col-md-4">
        <label class="form-margin-top "><strong>Codice Cliente (per generazione flusso riba)</strong><i class="fa fa-question-circle jsBankCodcli" style="color:#589AB8;cursor:pointer;"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('clientCode', array('div' => false, 'label' => false, 'class'=>'form-control', 'maxlength'=>'12')); ?>
         </div>
    </div>
       <div class=" col-md-4">
        <label class="form-margin-top "><strong>Conto (per generazione flusso riba)</strong><i class="fa fa-question-circle jsBankConto" style="color:#589AB8;cursor:pointer;"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('account', array('div' => false, 'label' => false, 'class'=>'form-control', 'maxlength'=>'12')); ?>
         </div>
    </div>
     <div class="col-md-4">
        <label class="form-margin-top "><strong>Utilizzato (per generazione flusso riba)</strong><i class="fa fa-question-circle jsBankUsedforriba" style="color:#589AB8;cursor:pointer;"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('forriba', array('div' => false, 'label' => false, 'class'=>'form-control','options'=>[0=>'No',1=>'Sì'])); ?>
         </div>
    </div>
      </div>
    
    <?php } ?>
    
	 <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?=  $this->Form->end(); ?>
	
	
		<script>
	 $(".jsBankIban").click(function()	{ showHelpMessageBanks('iban'); });
	 $(".jsBankCodcli").click(function()	{ showHelpMessageBanks('codicecliente'); });
	 $(".jsBankConto").click(function()	{ showHelpMessageBanks('conto'); });
	 $(".jsBankUsedforriba").click(function()	{ showHelpMessageBanks('usatoperriba'); });
	
	function showHelpMessageBanks(message)
	{
		 switch(message)
		 {
 		  	case 'iban':
				    $.alert
    	   ({
    				  icon: 'fa fa-question-circle',
	    			  title: 'Banche - codice iban',
    				  content: "<?= addslashes('Questo iban viene visualizzato nella fatture quanto il metodo di pagamento selezionato è collegato a questa banca.'); ?>",
        		type: 'blue',
				    });
			   break;
			   case 'codicecliente':
			    $.alert
    	   ({
    				  icon: 'fa fa-question-circle',
	    			  title: '',
    				  content: "<?= addslashes('Nostro codice cliente utilizzato nella generazione del flusso riba'); ?>",
        		type: 'blue',
				    });
			   break;
			   case 'conto':
			     $.alert
    	   ({
    				  icon: 'fa fa-question-circle',
	    			  title: '',
    				  content: "<?= addslashes('Nostro numero di conto conrrente per la generazione del flusso riba'); ?>",
        		type: 'blue',
				    });
			   break;
			    case 'usatoperriba':
			     $.alert
    	   ({
    				  icon: 'fa fa-question-circle',
	    			  title: '',
    				  content: "<?= addslashes('Indicare si se la banca verrà visualizzata nell\'elenco per la generazione dei flussi riba'); ?>",
        		type: 'blue',
				    });
			   break;
		 }
	 
	}
		 
		 </script>


